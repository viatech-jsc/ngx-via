import { TypeaheadItemDto } from "./typeahead.item.dto";
export declare class TypeaheadItemValueDto<T> extends TypeaheadItemDto {
    data: T;
    constructor(name: string, value: string, data: T);
}
