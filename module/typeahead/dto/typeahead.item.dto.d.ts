export declare class TypeaheadItemDto {
    name: string;
    value: string;
    constructor(name: string, value: string);
}
