import { ElementRef, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export declare class TypeaheadOutSiteDirective {
    private el;
    typeaheadClickOutSite: EventEmitter<string>;
    constructor(el: ElementRef);
    documentClick($event: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TypeaheadOutSiteDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<TypeaheadOutSiteDirective, "[typeaheadClickOutSite]", never, {}, { "typeaheadClickOutSite": "typeaheadClickOutSite"; }, never, never, false, never>;
}
