import { EventEmitter, ElementRef } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { TypeaheadItemDto } from "../dto/typeahead.item.dto";
import { TypeaheadOptionDto } from "../dto/typeahead.option.dto";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { TypeaheadI18nDto } from "../dto/typeahead.i18n.dto";
import * as i0 from "@angular/core";
export declare class MatTypeaheadComponent {
    typeaheadLoadData: EventEmitter<any>;
    typeaheadItemSelected: EventEmitter<any>;
    options: TypeaheadOptionDto;
    disabled: boolean;
    i18n: TypeaheadI18nDto;
    viaControl: FormControl;
    typeaheadInput?: ElementRef<HTMLInputElement>;
    typeaheadFormGroup: FormGroup<{
        searchFormControl: FormControl<string | null>;
    }>;
    separatorKeysCodes: number[];
    private isSettingValue;
    constructor();
    formControlInstance(): FormControl<any>;
    hide(): void;
    isSingleInlineSearch(): boolean;
    selected(items: Array<TypeaheadItemDto>): void;
    select(event: MatAutocompleteSelectedEvent): void;
    remove(item: TypeaheadItemDto): void;
    removeSingleItem(): void;
    ngAfterViewInit(): void;
    setSearchBoxValue(text: string): void;
    required(form?: FormControl): "" | "*";
    static ɵfac: i0.ɵɵFactoryDeclaration<MatTypeaheadComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MatTypeaheadComponent, "via-mat-typeahead", never, { "options": { "alias": "options"; "required": false; }; "disabled": { "alias": "disabled"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; "viaControl": { "alias": "viaControl"; "required": false; }; }, { "typeaheadLoadData": "typeaheadLoadData"; "typeaheadItemSelected": "typeaheadItemSelected"; }, never, never, false, never>;
}
