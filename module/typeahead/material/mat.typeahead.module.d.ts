import * as i0 from "@angular/core";
import * as i1 from "./mat.typeahead";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/material/chips";
import * as i5 from "@angular/material/icon";
import * as i6 from "@angular/material/autocomplete";
import * as i7 from "@angular/material/form-field";
import * as i8 from "@angular/material/progress-spinner";
import * as i9 from "@angular/material/input";
export declare class MatTypeaheadModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<MatTypeaheadModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<MatTypeaheadModule, [typeof i1.MatTypeaheadComponent], [typeof i2.CommonModule, typeof i3.FormsModule, typeof i3.ReactiveFormsModule, typeof i4.MatChipsModule, typeof i5.MatIconModule, typeof i6.MatAutocompleteModule, typeof i7.MatFormFieldModule, typeof i8.MatProgressSpinnerModule, typeof i9.MatInputModule], [typeof i1.MatTypeaheadComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<MatTypeaheadModule>;
}
