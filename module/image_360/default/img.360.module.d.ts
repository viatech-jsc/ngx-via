import * as i0 from "@angular/core";
import * as i1 from "./img.360";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
export declare class Image360Module {
    static ɵfac: i0.ɵɵFactoryDeclaration<Image360Module, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<Image360Module, [typeof i1.Image360Component], [typeof i2.NgbModalModule], [typeof i1.Image360Component]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<Image360Module>;
}
