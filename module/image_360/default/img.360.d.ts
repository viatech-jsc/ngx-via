import { OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import "pannellum/src/js/pannellum.js";
import "pannellum/src/js/libpannellum.js";
import * as i0 from "@angular/core";
export declare class Image360Component implements OnInit {
    private ngbActiveModal;
    imgPath: string;
    panoramaId: string;
    constructor(ngbActiveModal: NgbActiveModal);
    ngOnInit(): void;
    closeModal(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<Image360Component, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<Image360Component, "via-360-image", never, { "imgPath": { "alias": "imgPath"; "required": false; }; }, {}, never, never, false, never>;
}
