import * as i0 from "@angular/core";
import * as i1 from "./img.360";
import * as i2 from "@angular/material/dialog";
export declare class MatImage360Module {
    static ɵfac: i0.ɵɵFactoryDeclaration<MatImage360Module, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<MatImage360Module, [typeof i1.MatImage360Component], [typeof i2.MatDialogModule], [typeof i1.MatImage360Component]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<MatImage360Module>;
}
