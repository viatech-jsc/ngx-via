import { MatDialogRef } from "@angular/material/dialog";
import "pannellum/src/js/pannellum.js";
import "pannellum/src/js/libpannellum.js";
import * as i0 from "@angular/core";
export declare class MatImage360Component {
    dialogRef: MatDialogRef<MatImage360Component>;
    data: any;
    imgPath: string;
    panoramaId: string;
    constructor(dialogRef: MatDialogRef<MatImage360Component>, data: any);
    ngOnInit(): void;
    closeModal(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MatImage360Component, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MatImage360Component, "via-mat-360-image", never, { "imgPath": { "alias": "imgPath"; "required": false; }; }, {}, never, never, false, never>;
}
