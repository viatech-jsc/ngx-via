import * as i0 from "@angular/core";
import * as i1 from "./dropdown";
import * as i2 from "./pipe/dropdown.pipe";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/common";
export declare class DropdownModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DropdownModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DropdownModule, [typeof i1.DropdownComponent, typeof i2.FullTextSearchPipe], [typeof i3.FormsModule, typeof i4.CommonModule, typeof i3.ReactiveFormsModule], [typeof i1.DropdownComponent, typeof i2.FullTextSearchPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DropdownModule>;
}
