import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class FullTextSearchPipe implements PipeTransform {
    constructor();
    transform(value: any, keys: string, term: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<FullTextSearchPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<FullTextSearchPipe, "fullTextSearch", false>;
}
