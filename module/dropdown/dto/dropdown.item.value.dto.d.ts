import { DropdownItemDto } from "./dropdown.item.dto";
export declare class DropdownItemValueDto<T> extends DropdownItemDto {
    data: T;
    constructor(name: string, value: string, data: T);
}
