import { CarouselPosition } from "./carousel.position.dto";
export declare class CarouselInfoDto {
    direction: number;
    progressTrack: number;
    progressSlides: number;
    positions: Array<CarouselPosition>;
    position: number;
    speed: number;
    relativeSlide: number;
    absoluteSlide: number;
    size: number;
    widthOrHeight: number;
    constructor();
}
