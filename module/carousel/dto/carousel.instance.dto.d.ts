import { CarouselOptionDto } from "./carousel.option.dto";
import { CarouselInfoDto } from "./carousel.info.dto";
export declare class CarouselInstanceDto {
    controls: (active: boolean) => void;
    destroy: () => void;
    refresh: (options: CarouselOptionDto) => void;
    next: () => void;
    prev: () => void;
    moveToSlide: (idx: number, duration?: number) => void;
    moveToSlideRelative: (idx: number, nearest?: boolean, duration?: number) => void;
    resize: () => void;
    details: () => CarouselInfoDto;
    options: () => CarouselOptionDto;
    constructor();
}
