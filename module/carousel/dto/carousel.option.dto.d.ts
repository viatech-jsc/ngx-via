export declare class CarouselOptionDto {
    autoAdjustSlidesPerView: boolean;
    centered: boolean;
    scaleCenter: boolean;
    breakpoints: any;
    controls: boolean;
    dragSpeed: number;
    friction: number;
    loop: boolean;
    initial: number;
    duration: number;
    preventEvent: string;
    slides: string;
    vertical: boolean;
    resetSlide: boolean;
    slidesPerView: number;
    spacing: number;
    mode: "snap" | "free-snap" | "free";
    rtl: boolean;
    rubberband: boolean;
    cancelOnLeave: boolean;
    nav: boolean;
    dots: boolean;
    autoInit: boolean;
    constructor();
}
