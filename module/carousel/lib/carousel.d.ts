export default ViaCarousel;
declare function ViaCarousel(initialContainer: any, initialOptions?: {}): {
    controls: (active: any) => void;
    destroy: () => void;
    refresh(options: any): void;
    next(): void;
    prev(): void;
    moveToSlide(idx: any, duration: any): void;
    moveToSlideRelative(idx: any, nearest: boolean | undefined, duration: any): void;
    resize(): void;
    details(): {
        direction: any;
        progressTrack: number;
        progressSlides: number;
        positions: any;
        position: number;
        speed: any;
        relativeSlide: number;
        absoluteSlide: any;
        size: any;
        slidesPerView: any;
        widthOrHeight: any;
    };
    options(): any;
};
