import * as i0 from "@angular/core";
import * as i1 from "./carousel";
import * as i2 from "@angular/common";
export declare class CarouselModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<CarouselModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<CarouselModule, [typeof i1.CarouselComponent], [typeof i2.CommonModule], [typeof i1.CarouselComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<CarouselModule>;
}
