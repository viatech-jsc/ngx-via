export declare class UploadErrorDto {
    field: string;
    message: string;
    constructor(field?: string, message?: string);
}
