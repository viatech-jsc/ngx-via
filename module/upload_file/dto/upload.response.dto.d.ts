export declare class UploadResponseDto {
    private _responseType;
    private _message;
    private _code;
    constructor(responseType?: string, message?: string, code?: string);
    /**
     * Getter responseType
     * return {string}
     */
    get responseType(): string;
    /**
     * Getter message
     * return {string}
     */
    get message(): string;
    /**
     * Getter code
     * return {string}
     */
    get code(): string;
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value: string);
    /**
     * Setter message
     * param {string} value
     */
    set message(value: string);
    /**
     * Setter code
     * param {string} value
     */
    set code(value: string);
}
