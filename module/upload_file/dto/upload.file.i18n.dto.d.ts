export declare class UploadFileI18nDto {
    label: string;
    yes: string;
    cancel: string;
    dndFileHere: string;
    or: string;
    change: string;
    browseFile: string;
    browseFiles: string;
    constructor();
}
