export declare class ImageOptionDto {
    text: string;
    ratioStr: string;
    ratio: number;
    calculateHeightRatio: number;
    resizeToWidth?: number;
    constructor(text?: string, ratioStr?: string, ratio?: number, calculateHeightRatio?: number, resizeToWidth?: number);
}
