export declare class UploadOptionDto {
    private _acceptFileType;
    private _uploadApi;
    private _downloadFileApi;
    private _language;
    private _authorization;
    private _placeHolderImg;
    constructor(acceptFileType: string[] | undefined, uploadApi: string, downloadFileApi: string, language: string, authorization: string, placeHolderImg: string);
    /**
     * Getter acceptFileType
     * return {string[]}
     */
    get acceptFileType(): string[];
    /**
     * Getter uploadApi
     * return {string}
     */
    get uploadApi(): string;
    /**
     * Getter downloadFileApi
     * return {string}
     */
    get downloadFileApi(): string;
    /**
     * Getter language
     * return {string}
     */
    get language(): string;
    /**
     * Getter authorization
     * return {string}
     */
    get authorization(): string;
    /**
     * Getter placeHolderImg
     * return {string}
     */
    get placeHolderImg(): string;
    /**
     * Setter acceptFileType
     * param {string[]} value
     */
    set acceptFileType(value: string[]);
    /**
     * Setter uploadApi
     * param {string} value
     */
    set uploadApi(value: string);
    /**
     * Setter downloadFileApi
     * param {string} value
     */
    set downloadFileApi(value: string);
    /**
     * Setter language
     * param {string} value
     */
    set language(value: string);
    /**
     * Setter authorization
     * param {string} value
     */
    set authorization(value: string);
    /**
     * Setter placeHolderImg
     * param {string} value
     */
    set placeHolderImg(value: string);
}
