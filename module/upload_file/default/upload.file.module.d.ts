import "hammerjs";
import * as i0 from "@angular/core";
import * as i1 from "./upload.file";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common/http";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "@angular/common";
import * as i6 from "ngx-image-cropper";
import * as i7 from "ngx-uploader";
export declare class UploadFileModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<UploadFileModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<UploadFileModule, [typeof i1.UploadFileComponent], [typeof i2.ReactiveFormsModule, typeof i3.HttpClientModule, typeof i4.NgbProgressbarModule, typeof i5.CommonModule, typeof i6.ImageCropperModule, typeof i7.NgxUploaderModule], [typeof i1.UploadFileComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<UploadFileModule>;
}
