import { ImageOptionDto } from "./dto/image.option.dto";
export declare const UPLOAD_FILE_EVENT: {
    START_UPLOAD: string;
    UPLOAD_ERROR: string;
    UPLOAD_SUCCESSED: string;
    FINISH_UPLOAD: string;
};
export declare const uploadType: {
    IMAGE: string;
    FILE: string;
    ALL: string;
};
export declare const allowedContentTypesUploadTxt: Array<string>;
export declare const allowedContentTypesUploadImg: Array<string>;
export declare const allowedContentTypesUploadExel: Array<string>;
export declare const SIXTEEN_NINE: ImageOptionDto;
export declare const ONE_ONE: ImageOptionDto;
export declare const ONE_THREE: ImageOptionDto;
export declare const FOUR_ONE: ImageOptionDto;
