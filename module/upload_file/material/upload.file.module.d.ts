import "hammerjs";
import * as i0 from "@angular/core";
import * as i1 from "./upload.file";
import * as i2 from "@angular/common/http";
import * as i3 from "@angular/common";
import * as i4 from "ngx-image-cropper";
import * as i5 from "ngx-uploader";
import * as i6 from "@angular/material/progress-bar";
import * as i7 from "@angular/material/button";
export declare class MatUploadFileModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<MatUploadFileModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<MatUploadFileModule, [typeof i1.MatUploadFileComponent], [typeof i2.HttpClientModule, typeof i3.CommonModule, typeof i4.ImageCropperModule, typeof i5.NgxUploaderModule, typeof i6.MatProgressBarModule, typeof i7.MatButtonModule], [typeof i1.MatUploadFileComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<MatUploadFileModule>;
}
