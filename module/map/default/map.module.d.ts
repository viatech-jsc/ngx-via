import * as i0 from "@angular/core";
import * as i1 from "./map.component";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
export declare class MapModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<MapModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<MapModule, [typeof i1.MapComponent], [typeof i2.FormsModule, typeof i3.CommonModule, typeof i2.ReactiveFormsModule], [typeof i1.MapComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<MapModule>;
}
