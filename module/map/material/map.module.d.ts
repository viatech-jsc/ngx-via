import * as i0 from "@angular/core";
import * as i1 from "./map";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/form-field";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/material/icon";
import * as i7 from "@angular/material/input";
import * as i8 from "@angular/material/progress-spinner";
export declare class MatMapModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<MatMapModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<MatMapModule, [typeof i1.MatMapComponent], [typeof i2.FormsModule, typeof i3.CommonModule, typeof i2.ReactiveFormsModule, typeof i4.MatFormFieldModule, typeof i5.MatButtonModule, typeof i6.MatIconModule, typeof i7.MatInputModule, typeof i8.MatProgressSpinnerModule], [typeof i1.MatMapComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<MatMapModule>;
}
