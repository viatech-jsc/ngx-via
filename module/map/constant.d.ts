export declare const mapConfig: {
    defaultLattitude: number;
    defaultLongtitude: number;
    defaultTitle: string;
    mapEventNames: {
        search: string;
        myLocation: string;
        mapClick: string;
        markerDrag: string;
    };
    myLocation: string;
};
