import { EventEmitter } from '@angular/core';
import { ModalDto } from "../dto/modal.dto";
import { MatDialogRef } from "@angular/material/dialog";
import * as i0 from "@angular/core";
export interface DialogData {
    text: string;
    options: ModalDto;
}
export declare class MatModalComponent {
    dialogRef: MatDialogRef<MatModalComponent>;
    data: DialogData;
    modalOptions: ModalDto;
    onConfirm: EventEmitter<any>;
    modalTypes: {
        alert: string;
        confirm: string;
    };
    constructor(dialogRef: MatDialogRef<MatModalComponent>, data: DialogData);
    close(): void;
    ok(): void;
    handleCloseByIcon(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MatModalComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MatModalComponent, "via-mat-modal", never, { "modalOptions": { "alias": "modalOptions"; "required": false; }; }, { "onConfirm": "onConfirm"; }, never, never, false, never>;
}
