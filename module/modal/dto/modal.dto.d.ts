export declare class ModalDto {
    modalTitle: string;
    modalContent?: string;
    yesBtnTitle: string;
    noBtnTitle: string;
    type: string;
    translateParams: any;
    modalLogo?: string;
    constructor(modalTitle: string | undefined, yesBtnTitle: string | undefined, noBtnTitle: string | undefined, type: string | undefined, translateParams: any, modalContent?: string, modalLogo?: string);
}
