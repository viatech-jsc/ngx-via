import { EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalDto } from '../dto/modal.dto';
import * as i0 from "@angular/core";
export declare class ModalComponent {
    activeModal: NgbActiveModal;
    modalOptions: ModalDto;
    onConfirm: EventEmitter<any>;
    modalTypes: {
        alert: string;
        confirm: string;
    };
    constructor(activeModal: NgbActiveModal);
    close(): void;
    ok(): void;
    handleCloseByIcon(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ModalComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ModalComponent, "via-modal", never, { "modalOptions": { "alias": "modalOptions"; "required": false; }; }, { "onConfirm": "onConfirm"; }, never, never, false, never>;
}
