import { TableDndResultDto } from "../dto/table.dnd.result.dto";
export declare class DndSupport {
    table: HTMLElement;
    tbody: HTMLElement;
    currRow: HTMLElement | null;
    dragElem: HTMLElement | null;
    mouseDownX: number;
    mouseDownY: number;
    mouseX: number;
    mouseY: number;
    mouseDrag: boolean;
    dndResult: TableDndResultDto;
    swap: Function;
    constructor(table: HTMLElement, swap: (dto: TableDndResultDto) => void);
    init(): void;
    _bindMouse(): void;
    _swapRow(row: HTMLElement, index: number): void;
    _moveRow(x: number, y: number): void;
    _addDraggableRow(target: HTMLElement): void;
    _getRows(): NodeListOf<Element>;
    _getTargetRow(target: HTMLElement | null): HTMLElement | null | undefined;
    _getMouseCoords(event: MouseEvent): {
        x: number;
        y: number;
    };
    _getStyle(target: HTMLElement, styleName: string): string;
    _isIntersecting(min0: number, max0: number, min1: number, max1: number): boolean;
}
