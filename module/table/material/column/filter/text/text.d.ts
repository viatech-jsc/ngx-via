import { EventEmitter } from "@angular/core";
import { MatMenuTrigger } from "@angular/material/menu";
import { ColumnFilterBaseDto } from "../../../../dto/filter/column.filter.base.dto";
import { ColumnFilterTextDto } from "../../../../dto/filter/column.filter.text.dto";
import { TableColumnDto } from "../../../../dto/table.column.dto";
import { TableI18nDto } from "../../../../dto/table.i18n.dto";
import * as i0 from "@angular/core";
export declare class ColumnFilterTextComp {
    matMenuTrigger?: MatMenuTrigger;
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    column: TableColumnDto;
    i18n: TableI18nDto;
    filter: ColumnFilterTextDto;
    searchStr: string;
    badgeCount?: number;
    constructor();
    ngOnInit(): void;
    menuOpened(): void;
    doFilter(): void;
    doReset(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ColumnFilterTextComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ColumnFilterTextComp, "via-mat-table-column-filter-text", never, { "column": { "alias": "column"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; }, { "dropdownFilter": "dropdownFilter"; }, never, never, false, never>;
}
