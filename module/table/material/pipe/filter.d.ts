import { PipeTransform } from '@angular/core';
import { TableColumnDto } from '../../dto/table.column.dto';
import * as i0 from "@angular/core";
export declare class MatFilterPipe implements PipeTransform {
    transform(columns: Array<TableColumnDto>): TableColumnDto[];
    static ɵfac: i0.ɵɵFactoryDeclaration<MatFilterPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<MatFilterPipe, "matFilterColumn", false>;
}
