import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellDateTimeComp {
    cell: TableCellDto;
    dateFormat: string;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellDateTimeComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellDateTimeComp, "via-mat-table-cell-datetime", never, { "cell": { "alias": "cell"; "required": false; }; }, {}, never, never, false, never>;
}
