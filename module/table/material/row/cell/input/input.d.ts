import { ElementRef, EventEmitter } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import { TableRowDto } from "../../../../dto/table.row.dto";
import * as i0 from "@angular/core";
export declare class CellInputComp {
    inputElement?: ElementRef;
    rowUpdated: EventEmitter<TableRowDto>;
    row: TableRowDto;
    cell: TableCellDto;
    inputMaxLenght: number;
    enabled: boolean;
    oldValue: string;
    constructor();
    ngOnInit(): void;
    focusInput(): void;
    keyUpEnter(): void;
    edit(): void;
    focusin(): void;
    focusout(): void;
    doUpdateInputEvent(canEdit: any, newValue: any, originalValue: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellInputComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellInputComp, "via-mat-table-cell-input", never, { "row": { "alias": "row"; "required": false; }; "cell": { "alias": "cell"; "required": false; }; }, { "rowUpdated": "rowUpdated"; }, never, never, false, never>;
}
