import { EventEmitter } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import { TableRowDto } from "../../../../dto/table.row.dto";
import * as i0 from "@angular/core";
export declare class CellCheckboxComp {
    row: TableRowDto;
    cell: TableCellDto;
    rowUpdated: EventEmitter<TableRowDto>;
    checked: boolean;
    constructor();
    ngOnInit(): void;
    change(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellCheckboxComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellCheckboxComp, "via-mat-table-cell-checkbox", never, { "row": { "alias": "row"; "required": false; }; "cell": { "alias": "cell"; "required": false; }; }, { "rowUpdated": "rowUpdated"; }, never, never, false, never>;
}
