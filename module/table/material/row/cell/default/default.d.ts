import { Router } from "@angular/router";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellDefaultComp {
    private router;
    cell: TableCellDto;
    constructor(router: Router);
    ngOnInit(): void;
    cellClicked(cell: TableCellDto): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellDefaultComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellDefaultComp, "via-mat-table-cell-default", never, { "cell": { "alias": "cell"; "required": false; }; }, {}, never, never, false, never>;
}
