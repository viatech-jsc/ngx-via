import { EventEmitter } from "@angular/core";
import { TableCellDto } from "../../../dto/table.cell.dto";
import { TableRowDto } from "../../../dto/table.row.dto";
import * as i0 from "@angular/core";
export declare class CellComp {
    openImagePreview: EventEmitter<string>;
    rowUpdated: EventEmitter<TableRowDto>;
    row: TableRowDto;
    cell: TableCellDto;
    dndSupport: boolean;
    cellType: any;
    constructor();
    ngOnInit(): void;
    doOpenPreview(imgCode: string): void;
    editRow(str: TableRowDto): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellComp, "via-mat-table-cell", never, { "row": { "alias": "row"; "required": false; }; "cell": { "alias": "cell"; "required": false; }; "dndSupport": { "alias": "dndSupport"; "required": false; }; }, { "openImagePreview": "openImagePreview"; "rowUpdated": "rowUpdated"; }, never, never, false, never>;
}
