export declare class TableI18nDto {
    entriesPerPage: string;
    showing: string;
    page: string;
    filterBy: string;
    to: string;
    of: string;
    noDataFound: string;
    elements: string;
    filter: string;
    reset: string;
    search: string;
    constructor();
}
