import { TableCellDto } from "./table.cell.dto";
import { TableRowDto } from "./table.row.dto";
export declare class TableRowValueDto<T> extends TableRowDto {
    private _data;
    constructor(tableCells: TableCellDto[] | undefined, css: string | undefined, data: T);
    get data(): T;
    set data(value: T);
}
