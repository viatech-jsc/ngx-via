export declare class EntryPerpageDto {
    private _name;
    private _value;
    private _selected;
    constructor(name?: string, value?: number, selected?: boolean);
    get selected(): boolean;
    set selected(value: boolean);
    get value(): number;
    set value(value: number);
    get name(): string;
    set name(value: string);
}
