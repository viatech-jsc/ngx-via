import { TableBtnActionDto } from "./table.btn.action.dto";
export declare class TableCellDto {
    private _value;
    private _css;
    private _style;
    private _type;
    private _routeLink;
    private _buttonActions;
    private _buttonDropdownActions;
    constructor(value?: string, css?: string, style?: {
        [klass: string]: any;
    } | null, type?: string, routeLink?: string, buttonActions?: Array<TableBtnActionDto>, buttonDropdownActions?: Array<TableBtnActionDto>);
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    get style(): {
        [klass: string]: any;
    } | null;
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter type
     * return {string}
     */
    get type(): string;
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink(): string;
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions(): TableBtnActionDto[];
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions(): TableBtnActionDto[];
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    set style(value: {
        [klass: string]: any;
    } | null);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter type
     * param {string} value
     */
    set type(value: string);
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value: string);
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value: TableBtnActionDto[]);
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value: TableBtnActionDto[]);
}
