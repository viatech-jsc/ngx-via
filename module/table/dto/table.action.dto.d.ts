export declare class TableActionDto {
    private _name;
    private _type;
    private _routeLink;
    private _css;
    private _hided;
    disabled: boolean;
    constructor(name?: string, routeLink?: string, css?: string, type?: string, hided?: boolean, disabled?: boolean);
    /**
     * Getter hided
     * return {boolean}
     */
    get hided(): boolean;
    /**
     * Setter hided
     * param {boolean} value
     */
    set hided(value: boolean);
    get css(): string;
    set css(value: string);
    get name(): string;
    set name(value: string);
    get type(): string;
    set type(value: string);
    get routeLink(): string;
    set routeLink(value: string);
}
