import { ColumnFilterBaseDto } from "./column.filter.base.dto";
import { ItemDto } from "./item.dto";
export declare class ColumnFilterMultDto extends ColumnFilterBaseDto {
    filterData: Array<ItemDto>;
    constructor();
}
