import { TableRowDto } from "./table.row.dto";
import { TableActionDto } from "./table.action.dto";
import { TableSortDto } from "./table.sort.dto";
import { TableColumnDto } from "./table.column.dto";
import { TableFilterDto } from "./table.filter.dto";
export declare class TableOptionDto {
    private _tableColumns;
    private _tableRows;
    private _totalItems;
    private _itemsPerPage;
    private _currentPage;
    private _tableFilter;
    private _tableActions;
    private _defaultSort;
    private _loading;
    constructor(tableColumns?: Array<TableColumnDto>, tableRows?: Array<TableRowDto>);
    get loading(): boolean;
    set loading(value: boolean);
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns(): TableColumnDto[];
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows(): TableRowDto[];
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems(): number;
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage(): number;
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage(): number;
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter(): TableFilterDto;
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions(): TableActionDto[];
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort(): TableSortDto;
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value: TableColumnDto[]);
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value: TableRowDto[]);
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value: number);
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value: number);
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value: number);
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value: TableFilterDto);
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value: TableActionDto[]);
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value: TableSortDto);
}
