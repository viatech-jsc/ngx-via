import { ColumnFilterBaseDto } from './filter/column.filter.base.dto';
export declare class TableColumnDto {
    private _name;
    private _value;
    private _sortable;
    private _sort;
    private _css;
    private _hidden;
    filter: ColumnFilterBaseDto;
    type: string;
    constructor(value?: string, name?: string, sortable?: boolean, sort?: string, css?: string, hidden?: boolean);
    /**
     * Getter name
     * return {string}
     */
    get name(): string;
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable(): boolean;
    /**
     * Getter sort
     * return {string}
     */
    get sort(): string;
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden(): boolean;
    /**
     * Setter name
     * param {string} value
     */
    set name(value: string);
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value: boolean);
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value: string);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value: boolean);
}
