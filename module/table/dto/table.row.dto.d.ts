import { TableCellDto } from "./table.cell.dto";
export declare class TableRowDto {
    private _tableCells;
    private _css;
    private _editAble;
    private _deleteAble;
    private _deactiveAble;
    private _activeAble;
    constructor(tableCells?: Array<TableCellDto>, css?: string);
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells(): TableCellDto[];
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble(): boolean;
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble(): boolean;
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble(): boolean;
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble(): boolean;
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value: TableCellDto[]);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value: boolean);
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value: boolean);
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value: boolean);
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value: boolean);
}
