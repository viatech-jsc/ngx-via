import { ElementRef, EventEmitter, QueryList } from "@angular/core";
import { EntryPerpageDto } from "../dto/entry.per.page.dto";
import { ColumnFilterBaseDto } from "../dto/filter/column.filter.base.dto";
import { TableActionDto } from "../dto/table.action.dto";
import { TableColumnDto } from "../dto/table.column.dto";
import { TableDndResultDto } from "../dto/table.dnd.result.dto";
import { TableI18nDto } from "../dto/table.i18n.dto";
import { TableOptionDto } from "../dto/table.option.dto";
import { TableRowDto } from "../dto/table.row.dto";
import { ColumnComp } from "./column/column";
import { CellCheckboxComp } from "./row/cell/checkbox/component";
import * as i0 from "@angular/core";
export declare class TableComponent {
    tableView?: ElementRef<HTMLTableElement>;
    cellCheckboxs?: QueryList<CellCheckboxComp>;
    columnElements?: QueryList<ColumnComp>;
    tableOption: TableOptionDto;
    dndSupport: boolean;
    iconFilterImg?: string;
    visibleActionButtons: boolean;
    i18n: TableI18nDto;
    sortColumn: EventEmitter<TableColumnDto>;
    selectColumn: EventEmitter<TableColumnDto>;
    pageChange: EventEmitter<number>;
    entryPageChange: EventEmitter<number>;
    actionButton: EventEmitter<TableActionDto>;
    filterOnColumn: EventEmitter<ColumnFilterBaseDto>;
    openImagePreview: EventEmitter<string>;
    dndUpdated: EventEmitter<TableDndResultDto>;
    rowUpdated: EventEmitter<TableRowDto>;
    isShowFilter: boolean;
    currentSortColumnName?: string;
    currentSortType?: string;
    cellType: any;
    entryPerpages: EntryPerpageDto[];
    constructor();
    ngOnInit(): void;
    ngAfterViewInit(): void;
    getSibling(ui: any): string;
    startItemNumber(): number;
    endItemNumber(): number;
    noDataFound(): boolean;
    hideFilter(): void;
    showFilter(): void;
    toggleFilter(): void;
    doAction(actionBtn: TableActionDto): void;
    doPageChange(): void;
    doEntryPage(value: string): void;
    doSort(column: TableColumnDto): void;
    doSelect(column: TableColumnDto): void;
    doFilterOnColumn(req: ColumnFilterBaseDto): void;
    doUpdateIndex(dragPostionObj: TableDndResultDto): void;
    editRow(row: TableRowDto): void;
    doOpenPreview(imgCode: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TableComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TableComponent, "via-table", never, { "tableOption": { "alias": "tableOption"; "required": false; }; "dndSupport": { "alias": "dndSupport"; "required": false; }; "iconFilterImg": { "alias": "iconFilterImg"; "required": false; }; "visibleActionButtons": { "alias": "visibleActionButtons"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; }, { "sortColumn": "sortColumn"; "selectColumn": "selectColumn"; "pageChange": "pageChange"; "entryPageChange": "entryPageChange"; "actionButton": "actionButton"; "filterOnColumn": "filterOnColumn"; "openImagePreview": "openImagePreview"; "dndUpdated": "dndUpdated"; "rowUpdated": "rowUpdated"; }, never, ["[filter]"], false, never>;
}
