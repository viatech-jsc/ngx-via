import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellStatusComp {
    cell: TableCellDto;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellStatusComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellStatusComp, "via-table-cell-status", never, { "cell": { "alias": "cell"; "required": false; }; }, {}, never, never, false, never>;
}
