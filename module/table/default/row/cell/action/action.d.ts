import { TableCellDto } from "../../../../dto/table.cell.dto";
import { TableRowDto } from "../../../../dto/table.row.dto";
import * as i0 from "@angular/core";
export declare class CellActionComp {
    cell: TableCellDto;
    row: TableRowDto;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellActionComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellActionComp, "via-table-cell-action", never, { "cell": { "alias": "cell"; "required": false; }; "row": { "alias": "row"; "required": false; }; }, {}, never, never, false, never>;
}
