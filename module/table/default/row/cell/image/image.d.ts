import { EventEmitter } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellImageComp {
    openImagePreview: EventEmitter<string>;
    cell: TableCellDto;
    constructor();
    ngOnInit(): void;
    doOpenPreview(imgCode: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CellImageComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CellImageComp, "via-table-cell-image", never, { "cell": { "alias": "cell"; "required": false; }; }, { "openImagePreview": "openImagePreview"; }, never, never, false, never>;
}
