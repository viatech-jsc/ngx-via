import * as i0 from "@angular/core";
import * as i1 from "./table";
import * as i2 from "./pipe/filter";
import * as i3 from "./column/column";
import * as i4 from "./column/default/column";
import * as i5 from "./column/checkbox/column";
import * as i6 from "./column/filter/multiple/multiple";
import * as i7 from "./column/filter/text/text";
import * as i8 from "./row/cell/cell";
import * as i9 from "./row/cell/image/image";
import * as i10 from "./row/cell/input/input";
import * as i11 from "./row/cell/default/default";
import * as i12 from "./row/cell/status/status";
import * as i13 from "./row/cell/datetime/datetime";
import * as i14 from "./row/cell/action/action";
import * as i15 from "./row/cell/checkbox/component";
import * as i16 from "@angular/router";
import * as i17 from "@angular/forms";
import * as i18 from "@angular/common";
import * as i19 from "@ng-bootstrap/ng-bootstrap";
export declare class TableModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<TableModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<TableModule, [typeof i1.TableComponent, typeof i2.FilterPipe, typeof i3.ColumnComp, typeof i4.ColumnDefaultComp, typeof i5.ColumnCheckboxComp, typeof i6.ColumnFilterMultComp, typeof i7.ColumnFilterTextComp, typeof i8.CellComp, typeof i9.CellImageComp, typeof i10.CellInputComp, typeof i11.CellDefaultComp, typeof i12.CellStatusComp, typeof i13.CellDateTimeComp, typeof i14.CellActionComp, typeof i15.CellCheckboxComp], [typeof i16.RouterModule, typeof i17.FormsModule, typeof i18.CommonModule, typeof i19.NgbPaginationModule, typeof i19.NgbDropdownModule], [typeof i1.TableComponent, typeof i2.FilterPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<TableModule>;
}
