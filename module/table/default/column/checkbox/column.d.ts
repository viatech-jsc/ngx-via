import { EventEmitter } from "@angular/core";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
export declare class ColumnCheckboxComp {
    selectColumn: EventEmitter<TableColumnDto>;
    column: TableColumnDto;
    checked: boolean;
    constructor();
    ngOnInit(): void;
    buildUI(): void;
    change(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ColumnCheckboxComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ColumnCheckboxComp, "via-table-column-checkbox", never, { "column": { "alias": "column"; "required": false; }; }, { "selectColumn": "selectColumn"; }, never, never, false, never>;
}
