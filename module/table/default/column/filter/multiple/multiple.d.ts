import { EventEmitter } from "@angular/core";
import { ColumnFilterBaseDto } from "../../../../dto/filter/column.filter.base.dto";
import { ColumnFilterMultDto } from "../../../../dto/filter/column.filter.mult.dto";
import { TableColumnDto } from "../../../../dto/table.column.dto";
import { TableI18nDto } from "../../../../dto/table.i18n.dto";
import * as i0 from "@angular/core";
export declare class ColumnFilterMultComp {
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    column: TableColumnDto;
    i18n: TableI18nDto;
    filter: ColumnFilterMultDto;
    badgeCount?: number;
    constructor();
    ngOnInit(): void;
    doFilter(): void;
    doReset(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ColumnFilterMultComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ColumnFilterMultComp, "via-table-column-filter-mult", never, { "column": { "alias": "column"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; }, { "dropdownFilter": "dropdownFilter"; }, never, never, false, never>;
}
