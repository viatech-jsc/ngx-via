import { EventEmitter } from "@angular/core";
import { ColumnFilterBaseDto } from "../../dto/filter/column.filter.base.dto";
import { TableColumnDto } from "../../dto/table.column.dto";
import { TableI18nDto } from "../../dto/table.i18n.dto";
import { ColumnCheckboxComp } from "./checkbox/column";
import * as i0 from "@angular/core";
export declare class ColumnComp {
    columnCheckbox?: ColumnCheckboxComp;
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    sortColumn: EventEmitter<TableColumnDto>;
    selectColumn: EventEmitter<TableColumnDto>;
    column: TableColumnDto;
    i18n: TableI18nDto;
    columType: any;
    constructor();
    ngOnInit(): void;
    buildUI(): void;
    doFilterOnColumn(req: ColumnFilterBaseDto): void;
    isSortAsc(column: TableColumnDto): boolean;
    isSortDesc(column: TableColumnDto): boolean;
    doSort(column: TableColumnDto): void;
    doSelect(column: TableColumnDto): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ColumnComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ColumnComp, "via-table-column", never, { "column": { "alias": "column"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; }, { "dropdownFilter": "dropdownFilter"; "sortColumn": "sortColumn"; "selectColumn": "selectColumn"; }, never, never, false, never>;
}
