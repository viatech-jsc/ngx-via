import { EventEmitter } from "@angular/core";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
export declare class ColumnDefaultComp {
    actionSort: EventEmitter<TableColumnDto>;
    column: TableColumnDto;
    constructor();
    ngOnInit(): void;
    doSort(column: TableColumnDto): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ColumnDefaultComp, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ColumnDefaultComp, "via-table-column-default", never, { "column": { "alias": "column"; "required": false; }; }, { "actionSort": "actionSort"; }, never, never, false, never>;
}
