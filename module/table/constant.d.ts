export declare const SORT: {
    ASC: string;
    DESC: string;
    NONE: string;
};
export declare const CELL_TYPE: {
    STRING: string;
    DATE_TIME: string;
    STATUS: string;
    ACTION: string;
    IMAGE_THUMBNAIL: string;
    INPUT: string;
    CHECKBOX: string;
};
export declare const COLUMN_TYPE: {
    CHECKBOX: string;
    DEFAULT: string;
};
export declare const FILTER_ON_COLUMN_TYPE: {
    NONE: string;
    MULTIPLE: string;
    TEXT: string;
};
export declare const ACTION_TYPE: {
    BUTTON: string;
    LINK: string;
    MODAL: string;
};
export declare const TABLE_DATE_FORMAT = "hh:mma dd/MM/yyyy";
