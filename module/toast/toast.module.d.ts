import * as i0 from "@angular/core";
import * as i1 from "./toast";
import * as i2 from "@angular/common";
import * as i3 from "ngx-toastr";
export declare class ToastModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<ToastModule, [typeof i1.ToastComponent], [typeof i2.CommonModule, typeof i3.ToastrModule], [typeof i1.ToastComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<ToastModule>;
}
