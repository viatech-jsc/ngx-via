import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import * as i0 from "@angular/core";
export declare class ToastComponent extends Toast {
    protected toastrService: ToastrService;
    toastPackage: ToastPackage;
    listMessages: string[];
    constructor(toastrService: ToastrService, toastPackage: ToastPackage);
    static ɵfac: i0.ɵɵFactoryDeclaration<ToastComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ToastComponent, "[via-toast]", never, {}, {}, never, never, false, never>;
}
