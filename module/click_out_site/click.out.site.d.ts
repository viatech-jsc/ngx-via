import { ElementRef, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export declare class ClickOutSiteDirective {
    private el;
    viaClickOutSite: EventEmitter<string>;
    clickOutSite: EventEmitter<string>;
    constructor(el: ElementRef);
    documentClick($event: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ClickOutSiteDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<ClickOutSiteDirective, "[viaClickOutSite]", never, {}, { "viaClickOutSite": "viaClickOutSite"; "clickOutSite": "clickOutSite"; }, never, never, false, never>;
}
