import * as i0 from "@angular/core";
import * as i1 from "./click.out.site";
export declare class ClickOutSiteModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<ClickOutSiteModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<ClickOutSiteModule, [typeof i1.ClickOutSiteDirective], never, [typeof i1.ClickOutSiteDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<ClickOutSiteModule>;
}
