import * as i0 from "@angular/core";
export declare class GoogleAnalyticsService {
    constructor();
    eventEmitter(eventCategory: string, eventAction: string, eventLabel?: string | undefined, eventValue?: number | undefined): void;
    subscribeRouterEvents(event: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<GoogleAnalyticsService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<GoogleAnalyticsService>;
}
