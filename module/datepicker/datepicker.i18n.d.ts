import { NgbDateStruct, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { I18nService } from "./i18n.service";
import { TranslationWidth } from "@angular/common";
import * as i0 from "@angular/core";
export declare class DatepickerI18n extends NgbDatepickerI18n {
    private _i18n;
    getWeekdayLabel(weekday: number, width?: TranslationWidth | undefined): string;
    constructor(_i18n: I18nService);
    getWeekdayShortName(weekday: number): string;
    getMonthShortName(month: number): string;
    getMonthFullName(month: number): string;
    getDayAriaLabel(date: NgbDateStruct): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<DatepickerI18n, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DatepickerI18n>;
}
