import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as i0 from "@angular/core";
export declare class DateParserFormatter extends NgbDateParserFormatter {
    readonly DELIMITER = "/";
    parse(value: string): NgbDateStruct | null;
    format(date: NgbDateStruct | null): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<DateParserFormatter, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DateParserFormatter>;
}
