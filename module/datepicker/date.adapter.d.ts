import { NgbDateAdapter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as i0 from "@angular/core";
export declare class DateAdapter extends NgbDateAdapter<string> {
    readonly DELIMITER: string;
    fromModel(value: string | null): NgbDateStruct | null;
    toModel(date: NgbDateStruct | null): string | null;
    static ɵfac: i0.ɵɵFactoryDeclaration<DateAdapter, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DateAdapter>;
}
