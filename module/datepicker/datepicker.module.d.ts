import * as i0 from "@angular/core";
import * as i1 from "./datepicker";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
export declare class DatepickerModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DatepickerModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DatepickerModule, [typeof i1.DatepickerComponent], [typeof i2.CommonModule, typeof i3.ReactiveFormsModule, typeof i4.NgbDatepickerModule], [typeof i1.DatepickerComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DatepickerModule>;
}
