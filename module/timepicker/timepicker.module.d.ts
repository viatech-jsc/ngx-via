import * as i0 from "@angular/core";
import * as i1 from "./timepicker";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
export declare class TimepickerModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<TimepickerModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<TimepickerModule, [typeof i1.TimepickerComponent], [typeof i2.ReactiveFormsModule, typeof i3.CommonModule, typeof i4.NgbTimepickerModule, typeof i2.FormsModule], [typeof i1.TimepickerComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<TimepickerModule>;
}
