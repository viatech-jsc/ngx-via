import { EventEmitter, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import { TimepickerI18nDto } from "./dto/timepicker.i18n.dto";
import * as i0 from "@angular/core";
export declare class TimepickerComponent implements OnInit {
    timepickerModelChange: EventEmitter<NgbTimeStruct>;
    timepickerModel?: NgbTimeStruct;
    showSeccond: boolean;
    showMeridian: boolean;
    disabled: boolean;
    spinners: boolean;
    viaControl: FormControl;
    i18n?: TimepickerI18nDto;
    timepickerId: string;
    constructor();
    ngOnInit(): void;
    formControlInstance(): FormControl<any>;
    timeSelect(time: NgbTimeStruct): void;
    required(form?: FormControl): "" | "*";
    static ɵfac: i0.ɵɵFactoryDeclaration<TimepickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TimepickerComponent, "via-timepicker", never, { "timepickerModel": { "alias": "timepickerModel"; "required": false; }; "showSeccond": { "alias": "showSeccond"; "required": false; }; "showMeridian": { "alias": "showMeridian"; "required": false; }; "disabled": { "alias": "disabled"; "required": false; }; "spinners": { "alias": "spinners"; "required": false; }; "viaControl": { "alias": "viaControl"; "required": false; }; "i18n": { "alias": "i18n"; "required": false; }; }, { "timepickerModelChange": "timepickerModelChange"; }, never, never, false, never>;
}
