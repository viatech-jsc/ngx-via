import { FormControl } from "@angular/forms";
export declare class FormUtils {
    private static instance;
    constructor();
    static getInstance(): FormUtils;
    required(form?: FormControl): "" | "*";
}
