import * as i0 from '@angular/core';
import { EventEmitter, Directive, Output, HostListener, NgModule, Injectable, Component, Input, Pipe, ViewEncapsulation, Inject, ViewChild, ViewChildren } from '@angular/core';
import * as i1$3 from '@angular/router';
import { NavigationEnd, RouterModule } from '@angular/router';
import * as i1$1 from '@angular/forms';
import { FormControl, ReactiveFormsModule, FormsModule, FormGroup } from '@angular/forms';
import * as i2 from '@ng-bootstrap/ng-bootstrap';
import { NgbDate, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n, NgbDatepickerModule, NgbModalModule, NgbAlertModule, NgbPaginationModule, NgbDropdownModule, NgbTimepickerModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import random from 'lodash/random';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';
import 'pannellum/src/js/pannellum.js';
import 'pannellum/src/js/libpannellum.js';
import * as i1$2 from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import * as i4 from '@angular/material/button';
import { MatButtonModule } from '@angular/material/button';
import clone from 'lodash/clone';
import * as i2$3 from '@angular/material/paginator';
import { MatPaginatorModule } from '@angular/material/paginator';
import * as i3 from '@angular/material/icon';
import { MatIconModule } from '@angular/material/icon';
import * as i5$1 from '@angular/material/progress-spinner';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import * as i6 from '@angular/material/list';
import { MatListModule } from '@angular/material/list';
import * as i2$1 from '@angular/material/menu';
import { MatMenuTrigger, MatMenuModule } from '@angular/material/menu';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import * as i5 from '@angular/material/badge';
import { MatBadgeModule } from '@angular/material/badge';
import * as i7 from '@angular/material/form-field';
import { MatFormFieldModule } from '@angular/material/form-field';
import * as i7$1 from '@angular/material/input';
import { MatInputModule } from '@angular/material/input';
import * as i2$2 from '@angular/material/checkbox';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { trigger } from '@angular/animations';
import * as i1$4 from 'ngx-toastr';
import { Toast, ToastrModule } from 'ngx-toastr';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import * as i3$1 from '@angular/material/chips';
import { MatChipsModule } from '@angular/material/chips';
import * as i5$2 from '@angular/material/autocomplete';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import * as i6$1 from '@angular/material/core';
import * as i4$1 from 'ngx-image-cropper';
import { base64ToFile, ImageCropperComponent, ImageCropperModule } from 'ngx-image-cropper';
import * as i5$3 from 'ngx-uploader';
import { humanizeBytes, NgxUploaderModule } from 'ngx-uploader';
import * as i1$5 from '@angular/common/http';
import { HttpHeaders, HttpClientModule } from '@angular/common/http';
import 'hammerjs';
import * as i5$4 from '@angular/material/progress-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MarkerClusterer } from '@googlemaps/markerclusterer';

class ClickOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.viaClickOutSite = new EventEmitter();
        this.clickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.viaClickOutSite && this.viaClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1) {
                this.viaClickOutSite.emit("");
            }
        }
        else if (this.clickOutSite && this.clickOutSite.observers.length > 0) {
            if (!this.el.nativeElement.contains($event.target)) {
                this.clickOutSite.emit("");
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive }); }
    static { this.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "14.0.0", version: "17.3.5", type: ClickOutSiteDirective, selector: "[viaClickOutSite]", outputs: { viaClickOutSite: "viaClickOutSite", clickOutSite: "clickOutSite" }, host: { listeners: { "document:click": "documentClick($event)" } }, ngImport: i0 }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: "[viaClickOutSite]"
                }]
        }], ctorParameters: () => [{ type: i0.ElementRef }], propDecorators: { viaClickOutSite: [{
                type: Output
            }], clickOutSite: [{
                type: Output
            }], documentClick: [{
                type: HostListener,
                args: ["document:click", ["$event"]]
            }] } });

class ClickOutSiteModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, declarations: [ClickOutSiteDirective], exports: [ClickOutSiteDirective] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [],
                    declarations: [ClickOutSiteDirective],
                    exports: [ClickOutSiteDirective]
                }]
        }] });

class GoogleAnalyticsService {
    //************************
    // Import this to app.component.ts
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //     GoogleAnalyticsService.subscribeRouterEvents(event);
    //   });
    //************************
    constructor() { }
    // /**
    //   * Emit google analytics event
    //   * Fire event example:
    //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
    //   * param {string} eventCategory
    //   * param {string} eventAction
    //   * param {string} eventLabel
    //   * param {number} eventValue
    //   */
    eventEmitter(eventCategory, eventAction, eventLabel = undefined, eventValue = undefined) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventLabel: eventLabel,
            eventAction: eventAction,
            eventValue: eventValue
        });
    }
    subscribeRouterEvents(event) {
        if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService, decorators: [{
            type: Injectable
        }], ctorParameters: () => [] });

class FormUtils {
    static { this.instance = new FormUtils(); }
    constructor() { }
    static getInstance() {
        if (!FormUtils.instance) {
            FormUtils.instance = new FormUtils();
        }
        return FormUtils.instance;
    }
    required(form) {
        if (form && form.validator) {
            const validator = form.validator({});
            if (validator && validator['required']) {
                return '*';
            }
        }
        return '';
    }
}

const DATE_FORMAT = "hh:mma dd/MM/yyyy";
const DATE_FORMAT_WITHOUT_TIME = "dd/mm/yyyy";
const I18N_VALUES = {
    en: {
        weekdays: [
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa",
            "Su"
        ],
        months: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ]
    },
    vi: {
        weekdays: [
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "CN"
        ],
        months: [
            "T1",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "T8",
            "T9",
            "T10",
            "T11",
            "T12"
        ]
    }
};

class DatepickerI18nDto {
    constructor() {
        this.label = "";
    }
}

class I18nService {
    constructor() {
        this.language = "vi";
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: I18nService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: I18nService }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: I18nService, decorators: [{
            type: Injectable
        }], ctorParameters: () => [] });

class DatepickerComponent {
    constructor(_i18n, formatter, calendar) {
        this._i18n = _i18n;
        this.formatter = formatter;
        this.calendar = calendar;
        this.today = new Date();
        this.datepickerItemChange = new EventEmitter();
        this.dateFormat = undefined;
        this.datepickerItem = undefined;
        this.maxDate = undefined;
        this.minDate = undefined;
        this.autoClose = true;
        this.disabled = false;
        this.readOnly = false;
        this.placeholder = "dd/MM/yyy";
        this.pickerImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.datepickerId = "";
        this.datepickerId = "datepicker" + random();
        if (this.dateFormat) {
            this.dateFormat = DATE_FORMAT_WITHOUT_TIME;
        }
        if (this.minDate) {
            this.minDate = {
                day: 1,
                month: 1,
                year: this.today.getFullYear() - 100
            };
        }
        this._i18n.language = "en";
        this.setLanguage("en");
    }
    ngOnInit() {
    }
    formControlInstance() {
        return this.viaControl;
    }
    selected(date) {
        this.datepickerItem = date;
        this.viaControl.setValue(date);
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    setLanguage(language) {
        this._i18n.language = language;
        // this.placeholder = this._i18n.language == "en"? "dd/mm/yyyy": "nn/tt/nnnn";
    }
    getDateSelected() {
        if (this.datepickerItem) {
            return new Date(this.datepickerItem.year, this.datepickerItem.month - 1, this.datepickerItem.day);
        }
        return undefined;
    }
    dateSelect(date) {
        this.viaControl.setValue(date);
        this.datepickerItemChange?.emit(date);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    validateInput(input) {
        const parsed = this.formatter.parse(input);
        let value = undefined;
        if (parsed == undefined || parsed == null || (parsed && this.calendar.isValid(NgbDate.from(parsed)))) {
            let v = NgbDate.from(parsed);
            if (v) {
                value = v;
            }
        }
        this.dateSelect(value);
        return value;
    }
    format(datepickerItem) {
        return this.formatter.format(datepickerItem ? datepickerItem : null);
    }
    checkError(date) {
        const parsed = this.formatter.parse(date);
        let isValid = false;
        if (parsed && this.calendar.isValid(NgbDate.from(parsed))) {
            isValid = true;
        }
        return isValid;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerComponent, deps: [{ token: I18nService }, { token: i2.NgbDateParserFormatter }, { token: i2.NgbCalendar }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: DatepickerComponent, selector: "via-datepicker", inputs: { dateFormat: "dateFormat", datepickerItem: "datepickerItem", maxDate: "maxDate", minDate: "minDate", autoClose: "autoClose", disabled: "disabled", readOnly: "readOnly", placeholder: "placeholder", pickerImg: "pickerImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { datepickerItemChange: "datepickerItemChange" }, ngImport: i0, template: "<div class=\"via-datepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"datepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"input-wrapper\">\n        <input \n            [attr.id]=\"datepickerId\"\n            class=\"form-control\" \n            [placeholder]=\"placeholder\" \n            [startDate]=\"$any(datepickerItem)\" \n            #datePickerIpt \n            name=\"datePickerIpt\" \n            [value]=\"format(datepickerItem)\" \n            (input)=\"datepickerItem = validateInput(datePickerIpt.value)\" \n            ngbDatepicker \n            #d=\"ngbDatepicker\" \n            [minDate]=\"$any(minDate)\" \n            [maxDate]=\"$any(maxDate)\"\n            (dateSelect)=\"dateSelect($event)\"\n            [autoClose]=\"autoClose\"\n            [attr.disabled]=\"disabled\"\n            [disabled]=\"disabled\"\n            [readOnly]=\"readOnly\" />\n\n        <img *ngIf=\"pickerImg\" (click)=\"d.toggle()\" class=\"date-icon\" [src]=\"pickerImg\" />\n        <i *ngIf=\"!pickerImg\" (click)=\"d.toggle()\" class=\"far fa-calendar date-icon\" aria-hidden=\"true\"></i>\n    </div>\n</div>", styles: [".via-datepicker{width:100%;border-radius:3px;display:flex;flex-direction:column;position:relative;cursor:pointer}.via-datepicker .danger-text{color:brown}.via-datepicker .input-wrapper{position:relative}.via-datepicker .input-wrapper .form-control{min-height:46px}.via-datepicker .input-wrapper .date-icon{position:absolute;right:10px;top:13px;width:20px}.via-datepicker .input-wrapper .disabled-background{background:#f7f8f9}.via-datepicker .input-wrapper .datepicker-container{display:flex;list-style:none;margin:0;flex:1;color:#999;padding:5px 10px}.via-datepicker .input-wrapper .datepicker-container li{margin:0}.via-datepicker .input-wrapper .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}.via-datepicker .input-wrapper .datepicker-container .datepicker img{width:20px;margin-right:5px}.via-datepicker .input-wrapper .datepicker-container .datepicker span{flex:8}.via-datepicker .input-wrapper .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}.via-datepicker .input-wrapper .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0;border:0}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i2.NgbInputDatepicker, selector: "input[ngbDatepicker]", inputs: ["autoClose", "contentTemplate", "datepickerClass", "dayTemplate", "dayTemplateData", "displayMonths", "firstDayOfWeek", "footerTemplate", "markDisabled", "minDate", "maxDate", "navigation", "outsideDays", "placement", "popperOptions", "restoreFocus", "showWeekNumbers", "startDate", "container", "positionTarget", "weekdays", "disabled"], outputs: ["dateSelect", "navigate", "closed"], exportAs: ["ngbDatepicker"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-datepicker", template: "<div class=\"via-datepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"datepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"input-wrapper\">\n        <input \n            [attr.id]=\"datepickerId\"\n            class=\"form-control\" \n            [placeholder]=\"placeholder\" \n            [startDate]=\"$any(datepickerItem)\" \n            #datePickerIpt \n            name=\"datePickerIpt\" \n            [value]=\"format(datepickerItem)\" \n            (input)=\"datepickerItem = validateInput(datePickerIpt.value)\" \n            ngbDatepicker \n            #d=\"ngbDatepicker\" \n            [minDate]=\"$any(minDate)\" \n            [maxDate]=\"$any(maxDate)\"\n            (dateSelect)=\"dateSelect($event)\"\n            [autoClose]=\"autoClose\"\n            [attr.disabled]=\"disabled\"\n            [disabled]=\"disabled\"\n            [readOnly]=\"readOnly\" />\n\n        <img *ngIf=\"pickerImg\" (click)=\"d.toggle()\" class=\"date-icon\" [src]=\"pickerImg\" />\n        <i *ngIf=\"!pickerImg\" (click)=\"d.toggle()\" class=\"far fa-calendar date-icon\" aria-hidden=\"true\"></i>\n    </div>\n</div>", styles: [".via-datepicker{width:100%;border-radius:3px;display:flex;flex-direction:column;position:relative;cursor:pointer}.via-datepicker .danger-text{color:brown}.via-datepicker .input-wrapper{position:relative}.via-datepicker .input-wrapper .form-control{min-height:46px}.via-datepicker .input-wrapper .date-icon{position:absolute;right:10px;top:13px;width:20px}.via-datepicker .input-wrapper .disabled-background{background:#f7f8f9}.via-datepicker .input-wrapper .datepicker-container{display:flex;list-style:none;margin:0;flex:1;color:#999;padding:5px 10px}.via-datepicker .input-wrapper .datepicker-container li{margin:0}.via-datepicker .input-wrapper .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}.via-datepicker .input-wrapper .datepicker-container .datepicker img{width:20px;margin-right:5px}.via-datepicker .input-wrapper .datepicker-container .datepicker span{flex:8}.via-datepicker .input-wrapper .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}.via-datepicker .input-wrapper .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0;border:0}\n"] }]
        }], ctorParameters: () => [{ type: I18nService }, { type: i2.NgbDateParserFormatter }, { type: i2.NgbCalendar }], propDecorators: { datepickerItemChange: [{
                type: Output
            }], dateFormat: [{
                type: Input
            }], datepickerItem: [{
                type: Input
            }], maxDate: [{
                type: Input
            }], minDate: [{
                type: Input
            }], autoClose: [{
                type: Input
            }], disabled: [{
                type: Input
            }], readOnly: [{
                type: Input
            }], placeholder: [{
                type: Input
            }], pickerImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class DateAdapter extends NgbDateAdapter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '-';
    }
    fromModel(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    toModel(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter, deps: null, target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter, decorators: [{
            type: Injectable
        }] });

class DateParserFormatter extends NgbDateParserFormatter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '/';
    }
    parse(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    format(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateParserFormatter, deps: null, target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateParserFormatter }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateParserFormatter, decorators: [{
            type: Injectable
        }] });

class DatepickerI18n extends NgbDatepickerI18n {
    getWeekdayLabel(weekday, width) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    constructor(_i18n) {
        super();
        this._i18n = _i18n;
    }
    getWeekdayShortName(weekday) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month) {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month) {
        return this.getMonthShortName(month);
    }
    getDayAriaLabel(date) {
        return `${date.day}-${date.month}-${date.year}`;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n, deps: [{ token: I18nService }], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n, decorators: [{
            type: Injectable
        }], ctorParameters: () => [{ type: I18nService }] });

class DatepickerModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, declarations: [DatepickerComponent], imports: [CommonModule,
            ReactiveFormsModule,
            NgbDatepickerModule], exports: [DatepickerComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, providers: [
            I18nService,
            { provide: NgbDatepickerI18n, useClass: DatepickerI18n },
            { provide: NgbDateAdapter, useClass: DateAdapter },
            { provide: NgbDateParserFormatter, useClass: DateParserFormatter }
        ], imports: [CommonModule,
            ReactiveFormsModule,
            NgbDatepickerModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [DatepickerComponent],
                    imports: [
                        CommonModule,
                        ReactiveFormsModule,
                        NgbDatepickerModule,
                    ],
                    exports: [
                        DatepickerComponent
                    ],
                    providers: [
                        I18nService,
                        { provide: NgbDatepickerI18n, useClass: DatepickerI18n },
                        { provide: NgbDateAdapter, useClass: DateAdapter },
                        { provide: NgbDateParserFormatter, useClass: DateParserFormatter }
                    ]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class DropdownI18nDto {
    constructor() {
        this.label = "";
    }
}

class FullTextSearchPipe {
    constructor() { }
    transform(value, keys, term) {
        if (!term)
            return value;
        //@ts-ignore
        return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe }); }
    static { this.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, name: "fullTextSearch", pure: false }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'fullTextSearch',
                    pure: false
                }]
        }], ctorParameters: () => [] });

class DropdownComponent {
    constructor() {
        this.itemSelectedChange = new EventEmitter();
        this.items = new Array();
        this.itemSelected = new Array();
        this.multiple = false;
        this.disabled = false;
        this.canSearch = false;
        this.placeholder = "";
        this.iconImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.isShow = false;
        this.searchText = "";
        this.dropdownId = "";
        this.dropdownId = "dropdown" + random();
    }
    formControlInstance() {
        return this.viaControl;
    }
    isItemSelectedValid() {
        return Array.isArray(this.itemSelected) && this.itemSelected.length > 0;
    }
    toggle() {
        if (!this.disabled) {
            this.isShow = !this.isShow;
        }
    }
    hide() {
        this.isShow = false;
    }
    show() {
        this.isShow = true;
    }
    hasSelected(item) {
        let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
        return itemsFound.length > 0;
    }
    selected(item) {
        this.searchText = "";
        if (this.multiple) {
            let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
            if (itemsFound.length === 0) {
                this.itemSelected.push(item);
            }
            else {
                this.itemSelected = this.itemSelected.filter((i) => { return i.value !== item.value; });
            }
        }
        else {
            this.hide();
            this.itemSelected = [];
            this.itemSelected.push(item);
        }
        this.viaControl.setValue(this.itemSelected);
        this.itemSelectedChange?.emit(this.itemSelected);
    }
    clearSearchText() {
        this.searchText = "";
    }
    getSelectedStr(itemSelected) {
        let str = "";
        if (itemSelected) {
            let i = 0;
            itemSelected.forEach(item => {
                i = i + 1;
                str = str + item.name + (i < itemSelected.length ? ", " : "");
            });
        }
        return str;
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DropdownComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: DropdownComponent, selector: "via-dropdown", inputs: { items: "items", itemSelected: "itemSelected", multiple: "multiple", disabled: "disabled", canSearch: "canSearch", placeholder: "placeholder", iconImg: "iconImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { itemSelectedChange: "itemSelectedChange" }, ngImport: i0, template: "<div class=\"via-dropdown-wrapper form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"dropdownId\" (click)=\"toggle()\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"via-dropdown\">\n        <ul class=\"dropdown-container\" [ngClass]=\"{'disabled-background' : disabled}\">\n            <li class=\"dropdown\" [ngClass]=\"{'open' : isShow}\">\n                <div class=\"dropdown-selectedBox\" (click)=\"toggle()\">\n                    <div *ngIf=\"isItemSelectedValid()\" title=\"{{getSelectedStr(itemSelected)}}\" class=\"text-box\">\n                        {{getSelectedStr(itemSelected)}}\n                    </div>\n                    <span *ngIf=\"!isItemSelectedValid()\">{{placeholder}}</span>\n                    <span class=\"dropdown-arrow\">\n                        <i class=\"fas fa-angle-down arrow\" [ngClass]=\"{'arrow-rotate' : isShow}\" aria-hidden=\"true\"></i>\n                    </span>\n                </div>\n    \n                <ul class=\"dropdown-menu-items z-depth-1\">\n                    <div *ngIf=\"canSearch\" class=\"dropdown-search-bar\"> \n                        <input [(ngModel)]=\"searchText\" class=\"dropdown-search-input\">\n                        <i *ngIf=\"searchText\" class=\"fas fa-times clear-search-input\" (click)=\"clearSearchText()\" aria-hidden=\"true\"></i>\n                        <img *ngIf=\"iconImg && !searchText\" class=\"input-search-icon dropdown-search-icon\" [src]=\"iconImg\">\n                        <div *ngIf=\"!iconImg && !searchText\" class=\"input-search-icon dropdown-search-icon\" >\n                            <i class=\"fas fa-search\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <li *ngFor=\"let item of items | fullTextSearch:'name':searchText\" class=\"dropdown-menu-item\" [ngClass]=\"{'selected': (hasSelected(item))}\" (click)=\"selected(item)\">\n                        <span class=\"text-no-wrap\" title=\"{{item.name}}\">{{item.name}}</span>\n                        <span class=\"item-check\">\n                            <i class=\"fas fa-check\" aria-hidden=\"true\"></i>\n                        </span>\n                    </li>\n                </ul>\n                <div (click)=\"hide()\" [ngClass]=\"{'open' : isShow}\" class=\"dropdown-backdrop\"></div>\n            </li>\n        </ul>\n    </div>\n</div>", styles: [".via-dropdown-wrapper .danger-text{color:brown}.via-dropdown-wrapper .via-dropdown{width:100%;background:#fff;border:1px solid #EBEBEB;border-radius:3px;display:flex;flex-direction:row;cursor:pointer}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar{position:sticky;top:0}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .dropdown-search-input{width:100%;border:.5px solid #dedede;height:45px;font-size:13px;border-top:0;border-left:0;border-right:0;padding-left:10px}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .clear-search-input{color:#d9e0e5;position:absolute;right:13px;top:13px}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .dropdown-search-icon{width:20px;position:absolute;top:13px;right:13px}.via-dropdown-wrapper .via-dropdown .dropdown-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;width:100%}.via-dropdown-wrapper .via-dropdown .dropdown-container li{margin:0}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown{width:100%;display:flex}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown.open .dropdown-menu-items,.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown.open .dropdown-backdrop{display:block}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items{display:none;z-index:4;position:absolute;top:100%;list-style:none;margin:5px 0 38px;width:100%;border-radius:3px;padding:0;border:1px solid #EBEBEB;background:#fff;max-height:400px;overflow:auto}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item{display:flex;flex:9;justify-content:flex-start;align-items:center;padding:10px}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item:hover{background:#f7f8f9}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item.selected .item-check{display:block;color:var(--primary-color, #9a0000)}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item .text-no-wrap{white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item .item-check{display:none;color:#bcbec0;text-align:right;flex:1}.via-dropdown-wrapper .via-dropdown .arrow-rotate{transform:rotate(-180deg)}.via-dropdown-wrapper .via-dropdown .arrow{transition:.2s ease}.via-dropdown-wrapper .via-dropdown .dropdown-selectedBox{width:100%;padding:10px}.via-dropdown-wrapper .via-dropdown .dropdown-selectedBox .text-box{white-space:nowrap;width:90%;text-overflow:ellipsis;overflow:hidden}.via-dropdown-wrapper .via-dropdown .dropdown-arrow{flex:1;text-align:center;height:46px;line-height:46px;position:absolute;right:10px;top:2px}.via-dropdown-wrapper .via-dropdown .dropdown-arrow i{font-size:24px;color:#999}.via-dropdown-wrapper .via-dropdown .dropdown-backdrop{width:100%;height:100%;display:none;position:fixed;left:0;top:0;z-index:2}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "pipe", type: FullTextSearchPipe, name: "fullTextSearch" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DropdownComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-dropdown", template: "<div class=\"via-dropdown-wrapper form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"dropdownId\" (click)=\"toggle()\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"via-dropdown\">\n        <ul class=\"dropdown-container\" [ngClass]=\"{'disabled-background' : disabled}\">\n            <li class=\"dropdown\" [ngClass]=\"{'open' : isShow}\">\n                <div class=\"dropdown-selectedBox\" (click)=\"toggle()\">\n                    <div *ngIf=\"isItemSelectedValid()\" title=\"{{getSelectedStr(itemSelected)}}\" class=\"text-box\">\n                        {{getSelectedStr(itemSelected)}}\n                    </div>\n                    <span *ngIf=\"!isItemSelectedValid()\">{{placeholder}}</span>\n                    <span class=\"dropdown-arrow\">\n                        <i class=\"fas fa-angle-down arrow\" [ngClass]=\"{'arrow-rotate' : isShow}\" aria-hidden=\"true\"></i>\n                    </span>\n                </div>\n    \n                <ul class=\"dropdown-menu-items z-depth-1\">\n                    <div *ngIf=\"canSearch\" class=\"dropdown-search-bar\"> \n                        <input [(ngModel)]=\"searchText\" class=\"dropdown-search-input\">\n                        <i *ngIf=\"searchText\" class=\"fas fa-times clear-search-input\" (click)=\"clearSearchText()\" aria-hidden=\"true\"></i>\n                        <img *ngIf=\"iconImg && !searchText\" class=\"input-search-icon dropdown-search-icon\" [src]=\"iconImg\">\n                        <div *ngIf=\"!iconImg && !searchText\" class=\"input-search-icon dropdown-search-icon\" >\n                            <i class=\"fas fa-search\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <li *ngFor=\"let item of items | fullTextSearch:'name':searchText\" class=\"dropdown-menu-item\" [ngClass]=\"{'selected': (hasSelected(item))}\" (click)=\"selected(item)\">\n                        <span class=\"text-no-wrap\" title=\"{{item.name}}\">{{item.name}}</span>\n                        <span class=\"item-check\">\n                            <i class=\"fas fa-check\" aria-hidden=\"true\"></i>\n                        </span>\n                    </li>\n                </ul>\n                <div (click)=\"hide()\" [ngClass]=\"{'open' : isShow}\" class=\"dropdown-backdrop\"></div>\n            </li>\n        </ul>\n    </div>\n</div>", styles: [".via-dropdown-wrapper .danger-text{color:brown}.via-dropdown-wrapper .via-dropdown{width:100%;background:#fff;border:1px solid #EBEBEB;border-radius:3px;display:flex;flex-direction:row;cursor:pointer}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar{position:sticky;top:0}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .dropdown-search-input{width:100%;border:.5px solid #dedede;height:45px;font-size:13px;border-top:0;border-left:0;border-right:0;padding-left:10px}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .clear-search-input{color:#d9e0e5;position:absolute;right:13px;top:13px}.via-dropdown-wrapper .via-dropdown .dropdown-search-bar .dropdown-search-icon{width:20px;position:absolute;top:13px;right:13px}.via-dropdown-wrapper .via-dropdown .dropdown-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;width:100%}.via-dropdown-wrapper .via-dropdown .dropdown-container li{margin:0}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown{width:100%;display:flex}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown.open .dropdown-menu-items,.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown.open .dropdown-backdrop{display:block}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items{display:none;z-index:4;position:absolute;top:100%;list-style:none;margin:5px 0 38px;width:100%;border-radius:3px;padding:0;border:1px solid #EBEBEB;background:#fff;max-height:400px;overflow:auto}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item{display:flex;flex:9;justify-content:flex-start;align-items:center;padding:10px}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item:hover{background:#f7f8f9}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item.selected .item-check{display:block;color:var(--primary-color, #9a0000)}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item .text-no-wrap{white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.via-dropdown-wrapper .via-dropdown .dropdown-container li.dropdown .dropdown-menu-items .dropdown-menu-item .item-check{display:none;color:#bcbec0;text-align:right;flex:1}.via-dropdown-wrapper .via-dropdown .arrow-rotate{transform:rotate(-180deg)}.via-dropdown-wrapper .via-dropdown .arrow{transition:.2s ease}.via-dropdown-wrapper .via-dropdown .dropdown-selectedBox{width:100%;padding:10px}.via-dropdown-wrapper .via-dropdown .dropdown-selectedBox .text-box{white-space:nowrap;width:90%;text-overflow:ellipsis;overflow:hidden}.via-dropdown-wrapper .via-dropdown .dropdown-arrow{flex:1;text-align:center;height:46px;line-height:46px;position:absolute;right:10px;top:2px}.via-dropdown-wrapper .via-dropdown .dropdown-arrow i{font-size:24px;color:#999}.via-dropdown-wrapper .via-dropdown .dropdown-backdrop{width:100%;height:100%;display:none;position:fixed;left:0;top:0;z-index:2}\n"] }]
        }], ctorParameters: () => [], propDecorators: { itemSelectedChange: [{
                type: Output
            }], items: [{
                type: Input
            }], itemSelected: [{
                type: Input
            }], multiple: [{
                type: Input
            }], disabled: [{
                type: Input
            }], canSearch: [{
                type: Input
            }], placeholder: [{
                type: Input
            }], iconImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class DropdownModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DropdownModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: DropdownModule, declarations: [DropdownComponent,
            FullTextSearchPipe], imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule], exports: [DropdownComponent,
            FullTextSearchPipe] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DropdownModule, imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DropdownModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        DropdownComponent,
                        FullTextSearchPipe
                    ],
                    imports: [
                        FormsModule,
                        CommonModule,
                        ReactiveFormsModule
                    ],
                    exports: [
                        DropdownComponent,
                        FullTextSearchPipe
                    ]
                }]
        }] });

class DropdownItemDto {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

class DropdownItemValueDto extends DropdownItemDto {
    constructor(name, value, data) {
        super(name, value);
        this.data = data;
    }
}

/*
 * Public API Surface of ngx-via
 */

class Image360Component {
    constructor(ngbActiveModal) {
        this.ngbActiveModal = ngbActiveModal;
        this.imgPath = "";
        this.panoramaId = "";
        this.panoramaId = "panorama";
    }
    ngOnInit() {
        pannellum.viewer(this.panoramaId, {
            "type": "equirectangular",
            "autoLoad": true,
            "title": "360 Photo",
            "compass": true,
            "panorama": this.imgPath
        });
    }
    closeModal() {
        this.ngbActiveModal.close();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Component, deps: [{ token: i2.NgbActiveModal }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: Image360Component, selector: "via-360-image", inputs: { imgPath: "imgPath" }, ngImport: i0, template: "<div class=\"via-360-image-wrapper\">\n    <div (click)=\"closeModal()\" class=\"modal-360\">\n        <span aria-hidden=\"true\">&times;</span>\n    </div>\n    <div id=\"panorama\" class=\"panorama-wrapper\"></div>\n</div>\n", styles: [".via-360-image-wrapper{position:relative}.via-360-image-wrapper .panorama-wrapper{min-height:700px}.via-360-image-wrapper .modal-360{position:absolute;top:3px;right:3px;z-index:1;cursor:pointer;width:25px;height:25px;background:#fff;text-align:center;border-radius:4px;line-height:25px;border:1px solid #999;border-color:#0006}\n", ".pnlm-container{margin:0;padding:0;overflow:hidden;position:relative;cursor:default;width:100%;height:100%;font-family:Helvetica,Nimbus Sans L,Liberation Sans,Arial,sans-serif;background:#f4f4f4 url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"67\" height=\"100\" viewBox=\"0 0 67 100\">%0A<path stroke=\"%23ccc\" fill=\"none\" d=\"M33.5,50,0,63,33.5,75,67,63,33.5,50m-33.5-50,67,25m-0.5,0,0,75m-66.5-75,67-25m-33.5,75,0,25m0-100,0,50\"/>%0A</svg>%0A') repeat;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;-ms-user-select:none;user-select:none;outline:0;line-height:1.4;contain:content}.pnlm-container *{box-sizing:content-box}.pnlm-ui{position:absolute;width:100%;height:100%;z-index:1}.pnlm-grab{cursor:grab;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s6.38-6.73 4.64-8.24-3.47 1.01-3.47 1.01 3.61-5.72 1.41-6.49c-2.2-0.769-3.33 4.36-3.33 4.36s0.873-5.76-1.06-5.76-1.58 5.39-1.58 5.39-0.574-4.59-2.18-4.12c-1.61 0.468-0.572 5.51-0.572 5.51s-1.58-4.89-2.93-3.79c-1.35 1.11 0.258 5.25 0.572 6.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-grabbing{cursor:grabbing;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s5.07-5.29 3.77-6.74c-1.31-1.45-2.53 0.14-2.53 0.14s2.74-3.29 0.535-4.06c-2.2-0.769-2.52 1.3-2.52 1.3s0.81-2.13-1.12-2.13-1.52 1.77-1.52 1.77-0.261-1.59-1.87-1.12c-1.61 0.468-0.874 2.17-0.874 2.17s-0.651-1.55-2-0.445c-1.35 1.11-0.68 2.25-0.365 3.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-sprite{background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"26\" height=\"208\">%0A<circle fill-opacity=\".78\" cy=\"117\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle fill-opacity=\".78\" cy=\"143\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle cy=\"169\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cy=\"195\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cx=\"13\" cy=\"195\" r=\"2.5\"/>%0A<path d=\"m5 83v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-5 5v6h6v-6zm-5 5v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m13 110a7 7 0 0 0 -7 7 7 7 0 0 0 7 7 7 7 0 0 0 7 -7 7 7 0 0 0 -7 -7zm-1 3h2v2h-2zm0 3h2v5h-2z\"/>%0A<path d=\"m5 57v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-10 10v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m17 38v2h-8v-2z\"/>%0A<path d=\"m12 9v3h-3v2h3v3h2v-3h3v-2h-3v-3z\"/>%0A<path d=\"m13 136-6.125 6.125h4.375v7.875h3.5v-7.875h4.375z\"/>%0A<path d=\"m10.428 173.33v-5.77l5-2.89v5.77zm1-1.73 3-1.73-3.001-1.74z\"/>%0A</svg>%0A')}.pnlm-container:-moz-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-webkit-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-ms-fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-render-container{cursor:inherit;position:absolute;height:100%;width:100%}.pnlm-controls{margin-top:4px;background-color:#fff;border:1px solid #999;border-color:#0006;border-radius:3px;cursor:pointer;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-control:hover{background-color:#f8f8f8}.pnlm-controls-container{position:absolute;top:0;left:4px;z-index:1}.pnlm-zoom-controls{width:26px;height:52px}.pnlm-zoom-in{width:100%;height:50%;position:absolute;top:0;border-radius:3px 3px 0 0}.pnlm-zoom-out{width:100%;height:50%;position:absolute;bottom:0;background-position:0 -26px;border-top:1px solid #ddd;border-top-color:#0000001a;border-radius:0 0 3px 3px}.pnlm-fullscreen-toggle-button,.pnlm-orientation-button,.pnlm-hot-spot-debug-indicator{width:26px;height:26px}.pnlm-hot-spot-debug-indicator{position:absolute;top:50%;left:50%;width:26px;height:26px;margin:-13px 0 0 -13px;background-color:#ffffff80;border-radius:13px;display:none}.pnlm-orientation-button-inactive{background-position:0 -156px}.pnlm-orientation-button-active{background-position:0 -182px}.pnlm-fullscreen-toggle-button-inactive{background-position:0 -52px}.pnlm-fullscreen-toggle-button-active{background-position:0 -78px}.pnlm-panorama-info{position:absolute;bottom:4px;background-color:#000000b3;border-radius:0 3px 3px 0;padding-right:10px;color:#fff;text-align:left;display:none;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-title-box{position:relative;font-size:20px;display:table;padding-left:5px;margin-bottom:3px}.pnlm-author-box{position:relative;font-size:12px;display:table;padding-left:5px}.pnlm-load-box{position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:none;color:#fff}.pnlm-load-box p{margin:20px 0}.pnlm-lbox{position:absolute;top:50%;left:50%;width:20px;height:20px;margin:-10px 0 0 -10px;display:none}.pnlm-loading{animation-duration:1.5s;-webkit-animation-duration:1.5s;animation-name:pnlm-mv;-webkit-animation-name:pnlm-mv;animation-iteration-count:infinite;-webkit-animation-iteration-count:infinite;animation-timing-function:linear;-webkit-animation-timing-function:linear;height:10px;width:10px;background-color:#fff;position:relative}@keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}@-webkit-keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}.pnlm-load-button{position:absolute;top:50%;left:50%;width:200px;height:100px;margin:-50px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:table;color:#fff;cursor:pointer}.pnlm-load-button:hover{background-color:#000c}.pnlm-load-button p{display:table-cell;vertical-align:middle}.pnlm-info-box{font-size:15px;position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000;border-radius:3px;display:table;text-align:center;color:#fff;table-layout:fixed}.pnlm-info-box a,.pnlm-author-box a{color:#fff;word-wrap:break-word;overflow-wrap:break-word}.pnlm-info-box p{display:table-cell;vertical-align:middle;padding:0 5px}.pnlm-error-msg{display:none}.pnlm-about-msg{font-size:11px;line-height:11px;color:#fff;padding:5px 8px;background:#000000b3;border-radius:3px;position:absolute;top:50px;left:50px;display:none;opacity:0;-moz-transition:opacity .3s ease-in-out;-webkit-transition:opacity .3s ease-in-out;-o-transition:opacity .3s ease-in-out;-ms-transition:opacity .3s ease-in-out;transition:opacity .3s ease-in-out;z-index:1}.pnlm-about-msg a:link,.pnlm-about-msg a:visited{color:#fff}.pnlm-about-msg a:hover,.pnlm-about-msg a:active{color:#eee}.pnlm-hotspot-base{position:absolute;visibility:hidden;cursor:default;vertical-align:middle;top:0;z-index:1}.pnlm-hotspot{height:26px;width:26px;border-radius:13px}.pnlm-hotspot:hover{background-color:#fff3}.pnlm-hotspot.pnlm-info{background-position:0 -104px}.pnlm-hotspot.pnlm-scene{background-position:0 -130px}div.pnlm-tooltip span{visibility:hidden;position:absolute;border-radius:3px;background-color:#000000b3;color:#fff;text-align:center;max-width:200px;padding:5px 10px;margin-left:-220px;cursor:default}div.pnlm-tooltip:hover span{visibility:visible}div.pnlm-tooltip:hover span:after{content:\"\";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:rgba(0,0,0,.7) transparent transparent transparent;bottom:-20px;left:-10px;margin:0 50%}.pnlm-compass{position:absolute;width:50px;height:50px;right:4px;bottom:4px;border-radius:25px;background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"50\" width=\"50\">%0A<path d=\"m24.5078 6-3.2578 18h7.5l-3.25781-18h-0.984376zm-3.2578 20 3.2578 18h0.9844l3.2578-18h-7.5zm1.19531 0.9941h5.10938l-2.5547 14.1075-2.5547-14.1075z\"/>%0A</svg>%0A');cursor:default;display:none}.pnlm-world{position:absolute;left:50%;top:50%}.pnlm-face{position:absolute;-webkit-transform-origin:0 0 0;transform-origin:0 0 0}.pnlm-dragfix,.pnlm-preview-img{position:absolute;height:100%;width:100%}.pnlm-preview-img{background-size:cover;background-position:center}.pnlm-lbar{width:150px;margin:0 auto;border:#fff 1px solid;height:6px}.pnlm-lbar-fill{background:#fff;height:100%;width:0}.pnlm-lmsg{font-size:12px}.pnlm-fade-img{position:absolute;top:0;left:0}.pnlm-pointer{cursor:pointer}\n"], encapsulation: i0.ViewEncapsulation.None }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Component, decorators: [{
            type: Component,
            args: [{ selector: "via-360-image", encapsulation: ViewEncapsulation.None, template: "<div class=\"via-360-image-wrapper\">\n    <div (click)=\"closeModal()\" class=\"modal-360\">\n        <span aria-hidden=\"true\">&times;</span>\n    </div>\n    <div id=\"panorama\" class=\"panorama-wrapper\"></div>\n</div>\n", styles: [".via-360-image-wrapper{position:relative}.via-360-image-wrapper .panorama-wrapper{min-height:700px}.via-360-image-wrapper .modal-360{position:absolute;top:3px;right:3px;z-index:1;cursor:pointer;width:25px;height:25px;background:#fff;text-align:center;border-radius:4px;line-height:25px;border:1px solid #999;border-color:#0006}\n", ".pnlm-container{margin:0;padding:0;overflow:hidden;position:relative;cursor:default;width:100%;height:100%;font-family:Helvetica,Nimbus Sans L,Liberation Sans,Arial,sans-serif;background:#f4f4f4 url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"67\" height=\"100\" viewBox=\"0 0 67 100\">%0A<path stroke=\"%23ccc\" fill=\"none\" d=\"M33.5,50,0,63,33.5,75,67,63,33.5,50m-33.5-50,67,25m-0.5,0,0,75m-66.5-75,67-25m-33.5,75,0,25m0-100,0,50\"/>%0A</svg>%0A') repeat;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;-ms-user-select:none;user-select:none;outline:0;line-height:1.4;contain:content}.pnlm-container *{box-sizing:content-box}.pnlm-ui{position:absolute;width:100%;height:100%;z-index:1}.pnlm-grab{cursor:grab;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s6.38-6.73 4.64-8.24-3.47 1.01-3.47 1.01 3.61-5.72 1.41-6.49c-2.2-0.769-3.33 4.36-3.33 4.36s0.873-5.76-1.06-5.76-1.58 5.39-1.58 5.39-0.574-4.59-2.18-4.12c-1.61 0.468-0.572 5.51-0.572 5.51s-1.58-4.89-2.93-3.79c-1.35 1.11 0.258 5.25 0.572 6.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-grabbing{cursor:grabbing;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s5.07-5.29 3.77-6.74c-1.31-1.45-2.53 0.14-2.53 0.14s2.74-3.29 0.535-4.06c-2.2-0.769-2.52 1.3-2.52 1.3s0.81-2.13-1.12-2.13-1.52 1.77-1.52 1.77-0.261-1.59-1.87-1.12c-1.61 0.468-0.874 2.17-0.874 2.17s-0.651-1.55-2-0.445c-1.35 1.11-0.68 2.25-0.365 3.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-sprite{background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"26\" height=\"208\">%0A<circle fill-opacity=\".78\" cy=\"117\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle fill-opacity=\".78\" cy=\"143\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle cy=\"169\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cy=\"195\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cx=\"13\" cy=\"195\" r=\"2.5\"/>%0A<path d=\"m5 83v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-5 5v6h6v-6zm-5 5v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m13 110a7 7 0 0 0 -7 7 7 7 0 0 0 7 7 7 7 0 0 0 7 -7 7 7 0 0 0 -7 -7zm-1 3h2v2h-2zm0 3h2v5h-2z\"/>%0A<path d=\"m5 57v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-10 10v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m17 38v2h-8v-2z\"/>%0A<path d=\"m12 9v3h-3v2h3v3h2v-3h3v-2h-3v-3z\"/>%0A<path d=\"m13 136-6.125 6.125h4.375v7.875h3.5v-7.875h4.375z\"/>%0A<path d=\"m10.428 173.33v-5.77l5-2.89v5.77zm1-1.73 3-1.73-3.001-1.74z\"/>%0A</svg>%0A')}.pnlm-container:-moz-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-webkit-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-ms-fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-render-container{cursor:inherit;position:absolute;height:100%;width:100%}.pnlm-controls{margin-top:4px;background-color:#fff;border:1px solid #999;border-color:#0006;border-radius:3px;cursor:pointer;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-control:hover{background-color:#f8f8f8}.pnlm-controls-container{position:absolute;top:0;left:4px;z-index:1}.pnlm-zoom-controls{width:26px;height:52px}.pnlm-zoom-in{width:100%;height:50%;position:absolute;top:0;border-radius:3px 3px 0 0}.pnlm-zoom-out{width:100%;height:50%;position:absolute;bottom:0;background-position:0 -26px;border-top:1px solid #ddd;border-top-color:#0000001a;border-radius:0 0 3px 3px}.pnlm-fullscreen-toggle-button,.pnlm-orientation-button,.pnlm-hot-spot-debug-indicator{width:26px;height:26px}.pnlm-hot-spot-debug-indicator{position:absolute;top:50%;left:50%;width:26px;height:26px;margin:-13px 0 0 -13px;background-color:#ffffff80;border-radius:13px;display:none}.pnlm-orientation-button-inactive{background-position:0 -156px}.pnlm-orientation-button-active{background-position:0 -182px}.pnlm-fullscreen-toggle-button-inactive{background-position:0 -52px}.pnlm-fullscreen-toggle-button-active{background-position:0 -78px}.pnlm-panorama-info{position:absolute;bottom:4px;background-color:#000000b3;border-radius:0 3px 3px 0;padding-right:10px;color:#fff;text-align:left;display:none;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-title-box{position:relative;font-size:20px;display:table;padding-left:5px;margin-bottom:3px}.pnlm-author-box{position:relative;font-size:12px;display:table;padding-left:5px}.pnlm-load-box{position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:none;color:#fff}.pnlm-load-box p{margin:20px 0}.pnlm-lbox{position:absolute;top:50%;left:50%;width:20px;height:20px;margin:-10px 0 0 -10px;display:none}.pnlm-loading{animation-duration:1.5s;-webkit-animation-duration:1.5s;animation-name:pnlm-mv;-webkit-animation-name:pnlm-mv;animation-iteration-count:infinite;-webkit-animation-iteration-count:infinite;animation-timing-function:linear;-webkit-animation-timing-function:linear;height:10px;width:10px;background-color:#fff;position:relative}@keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}@-webkit-keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}.pnlm-load-button{position:absolute;top:50%;left:50%;width:200px;height:100px;margin:-50px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:table;color:#fff;cursor:pointer}.pnlm-load-button:hover{background-color:#000c}.pnlm-load-button p{display:table-cell;vertical-align:middle}.pnlm-info-box{font-size:15px;position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000;border-radius:3px;display:table;text-align:center;color:#fff;table-layout:fixed}.pnlm-info-box a,.pnlm-author-box a{color:#fff;word-wrap:break-word;overflow-wrap:break-word}.pnlm-info-box p{display:table-cell;vertical-align:middle;padding:0 5px}.pnlm-error-msg{display:none}.pnlm-about-msg{font-size:11px;line-height:11px;color:#fff;padding:5px 8px;background:#000000b3;border-radius:3px;position:absolute;top:50px;left:50px;display:none;opacity:0;-moz-transition:opacity .3s ease-in-out;-webkit-transition:opacity .3s ease-in-out;-o-transition:opacity .3s ease-in-out;-ms-transition:opacity .3s ease-in-out;transition:opacity .3s ease-in-out;z-index:1}.pnlm-about-msg a:link,.pnlm-about-msg a:visited{color:#fff}.pnlm-about-msg a:hover,.pnlm-about-msg a:active{color:#eee}.pnlm-hotspot-base{position:absolute;visibility:hidden;cursor:default;vertical-align:middle;top:0;z-index:1}.pnlm-hotspot{height:26px;width:26px;border-radius:13px}.pnlm-hotspot:hover{background-color:#fff3}.pnlm-hotspot.pnlm-info{background-position:0 -104px}.pnlm-hotspot.pnlm-scene{background-position:0 -130px}div.pnlm-tooltip span{visibility:hidden;position:absolute;border-radius:3px;background-color:#000000b3;color:#fff;text-align:center;max-width:200px;padding:5px 10px;margin-left:-220px;cursor:default}div.pnlm-tooltip:hover span{visibility:visible}div.pnlm-tooltip:hover span:after{content:\"\";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:rgba(0,0,0,.7) transparent transparent transparent;bottom:-20px;left:-10px;margin:0 50%}.pnlm-compass{position:absolute;width:50px;height:50px;right:4px;bottom:4px;border-radius:25px;background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"50\" width=\"50\">%0A<path d=\"m24.5078 6-3.2578 18h7.5l-3.25781-18h-0.984376zm-3.2578 20 3.2578 18h0.9844l3.2578-18h-7.5zm1.19531 0.9941h5.10938l-2.5547 14.1075-2.5547-14.1075z\"/>%0A</svg>%0A');cursor:default;display:none}.pnlm-world{position:absolute;left:50%;top:50%}.pnlm-face{position:absolute;-webkit-transform-origin:0 0 0;transform-origin:0 0 0}.pnlm-dragfix,.pnlm-preview-img{position:absolute;height:100%;width:100%}.pnlm-preview-img{background-size:cover;background-position:center}.pnlm-lbar{width:150px;margin:0 auto;border:#fff 1px solid;height:6px}.pnlm-lbar-fill{background:#fff;height:100%;width:0}.pnlm-lmsg{font-size:12px}.pnlm-fade-img{position:absolute;top:0;left:0}.pnlm-pointer{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i2.NgbActiveModal }], propDecorators: { imgPath: [{
                type: Input
            }] } });

class Image360Module {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, declarations: [Image360Component], imports: [NgbModalModule], exports: [Image360Component] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, imports: [NgbModalModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [Image360Component],
                    imports: [
                        NgbModalModule
                    ],
                    exports: [Image360Component]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class MatImage360Component {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.imgPath = "";
        this.panoramaId = "";
        this.panoramaId = "panorama";
    }
    ngOnInit() {
        pannellum.viewer(this.panoramaId, {
            "type": "equirectangular",
            "autoLoad": true,
            "title": "360 Photo",
            "compass": true,
            "panorama": this.data.imgPath
        });
    }
    closeModal() {
        // this.ngbActiveModal.close();
        this.dialogRef.close("close");
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Component, deps: [{ token: i1$2.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatImage360Component, selector: "via-mat-360-image", inputs: { imgPath: "imgPath" }, ngImport: i0, template: "<div class=\"via-360-image-wrapper\">\n    <div (click)=\"closeModal()\" class=\"modal-360\">\n        <span aria-hidden=\"true\">&times;</span>\n    </div>\n    <div id=\"panorama\" class=\"panorama-wrapper\"></div>\n</div>\n", styles: [".via-360-image-wrapper{position:relative}.via-360-image-wrapper .panorama-wrapper{min-height:700px}.via-360-image-wrapper .modal-360{position:absolute;top:3px;right:3px;z-index:1;cursor:pointer;width:25px;height:25px;background:#fff;text-align:center;border-radius:4px;line-height:25px;border:1px solid #999;border-color:#0006}\n", ".pnlm-container{margin:0;padding:0;overflow:hidden;position:relative;cursor:default;width:100%;height:100%;font-family:Helvetica,Nimbus Sans L,Liberation Sans,Arial,sans-serif;background:#f4f4f4 url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"67\" height=\"100\" viewBox=\"0 0 67 100\">%0A<path stroke=\"%23ccc\" fill=\"none\" d=\"M33.5,50,0,63,33.5,75,67,63,33.5,50m-33.5-50,67,25m-0.5,0,0,75m-66.5-75,67-25m-33.5,75,0,25m0-100,0,50\"/>%0A</svg>%0A') repeat;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;-ms-user-select:none;user-select:none;outline:0;line-height:1.4;contain:content}.pnlm-container *{box-sizing:content-box}.pnlm-ui{position:absolute;width:100%;height:100%;z-index:1}.pnlm-grab{cursor:grab;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s6.38-6.73 4.64-8.24-3.47 1.01-3.47 1.01 3.61-5.72 1.41-6.49c-2.2-0.769-3.33 4.36-3.33 4.36s0.873-5.76-1.06-5.76-1.58 5.39-1.58 5.39-0.574-4.59-2.18-4.12c-1.61 0.468-0.572 5.51-0.572 5.51s-1.58-4.89-2.93-3.79c-1.35 1.11 0.258 5.25 0.572 6.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-grabbing{cursor:grabbing;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s5.07-5.29 3.77-6.74c-1.31-1.45-2.53 0.14-2.53 0.14s2.74-3.29 0.535-4.06c-2.2-0.769-2.52 1.3-2.52 1.3s0.81-2.13-1.12-2.13-1.52 1.77-1.52 1.77-0.261-1.59-1.87-1.12c-1.61 0.468-0.874 2.17-0.874 2.17s-0.651-1.55-2-0.445c-1.35 1.11-0.68 2.25-0.365 3.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-sprite{background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"26\" height=\"208\">%0A<circle fill-opacity=\".78\" cy=\"117\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle fill-opacity=\".78\" cy=\"143\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle cy=\"169\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cy=\"195\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cx=\"13\" cy=\"195\" r=\"2.5\"/>%0A<path d=\"m5 83v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-5 5v6h6v-6zm-5 5v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m13 110a7 7 0 0 0 -7 7 7 7 0 0 0 7 7 7 7 0 0 0 7 -7 7 7 0 0 0 -7 -7zm-1 3h2v2h-2zm0 3h2v5h-2z\"/>%0A<path d=\"m5 57v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-10 10v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m17 38v2h-8v-2z\"/>%0A<path d=\"m12 9v3h-3v2h3v3h2v-3h3v-2h-3v-3z\"/>%0A<path d=\"m13 136-6.125 6.125h4.375v7.875h3.5v-7.875h4.375z\"/>%0A<path d=\"m10.428 173.33v-5.77l5-2.89v5.77zm1-1.73 3-1.73-3.001-1.74z\"/>%0A</svg>%0A')}.pnlm-container:-moz-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-webkit-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-ms-fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-render-container{cursor:inherit;position:absolute;height:100%;width:100%}.pnlm-controls{margin-top:4px;background-color:#fff;border:1px solid #999;border-color:#0006;border-radius:3px;cursor:pointer;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-control:hover{background-color:#f8f8f8}.pnlm-controls-container{position:absolute;top:0;left:4px;z-index:1}.pnlm-zoom-controls{width:26px;height:52px}.pnlm-zoom-in{width:100%;height:50%;position:absolute;top:0;border-radius:3px 3px 0 0}.pnlm-zoom-out{width:100%;height:50%;position:absolute;bottom:0;background-position:0 -26px;border-top:1px solid #ddd;border-top-color:#0000001a;border-radius:0 0 3px 3px}.pnlm-fullscreen-toggle-button,.pnlm-orientation-button,.pnlm-hot-spot-debug-indicator{width:26px;height:26px}.pnlm-hot-spot-debug-indicator{position:absolute;top:50%;left:50%;width:26px;height:26px;margin:-13px 0 0 -13px;background-color:#ffffff80;border-radius:13px;display:none}.pnlm-orientation-button-inactive{background-position:0 -156px}.pnlm-orientation-button-active{background-position:0 -182px}.pnlm-fullscreen-toggle-button-inactive{background-position:0 -52px}.pnlm-fullscreen-toggle-button-active{background-position:0 -78px}.pnlm-panorama-info{position:absolute;bottom:4px;background-color:#000000b3;border-radius:0 3px 3px 0;padding-right:10px;color:#fff;text-align:left;display:none;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-title-box{position:relative;font-size:20px;display:table;padding-left:5px;margin-bottom:3px}.pnlm-author-box{position:relative;font-size:12px;display:table;padding-left:5px}.pnlm-load-box{position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:none;color:#fff}.pnlm-load-box p{margin:20px 0}.pnlm-lbox{position:absolute;top:50%;left:50%;width:20px;height:20px;margin:-10px 0 0 -10px;display:none}.pnlm-loading{animation-duration:1.5s;-webkit-animation-duration:1.5s;animation-name:pnlm-mv;-webkit-animation-name:pnlm-mv;animation-iteration-count:infinite;-webkit-animation-iteration-count:infinite;animation-timing-function:linear;-webkit-animation-timing-function:linear;height:10px;width:10px;background-color:#fff;position:relative}@keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}@-webkit-keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}.pnlm-load-button{position:absolute;top:50%;left:50%;width:200px;height:100px;margin:-50px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:table;color:#fff;cursor:pointer}.pnlm-load-button:hover{background-color:#000c}.pnlm-load-button p{display:table-cell;vertical-align:middle}.pnlm-info-box{font-size:15px;position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000;border-radius:3px;display:table;text-align:center;color:#fff;table-layout:fixed}.pnlm-info-box a,.pnlm-author-box a{color:#fff;word-wrap:break-word;overflow-wrap:break-word}.pnlm-info-box p{display:table-cell;vertical-align:middle;padding:0 5px}.pnlm-error-msg{display:none}.pnlm-about-msg{font-size:11px;line-height:11px;color:#fff;padding:5px 8px;background:#000000b3;border-radius:3px;position:absolute;top:50px;left:50px;display:none;opacity:0;-moz-transition:opacity .3s ease-in-out;-webkit-transition:opacity .3s ease-in-out;-o-transition:opacity .3s ease-in-out;-ms-transition:opacity .3s ease-in-out;transition:opacity .3s ease-in-out;z-index:1}.pnlm-about-msg a:link,.pnlm-about-msg a:visited{color:#fff}.pnlm-about-msg a:hover,.pnlm-about-msg a:active{color:#eee}.pnlm-hotspot-base{position:absolute;visibility:hidden;cursor:default;vertical-align:middle;top:0;z-index:1}.pnlm-hotspot{height:26px;width:26px;border-radius:13px}.pnlm-hotspot:hover{background-color:#fff3}.pnlm-hotspot.pnlm-info{background-position:0 -104px}.pnlm-hotspot.pnlm-scene{background-position:0 -130px}div.pnlm-tooltip span{visibility:hidden;position:absolute;border-radius:3px;background-color:#000000b3;color:#fff;text-align:center;max-width:200px;padding:5px 10px;margin-left:-220px;cursor:default}div.pnlm-tooltip:hover span{visibility:visible}div.pnlm-tooltip:hover span:after{content:\"\";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:rgba(0,0,0,.7) transparent transparent transparent;bottom:-20px;left:-10px;margin:0 50%}.pnlm-compass{position:absolute;width:50px;height:50px;right:4px;bottom:4px;border-radius:25px;background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"50\" width=\"50\">%0A<path d=\"m24.5078 6-3.2578 18h7.5l-3.25781-18h-0.984376zm-3.2578 20 3.2578 18h0.9844l3.2578-18h-7.5zm1.19531 0.9941h5.10938l-2.5547 14.1075-2.5547-14.1075z\"/>%0A</svg>%0A');cursor:default;display:none}.pnlm-world{position:absolute;left:50%;top:50%}.pnlm-face{position:absolute;-webkit-transform-origin:0 0 0;transform-origin:0 0 0}.pnlm-dragfix,.pnlm-preview-img{position:absolute;height:100%;width:100%}.pnlm-preview-img{background-size:cover;background-position:center}.pnlm-lbar{width:150px;margin:0 auto;border:#fff 1px solid;height:6px}.pnlm-lbar-fill{background:#fff;height:100%;width:0}.pnlm-lmsg{font-size:12px}.pnlm-fade-img{position:absolute;top:0;left:0}.pnlm-pointer{cursor:pointer}\n"], encapsulation: i0.ViewEncapsulation.None }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Component, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-360-image", encapsulation: ViewEncapsulation.None, template: "<div class=\"via-360-image-wrapper\">\n    <div (click)=\"closeModal()\" class=\"modal-360\">\n        <span aria-hidden=\"true\">&times;</span>\n    </div>\n    <div id=\"panorama\" class=\"panorama-wrapper\"></div>\n</div>\n", styles: [".via-360-image-wrapper{position:relative}.via-360-image-wrapper .panorama-wrapper{min-height:700px}.via-360-image-wrapper .modal-360{position:absolute;top:3px;right:3px;z-index:1;cursor:pointer;width:25px;height:25px;background:#fff;text-align:center;border-radius:4px;line-height:25px;border:1px solid #999;border-color:#0006}\n", ".pnlm-container{margin:0;padding:0;overflow:hidden;position:relative;cursor:default;width:100%;height:100%;font-family:Helvetica,Nimbus Sans L,Liberation Sans,Arial,sans-serif;background:#f4f4f4 url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"67\" height=\"100\" viewBox=\"0 0 67 100\">%0A<path stroke=\"%23ccc\" fill=\"none\" d=\"M33.5,50,0,63,33.5,75,67,63,33.5,50m-33.5-50,67,25m-0.5,0,0,75m-66.5-75,67-25m-33.5,75,0,25m0-100,0,50\"/>%0A</svg>%0A') repeat;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;-ms-user-select:none;user-select:none;outline:0;line-height:1.4;contain:content}.pnlm-container *{box-sizing:content-box}.pnlm-ui{position:absolute;width:100%;height:100%;z-index:1}.pnlm-grab{cursor:grab;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s6.38-6.73 4.64-8.24-3.47 1.01-3.47 1.01 3.61-5.72 1.41-6.49c-2.2-0.769-3.33 4.36-3.33 4.36s0.873-5.76-1.06-5.76-1.58 5.39-1.58 5.39-0.574-4.59-2.18-4.12c-1.61 0.468-0.572 5.51-0.572 5.51s-1.58-4.89-2.93-3.79c-1.35 1.11 0.258 5.25 0.572 6.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-grabbing{cursor:grabbing;cursor:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"26\" width=\"26\">%0A<path stroke=\"%23000\" stroke-width=\"1px\" fill=\"%23fff\" d=\"m15.3 20.5s5.07-5.29 3.77-6.74c-1.31-1.45-2.53 0.14-2.53 0.14s2.74-3.29 0.535-4.06c-2.2-0.769-2.52 1.3-2.52 1.3s0.81-2.13-1.12-2.13-1.52 1.77-1.52 1.77-0.261-1.59-1.87-1.12c-1.61 0.468-0.874 2.17-0.874 2.17s-0.651-1.55-2-0.445c-1.35 1.11-0.68 2.25-0.365 3.62 0.836 2.43 2.03 2.94 2.17 5.55\"/>%0A</svg>%0A') 12 8,default}.pnlm-sprite{background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"26\" height=\"208\">%0A<circle fill-opacity=\".78\" cy=\"117\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle fill-opacity=\".78\" cy=\"143\" cx=\"13\" r=\"11\" fill=\"%23fff\"/>%0A<circle cy=\"169\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cy=\"195\" cx=\"13\" r=\"7\" fill=\"none\" stroke=\"%23000\" stroke-width=\"2\"/>%0A<circle cx=\"13\" cy=\"195\" r=\"2.5\"/>%0A<path d=\"m5 83v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-5 5v6h6v-6zm-5 5v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m13 110a7 7 0 0 0 -7 7 7 7 0 0 0 7 7 7 7 0 0 0 7 -7 7 7 0 0 0 -7 -7zm-1 3h2v2h-2zm0 3h2v5h-2z\"/>%0A<path d=\"m5 57v6h2v-4h4v-2zm10 0v2h4v4h2v-6zm-10 10v6h6v-2h-4v-4zm14 0v4h-4v2h6v-6z\"/>%0A<path d=\"m17 38v2h-8v-2z\"/>%0A<path d=\"m12 9v3h-3v2h3v3h2v-3h3v-2h-3v-3z\"/>%0A<path d=\"m13 136-6.125 6.125h4.375v7.875h3.5v-7.875h4.375z\"/>%0A<path d=\"m10.428 173.33v-5.77l5-2.89v5.77zm1-1.73 3-1.73-3.001-1.74z\"/>%0A</svg>%0A')}.pnlm-container:-moz-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-webkit-full-screen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:-ms-fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-container:fullscreen{height:100%!important;width:100%!important;position:static!important}.pnlm-render-container{cursor:inherit;position:absolute;height:100%;width:100%}.pnlm-controls{margin-top:4px;background-color:#fff;border:1px solid #999;border-color:#0006;border-radius:3px;cursor:pointer;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-control:hover{background-color:#f8f8f8}.pnlm-controls-container{position:absolute;top:0;left:4px;z-index:1}.pnlm-zoom-controls{width:26px;height:52px}.pnlm-zoom-in{width:100%;height:50%;position:absolute;top:0;border-radius:3px 3px 0 0}.pnlm-zoom-out{width:100%;height:50%;position:absolute;bottom:0;background-position:0 -26px;border-top:1px solid #ddd;border-top-color:#0000001a;border-radius:0 0 3px 3px}.pnlm-fullscreen-toggle-button,.pnlm-orientation-button,.pnlm-hot-spot-debug-indicator{width:26px;height:26px}.pnlm-hot-spot-debug-indicator{position:absolute;top:50%;left:50%;width:26px;height:26px;margin:-13px 0 0 -13px;background-color:#ffffff80;border-radius:13px;display:none}.pnlm-orientation-button-inactive{background-position:0 -156px}.pnlm-orientation-button-active{background-position:0 -182px}.pnlm-fullscreen-toggle-button-inactive{background-position:0 -52px}.pnlm-fullscreen-toggle-button-active{background-position:0 -78px}.pnlm-panorama-info{position:absolute;bottom:4px;background-color:#000000b3;border-radius:0 3px 3px 0;padding-right:10px;color:#fff;text-align:left;display:none;z-index:2;-webkit-transform:translateZ(9999px);transform:translateZ(9999px)}.pnlm-title-box{position:relative;font-size:20px;display:table;padding-left:5px;margin-bottom:3px}.pnlm-author-box{position:relative;font-size:12px;display:table;padding-left:5px}.pnlm-load-box{position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:none;color:#fff}.pnlm-load-box p{margin:20px 0}.pnlm-lbox{position:absolute;top:50%;left:50%;width:20px;height:20px;margin:-10px 0 0 -10px;display:none}.pnlm-loading{animation-duration:1.5s;-webkit-animation-duration:1.5s;animation-name:pnlm-mv;-webkit-animation-name:pnlm-mv;animation-iteration-count:infinite;-webkit-animation-iteration-count:infinite;animation-timing-function:linear;-webkit-animation-timing-function:linear;height:10px;width:10px;background-color:#fff;position:relative}@keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}@-webkit-keyframes pnlm-mv{0%{left:0;top:0}25%{left:10px;top:0}50%{left:10px;top:10px}75%{left:0;top:10px}to{left:0;top:0}}.pnlm-load-button{position:absolute;top:50%;left:50%;width:200px;height:100px;margin:-50px 0 0 -100px;background-color:#000000b3;border-radius:3px;text-align:center;font-size:20px;display:table;color:#fff;cursor:pointer}.pnlm-load-button:hover{background-color:#000c}.pnlm-load-button p{display:table-cell;vertical-align:middle}.pnlm-info-box{font-size:15px;position:absolute;top:50%;left:50%;width:200px;height:150px;margin:-75px 0 0 -100px;background-color:#000;border-radius:3px;display:table;text-align:center;color:#fff;table-layout:fixed}.pnlm-info-box a,.pnlm-author-box a{color:#fff;word-wrap:break-word;overflow-wrap:break-word}.pnlm-info-box p{display:table-cell;vertical-align:middle;padding:0 5px}.pnlm-error-msg{display:none}.pnlm-about-msg{font-size:11px;line-height:11px;color:#fff;padding:5px 8px;background:#000000b3;border-radius:3px;position:absolute;top:50px;left:50px;display:none;opacity:0;-moz-transition:opacity .3s ease-in-out;-webkit-transition:opacity .3s ease-in-out;-o-transition:opacity .3s ease-in-out;-ms-transition:opacity .3s ease-in-out;transition:opacity .3s ease-in-out;z-index:1}.pnlm-about-msg a:link,.pnlm-about-msg a:visited{color:#fff}.pnlm-about-msg a:hover,.pnlm-about-msg a:active{color:#eee}.pnlm-hotspot-base{position:absolute;visibility:hidden;cursor:default;vertical-align:middle;top:0;z-index:1}.pnlm-hotspot{height:26px;width:26px;border-radius:13px}.pnlm-hotspot:hover{background-color:#fff3}.pnlm-hotspot.pnlm-info{background-position:0 -104px}.pnlm-hotspot.pnlm-scene{background-position:0 -130px}div.pnlm-tooltip span{visibility:hidden;position:absolute;border-radius:3px;background-color:#000000b3;color:#fff;text-align:center;max-width:200px;padding:5px 10px;margin-left:-220px;cursor:default}div.pnlm-tooltip:hover span{visibility:visible}div.pnlm-tooltip:hover span:after{content:\"\";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:rgba(0,0,0,.7) transparent transparent transparent;bottom:-20px;left:-10px;margin:0 50%}.pnlm-compass{position:absolute;width:50px;height:50px;right:4px;bottom:4px;border-radius:25px;background-image:url('data:image/svg+xml,<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"50\" width=\"50\">%0A<path d=\"m24.5078 6-3.2578 18h7.5l-3.25781-18h-0.984376zm-3.2578 20 3.2578 18h0.9844l3.2578-18h-7.5zm1.19531 0.9941h5.10938l-2.5547 14.1075-2.5547-14.1075z\"/>%0A</svg>%0A');cursor:default;display:none}.pnlm-world{position:absolute;left:50%;top:50%}.pnlm-face{position:absolute;-webkit-transform-origin:0 0 0;transform-origin:0 0 0}.pnlm-dragfix,.pnlm-preview-img{position:absolute;height:100%;width:100%}.pnlm-preview-img{background-size:cover;background-position:center}.pnlm-lbar{width:150px;margin:0 auto;border:#fff 1px solid;height:6px}.pnlm-lbar-fill{background:#fff;height:100%;width:0}.pnlm-lmsg{font-size:12px}.pnlm-fade-img{position:absolute;top:0;left:0}.pnlm-pointer{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i1$2.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }], propDecorators: { imgPath: [{
                type: Input
            }] } });

class MatImage360Module {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, declarations: [MatImage360Component], imports: [MatDialogModule], exports: [MatImage360Component] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, imports: [MatDialogModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MatImage360Component],
                    imports: [
                        MatDialogModule
                    ],
                    exports: [MatImage360Component]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

const modalConfigs = {
    alert: "ALERT",
    confirm: "CONFIRM"
};

class ModalDto {
    constructor(modalTitle = "", yesBtnTitle = "Ok", noBtnTitle = "Close", type = "", translateParams, modalContent, modalLogo) {
        this.modalTitle = modalTitle;
        this.modalContent = modalContent;
        this.yesBtnTitle = yesBtnTitle;
        this.noBtnTitle = noBtnTitle;
        this.type = type;
        this.translateParams = translateParams;
        this.modalLogo = modalLogo;
    }
}

/*
 * Public API Surface of ngx-via
 */

class ModalComponent {
    constructor(activeModal) {
        this.activeModal = activeModal;
        this.modalOptions = new ModalDto("", "Ok", "Close", "", undefined);
        this.onConfirm = new EventEmitter();
        this.modalTypes = modalConfigs;
        //super();
    }
    close() {
        this.activeModal.dismiss("close");
    }
    ok() {
        this.activeModal.close("ok");
    }
    handleCloseByIcon() {
        if (this.modalOptions && this.modalOptions.type === modalConfigs.confirm) {
            this.close();
        }
        else {
            this.ok();
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ModalComponent, deps: [{ token: i2.NgbActiveModal }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ModalComponent, selector: "via-modal", inputs: { modalOptions: "modalOptions" }, outputs: { onConfirm: "onConfirm" }, ngImport: i0, template: "<div class=\"modal-wrapper\">\n    <div class=\"center-text\" *ngIf=\"modalOptions.modalLogo\" >\n        <img [src]=\"modalOptions.modalLogo\">\n    </div>\n    <div class=\"center-text\">\n        <h4 class=\"modal-title\">{{modalOptions.modalTitle}}</h4>\n    </div>\n    <!-- <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"handleCloseByIcon()\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div> -->\n    <div class=\"modal-body modal-body-text-p\">\n        <p [innerHtml]=\"modalOptions.modalContent\"></p>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn uppercase-text btn-primary modal-action-btn\" (click)=\"ok()\">\n            {{modalOptions.yesBtnTitle}}\n        </button>\n        <button type=\"button\" class=\"btn uppercase-text btn-light modal-action-btn\" (click)=\"close()\" *ngIf=\"modalOptions.type===modalTypes.confirm\">\n            {{modalOptions.noBtnTitle}}\n        </button>\n    </div>\n</div>", styles: [".modal-wrapper .center-text{text-align:center}.modal-wrapper .center-text img{margin-top:20px}.modal-wrapper .modal-title{font-weight:700}.modal-wrapper .modal-body{padding:0 45px;min-height:65px}.modal-wrapper .modal-body p{margin-top:24px}.modal-wrapper .modal-footer{border-top:0;display:block;margin-bottom:4px;text-align:center}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ModalComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-modal', template: "<div class=\"modal-wrapper\">\n    <div class=\"center-text\" *ngIf=\"modalOptions.modalLogo\" >\n        <img [src]=\"modalOptions.modalLogo\">\n    </div>\n    <div class=\"center-text\">\n        <h4 class=\"modal-title\">{{modalOptions.modalTitle}}</h4>\n    </div>\n    <!-- <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"handleCloseByIcon()\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div> -->\n    <div class=\"modal-body modal-body-text-p\">\n        <p [innerHtml]=\"modalOptions.modalContent\"></p>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn uppercase-text btn-primary modal-action-btn\" (click)=\"ok()\">\n            {{modalOptions.yesBtnTitle}}\n        </button>\n        <button type=\"button\" class=\"btn uppercase-text btn-light modal-action-btn\" (click)=\"close()\" *ngIf=\"modalOptions.type===modalTypes.confirm\">\n            {{modalOptions.noBtnTitle}}\n        </button>\n    </div>\n</div>", styles: [".modal-wrapper .center-text{text-align:center}.modal-wrapper .center-text img{margin-top:20px}.modal-wrapper .modal-title{font-weight:700}.modal-wrapper .modal-body{padding:0 45px;min-height:65px}.modal-wrapper .modal-body p{margin-top:24px}.modal-wrapper .modal-footer{border-top:0;display:block;margin-bottom:4px;text-align:center}\n"] }]
        }], ctorParameters: () => [{ type: i2.NgbActiveModal }], propDecorators: { modalOptions: [{
                type: Input
            }], onConfirm: [{
                type: Output
            }] } });

class ModalModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ModalModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: ModalModule, declarations: [ModalComponent], imports: [NgbAlertModule,
            CommonModule], exports: [ModalComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ModalModule, imports: [NgbAlertModule,
            CommonModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ModalModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ModalComponent],
                    imports: [
                        NgbAlertModule,
                        CommonModule
                    ],
                    exports: [ModalComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class MatModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalOptions = new ModalDto("", "Ok", "Close", "", undefined);
        this.onConfirm = new EventEmitter();
        this.modalTypes = modalConfigs;
    }
    close() {
        this.dialogRef.close("close");
    }
    ok() {
        this.dialogRef.close("ok");
    }
    handleCloseByIcon() {
        if (this.modalOptions && this.modalOptions.type === modalConfigs.confirm) {
            this.close();
        }
        else {
            this.ok();
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatModalComponent, deps: [{ token: i1$2.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatModalComponent, selector: "via-mat-modal", inputs: { modalOptions: "modalOptions" }, outputs: { onConfirm: "onConfirm" }, ngImport: i0, template: "<div class=\"modal-wrapper\">\n    <div class=\"center-text\" *ngIf=\"data.options.modalLogo\">\n        <img [src]=\"data.options.modalLogo\">\n    </div>\n    <div class=\"center-text\">\n        <h4 mat-dialog-title class=\"modal-title\">{{data.options.modalTitle}}</h4>\n    </div>\n    \n    <div class=\"modal-body modal-body-text-p\">\n        <div mat-dialog-content [innerHtml]=\"data.text\">{{data.text}}</div>\n    </div>\n    \n    <div class=\"modal-footer\">\n        <div mat-dialog-actions>\n            <button type=\"button\" mat-flat-button color=\"primary\" class=\"uppercase-text\" (click)=\"ok()\">\n                {{data.options.yesBtnTitle}}\n            </button>\n            <button type=\"button\" mat-flat-button class=\"uppercase-text flat-background\" (click)=\"close()\"\n                *ngIf=\"data.options.type===modalTypes.confirm\">\n                {{data.options.noBtnTitle}}\n            </button>\n        </div>\n    </div>\n</div>", styles: [".modal-wrapper .center-text{text-align:center}.modal-wrapper .center-text img{margin-top:20px}.modal-wrapper .modal-title{font-weight:700}.modal-wrapper .modal-body{padding:0 45px;min-height:65px}.modal-wrapper .modal-body p{margin-top:24px}.modal-wrapper .modal-footer{border-top:0;display:block;margin-bottom:4px;text-align:center}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i4.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", exportAs: ["matButton"] }, { kind: "directive", type: i1$2.MatDialogTitle, selector: "[mat-dialog-title], [matDialogTitle]", inputs: ["id"], exportAs: ["matDialogTitle"] }, { kind: "directive", type: i1$2.MatDialogActions, selector: "[mat-dialog-actions], mat-dialog-actions, [matDialogActions]", inputs: ["align"] }, { kind: "directive", type: i1$2.MatDialogContent, selector: "[mat-dialog-content], mat-dialog-content, [matDialogContent]" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatModalComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-mat-modal', template: "<div class=\"modal-wrapper\">\n    <div class=\"center-text\" *ngIf=\"data.options.modalLogo\">\n        <img [src]=\"data.options.modalLogo\">\n    </div>\n    <div class=\"center-text\">\n        <h4 mat-dialog-title class=\"modal-title\">{{data.options.modalTitle}}</h4>\n    </div>\n    \n    <div class=\"modal-body modal-body-text-p\">\n        <div mat-dialog-content [innerHtml]=\"data.text\">{{data.text}}</div>\n    </div>\n    \n    <div class=\"modal-footer\">\n        <div mat-dialog-actions>\n            <button type=\"button\" mat-flat-button color=\"primary\" class=\"uppercase-text\" (click)=\"ok()\">\n                {{data.options.yesBtnTitle}}\n            </button>\n            <button type=\"button\" mat-flat-button class=\"uppercase-text flat-background\" (click)=\"close()\"\n                *ngIf=\"data.options.type===modalTypes.confirm\">\n                {{data.options.noBtnTitle}}\n            </button>\n        </div>\n    </div>\n</div>", styles: [".modal-wrapper .center-text{text-align:center}.modal-wrapper .center-text img{margin-top:20px}.modal-wrapper .modal-title{font-weight:700}.modal-wrapper .modal-body{padding:0 45px;min-height:65px}.modal-wrapper .modal-body p{margin-top:24px}.modal-wrapper .modal-footer{border-top:0;display:block;margin-bottom:4px;text-align:center}\n"] }]
        }], ctorParameters: () => [{ type: i1$2.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }], propDecorators: { modalOptions: [{
                type: Input
            }], onConfirm: [{
                type: Output
            }] } });

class MatModalModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatModalModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatModalModule, declarations: [MatModalComponent], imports: [CommonModule,
            MatButtonModule,
            MatDialogModule], exports: [MatModalComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatModalModule, imports: [CommonModule,
            MatButtonModule,
            MatDialogModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatModalModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MatModalComponent],
                    imports: [
                        CommonModule,
                        MatButtonModule,
                        MatDialogModule,
                    ],
                    exports: [MatModalComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

const SORT = {
    ASC: "ASC",
    DESC: "DESC",
    NONE: ""
};
const CELL_TYPE = {
    STRING: "STRING",
    DATE_TIME: "DATE_TIME",
    STATUS: "STATUS",
    ACTION: "ACTION",
    IMAGE_THUMBNAIL: "IMAGE_THUMBNAIL",
    INPUT: "INPUT",
    CHECKBOX: "CHECKBOX"
};
const COLUMN_TYPE = {
    CHECKBOX: "CHECKBOX",
    DEFAULT: "DEFAULT",
};
const FILTER_ON_COLUMN_TYPE = {
    NONE: "NONE",
    MULTIPLE: "MULTIPLE",
    TEXT: "TEXT",
};
const ACTION_TYPE = {
    BUTTON: "BUTTON",
    LINK: "LINK",
    MODAL: "MODAL"
};
const TABLE_DATE_FORMAT = "hh:mma dd/MM/yyyy";

class TableActionDto {
    constructor(name = "", routeLink = "", css = "btn-primary", type = ACTION_TYPE.BUTTON, hided = false, disabled = false) {
        this._name = name;
        this._type = type;
        this._routeLink = routeLink;
        this._css = css;
        this._hided = hided;
        this.disabled = disabled;
    }
    /**
     * Getter hided
     * return {boolean}
     */
    get hided() {
        return this._hided;
    }
    /**
     * Setter hided
     * param {boolean} value
     */
    set hided(value) {
        this._hided = value;
    }
    get css() {
        return this._css;
    }
    set css(value) {
        this._css = value;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }
    get routeLink() {
        return this._routeLink;
    }
    set routeLink(value) {
        this._routeLink = value;
    }
}

class TableBtnActionDto {
    constructor(name = "", callbackFunc, disabled = false, iconUrl = undefined, isDropdownItem = false) {
        this._isDropdownItem = false;
        this._name = name;
        this._buttonAction = callbackFunc;
        this._disabled = disabled;
        this._iconUrl = iconUrl;
        this._isDropdownItem = isDropdownItem;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction() {
        return this._buttonAction;
    }
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl() {
        return this._iconUrl;
    }
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem() {
        return this._isDropdownItem;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value) {
        this._buttonAction = value;
    }
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value) {
        this._disabled = value;
    }
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value) {
        this._iconUrl = value;
    }
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value) {
        this._isDropdownItem = value;
    }
}

class TableCellDto {
    constructor(value = "", css = "", style = null, type = CELL_TYPE.STRING, routeLink = "", buttonActions = [], buttonDropdownActions = []) {
        this._value = value;
        this._css = css;
        this._type = type;
        this._routeLink = routeLink;
        this._buttonActions = buttonActions;
        this._buttonDropdownActions = buttonDropdownActions;
        this._style = style;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    get style() {
        return this._style;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter type
     * return {string}
     */
    get type() {
        return this._type;
    }
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink() {
        return this._routeLink;
    }
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions() {
        return this._buttonActions;
    }
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions() {
        return this._buttonDropdownActions;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    set style(value) {
        this._style = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter type
     * param {string} value
     */
    set type(value) {
        this._type = value;
    }
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value) {
        this._routeLink = value;
    }
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value) {
        this._buttonActions = value;
    }
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value) {
        this._buttonDropdownActions = value;
    }
}

class ColumnFilterBaseDto {
    constructor() {
        this.columnName = "";
        this.type = FILTER_ON_COLUMN_TYPE.NONE;
    }
}

class TableColumnDto {
    constructor(value = "", name = "", sortable = false, sort = SORT.NONE, css = "", hidden = false) {
        this._name = name;
        this._value = value;
        this._sort = sort;
        this._sortable = sortable;
        this._css = css;
        this._hidden = hidden;
        this.filter = new ColumnFilterBaseDto();
        this.type = COLUMN_TYPE.DEFAULT;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable() {
        return this._sortable;
    }
    /**
     * Getter sort
     * return {string}
     */
    get sort() {
        return this._sort;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value) {
        this._sortable = value;
    }
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value) {
        this._sort = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value) {
        this._hidden = value;
    }
}

class TableSortDto {
    constructor(sortColumnName = "", sortType = SORT.ASC) {
        this._sortColumnName = sortColumnName;
        this._sortType = sortType;
    }
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName() {
        return this._sortColumnName;
    }
    /**
     * Getter sortType
     * return {string}
     */
    get sortType() {
        return this._sortType;
    }
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value) {
        this._sortColumnName = value;
    }
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value) {
        this._sortType = value;
    }
}

class EntryPerpageDto {
    constructor(name = "5", value = 5, selected = false) {
        this._name = name;
        this._value = value;
        this._selected = selected;
    }
    get selected() {
        return this._selected;
    }
    set selected(value) {
        this._selected = value;
    }
    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
}

class TableFilterDto {
    constructor(hasFilter = true) {
        this.hasFilter = hasFilter;
    }
}

class TableOptionDto {
    constructor(tableColumns = [], tableRows = []) {
        this._tableColumns = tableColumns;
        this._tableRows = tableRows;
        this._totalItems = 0;
        this._itemsPerPage = 10;
        this._currentPage = 0;
        this._tableFilter = new TableFilterDto();
        this._tableActions = new Array();
        this._defaultSort = new TableSortDto();
        this._loading = false;
    }
    get loading() {
        return this._loading;
    }
    set loading(value) {
        this._loading = value;
    }
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns() {
        return this._tableColumns;
    }
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows() {
        return this._tableRows;
    }
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems() {
        return this._totalItems;
    }
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage() {
        return this._itemsPerPage;
    }
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage() {
        return this._currentPage;
    }
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter() {
        return this._tableFilter;
    }
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions() {
        return this._tableActions;
    }
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort() {
        return this._defaultSort;
    }
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value) {
        this._tableColumns = value;
    }
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value) {
        this._tableRows = value;
    }
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value) {
        this._totalItems = value;
    }
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value) {
        this._itemsPerPage = value;
    }
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value) {
        this._currentPage = value;
    }
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value) {
        this._tableFilter = value;
    }
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value) {
        this._tableActions = value;
    }
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value) {
        this._defaultSort = value;
    }
}

class TableRowDto {
    constructor(tableCells = [], css = "") {
        this._tableCells = tableCells;
        this._css = css;
        this._editAble = false;
        this._activeAble = false;
        this._deactiveAble = false;
        this._deleteAble = false;
    }
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells() {
        return this._tableCells;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble() {
        return this._editAble;
    }
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble() {
        return this._deleteAble;
    }
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble() {
        return this._deactiveAble;
    }
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble() {
        return this._activeAble;
    }
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value) {
        this._tableCells = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value) {
        this._editAble = value;
    }
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value) {
        this._deleteAble = value;
    }
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value) {
        this._deactiveAble = value;
    }
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value) {
        this._activeAble = value;
    }
}

class TableRowValueDto extends TableRowDto {
    constructor(tableCells = [], css = "", data) {
        super(tableCells, css);
        this._data = data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
}

class TableDndResultDto {
    constructor() {
        this.fromIndex = 0;
        this.toIndex = 0;
    }
}

class ItemDto {
    constructor() {
        this.title = "";
        this.value = "";
        this.checked = false;
    }
}

class ColumnFilterMultDto extends ColumnFilterBaseDto {
    constructor() {
        super();
        this.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.filterData = [];
    }
}

class ColumnFilterTextDto extends ColumnFilterBaseDto {
    constructor() {
        super();
        this.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.searchStr = "";
    }
}

class TableI18nDto {
    constructor() {
        this.entriesPerPage = "Entries per page";
        this.showing = "Showing";
        this.page = "Page";
        this.filterBy = "Filter by";
        this.to = "to";
        this.of = "of";
        this.noDataFound = "No data found";
        this.elements = "elements";
        this.filter = "Filter";
        this.reset = "Reset";
        this.search = "Search";
    }
}

/*
 * Public API Surface of ngx-via
 */

class DndSupport {
    constructor(table, swap) {
        this.currRow = null;
        this.dragElem = null;
        this.mouseDownX = 0;
        this.mouseDownY = 0;
        this.mouseX = 0;
        this.mouseY = 0;
        this.mouseDrag = false;
        this.dndResult = new TableDndResultDto();
        this.swap = (_dto) => { };
        this.swap = swap;
        this.table = table;
        this.tbody = this.table.querySelector('tbody');
    }
    init() {
        this._bindMouse();
    }
    _bindMouse() {
        this.table.addEventListener('mousedown', (event) => {
            if (event.button != 0)
                return;
            let target = this._getTargetRow(event.target);
            if (target) {
                this.currRow = target;
                this.dndResult.fromIndex = Array.from(this.tbody.children).indexOf(this.currRow);
                this._addDraggableRow(target);
                this.currRow.classList.add('dragging');
                let coords = this._getMouseCoords(event);
                this.mouseDownX = coords.x;
                this.mouseDownY = coords.y;
                this.mouseDrag = true;
            }
        });
        this.table.addEventListener('mousemove', (event) => {
            if (!this.mouseDrag)
                return;
            let coords = this._getMouseCoords(event);
            this.mouseX = coords.x - this.mouseDownX;
            this.mouseY = coords.y - this.mouseDownY;
            this._moveRow(this.mouseX, this.mouseY);
        });
        this.table.addEventListener('mouseup', (_event) => {
            if (!this.mouseDrag)
                return;
            this.currRow.classList.remove('dragging');
            this.table.removeChild(this.dragElem);
            this.dragElem = null;
            this.mouseDrag = false;
            this.dndResult.toIndex = Array.from(this.tbody.children).indexOf(this.currRow);
            if (this.dndResult.fromIndex !== this.dndResult.toIndex) {
                this.swap(this.dndResult);
            }
            this.dndResult = new TableDndResultDto();
        });
    }
    _swapRow(row, index) {
        let currIndex = Array.from(this.tbody.children).indexOf(this.currRow), row1 = currIndex > index ? this.currRow : row, row2 = currIndex > index ? row : this.currRow;
        if (currIndex != -1) {
            this.tbody.insertBefore(row1, row2);
        }
        else {
            console.log("remove drag element");
            this.currRow.classList.remove('dragging');
            this.table.removeChild(this.dragElem);
            this.dragElem = null;
        }
    }
    _moveRow(x, y) {
        this.dragElem.style.transform = "translate3d(" + x + "px, " + y + "px, 0)";
        let dPos = this.dragElem.getBoundingClientRect(), currStartY = dPos.y, currEndY = currStartY + dPos.height, rows = this._getRows();
        for (var i = 0; i < rows.length; i++) {
            let rowElem = rows[i], rowSize = rowElem.getBoundingClientRect(), rowStartY = rowSize.y, rowEndY = rowStartY + rowSize.height;
            if (this.currRow !== rowElem && this._isIntersecting(currStartY, currEndY, rowStartY, rowEndY)) {
                if (Math.abs(currStartY - rowStartY) < rowSize.height / 2) {
                    this._swapRow(rowElem, i);
                }
            }
        }
    }
    _addDraggableRow(target) {
        this.dragElem = target.cloneNode(true);
        this.dragElem.classList.add('draggable-drag');
        //@ts-ignore
        this.dragElem.style.height = this._getStyle(target, 'height');
        this.dragElem.style.width = this._getStyle(target, 'width');
        //@ts-ignore
        this.dragElem.style.backgroundColor = this._getStyle(target, 'backgroundColor');
        for (var i = 0; i < target.children.length; i++) {
            let oldTD = target.children[i], newTD = this.dragElem.children[i];
            newTD.style.width = this._getStyle(oldTD, 'width');
            newTD.style.height = this._getStyle(oldTD, 'height');
            newTD.style.padding = this._getStyle(oldTD, 'padding');
            newTD.style.margin = this._getStyle(oldTD, 'margin');
        }
        this.table.appendChild(this.dragElem);
        let tPos = target.getBoundingClientRect(), dPos = this.dragElem.getBoundingClientRect();
        this.dragElem.style.bottom = ((dPos.y - tPos.y) - tPos.height / 3) + "px";
        this.dragElem.style.left = "-1px";
        this.table.dispatchEvent(new MouseEvent('mousemove', { view: window, cancelable: true, bubbles: true }));
    }
    _getRows() {
        return this.table.querySelectorAll('tbody tr');
    }
    _getTargetRow(target) {
        if (target) {
            let elemName = target.tagName.toLowerCase();
            if (elemName == 'tr')
                return target;
            if (elemName == 'td')
                return target.closest('tr');
        }
        return undefined;
    }
    _getMouseCoords(event) {
        return {
            x: event.clientX,
            y: event.clientY
        };
    }
    _getStyle(target, styleName) {
        let compStyle = getComputedStyle(target), 
        //@ts-ignore
        style = compStyle[styleName];
        return style ? style : "none";
    }
    _isIntersecting(min0, max0, min1, max1) {
        return Math.max(min0, max0) >= Math.min(min1, max1)
            && Math.min(min0, max0) <= Math.max(min1, max1);
    }
}

class ColumnCheckboxComp {
    constructor() {
        this.selectColumn = new EventEmitter();
        this.column = new TableColumnDto();
        this.checked = false;
    }
    ngOnInit() {
        this.buildUI();
    }
    buildUI() {
        this.checked = this.column.value.toLowerCase() === JSON.stringify(true);
    }
    change() {
        this.column.value = JSON.stringify(this.checked);
        this.selectColumn.emit(this.column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnCheckboxComp, selector: "via-table-column-checkbox", inputs: { column: "column" }, outputs: { selectColumn: "selectColumn" }, ngImport: i0, template: "<span class=\"column-checkbox-wrapper\">\n    <input \n        type=\"checkbox\"\n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".column-checkbox-wrapper{padding:3px}.column-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1$1.CheckboxControlValueAccessor, selector: "input[type=checkbox][formControlName],input[type=checkbox][formControl],input[type=checkbox][ngModel]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnCheckboxComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-checkbox", template: "<span class=\"column-checkbox-wrapper\">\n    <input \n        type=\"checkbox\"\n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".column-checkbox-wrapper{padding:3px}.column-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { selectColumn: [{
                type: Output
            }], column: [{
                type: Input
            }] } });

class ColumnDefaultComp {
    constructor() {
        this.actionSort = new EventEmitter();
        this.column = new TableColumnDto();
    }
    ngOnInit() {
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.actionSort.emit(column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnDefaultComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnDefaultComp, selector: "via-table-column-default", inputs: { column: "column" }, outputs: { actionSort: "actionSort" }, ngImport: i0, template: "<span (click)=\"doSort(column)\">{{column.value}}</span>", styles: [""] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnDefaultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-default", template: "<span (click)=\"doSort(column)\">{{column.value}}</span>" }]
        }], ctorParameters: () => [], propDecorators: { actionSort: [{
                type: Output
            }], column: [{
                type: Input
            }] } });

let ColumnFilterMultComp$1 = class ColumnFilterMultComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
        this.filter = new ColumnFilterMultDto();
        this.badgeCount = undefined;
    }
    ngOnInit() {
        this.filter = this.column.filter;
    }
    doFilter() {
        let items = this.filter.filterData.filter(f => f.checked);
        this.badgeCount = items.length > 0 ? items.length : undefined;
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = items ? items : [];
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.badgeCount = undefined;
        this.filter.filterData.forEach(element => {
            element.checked = false;
        });
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = [];
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterMultComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnFilterMultComp, selector: "via-table-column-filter-mult", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter" }, ngImport: i0, template: "<ng-container>\n    <div ngbDropdown display=\"dynamic\" class=\"via-table-filter-mult-wrapper\">\n        <!-- <img ngbDropdownToggle src=\"./assets/icons/filter.svg\" /> -->\n        <i ngbDropdownToggle class=\"fas fa-filter\" aria-hidden=\"true\"></i>\n        <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n            <div class=\"table-dropdown-item\" *ngFor=\"let filterOption of filter.filterData; let i = index\">\n                <input class=\"styled-checkbox\" type=\"checkbox\" value=\"{{filterOption.value}}\" [(ngModel)]=\"filterOption.checked\">\n                <label>{{filterOption.title}}</label>\n            </div>\n            <div class=\"table-dropdown-btm-wrapper\">\n                <button class=\"table-dropdown-btn\" (click)=\"doFilter()\">{{i18n.filter}}</button>\n                <button class=\"table-dropdown-btn\" (click)=\"doReset()\">{{i18n.reset}}</button>\n            </div>\n        </div>\n    </div>\n</ng-container>", styles: [".via-table-filter-mult-wrapperi{font-size:14px}.via-table-filter-mult-wrapper .dropdown-togglei:after{content:none}.via-table-filter-mult-wrapper .dropdown-toggle:after{content:none}.via-table-filter-mult-wrapper .dropdown-menu{top:25px!important;-webkit-box-shadow:0px 3px 11px -1px rgb(214,214,214);-moz-box-shadow:0px 3px 11px -1px rgb(214,214,214);box-shadow:0 3px 11px -1px #d6d6d6;border:none;padding:10px 10px 0}.via-table-filter-mult-wrapper .dropdown-menu:after{position:absolute;top:-6px;right:3px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #ffffff;border-left:6px solid transparent;content:\"\"}.via-table-filter-mult-wrapper .dropdown-menu:before{position:absolute;top:-7px;right:2px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:#00000008;content:\"\"}.via-table-filter-mult-wrapper .dropdown-menu .styled-checkbox+label{position:relative;cursor:pointer;padding:0;text-transform:none;font-weight:200}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-search-input{height:40px;width:96%;border-radius:5px;border:1px solid #d8d8d8;margin-left:5px;padding-left:10px;margin-bottom:10px;margin-top:10px}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-btn-wrapper{text-align:center;display:inline-flex}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-btn-wrapper .table-dropdown-btn{min-width:100px;height:40px;background:var(--primary-color, #9a0000);color:#fff;border:none;border-radius:5px;text-align:center;margin:5px;cursor:pointer}.via-table-filter-mult-wrapper .dropdown-menu-right{right:0!important;left:auto!important}\n"], dependencies: [{ kind: "directive", type: i1$1.CheckboxControlValueAccessor, selector: "input[type=checkbox][formControlName],input[type=checkbox][formControl],input[type=checkbox][ngModel]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i2.NgbDropdown, selector: "[ngbDropdown]", inputs: ["autoClose", "dropdownClass", "open", "placement", "popperOptions", "container", "display"], outputs: ["openChange"], exportAs: ["ngbDropdown"] }, { kind: "directive", type: i2.NgbDropdownToggle, selector: "[ngbDropdownToggle]" }, { kind: "directive", type: i2.NgbDropdownMenu, selector: "[ngbDropdownMenu]" }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterMultComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-filter-mult", template: "<ng-container>\n    <div ngbDropdown display=\"dynamic\" class=\"via-table-filter-mult-wrapper\">\n        <!-- <img ngbDropdownToggle src=\"./assets/icons/filter.svg\" /> -->\n        <i ngbDropdownToggle class=\"fas fa-filter\" aria-hidden=\"true\"></i>\n        <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n            <div class=\"table-dropdown-item\" *ngFor=\"let filterOption of filter.filterData; let i = index\">\n                <input class=\"styled-checkbox\" type=\"checkbox\" value=\"{{filterOption.value}}\" [(ngModel)]=\"filterOption.checked\">\n                <label>{{filterOption.title}}</label>\n            </div>\n            <div class=\"table-dropdown-btm-wrapper\">\n                <button class=\"table-dropdown-btn\" (click)=\"doFilter()\">{{i18n.filter}}</button>\n                <button class=\"table-dropdown-btn\" (click)=\"doReset()\">{{i18n.reset}}</button>\n            </div>\n        </div>\n    </div>\n</ng-container>", styles: [".via-table-filter-mult-wrapperi{font-size:14px}.via-table-filter-mult-wrapper .dropdown-togglei:after{content:none}.via-table-filter-mult-wrapper .dropdown-toggle:after{content:none}.via-table-filter-mult-wrapper .dropdown-menu{top:25px!important;-webkit-box-shadow:0px 3px 11px -1px rgb(214,214,214);-moz-box-shadow:0px 3px 11px -1px rgb(214,214,214);box-shadow:0 3px 11px -1px #d6d6d6;border:none;padding:10px 10px 0}.via-table-filter-mult-wrapper .dropdown-menu:after{position:absolute;top:-6px;right:3px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #ffffff;border-left:6px solid transparent;content:\"\"}.via-table-filter-mult-wrapper .dropdown-menu:before{position:absolute;top:-7px;right:2px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:#00000008;content:\"\"}.via-table-filter-mult-wrapper .dropdown-menu .styled-checkbox+label{position:relative;cursor:pointer;padding:0;text-transform:none;font-weight:200}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-search-input{height:40px;width:96%;border-radius:5px;border:1px solid #d8d8d8;margin-left:5px;padding-left:10px;margin-bottom:10px;margin-top:10px}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-btn-wrapper{text-align:center;display:inline-flex}.via-table-filter-mult-wrapper .dropdown-menu .table-dropdown-btn-wrapper .table-dropdown-btn{min-width:100px;height:40px;background:var(--primary-color, #9a0000);color:#fff;border:none;border-radius:5px;text-align:center;margin:5px;cursor:pointer}.via-table-filter-mult-wrapper .dropdown-menu-right{right:0!important;left:auto!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { dropdownFilter: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

let ColumnFilterTextComp$1 = class ColumnFilterTextComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
        this.filter = new ColumnFilterTextDto();
        this.searchStr = "";
        this.badgeCount = undefined;
    }
    ngOnInit() {
        this.filter = this.column.filter;
        this.searchStr = this.filter.searchStr;
    }
    menuOpened() {
        this.searchStr = this.filter.searchStr;
    }
    doFilter() {
        this.filter.searchStr = this.searchStr;
        this.badgeCount = this.searchStr.length > 0 ? 1 : undefined;
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = this.filter.searchStr;
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.filter.searchStr = "";
        this.badgeCount = undefined;
        this.searchStr = "";
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = "";
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterTextComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnFilterTextComp, selector: "via-table-column-filter-text", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter" }, ngImport: i0, template: "<ng-container>\n    <div ngbDropdown display=\"dynamic\" class=\"via-table-filter-text-wrapper\">\n        <!-- <img ngbDropdownToggle class=\"search-icon\" src=\"./assets/icons/search-filter.svg\" /> -->\n        <div ngbDropdownToggle class=\"search-icon\">\n            <i class=\"fas fa-search\" aria-hidden=\"true\"></i>\n        </div>\n        <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n            <input class=\"table-dropdown-search-input\" [(ngModel)]=\"searchStr\" />\n            <div class=\"table-dropdown-btn-wrapper\">\n                <button class=\"table-dropdown-btn\" (click)=\"doFilter()\">{{i18n.search}}</button>\n                <button class=\"table-dropdown-btn\" (click)=\"doReset()\">{{i18n.reset}}</button>\n            </div>\n        </div>\n    </div>\n</ng-container>", styles: [".via-table-filter-text-wrapper .dropdown-togglei:after{content:none}.via-table-filter-text-wrapper .dropdown-toggle:after{content:none}.via-table-filter-text-wrapper .dropdown-menu{top:25px!important;-webkit-box-shadow:0px 3px 11px -1px rgb(214,214,214);-moz-box-shadow:0px 3px 11px -1px rgb(214,214,214);box-shadow:0 3px 11px -1px #d6d6d6;border:none;padding:10px 10px 0}.via-table-filter-text-wrapper .dropdown-menu:after{position:absolute;top:-6px;right:3px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #ffffff;border-left:6px solid transparent;content:\"\"}.via-table-filter-text-wrapper .dropdown-menu:before{position:absolute;top:-7px;right:2px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:#00000008;content:\"\"}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-search-input{height:40px;width:96%;border-radius:5px;border:1px solid #d8d8d8;margin-left:5px;padding-left:10px;margin-bottom:10px;margin-top:10px}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-btn-wrapper{text-align:center;display:inline-flex}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-btn-wrapper .table-dropdown-btn{min-width:100px;height:40px;background:var(--primary-color, #9a0000);color:#fff;border:none;border-radius:5px;text-align:center;margin:5px;cursor:pointer}.via-table-filter-text-wrapper .dropdown-menu-right{right:0!important;left:auto!important}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i2.NgbDropdown, selector: "[ngbDropdown]", inputs: ["autoClose", "dropdownClass", "open", "placement", "popperOptions", "container", "display"], outputs: ["openChange"], exportAs: ["ngbDropdown"] }, { kind: "directive", type: i2.NgbDropdownToggle, selector: "[ngbDropdownToggle]" }, { kind: "directive", type: i2.NgbDropdownMenu, selector: "[ngbDropdownMenu]" }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterTextComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-filter-text", template: "<ng-container>\n    <div ngbDropdown display=\"dynamic\" class=\"via-table-filter-text-wrapper\">\n        <!-- <img ngbDropdownToggle class=\"search-icon\" src=\"./assets/icons/search-filter.svg\" /> -->\n        <div ngbDropdownToggle class=\"search-icon\">\n            <i class=\"fas fa-search\" aria-hidden=\"true\"></i>\n        </div>\n        <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n            <input class=\"table-dropdown-search-input\" [(ngModel)]=\"searchStr\" />\n            <div class=\"table-dropdown-btn-wrapper\">\n                <button class=\"table-dropdown-btn\" (click)=\"doFilter()\">{{i18n.search}}</button>\n                <button class=\"table-dropdown-btn\" (click)=\"doReset()\">{{i18n.reset}}</button>\n            </div>\n        </div>\n    </div>\n</ng-container>", styles: [".via-table-filter-text-wrapper .dropdown-togglei:after{content:none}.via-table-filter-text-wrapper .dropdown-toggle:after{content:none}.via-table-filter-text-wrapper .dropdown-menu{top:25px!important;-webkit-box-shadow:0px 3px 11px -1px rgb(214,214,214);-moz-box-shadow:0px 3px 11px -1px rgb(214,214,214);box-shadow:0 3px 11px -1px #d6d6d6;border:none;padding:10px 10px 0}.via-table-filter-text-wrapper .dropdown-menu:after{position:absolute;top:-6px;right:3px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #ffffff;border-left:6px solid transparent;content:\"\"}.via-table-filter-text-wrapper .dropdown-menu:before{position:absolute;top:-7px;right:2px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:#00000008;content:\"\"}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-search-input{height:40px;width:96%;border-radius:5px;border:1px solid #d8d8d8;margin-left:5px;padding-left:10px;margin-bottom:10px;margin-top:10px}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-btn-wrapper{text-align:center;display:inline-flex}.via-table-filter-text-wrapper .dropdown-menu .table-dropdown-btn-wrapper .table-dropdown-btn{min-width:100px;height:40px;background:var(--primary-color, #9a0000);color:#fff;border:none;border-radius:5px;text-align:center;margin:5px;cursor:pointer}.via-table-filter-text-wrapper .dropdown-menu-right{right:0!important;left:auto!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { dropdownFilter: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

let ColumnComp$1 = class ColumnComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.sortColumn = new EventEmitter();
        this.selectColumn = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
        this.columType = COLUMN_TYPE;
    }
    ngOnInit() {
    }
    buildUI() {
        this.columnCheckbox?.buildUI();
    }
    doFilterOnColumn(req) {
        this.dropdownFilter.emit(req);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC;
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC;
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.sortColumn.emit(column);
    }
    doSelect(column) {
        this.selectColumn.emit(column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnComp, selector: "via-table-column", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter", sortColumn: "sortColumn", selectColumn: "selectColumn" }, viewQueries: [{ propertyName: "columnCheckbox", first: true, predicate: ["columnCheckbox"], descendants: true }], ngImport: i0, template: "<div class=\"header-container\">\n    <ng-container [ngSwitch]=\"column.type\">\n        \n        <ng-container *ngSwitchCase=columType.CHECKBOX>\n            <via-table-column-checkbox\n                #columnCheckbox\n                [column]=\"column\"\n                (selectColumn)=\"doSelect($event)\"\n            ></via-table-column-checkbox>\n        </ng-container>\n        \n        <ng-container *ngSwitchDefault>\n            <via-table-column-default\n                [column]=\"column\"\n                (actionSort)=\"doSort($event)\"\n            ></via-table-column-default>\n        </ng-container>\n    </ng-container>\n\n    <via-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-table-column-filter-mult>\n    \n    <via-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "component", type: ColumnDefaultComp, selector: "via-table-column-default", inputs: ["column"], outputs: ["actionSort"] }, { kind: "component", type: ColumnCheckboxComp, selector: "via-table-column-checkbox", inputs: ["column"], outputs: ["selectColumn"] }, { kind: "component", type: ColumnFilterMultComp$1, selector: "via-table-column-filter-mult", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }, { kind: "component", type: ColumnFilterTextComp$1, selector: "via-table-column-filter-text", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column", template: "<div class=\"header-container\">\n    <ng-container [ngSwitch]=\"column.type\">\n        \n        <ng-container *ngSwitchCase=columType.CHECKBOX>\n            <via-table-column-checkbox\n                #columnCheckbox\n                [column]=\"column\"\n                (selectColumn)=\"doSelect($event)\"\n            ></via-table-column-checkbox>\n        </ng-container>\n        \n        <ng-container *ngSwitchDefault>\n            <via-table-column-default\n                [column]=\"column\"\n                (actionSort)=\"doSort($event)\"\n            ></via-table-column-default>\n        </ng-container>\n    </ng-container>\n\n    <via-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-table-column-filter-mult>\n    \n    <via-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"] }]
        }], ctorParameters: () => [], propDecorators: { columnCheckbox: [{
                type: ViewChild,
                args: ["columnCheckbox"]
            }], dropdownFilter: [{
                type: Output
            }], sortColumn: [{
                type: Output
            }], selectColumn: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

let CellImageComp$1 = class CellImageComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellImageComp, selector: "via-table-cell-image", inputs: { cell: "cell" }, outputs: { openImagePreview: "openImagePreview" }, ngImport: i0, template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-image", template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], cell: [{
                type: Input
            }] } });

let CellInputComp$1 = class CellInputComp {
    constructor() {
        this.inputElement = undefined;
        this.rowUpdated = new EventEmitter();
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.inputMaxLenght = 255;
        // originalValue: string = "";
        this.enabled = false;
        this.oldValue = "";
    }
    ngOnInit() {
        // this.originalValue = clone(this.cell.value);
        this.oldValue = clone(this.cell.value);
    }
    focusInput() {
        setTimeout(() => {
            this.inputElement?.nativeElement.focus();
        }, 0);
    }
    keyUpEnter() {
        this.enabled = false;
        this.doUpdateInputEvent(true, this.cell, this.oldValue);
    }
    edit() {
        this.enabled = true;
        this.focusInput();
    }
    focusin() {
        this.oldValue = clone(this.cell.value);
    }
    focusout() {
        this.doUpdateInputEvent(this.enabled, this.cell, this.oldValue);
        this.enabled = false;
    }
    doUpdateInputEvent(canEdit, newValue, originalValue) {
        if (canEdit && newValue && newValue.value !== originalValue) {
            if (newValue.value && newValue.value.length <= this.inputMaxLenght) {
                this.cell.value = newValue.value;
                this.rowUpdated.emit(this.row);
            }
            else {
                newValue.value = originalValue;
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellInputComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellInputComp, selector: "via-table-cell-input", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, viewQueries: [{ propertyName: "inputElement", first: true, predicate: ["inputElement"], descendants: true }], ngImport: i0, template: "<span class=\"cell-input-wrapper\">\n    <!-- {{cell.value}} -->\n    <input \n        #inputElement\n        class=\"cell-input\" \n        [(ngModel)]=\"cell.value\"\n        [disabled]=\"!enabled\" \n        (focusin)=\"focusin()\"\n        (focusout)=\"focusout()\"\n        (keyup.enter)=\"keyUpEnter()\"\n        >\n    <!-- <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon>edit</mat-icon>\n    </button> -->\n    <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>\n        maxlength\n    </div>\n</span>\n<!-- \n\n<span *ngSwitchCase=cellType.INPUT class=\"cell-input-wrapper\">\n    {{cell.originalValue}}\n    <input class=\"cell-input\" [(ngModel)]=\"cell.value\" id=\"{{'input-' + i}}\"\n        [disabled]=\"!cell.enabled || !dndSupport\" (focusin)=\"cell.oldValue = cell.value\"\n        (focusout)=\"doUpdateInputEvent(row, cell.enabled, cell.value, cell.oldValue); cell.enabled = false\"\n        (keyup.enter)=\"cell.enabled = false; doUpdateInputEvent(row, true, cell, cell.oldValue)\">\n    <button (click)=\"cell.enabled = true; focusInput('input-' + i);\"\n        *ngIf=\"!cell.enabled && dndSupport\" class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>maxlength\n    </div>\n</span>\n\n -->", styles: [".cell-input-wrapper .cell-edit-icon{display:contents;cursor:pointer;width:35px!important;color:#5a5a5a;margin-top:0;margin-right:0;font-size:14px}.cell-input-wrapper .cell-edit-icon:hover{color:var(--primary-color, #9a0000)}.cell-input-wrapper .cell-input{border:0;background-color:inherit;width:calc(100% - 50px)}.cell-input-wrapper .cell-input:focus{border-bottom:1px solid rgb(0,119,255)}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellInputComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-input", template: "<span class=\"cell-input-wrapper\">\n    <!-- {{cell.value}} -->\n    <input \n        #inputElement\n        class=\"cell-input\" \n        [(ngModel)]=\"cell.value\"\n        [disabled]=\"!enabled\" \n        (focusin)=\"focusin()\"\n        (focusout)=\"focusout()\"\n        (keyup.enter)=\"keyUpEnter()\"\n        >\n    <!-- <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon>edit</mat-icon>\n    </button> -->\n    <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>\n        maxlength\n    </div>\n</span>\n<!-- \n\n<span *ngSwitchCase=cellType.INPUT class=\"cell-input-wrapper\">\n    {{cell.originalValue}}\n    <input class=\"cell-input\" [(ngModel)]=\"cell.value\" id=\"{{'input-' + i}}\"\n        [disabled]=\"!cell.enabled || !dndSupport\" (focusin)=\"cell.oldValue = cell.value\"\n        (focusout)=\"doUpdateInputEvent(row, cell.enabled, cell.value, cell.oldValue); cell.enabled = false\"\n        (keyup.enter)=\"cell.enabled = false; doUpdateInputEvent(row, true, cell, cell.oldValue)\">\n    <button (click)=\"cell.enabled = true; focusInput('input-' + i);\"\n        *ngIf=\"!cell.enabled && dndSupport\" class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>maxlength\n    </div>\n</span>\n\n -->", styles: [".cell-input-wrapper .cell-edit-icon{display:contents;cursor:pointer;width:35px!important;color:#5a5a5a;margin-top:0;margin-right:0;font-size:14px}.cell-input-wrapper .cell-edit-icon:hover{color:var(--primary-color, #9a0000)}.cell-input-wrapper .cell-input{border:0;background-color:inherit;width:calc(100% - 50px)}.cell-input-wrapper .cell-input:focus{border-bottom:1px solid rgb(0,119,255)}\n"] }]
        }], ctorParameters: () => [], propDecorators: { inputElement: [{
                type: ViewChild,
                args: ['inputElement']
            }], rowUpdated: [{
                type: Output
            }], row: [{
                type: Input
            }], cell: [{
                type: Input
            }] } });

let CellDefaultComp$1 = class CellDefaultComp {
    constructor(router) {
        this.router = router;
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, deps: [{ token: i1$3.Router }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDefaultComp, selector: "via-table-cell-default", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-default", template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i1$3.Router }], propDecorators: { cell: [{
                type: Input
            }] } });

let CellStatusComp$1 = class CellStatusComp {
    constructor() {
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellStatusComp, selector: "via-table-cell-status", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-status", template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });

let CellDateTimeComp$1 = class CellDateTimeComp {
    constructor() {
        this.cell = new TableCellDto();
        this.dateFormat = TABLE_DATE_FORMAT;
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDateTimeComp, selector: "via-table-cell-datetime", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>", styles: [""], dependencies: [{ kind: "pipe", type: i1.DatePipe, name: "date" }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-datetime", template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>" }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });

let CellActionComp$1 = class CellActionComp {
    constructor() {
        this.cell = new TableCellDto();
        this.row = new TableRowDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellActionComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellActionComp, selector: "via-table-cell-action", inputs: { cell: "cell", row: "row" }, ngImport: i0, template: "<span>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"*ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">\n                {{buttonAction.name}}\n            </button>\n        </span>\n    </span>\n\n    <div class=\"btn-group\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group table-dropdown\" ngbDropdown placement=\"bottom-right\" role=\"group\">\n            <button class=\"btn\" ngbDropdownToggle>\n                <i class=\"fas fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </button>\n            <div class=\"dropdown-menu\" ngbDropdownMenu>\n                <span *ngFor=\"let buttonAction of cell.buttonDropdownActions\">\n                    <button ngbDropdownItem [disabled]=\"buttonAction.disabled\"\n                        (click)=\"buttonAction.disabled!=true?buttonAction.buttonAction(row):''\">\n                        {{buttonAction.name}}\n                    </button>\n                </span>\n            </div>\n        </div>\n    </div>\n</span>\n\n\n<!-- <span *ngSwitchCase=cellType.ACTION>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"\n                *ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">{{buttonAction.name\n            }}</button>\n        </span>\n    </span>\n    <div class=\"btn-group mr-3\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group table-dropdown\" ngbDropdown placement=\"bottom-right\" role=\"group\" aria-label=\"Button group with nested dropdown\">\n            <button class=\"btn\" ngbDropdownToggle><i class=\"fas fa-ellipsis-v\"\n                        aria-hidden=\"true\"></i></button>\n            <div class=\"dropdown-menu\" ngbDropdownMenu>\n                <span *ngFor=\"let buttonAction of cell.buttonDropdownActions\">\n                    <button ngbDropdownItem [disabled]=\"buttonAction.disabled\"\n                        (click)=\"buttonAction.disabled!=true?buttonAction.buttonAction(row):''\"> {{buttonAction.name\n                    }}</button>\n                </span>\n            </div>\n        </div>\n    </div>\n</span> -->", styles: ["button.btn-action{padding:10px 0!important;width:80px;margin-top:5px;text-transform:uppercase;margin-right:5px}button.btn{padding:12px 25px!important;font-size:12px;height:40px;border-radius:3px}button.btn-primary{background:var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000)}button.btn-primary:hover{background-color:var(--primary-color, #b82d2d);border-color:var(--primary-color, #b82d2d)}button.btn-light{background:#f7f8f9;border-color:#f7f8f9;color:#5a5a5a}button.btn-light:hover{background-color:#efefef;border-color:#efefef}.icon-btn{width:50px!important}.icon-btn img{width:20px}.dropdown-toggle:after{display:none}.dropdown-menu{padding:10px}.dropdown-menu .dropdown-item{height:46px}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i2.NgbDropdown, selector: "[ngbDropdown]", inputs: ["autoClose", "dropdownClass", "open", "placement", "popperOptions", "container", "display"], outputs: ["openChange"], exportAs: ["ngbDropdown"] }, { kind: "directive", type: i2.NgbDropdownToggle, selector: "[ngbDropdownToggle]" }, { kind: "directive", type: i2.NgbDropdownMenu, selector: "[ngbDropdownMenu]" }, { kind: "directive", type: i2.NgbDropdownItem, selector: "[ngbDropdownItem]", inputs: ["tabindex", "disabled"] }, { kind: "directive", type: i2.NgbDropdownButtonItem, selector: "button[ngbDropdownItem]" }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellActionComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-action", template: "<span>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"*ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">\n                {{buttonAction.name}}\n            </button>\n        </span>\n    </span>\n\n    <div class=\"btn-group\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group table-dropdown\" ngbDropdown placement=\"bottom-right\" role=\"group\">\n            <button class=\"btn\" ngbDropdownToggle>\n                <i class=\"fas fa-ellipsis-v\" aria-hidden=\"true\"></i>\n            </button>\n            <div class=\"dropdown-menu\" ngbDropdownMenu>\n                <span *ngFor=\"let buttonAction of cell.buttonDropdownActions\">\n                    <button ngbDropdownItem [disabled]=\"buttonAction.disabled\"\n                        (click)=\"buttonAction.disabled!=true?buttonAction.buttonAction(row):''\">\n                        {{buttonAction.name}}\n                    </button>\n                </span>\n            </div>\n        </div>\n    </div>\n</span>\n\n\n<!-- <span *ngSwitchCase=cellType.ACTION>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"\n                *ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">{{buttonAction.name\n            }}</button>\n        </span>\n    </span>\n    <div class=\"btn-group mr-3\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group table-dropdown\" ngbDropdown placement=\"bottom-right\" role=\"group\" aria-label=\"Button group with nested dropdown\">\n            <button class=\"btn\" ngbDropdownToggle><i class=\"fas fa-ellipsis-v\"\n                        aria-hidden=\"true\"></i></button>\n            <div class=\"dropdown-menu\" ngbDropdownMenu>\n                <span *ngFor=\"let buttonAction of cell.buttonDropdownActions\">\n                    <button ngbDropdownItem [disabled]=\"buttonAction.disabled\"\n                        (click)=\"buttonAction.disabled!=true?buttonAction.buttonAction(row):''\"> {{buttonAction.name\n                    }}</button>\n                </span>\n            </div>\n        </div>\n    </div>\n</span> -->", styles: ["button.btn-action{padding:10px 0!important;width:80px;margin-top:5px;text-transform:uppercase;margin-right:5px}button.btn{padding:12px 25px!important;font-size:12px;height:40px;border-radius:3px}button.btn-primary{background:var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000)}button.btn-primary:hover{background-color:var(--primary-color, #b82d2d);border-color:var(--primary-color, #b82d2d)}button.btn-light{background:#f7f8f9;border-color:#f7f8f9;color:#5a5a5a}button.btn-light:hover{background-color:#efefef;border-color:#efefef}.icon-btn{width:50px!important}.icon-btn img{width:20px}.dropdown-toggle:after{display:none}.dropdown-menu{padding:10px}.dropdown-menu .dropdown-item{height:46px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }], row: [{
                type: Input
            }] } });

let CellCheckboxComp$1 = class CellCheckboxComp {
    constructor() {
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.rowUpdated = new EventEmitter();
        this.checked = false;
    }
    ngOnInit() {
        this.buildUI();
    }
    buildUI() {
        this.checked = this.cell.value.toLowerCase() === JSON.stringify(true);
    }
    change() {
        this.cell.value = JSON.stringify(this.checked);
        this.rowUpdated.emit(this.row);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellCheckboxComp, selector: "via-table-cell-checkbox", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, ngImport: i0, template: "<span class=\"cell-checkbox-wrapper\">\n    <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1$1.CheckboxControlValueAccessor, selector: "input[type=checkbox][formControlName],input[type=checkbox][formControl],input[type=checkbox][ngModel]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-checkbox", template: "<span class=\"cell-checkbox-wrapper\">\n    <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { row: [{
                type: Input
            }], cell: [{
                type: Input
            }], rowUpdated: [{
                type: Output
            }] } });

class FilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FilterPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe }); }
    static { this.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: FilterPipe, name: "filterColumn" }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FilterPipe, decorators: [{
            type: Pipe,
            args: [{ name: 'filterColumn' }]
        }] });

class TableComponent {
    constructor() {
        this.tableOption = new TableOptionDto();
        this.dndSupport = false;
        this.iconFilterImg = undefined;
        this.visibleActionButtons = true;
        this.i18n = new TableI18nDto();
        this.sortColumn = new EventEmitter();
        this.selectColumn = new EventEmitter();
        this.pageChange = new EventEmitter();
        this.entryPageChange = new EventEmitter();
        this.actionButton = new EventEmitter();
        this.filterOnColumn = new EventEmitter();
        this.openImagePreview = new EventEmitter();
        this.dndUpdated = new EventEmitter();
        this.rowUpdated = new EventEmitter();
        this.isShowFilter = false;
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        this.cellType = CELL_TYPE;
        this.entryPerpages = new Array();
    }
    ngOnInit() {
        this.entryPerpages.push(new EntryPerpageDto("5", 5), new EntryPerpageDto("10", 10), new EntryPerpageDto("25", 25), new EntryPerpageDto("100", 100));
        this.entryPerpages.map(e => {
            e.selected = false;
            if (e.value === this.tableOption.itemsPerPage) {
                e.selected = true;
            }
        });
    }
    ngAfterViewInit() {
        if (this.dndSupport && this.tableView) {
            setTimeout(() => {
                let dnd = new DndSupport(this.tableView.nativeElement, (dto) => {
                    this.doUpdateIndex(dto);
                });
                dnd.init();
            }, 0);
        }
    }
    getSibling(ui) {
        let text = "";
        if (ui.nextElementSibling && ui.nextElementSibling.cells) {
            text = ui.nextElementSibling.cells[1].innerText;
        }
        else if (ui.previousElementSibling && ui.previousElementSibling.cells) {
            text = ui.previousElementSibling.cells[1].innerText;
        }
        return text;
    }
    startItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) - this.tableOption.itemsPerPage + 1;
    }
    endItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) < this.tableOption.totalItems ? (this.tableOption.currentPage * this.tableOption.itemsPerPage) : this.tableOption.totalItems;
    }
    noDataFound() {
        return this.tableOption.tableRows.length === 0 && this.tableOption.currentPage <= 1;
    }
    hideFilter() {
        this.isShowFilter = false;
    }
    showFilter() {
        this.isShowFilter = true;
    }
    toggleFilter() {
        this.isShowFilter = !this.isShowFilter;
    }
    doAction(actionBtn) {
        this.actionButton?.emit(actionBtn);
    }
    doPageChange() {
        let currentOffset = (this.tableOption.currentPage - 1) * this.tableOption.itemsPerPage;
        this.pageChange?.emit(currentOffset);
    }
    doEntryPage(value) {
        let v = parseInt(value);
        this.tableOption.itemsPerPage = v;
        this.tableOption.currentPage = 1;
        this.entryPageChange?.emit(v);
    }
    doSort(column) {
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        for (let i = 0; i < this.tableOption.tableColumns.length; i++) {
            let col = this.tableOption.tableColumns[i];
            if (col.name === column.name) {
                if (col.sort === SORT.ASC) {
                    col.sort = SORT.DESC;
                }
                else if (col.sort === SORT.DESC) {
                    col.sort = SORT.NONE;
                    col.sort = this.tableOption.defaultSort.sortColumnName === col.name ? SORT.ASC : SORT.NONE;
                    this.currentSortColumnName = this.tableOption.defaultSort.sortColumnName;
                    this.currentSortType = this.tableOption.defaultSort.sortType;
                }
                else if (col.sort === SORT.NONE) {
                    col.sort = SORT.ASC;
                }
            }
            else {
                col.sort = SORT.NONE;
            }
        }
        this.sortColumn?.emit(column);
    }
    doSelect(column) {
        let index = this.tableOption.tableColumns.indexOf(column);
        this.tableOption.tableRows.forEach(r => {
            if (r.tableCells.length > index) {
                r.tableCells[index].value = column.value;
            }
        });
        this.cellCheckboxs?.forEach(c => {
            c.buildUI();
        });
        this.selectColumn?.emit(column);
    }
    doFilterOnColumn(req) {
        this.filterOnColumn?.emit(req);
    }
    doUpdateIndex(dragPostionObj) {
        this.dndUpdated?.emit(dragPostionObj);
    }
    editRow(row) {
        row.tableCells.forEach((c, index) => {
            if (c.type === CELL_TYPE.CHECKBOX) {
                let column = this.tableOption.tableColumns[index];
                let columnElement = this.columnElements?.get(index);
                if (column.type === COLUMN_TYPE.CHECKBOX) {
                    if (c.value === JSON.stringify(false)) {
                        column.value = c.value;
                    }
                    else {
                        let flag = true;
                        this.tableOption.tableRows.forEach(r => {
                            if (r.tableCells[index].type === CELL_TYPE.CHECKBOX
                                && r.tableCells[index].value === JSON.stringify(false)) {
                                flag = false;
                            }
                        });
                        column.value = JSON.stringify(flag);
                    }
                }
                columnElement?.buildUI();
            }
        });
        this.rowUpdated?.emit(row);
    }
    doOpenPreview(imgCode) {
        this.openImagePreview?.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: TableComponent, selector: "via-table", inputs: { tableOption: "tableOption", dndSupport: "dndSupport", iconFilterImg: "iconFilterImg", visibleActionButtons: "visibleActionButtons", i18n: "i18n" }, outputs: { sortColumn: "sortColumn", selectColumn: "selectColumn", pageChange: "pageChange", entryPageChange: "entryPageChange", actionButton: "actionButton", filterOnColumn: "filterOnColumn", openImagePreview: "openImagePreview", dndUpdated: "dndUpdated", rowUpdated: "rowUpdated" }, viewQueries: [{ propertyName: "tableView", first: true, predicate: ["tableView"], descendants: true }, { propertyName: "cellCheckboxs", predicate: ["cellCheckboxs"], descendants: true }, { propertyName: "columnElements", predicate: ["columnElements"], descendants: true }], ngImport: i0, template: "<div  class=\"win-table-container\">\n    <div class=\"win-table-wrapper\">\n        <div class=\"funtion-wrapper\">\n            <div class=\"action-wrapper\">\n                <div class=\"action-container\">\n                    <div class=\"filter\" *ngIf=\"tableOption.tableFilter.hasFilter\">\n                        <ul class=\"filter-container\">\n                            <li class=\"dropdown\" [ngClass]=\"isShowFilter ? 'open' : ''\">\n                                <span class=\"dropdown-content\">\n                                    <img *ngIf=\"iconFilterImg\" (click)=\"toggleFilter()\" [src]=\"iconFilterImg\" />\n\n                                    <i *ngIf=\"!iconFilterImg\" (click)=\"toggleFilter()\" class=\"fas fa-filter\" aria-hidden=\"true\"></i>\n\n                                    <span (click)=\"toggleFilter()\"\n                                        class=\"filter-text\">{{i18n.filterBy}}</span>\n                                </span>\n                                <div (click)=\"hideFilter()\" class=\"filter-mask\"></div>\n                                <ul class=\"dropdown-menu-items z-depth-1\">\n                                    <li>\n                                        <ng-content select=\"[filter]\"></ng-content>\n                                    </li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"action-button\">\n                        <div *ngIf=\"tableOption.tableActions.length > 0 && visibleActionButtons\">\n                            <span *ngFor=\"let btn of tableOption.tableActions\">\n                                <button *ngIf=\"!btn.hided\" [disabled]=btn.disabled type=\"button\" class=\"btn uppercase-text m-l-10\"\n                                    [ngClass]=\"btn.css\" (click)=\"doAction(btn)\">\n                                    {{btn.name}}\n                                </button>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"paging\">\n                <div class=\"paging-container\">\n                    <div class=\"number-per-page\">\n                        <select #entry (change)=\"doEntryPage(entry.value)\">\n                            <option *ngFor=\"let entry of entryPerpages\" [attr.value]=\"entry.value\"\n                                [selected]=\"entry.selected\">{{entry.name}}</option>\n                        </select>\n                        <span> {{i18n.entriesPerPage }}</span>\n                    </div>\n                    <div class=\"paging-wrapper\" *ngIf=\"!noDataFound()\">\n                        <span class=\"paging-title\">{{i18n.page }} </span>\n                        <ngb-pagination size=\"sm\" [directionLinks]=\"false\" [collectionSize]=\"tableOption.totalItems\" [rotate]=\"true\" [maxSize]=\"5\" [pageSize]=\"tableOption.itemsPerPage\" [(page)]=\"tableOption.currentPage\" (pageChange)=\"doPageChange()\"></ngb-pagination>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <table #tableView [ngClass]=\"dndSupport ? 'draggable-table' : ''\">\n            <thead *ngIf=\"!noDataFound()\">\n                <tr class=\"heading\">\n                    <th class='handle' *ngFor=\"let column of tableOption.tableColumns | filterColumn\" [ngClass]=\"[column.sortable ? 'sortable' : '', column.sort, column.css]\">\n                        <via-table-column \n                            #columnElements \n                            [column]=\"column\" \n                            [i18n]=\"i18n\" \n                            (sortColumn)=\"doSort($event)\"\n                            (selectColumn)=\"doSelect($event)\"\n                            (dropdownFilter)=\"doFilterOnColumn($event)\">\n                        </via-table-column>\n                    </th>\n                </tr>\n            </thead>\n            <tbody id=\"sortable\">\n                <ng-container *ngFor=\"let row of tableOption.tableRows; let i = index\">\n                    <tr [ngClass]=\"row.css\">\n                        <td class=\"tablerow\" *ngFor=\"let cell of row.tableCells\" [ngClass]=\"cell.type + ' ' + cell.css \" [ngStyle]=\"cell.style\">\n                            <!-- <via-table-cell \n                                [dndSupport]=\"dndSupport\" \n                                [row]=\"row\"\n                                [cell]=\"cell\"\n                                (openImagePreview)=\"doOpenPreview($event)\"\n                                (rowUpdated)=\"editRow($event)\">\n                            </via-table-cell> -->\n                            <ng-container [ngSwitch]=\"cell.type\">\n                                <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n                                    <via-table-cell-image \n                                        [cell]=\"cell\"\n                                        (openImagePreview)=\"doOpenPreview($event)\">\n                                    </via-table-cell-image>\n                                </ng-container>\n\n                                <ng-container *ngSwitchCase=cellType.INPUT>\n                                    <via-table-cell-input\n                                        [row]=\"row\"\n                                        [cell]=\"cell\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-table-cell-input>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.STATUS>\n                                    <via-table-cell-status \n                                        [cell]=\"cell\">\n                                    </via-table-cell-status>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.DATE_TIME>\n                                    <via-table-cell-datetime \n                                        [cell]=\"cell\">\n                                    </via-table-cell-datetime>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.ACTION>\n                                    <via-table-cell-action \n                                        [cell]=\"cell\"\n                                        [row]=\"row\">\n                                    </via-table-cell-action>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.CHECKBOX>\n                                    <via-table-cell-checkbox\n                                        #cellCheckboxs\n                                        [cell]=\"cell\"\n                                        [row]=\"row\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-table-cell-checkbox>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchDefault>\n                                    <via-table-cell-default \n                                        [cell]=\"cell\">\n                                    </via-table-cell-default>\n                                </ng-container>\n                            </ng-container>\n                        </td>\n                    </tr>\n                </ng-container>\n\n            </tbody>\n            <tfoot *ngIf=\"noDataFound()\">\n                <tr>\n                    <td [attr.colspan]=\"tableOption.tableColumns.length\">\n                        {{i18n.noDataFound}}\n                    </td>\n                </tr>\n            </tfoot>\n        </table>\n    </div>\n\n    <div class=\"win-table-footer\" *ngIf=\"!noDataFound()\">\n        {{i18n.showing }}\n        <span class=\"number\">{{startItemNumber()}}</span> {{i18n.to}}\n        <span class=\"number\">{{endItemNumber()}}</span> {{i18n.of}}\n        <span class=\"total\">{{tableOption.totalItems}} {{i18n.elements }}</span>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"tableOption.loading\">\n        <div class=\"overlay\"></div>\n        <div class=\"spinner\">\n            <div class=\"spinner-circle lds-dual-ring\"></div>\n        </div>\n    </div>\n</div>", styles: [".win-table-container{position:relative;flex-direction:column;align-items:center;background-color:#fff}.win-table-container .ui-sortable-helper{display:inline-table}.win-table-container .win-table-wrapper{padding-bottom:40px;display:flex;flex-direction:column;align-items:center;width:100%;font-size:14px;border:1px solid #ebedf8}.win-table-container .win-table-wrapper .funtion-wrapper{width:100%}.win-table-container .win-table-wrapper .action-wrapper{width:100%;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .action-wrapper .action-container{margin-top:20px;width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter{flex:3;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .filter-mask{position:fixed;inset:0;display:none}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .open .filter-mask{display:block}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container li{margin:0;padding:0}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown{width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content{display:flex;flex-direction:row;justify-content:flex-start;align-items:center}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content img{cursor:pointer;width:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content i{cursor:pointer;font-size:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content span{cursor:pointer}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content .filter-text{color:var(--primary-color, #9a0000);font-weight:700}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items{display:none;position:absolute;top:100%;list-style:none;width:250%;border-radius:3px;padding:10px 10px 30px;border:1px solid #EBEBEB;background:#fff;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items li{z-index:1;width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown.open .dropdown-menu-items{display:flex}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button{flex:7;display:flex;justify-content:flex-end;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button button{margin-left:10px}.win-table-container .win-table-wrapper .paging{width:100%;border-bottom:1px solid #F0F0F0;display:flex;align-items:center;justify-content:center;padding:10px 0}.win-table-container .win-table-wrapper .paging .paging-container{width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .number-per-page{flex:1;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .number-per-page select{padding:5px;background:#fff;border:1px solid #F0F0F0;width:100px;max-width:120px;border-radius:5px}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper{flex:1;display:flex;justify-content:flex-end;align-items:baseline;color:#aab2c0}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper .paging-title{margin-right:10px}.win-table-container .win-table-wrapper table{width:95%}.win-table-container .win-table-wrapper table.draggable-table{-webkit-user-select:none;user-select:none;background:#fff;cursor:grab}.win-table-container .win-table-wrapper table.draggable-table .draggable-drag{position:absolute;width:100%;border:1px solid var(--primary-lighter-color, #f1f1f1);z-index:10;cursor:grabbing;opacity:1}.win-table-container .win-table-wrapper table thead tr th{text-transform:uppercase;color:#b4bac6;font-size:12px;text-align:left;padding:30px 15px 10px 0}.win-table-container .win-table-wrapper table thead tr th.sortable{cursor:pointer}.win-table-container .win-table-wrapper table thead tr th.ACTION{text-align:center!important}.win-table-container .win-table-wrapper table tbody tr.dragging{background-color:var(--primary-lighter-color, rgba(128, 128, 128, .3))}.win-table-container .win-table-wrapper table tbody tr{min-height:40px;vertical-align:text-top;background-color:#fff}.win-table-container .win-table-wrapper table tbody tr:hover{background-color:#f5f5f5}.win-table-container .win-table-wrapper table tbody tr td{font-size:14px;color:#b4bac6;padding:10px 0}.win-table-container .win-table-wrapper table tbody tr td.bold{color:#354052;font-weight:700}.win-table-container .win-table-wrapper table tbody tr td.ellipsis-td{white-space:nowrap;max-width:200px;padding:0 10px!important;overflow:hidden;text-overflow:ellipsis}.win-table-container .win-table-wrapper table tbody tr td.wordwrap-td{display:block;max-width:200px;word-break:break-word}.win-table-container .win-table-wrapper table tbody tr td.CHECKBOX{vertical-align:middle}.win-table-container .win-table-wrapper table tfoot{text-align:center;font-size:24px;font-weight:700}.win-table-container .win-table-footer{width:95%;margin-top:20px;color:#aab2c0;font-size:15px}.win-table-container .win-table-footer .number{font-weight:700;color:#000}.win-table-container .win-table-footer .total{font-weight:700}.win-table-container .spinner-wrapper{position:absolute;width:100%;height:100%;top:0;z-index:9999}.win-table-container .spinner-wrapper .lds-dual-ring{display:inline-block;width:30px;height:30px}.win-table-container .spinner-wrapper .lds-dual-ring:after{content:\" \";display:block;width:30px;height:30px;margin:1px;border-radius:50%;border:1px solid var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000) transparent var(--primary-color, #9a0000) transparent;animation:lds-dual-ring 1.2s linear infinite}@keyframes lds-dual-ring{0%{transform:rotate(0)}to{transform:rotate(360deg)}}.win-table-container .spinner-wrapper .overlay{width:100%;height:100%;background:#000;opacity:.2;z-index:1;position:absolute}.win-table-container .spinner-wrapper .spinner{top:calc(50% - 30px);left:calc(50% - 30px);z-index:2;position:absolute}\n"], dependencies: [{ kind: "directive", type: i1$1.NgSelectOption, selector: "option", inputs: ["ngValue", "value"] }, { kind: "directive", type: i1$1.ɵNgSelectMultipleOption, selector: "option", inputs: ["ngValue", "value"] }, { kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "component", type: i2.NgbPagination, selector: "ngb-pagination", inputs: ["disabled", "boundaryLinks", "directionLinks", "ellipses", "rotate", "collectionSize", "maxSize", "page", "pageSize", "size"], outputs: ["pageChange"] }, { kind: "component", type: ColumnComp$1, selector: "via-table-column", inputs: ["column", "i18n"], outputs: ["dropdownFilter", "sortColumn", "selectColumn"] }, { kind: "component", type: CellImageComp$1, selector: "via-table-cell-image", inputs: ["cell"], outputs: ["openImagePreview"] }, { kind: "component", type: CellInputComp$1, selector: "via-table-cell-input", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "component", type: CellDefaultComp$1, selector: "via-table-cell-default", inputs: ["cell"] }, { kind: "component", type: CellStatusComp$1, selector: "via-table-cell-status", inputs: ["cell"] }, { kind: "component", type: CellDateTimeComp$1, selector: "via-table-cell-datetime", inputs: ["cell"] }, { kind: "component", type: CellActionComp$1, selector: "via-table-cell-action", inputs: ["cell", "row"] }, { kind: "component", type: CellCheckboxComp$1, selector: "via-table-cell-checkbox", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "pipe", type: FilterPipe, name: "filterColumn" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-table", template: "<div  class=\"win-table-container\">\n    <div class=\"win-table-wrapper\">\n        <div class=\"funtion-wrapper\">\n            <div class=\"action-wrapper\">\n                <div class=\"action-container\">\n                    <div class=\"filter\" *ngIf=\"tableOption.tableFilter.hasFilter\">\n                        <ul class=\"filter-container\">\n                            <li class=\"dropdown\" [ngClass]=\"isShowFilter ? 'open' : ''\">\n                                <span class=\"dropdown-content\">\n                                    <img *ngIf=\"iconFilterImg\" (click)=\"toggleFilter()\" [src]=\"iconFilterImg\" />\n\n                                    <i *ngIf=\"!iconFilterImg\" (click)=\"toggleFilter()\" class=\"fas fa-filter\" aria-hidden=\"true\"></i>\n\n                                    <span (click)=\"toggleFilter()\"\n                                        class=\"filter-text\">{{i18n.filterBy}}</span>\n                                </span>\n                                <div (click)=\"hideFilter()\" class=\"filter-mask\"></div>\n                                <ul class=\"dropdown-menu-items z-depth-1\">\n                                    <li>\n                                        <ng-content select=\"[filter]\"></ng-content>\n                                    </li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"action-button\">\n                        <div *ngIf=\"tableOption.tableActions.length > 0 && visibleActionButtons\">\n                            <span *ngFor=\"let btn of tableOption.tableActions\">\n                                <button *ngIf=\"!btn.hided\" [disabled]=btn.disabled type=\"button\" class=\"btn uppercase-text m-l-10\"\n                                    [ngClass]=\"btn.css\" (click)=\"doAction(btn)\">\n                                    {{btn.name}}\n                                </button>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"paging\">\n                <div class=\"paging-container\">\n                    <div class=\"number-per-page\">\n                        <select #entry (change)=\"doEntryPage(entry.value)\">\n                            <option *ngFor=\"let entry of entryPerpages\" [attr.value]=\"entry.value\"\n                                [selected]=\"entry.selected\">{{entry.name}}</option>\n                        </select>\n                        <span> {{i18n.entriesPerPage }}</span>\n                    </div>\n                    <div class=\"paging-wrapper\" *ngIf=\"!noDataFound()\">\n                        <span class=\"paging-title\">{{i18n.page }} </span>\n                        <ngb-pagination size=\"sm\" [directionLinks]=\"false\" [collectionSize]=\"tableOption.totalItems\" [rotate]=\"true\" [maxSize]=\"5\" [pageSize]=\"tableOption.itemsPerPage\" [(page)]=\"tableOption.currentPage\" (pageChange)=\"doPageChange()\"></ngb-pagination>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <table #tableView [ngClass]=\"dndSupport ? 'draggable-table' : ''\">\n            <thead *ngIf=\"!noDataFound()\">\n                <tr class=\"heading\">\n                    <th class='handle' *ngFor=\"let column of tableOption.tableColumns | filterColumn\" [ngClass]=\"[column.sortable ? 'sortable' : '', column.sort, column.css]\">\n                        <via-table-column \n                            #columnElements \n                            [column]=\"column\" \n                            [i18n]=\"i18n\" \n                            (sortColumn)=\"doSort($event)\"\n                            (selectColumn)=\"doSelect($event)\"\n                            (dropdownFilter)=\"doFilterOnColumn($event)\">\n                        </via-table-column>\n                    </th>\n                </tr>\n            </thead>\n            <tbody id=\"sortable\">\n                <ng-container *ngFor=\"let row of tableOption.tableRows; let i = index\">\n                    <tr [ngClass]=\"row.css\">\n                        <td class=\"tablerow\" *ngFor=\"let cell of row.tableCells\" [ngClass]=\"cell.type + ' ' + cell.css \" [ngStyle]=\"cell.style\">\n                            <!-- <via-table-cell \n                                [dndSupport]=\"dndSupport\" \n                                [row]=\"row\"\n                                [cell]=\"cell\"\n                                (openImagePreview)=\"doOpenPreview($event)\"\n                                (rowUpdated)=\"editRow($event)\">\n                            </via-table-cell> -->\n                            <ng-container [ngSwitch]=\"cell.type\">\n                                <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n                                    <via-table-cell-image \n                                        [cell]=\"cell\"\n                                        (openImagePreview)=\"doOpenPreview($event)\">\n                                    </via-table-cell-image>\n                                </ng-container>\n\n                                <ng-container *ngSwitchCase=cellType.INPUT>\n                                    <via-table-cell-input\n                                        [row]=\"row\"\n                                        [cell]=\"cell\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-table-cell-input>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.STATUS>\n                                    <via-table-cell-status \n                                        [cell]=\"cell\">\n                                    </via-table-cell-status>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.DATE_TIME>\n                                    <via-table-cell-datetime \n                                        [cell]=\"cell\">\n                                    </via-table-cell-datetime>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.ACTION>\n                                    <via-table-cell-action \n                                        [cell]=\"cell\"\n                                        [row]=\"row\">\n                                    </via-table-cell-action>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.CHECKBOX>\n                                    <via-table-cell-checkbox\n                                        #cellCheckboxs\n                                        [cell]=\"cell\"\n                                        [row]=\"row\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-table-cell-checkbox>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchDefault>\n                                    <via-table-cell-default \n                                        [cell]=\"cell\">\n                                    </via-table-cell-default>\n                                </ng-container>\n                            </ng-container>\n                        </td>\n                    </tr>\n                </ng-container>\n\n            </tbody>\n            <tfoot *ngIf=\"noDataFound()\">\n                <tr>\n                    <td [attr.colspan]=\"tableOption.tableColumns.length\">\n                        {{i18n.noDataFound}}\n                    </td>\n                </tr>\n            </tfoot>\n        </table>\n    </div>\n\n    <div class=\"win-table-footer\" *ngIf=\"!noDataFound()\">\n        {{i18n.showing }}\n        <span class=\"number\">{{startItemNumber()}}</span> {{i18n.to}}\n        <span class=\"number\">{{endItemNumber()}}</span> {{i18n.of}}\n        <span class=\"total\">{{tableOption.totalItems}} {{i18n.elements }}</span>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"tableOption.loading\">\n        <div class=\"overlay\"></div>\n        <div class=\"spinner\">\n            <div class=\"spinner-circle lds-dual-ring\"></div>\n        </div>\n    </div>\n</div>", styles: [".win-table-container{position:relative;flex-direction:column;align-items:center;background-color:#fff}.win-table-container .ui-sortable-helper{display:inline-table}.win-table-container .win-table-wrapper{padding-bottom:40px;display:flex;flex-direction:column;align-items:center;width:100%;font-size:14px;border:1px solid #ebedf8}.win-table-container .win-table-wrapper .funtion-wrapper{width:100%}.win-table-container .win-table-wrapper .action-wrapper{width:100%;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .action-wrapper .action-container{margin-top:20px;width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter{flex:3;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .filter-mask{position:fixed;inset:0;display:none}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .open .filter-mask{display:block}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container li{margin:0;padding:0}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown{width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content{display:flex;flex-direction:row;justify-content:flex-start;align-items:center}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content img{cursor:pointer;width:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content i{cursor:pointer;font-size:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content span{cursor:pointer}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content .filter-text{color:var(--primary-color, #9a0000);font-weight:700}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items{display:none;position:absolute;top:100%;list-style:none;width:250%;border-radius:3px;padding:10px 10px 30px;border:1px solid #EBEBEB;background:#fff;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items li{z-index:1;width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown.open .dropdown-menu-items{display:flex}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button{flex:7;display:flex;justify-content:flex-end;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button button{margin-left:10px}.win-table-container .win-table-wrapper .paging{width:100%;border-bottom:1px solid #F0F0F0;display:flex;align-items:center;justify-content:center;padding:10px 0}.win-table-container .win-table-wrapper .paging .paging-container{width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .number-per-page{flex:1;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .number-per-page select{padding:5px;background:#fff;border:1px solid #F0F0F0;width:100px;max-width:120px;border-radius:5px}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper{flex:1;display:flex;justify-content:flex-end;align-items:baseline;color:#aab2c0}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper .paging-title{margin-right:10px}.win-table-container .win-table-wrapper table{width:95%}.win-table-container .win-table-wrapper table.draggable-table{-webkit-user-select:none;user-select:none;background:#fff;cursor:grab}.win-table-container .win-table-wrapper table.draggable-table .draggable-drag{position:absolute;width:100%;border:1px solid var(--primary-lighter-color, #f1f1f1);z-index:10;cursor:grabbing;opacity:1}.win-table-container .win-table-wrapper table thead tr th{text-transform:uppercase;color:#b4bac6;font-size:12px;text-align:left;padding:30px 15px 10px 0}.win-table-container .win-table-wrapper table thead tr th.sortable{cursor:pointer}.win-table-container .win-table-wrapper table thead tr th.ACTION{text-align:center!important}.win-table-container .win-table-wrapper table tbody tr.dragging{background-color:var(--primary-lighter-color, rgba(128, 128, 128, .3))}.win-table-container .win-table-wrapper table tbody tr{min-height:40px;vertical-align:text-top;background-color:#fff}.win-table-container .win-table-wrapper table tbody tr:hover{background-color:#f5f5f5}.win-table-container .win-table-wrapper table tbody tr td{font-size:14px;color:#b4bac6;padding:10px 0}.win-table-container .win-table-wrapper table tbody tr td.bold{color:#354052;font-weight:700}.win-table-container .win-table-wrapper table tbody tr td.ellipsis-td{white-space:nowrap;max-width:200px;padding:0 10px!important;overflow:hidden;text-overflow:ellipsis}.win-table-container .win-table-wrapper table tbody tr td.wordwrap-td{display:block;max-width:200px;word-break:break-word}.win-table-container .win-table-wrapper table tbody tr td.CHECKBOX{vertical-align:middle}.win-table-container .win-table-wrapper table tfoot{text-align:center;font-size:24px;font-weight:700}.win-table-container .win-table-footer{width:95%;margin-top:20px;color:#aab2c0;font-size:15px}.win-table-container .win-table-footer .number{font-weight:700;color:#000}.win-table-container .win-table-footer .total{font-weight:700}.win-table-container .spinner-wrapper{position:absolute;width:100%;height:100%;top:0;z-index:9999}.win-table-container .spinner-wrapper .lds-dual-ring{display:inline-block;width:30px;height:30px}.win-table-container .spinner-wrapper .lds-dual-ring:after{content:\" \";display:block;width:30px;height:30px;margin:1px;border-radius:50%;border:1px solid var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000) transparent var(--primary-color, #9a0000) transparent;animation:lds-dual-ring 1.2s linear infinite}@keyframes lds-dual-ring{0%{transform:rotate(0)}to{transform:rotate(360deg)}}.win-table-container .spinner-wrapper .overlay{width:100%;height:100%;background:#000;opacity:.2;z-index:1;position:absolute}.win-table-container .spinner-wrapper .spinner{top:calc(50% - 30px);left:calc(50% - 30px);z-index:2;position:absolute}\n"] }]
        }], ctorParameters: () => [], propDecorators: { tableView: [{
                type: ViewChild,
                args: ["tableView"]
            }], cellCheckboxs: [{
                type: ViewChildren,
                args: ['cellCheckboxs']
            }], columnElements: [{
                type: ViewChildren,
                args: ['columnElements']
            }], tableOption: [{
                type: Input
            }], dndSupport: [{
                type: Input
            }], iconFilterImg: [{
                type: Input
            }], visibleActionButtons: [{
                type: Input
            }], i18n: [{
                type: Input
            }], sortColumn: [{
                type: Output
            }], selectColumn: [{
                type: Output
            }], pageChange: [{
                type: Output
            }], entryPageChange: [{
                type: Output
            }], actionButton: [{
                type: Output
            }], filterOnColumn: [{
                type: Output
            }], openImagePreview: [{
                type: Output
            }], dndUpdated: [{
                type: Output
            }], rowUpdated: [{
                type: Output
            }] } });

let CellComp$1 = class CellComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.rowUpdated = new EventEmitter();
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.dndSupport = false;
        this.cellType = CELL_TYPE;
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
    editRow(str) {
        // this.cell.value = str;
        this.rowUpdated.emit(str);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellComp, selector: "via-table-cell", inputs: { row: "row", cell: "cell", dndSupport: "dndSupport" }, outputs: { openImagePreview: "openImagePreview", rowUpdated: "rowUpdated" }, ngImport: i0, template: "<ng-container [ngSwitch]=\"cell.type\">\n    <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n        <via-table-cell-image\n            [cell]=\"cell\"\n            (openImagePreview)=\"doOpenPreview($event)\">\n        </via-table-cell-image>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.INPUT>\n        <via-table-cell-input\n            [cell]=\"cell\"\n            (rowUpdated)=\"editRow($event)\">\n        </via-table-cell-input>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.STATUS>\n        <via-table-cell-status [cell]=\"cell\"></via-table-cell-status>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.DATE_TIME>\n        <via-table-cell-datetime [cell]=\"cell\"></via-table-cell-datetime>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.ACTION>\n        <via-table-cell-action [cell]=\"cell\"></via-table-cell-action>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.CHECKBOX>\n        <via-table-cell-checkbox [cell]=\"cell\"></via-table-cell-checkbox>\n    </ng-container>\n    \n    <ng-container *ngSwitchDefault>\n        <via-table-cell-default [cell]=\"cell\"></via-table-cell-default>\n    </ng-container>\n</ng-container>", styles: [""], dependencies: [{ kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "component", type: CellImageComp$1, selector: "via-table-cell-image", inputs: ["cell"], outputs: ["openImagePreview"] }, { kind: "component", type: CellInputComp$1, selector: "via-table-cell-input", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "component", type: CellDefaultComp$1, selector: "via-table-cell-default", inputs: ["cell"] }, { kind: "component", type: CellStatusComp$1, selector: "via-table-cell-status", inputs: ["cell"] }, { kind: "component", type: CellDateTimeComp$1, selector: "via-table-cell-datetime", inputs: ["cell"] }, { kind: "component", type: CellActionComp$1, selector: "via-table-cell-action", inputs: ["cell", "row"] }, { kind: "component", type: CellCheckboxComp$1, selector: "via-table-cell-checkbox", inputs: ["row", "cell"], outputs: ["rowUpdated"] }] }); }
};
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellComp$1, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell", template: "<ng-container [ngSwitch]=\"cell.type\">\n    <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n        <via-table-cell-image\n            [cell]=\"cell\"\n            (openImagePreview)=\"doOpenPreview($event)\">\n        </via-table-cell-image>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.INPUT>\n        <via-table-cell-input\n            [cell]=\"cell\"\n            (rowUpdated)=\"editRow($event)\">\n        </via-table-cell-input>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.STATUS>\n        <via-table-cell-status [cell]=\"cell\"></via-table-cell-status>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.DATE_TIME>\n        <via-table-cell-datetime [cell]=\"cell\"></via-table-cell-datetime>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.ACTION>\n        <via-table-cell-action [cell]=\"cell\"></via-table-cell-action>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.CHECKBOX>\n        <via-table-cell-checkbox [cell]=\"cell\"></via-table-cell-checkbox>\n    </ng-container>\n    \n    <ng-container *ngSwitchDefault>\n        <via-table-cell-default [cell]=\"cell\"></via-table-cell-default>\n    </ng-container>\n</ng-container>" }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], rowUpdated: [{
                type: Output
            }], row: [{
                type: Input
            }], cell: [{
                type: Input
            }], dndSupport: [{
                type: Input
            }] } });

class TableModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: TableModule, declarations: [TableComponent,
            FilterPipe,
            ColumnComp$1,
            ColumnDefaultComp,
            ColumnCheckboxComp,
            ColumnFilterMultComp$1,
            ColumnFilterTextComp$1,
            CellComp$1,
            CellImageComp$1,
            CellInputComp$1,
            CellDefaultComp$1,
            CellStatusComp$1,
            CellDateTimeComp$1,
            CellActionComp$1,
            CellCheckboxComp$1], imports: [RouterModule,
            FormsModule,
            CommonModule,
            NgbPaginationModule,
            NgbDropdownModule], exports: [TableComponent,
            FilterPipe] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, imports: [RouterModule,
            FormsModule,
            CommonModule,
            NgbPaginationModule,
            NgbDropdownModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        RouterModule,
                        FormsModule,
                        CommonModule,
                        NgbPaginationModule,
                        NgbDropdownModule,
                    ],
                    declarations: [
                        TableComponent,
                        FilterPipe,
                        ColumnComp$1,
                        ColumnDefaultComp,
                        ColumnCheckboxComp,
                        ColumnFilterMultComp$1,
                        ColumnFilterTextComp$1,
                        CellComp$1,
                        CellImageComp$1,
                        CellInputComp$1,
                        CellDefaultComp$1,
                        CellStatusComp$1,
                        CellDateTimeComp$1,
                        CellActionComp$1,
                        CellCheckboxComp$1,
                    ],
                    exports: [
                        TableComponent,
                        FilterPipe
                    ]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class ColumnFilterMultComp {
    constructor() {
        this.matMenuTrigger = undefined;
        this.matSelectionList = undefined;
        this.dropdownFilter = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
        this.filter = new ColumnFilterMultDto();
        this.badgeCount = undefined;
    }
    ngOnInit() {
        this.filter = this.column.filter;
    }
    menuOpened() {
        this.matSelectionList?.options.forEach((item) => {
            let element = find(this.filter.filterData, i => i.value === item.value.value);
            if (element) {
                item.selected = element.checked;
            }
        });
    }
    doFilter() {
        this.matMenuTrigger?.closeMenu();
        let items = this.matSelectionList?.selectedOptions.selected.map(item => item.value);
        if (Array.isArray(items)) {
            items.forEach(item => item.checked = true);
            this.badgeCount = items.length > 0 ? items.length : undefined;
        }
        this.filter.filterData.forEach(element => {
            let index = findIndex(items, item => item.value === element.value);
            element.checked = (index !== -1);
        });
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = items ? items : [];
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.matMenuTrigger?.closeMenu();
        this.matSelectionList?.deselectAll();
        this.badgeCount = undefined;
        this.filter.filterData.forEach(element => {
            element.checked = false;
        });
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = [];
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterMultComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnFilterMultComp, selector: "via-mat-table-column-filter-mult", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter" }, viewQueries: [{ propertyName: "matMenuTrigger", first: true, predicate: MatMenuTrigger, descendants: true }, { propertyName: "matSelectionList", first: true, predicate: ["matSelectionList"], descendants: true }], ngImport: i0, template: "<ng-container>\n    <button\n        [matMenuTriggerFor]=\"filterMenu\" \n        (menuOpened)=\"menuOpened()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon\n            [matBadge]=\"badgeCount\" \n            matBadgeColor=\"warn\" \n            matBadgeOverlap=\"true\" \n            matBadgeSize=\"small\">filter_alt\n        </mat-icon>\n    </button>\n    <mat-menu \n        #filterMenu=\"matMenu\" \n        overlapTrigger=\"true\">\n        <mat-selection-list \n            #matSelectionList \n            (click) = \"$event.stopPropagation()\">\n            <mat-list-option\n                *ngFor=\"let filter of filter.filterData\"\n                checkboxPosition=\"before\"\n                color=\"warn\"\n                [value]=\"filter\"\n                [selected]=\"filter.checked\">\n                {{filter.title}}\n            </mat-list-option>\n        </mat-selection-list>\n        <div \n            style=\"display: flex; margin-top: 10px; justify-content: space-evenly;\"\n            (click) = \"$event.stopPropagation()\">\n            <button \n                mat-flat-button \n                color=\"primary\" \n                (click)=\"doFilter()\">\n                {{i18n.filter}}\n            </button>\n            <button \n                mat-flat-button \n                (click)=\"doReset()\">\n                {{i18n.reset}}\n            </button>\n        </div>\n    </mat-menu>\n</ng-container>", styles: [""], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "component", type: i2$1.MatMenu, selector: "mat-menu", inputs: ["backdropClass", "aria-label", "aria-labelledby", "aria-describedby", "xPosition", "yPosition", "overlapTrigger", "hasBackdrop", "class", "classList"], outputs: ["closed", "close"], exportAs: ["matMenu"] }, { kind: "directive", type: i2$1.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", inputs: ["mat-menu-trigger-for", "matMenuTriggerFor", "matMenuTriggerData", "matMenuTriggerRestoreFocus"], outputs: ["menuOpened", "onMenuOpen", "menuClosed", "onMenuClose"], exportAs: ["matMenuTrigger"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i4.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", exportAs: ["matButton"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }, { kind: "directive", type: i5.MatBadge, selector: "[matBadge]", inputs: ["matBadgeColor", "matBadgeOverlap", "matBadgeDisabled", "matBadgePosition", "matBadge", "matBadgeDescription", "matBadgeSize", "matBadgeHidden"] }, { kind: "component", type: i6.MatSelectionList, selector: "mat-selection-list", inputs: ["color", "compareWith", "multiple", "hideSingleSelectionIndicator", "disabled"], outputs: ["selectionChange"], exportAs: ["matSelectionList"] }, { kind: "component", type: i6.MatListOption, selector: "mat-list-option", inputs: ["togglePosition", "checkboxPosition", "color", "value", "selected"], outputs: ["selectedChange"], exportAs: ["matListOption"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterMultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-column-filter-mult", template: "<ng-container>\n    <button\n        [matMenuTriggerFor]=\"filterMenu\" \n        (menuOpened)=\"menuOpened()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon\n            [matBadge]=\"badgeCount\" \n            matBadgeColor=\"warn\" \n            matBadgeOverlap=\"true\" \n            matBadgeSize=\"small\">filter_alt\n        </mat-icon>\n    </button>\n    <mat-menu \n        #filterMenu=\"matMenu\" \n        overlapTrigger=\"true\">\n        <mat-selection-list \n            #matSelectionList \n            (click) = \"$event.stopPropagation()\">\n            <mat-list-option\n                *ngFor=\"let filter of filter.filterData\"\n                checkboxPosition=\"before\"\n                color=\"warn\"\n                [value]=\"filter\"\n                [selected]=\"filter.checked\">\n                {{filter.title}}\n            </mat-list-option>\n        </mat-selection-list>\n        <div \n            style=\"display: flex; margin-top: 10px; justify-content: space-evenly;\"\n            (click) = \"$event.stopPropagation()\">\n            <button \n                mat-flat-button \n                color=\"primary\" \n                (click)=\"doFilter()\">\n                {{i18n.filter}}\n            </button>\n            <button \n                mat-flat-button \n                (click)=\"doReset()\">\n                {{i18n.reset}}\n            </button>\n        </div>\n    </mat-menu>\n</ng-container>" }]
        }], ctorParameters: () => [], propDecorators: { matMenuTrigger: [{
                type: ViewChild,
                args: [MatMenuTrigger]
            }], matSelectionList: [{
                type: ViewChild,
                args: ["matSelectionList"]
            }], dropdownFilter: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class ColumnFilterTextComp {
    constructor() {
        this.matMenuTrigger = undefined;
        this.dropdownFilter = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
        this.filter = new ColumnFilterTextDto();
        this.searchStr = "";
        this.badgeCount = undefined;
    }
    ngOnInit() {
        this.filter = this.column.filter;
        this.searchStr = this.filter.searchStr;
    }
    menuOpened() {
        this.searchStr = this.filter.searchStr;
    }
    doFilter() {
        this.matMenuTrigger?.closeMenu();
        this.filter.searchStr = this.searchStr;
        this.badgeCount = this.searchStr.length > 0 ? 1 : undefined;
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = this.filter.searchStr;
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.matMenuTrigger?.closeMenu();
        this.filter.searchStr = "";
        this.badgeCount = undefined;
        this.searchStr = "";
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = "";
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterTextComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnFilterTextComp, selector: "via-mat-table-column-filter-text", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter" }, viewQueries: [{ propertyName: "matMenuTrigger", first: true, predicate: MatMenuTrigger, descendants: true }], ngImport: i0, template: "<ng-container>\n    <button\n        [matMenuTriggerFor]=\"filterMenu\" \n        (menuOpened)=\"menuOpened()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon\n            [matBadge]=\"badgeCount\"\n            matBadgeColor=\"warn\" \n            matBadgeOverlap=\"true\" \n            matBadgeSize=\"small\">search\n        </mat-icon>\n    </button>\n    <mat-menu \n        #filterMenu=\"matMenu\" \n        overlapTrigger=\"true\">\n        <mat-form-field appearance=\"fill\"\n            style=\"margin: 0 10px;\"\n            (click) = \"$event.stopPropagation()\">\n            <!-- <mat-label>{{i18n.column_filter_type_text_lable}}</mat-label> -->\n            <input matInput\n                [(ngModel)]=\"searchStr\">\n        </mat-form-field>\n        <div\n            style=\"display: flex; justify-content: space-evenly;\" \n            (click) = \"$event.stopPropagation()\">\n            <button \n                mat-flat-button \n                color=\"primary\" \n                (click)=\"doFilter()\">\n                {{i18n.filter}}\n            </button>\n            <button \n                mat-flat-button \n                (click)=\"doReset()\">\n                {{i18n.reset}}\n            </button>\n        </div>\n    </mat-menu>\n</ng-container>", styles: [""], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "component", type: i2$1.MatMenu, selector: "mat-menu", inputs: ["backdropClass", "aria-label", "aria-labelledby", "aria-describedby", "xPosition", "yPosition", "overlapTrigger", "hasBackdrop", "class", "classList"], outputs: ["closed", "close"], exportAs: ["matMenu"] }, { kind: "directive", type: i2$1.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", inputs: ["mat-menu-trigger-for", "matMenuTriggerFor", "matMenuTriggerData", "matMenuTriggerRestoreFocus"], outputs: ["menuOpened", "onMenuOpen", "menuClosed", "onMenuClose"], exportAs: ["matMenuTrigger"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i4.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", exportAs: ["matButton"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }, { kind: "directive", type: i5.MatBadge, selector: "[matBadge]", inputs: ["matBadgeColor", "matBadgeOverlap", "matBadgeDisabled", "matBadgePosition", "matBadge", "matBadgeDescription", "matBadgeSize", "matBadgeHidden"] }, { kind: "component", type: i7.MatFormField, selector: "mat-form-field", inputs: ["hideRequiredMarker", "color", "floatLabel", "appearance", "subscriptSizing", "hintLabel"], exportAs: ["matFormField"] }, { kind: "directive", type: i7$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["disabled", "id", "placeholder", "name", "required", "type", "errorStateMatcher", "aria-describedby", "value", "readonly"], exportAs: ["matInput"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnFilterTextComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-column-filter-text", template: "<ng-container>\n    <button\n        [matMenuTriggerFor]=\"filterMenu\" \n        (menuOpened)=\"menuOpened()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon\n            [matBadge]=\"badgeCount\"\n            matBadgeColor=\"warn\" \n            matBadgeOverlap=\"true\" \n            matBadgeSize=\"small\">search\n        </mat-icon>\n    </button>\n    <mat-menu \n        #filterMenu=\"matMenu\" \n        overlapTrigger=\"true\">\n        <mat-form-field appearance=\"fill\"\n            style=\"margin: 0 10px;\"\n            (click) = \"$event.stopPropagation()\">\n            <!-- <mat-label>{{i18n.column_filter_type_text_lable}}</mat-label> -->\n            <input matInput\n                [(ngModel)]=\"searchStr\">\n        </mat-form-field>\n        <div\n            style=\"display: flex; justify-content: space-evenly;\" \n            (click) = \"$event.stopPropagation()\">\n            <button \n                mat-flat-button \n                color=\"primary\" \n                (click)=\"doFilter()\">\n                {{i18n.filter}}\n            </button>\n            <button \n                mat-flat-button \n                (click)=\"doReset()\">\n                {{i18n.reset}}\n            </button>\n        </div>\n    </mat-menu>\n</ng-container>" }]
        }], ctorParameters: () => [], propDecorators: { matMenuTrigger: [{
                type: ViewChild,
                args: [MatMenuTrigger]
            }], dropdownFilter: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class ColumnComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.sortColumn = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
    }
    ngOnInit() {
    }
    doFilterOnColumn(req) {
        this.dropdownFilter.emit(req);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC;
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC;
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.sortColumn.emit(column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnComp, selector: "via-mat-table-column", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter", sortColumn: "sortColumn" }, ngImport: i0, template: "<div class=\"header-container\">\n    <span (click)=\"doSort(column)\">{{column.value}}</span>\n\n    <via-mat-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-mult>\n    \n    <via-mat-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: ColumnFilterMultComp, selector: "via-mat-table-column-filter-mult", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }, { kind: "component", type: ColumnFilterTextComp, selector: "via-mat-table-column-filter-text", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-column", template: "<div class=\"header-container\">\n    <span (click)=\"doSort(column)\">{{column.value}}</span>\n\n    <via-mat-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-mult>\n    \n    <via-mat-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"] }]
        }], ctorParameters: () => [], propDecorators: { dropdownFilter: [{
                type: Output
            }], sortColumn: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class CellImageComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview?.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellImageComp, selector: "via-mat-table-cell-image", inputs: { cell: "cell" }, outputs: { openImagePreview: "openImagePreview" }, ngImport: i0, template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-image", template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], cell: [{
                type: Input
            }] } });

class CellInputComp {
    constructor() {
        this.inputElement = undefined;
        this.rowUpdated = new EventEmitter();
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.inputMaxLenght = 255;
        // originalValue: string = "";
        this.enabled = false;
        this.oldValue = "";
    }
    ngOnInit() {
        // this.originalValue = clone(this.cell.value);
        this.oldValue = clone(this.cell.value);
    }
    focusInput() {
        setTimeout(() => {
            this.inputElement?.nativeElement.focus();
        }, 0);
    }
    keyUpEnter() {
        this.enabled = false;
        this.doUpdateInputEvent(true, this.cell, this.oldValue);
    }
    edit() {
        this.enabled = true;
        this.focusInput();
    }
    focusin() {
        this.oldValue = clone(this.cell.value);
    }
    focusout() {
        this.doUpdateInputEvent(this.enabled, this.cell, this.oldValue);
        this.enabled = false;
    }
    doUpdateInputEvent(canEdit, newValue, originalValue) {
        if (canEdit && newValue && newValue.value !== originalValue) {
            if (newValue.value && newValue.value.length <= this.inputMaxLenght) {
                this.cell.value = newValue.value;
                this.rowUpdated.emit(this.row);
            }
            else {
                newValue.value = originalValue;
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellInputComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellInputComp, selector: "via-mat-table-cell-input", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, viewQueries: [{ propertyName: "inputElement", first: true, predicate: ["inputElement"], descendants: true }], ngImport: i0, template: "<span class=\"cell-input-wrapper\">\n    <!-- {{cell.value}} -->\n    <input \n        #inputElement\n        class=\"cell-input\" \n        [(ngModel)]=\"cell.value\"\n        [disabled]=\"!enabled\" \n        (focusin)=\"focusin()\"\n        (focusout)=\"focusout()\"\n        (keyup.enter)=\"keyUpEnter()\"\n        >\n    <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon>edit</mat-icon>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>\n        maxlength\n    </div>\n</span>\n<!-- \n\n<span *ngSwitchCase=cellType.INPUT class=\"cell-input-wrapper\">\n    {{cell.originalValue}}\n    <input class=\"cell-input\" [(ngModel)]=\"cell.value\" id=\"{{'input-' + i}}\"\n        [disabled]=\"!cell.enabled || !dndSupport\" (focusin)=\"cell.oldValue = cell.value\"\n        (focusout)=\"doUpdateInputEvent(row, cell.enabled, cell.value, cell.oldValue); cell.enabled = false\"\n        (keyup.enter)=\"cell.enabled = false; doUpdateInputEvent(row, true, cell, cell.oldValue)\">\n    <button (click)=\"cell.enabled = true; focusInput('input-' + i);\"\n        *ngIf=\"!cell.enabled && dndSupport\" class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>maxlength\n    </div>\n</span>\n\n -->", styles: [".cell-input-wrapper mat-icon{font-size:15px}.cell-input-wrapper .cell-input{width:calc(100% - 50px);border:0;background-color:inherit}.cell-input-wrapper .cell-input:focus{border-bottom:1px solid rgb(0,119,255)}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellInputComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-input", template: "<span class=\"cell-input-wrapper\">\n    <!-- {{cell.value}} -->\n    <input \n        #inputElement\n        class=\"cell-input\" \n        [(ngModel)]=\"cell.value\"\n        [disabled]=\"!enabled\" \n        (focusin)=\"focusin()\"\n        (focusout)=\"focusout()\"\n        (keyup.enter)=\"keyUpEnter()\"\n        >\n    <button \n        *ngIf=\"!enabled\" \n        (click)=\"edit()\"\n        mat-icon-button \n        color=\"primary\">\n        <mat-icon>edit</mat-icon>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>\n        maxlength\n    </div>\n</span>\n<!-- \n\n<span *ngSwitchCase=cellType.INPUT class=\"cell-input-wrapper\">\n    {{cell.originalValue}}\n    <input class=\"cell-input\" [(ngModel)]=\"cell.value\" id=\"{{'input-' + i}}\"\n        [disabled]=\"!cell.enabled || !dndSupport\" (focusin)=\"cell.oldValue = cell.value\"\n        (focusout)=\"doUpdateInputEvent(row, cell.enabled, cell.value, cell.oldValue); cell.enabled = false\"\n        (keyup.enter)=\"cell.enabled = false; doUpdateInputEvent(row, true, cell, cell.oldValue)\">\n    <button (click)=\"cell.enabled = true; focusInput('input-' + i);\"\n        *ngIf=\"!cell.enabled && dndSupport\" class=\"btn btn-link btn-action cell-edit-icon\">\n        <i class=\"far fa-edit\" aria-hidden=\"true\"></i>\n    </button>\n    <div class=\"danger-text\" *ngIf=\"cell.value.length>inputMaxLenght\" translate>maxlength\n    </div>\n</span>\n\n -->", styles: [".cell-input-wrapper mat-icon{font-size:15px}.cell-input-wrapper .cell-input{width:calc(100% - 50px);border:0;background-color:inherit}.cell-input-wrapper .cell-input:focus{border-bottom:1px solid rgb(0,119,255)}\n"] }]
        }], ctorParameters: () => [], propDecorators: { inputElement: [{
                type: ViewChild,
                args: ['inputElement']
            }], rowUpdated: [{
                type: Output
            }], row: [{
                type: Input
            }], cell: [{
                type: Input
            }] } });

class CellDefaultComp {
    constructor(router) {
        this.router = router;
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, deps: [{ token: i1$3.Router }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDefaultComp, selector: "via-mat-table-cell-default", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-default", template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i1$3.Router }], propDecorators: { cell: [{
                type: Input
            }] } });

class CellStatusComp {
    constructor() {
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellStatusComp, selector: "via-mat-table-cell-status", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-status", template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });

class CellDateTimeComp {
    constructor() {
        this.cell = new TableCellDto();
        this.dateFormat = TABLE_DATE_FORMAT;
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDateTimeComp, selector: "via-mat-table-cell-datetime", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>", styles: [""], dependencies: [{ kind: "pipe", type: i1.DatePipe, name: "date" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-datetime", template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>" }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });

class CellActionComp {
    constructor() {
        this.cell = new TableCellDto();
        this.row = new TableRowDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellActionComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellActionComp, selector: "via-mat-table-cell-action", inputs: { cell: "cell", row: "row" }, ngImport: i0, template: "<span>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"\n                *ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">\n                {{buttonAction.name}}\n            </button>\n        </span>\n    </span>\n\n    <div class=\"btn-group\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group\">\n            <button mat-icon-button color=\"primary\" [matMenuTriggerFor]=\"moreActionsMenu\">\n                <mat-icon>more_vert</mat-icon>\n            </button>\n            <mat-menu #moreActionsMenu=\"matMenu\">\n                <button *ngFor=\"let btnAction of cell.buttonDropdownActions\" \n                    [disabled]=\"btnAction.disabled\" \n                    mat-menu-item \n                    (click)=\"btnAction.disabled!=true?btnAction.buttonAction(row):''\">\n                    {{btnAction.name}}\n                </button>\n            </mat-menu>\n        </div>\n    </div>\n</span>", styles: ["button.btn-action{padding:10px 0!important;width:80px;margin-top:5px;text-transform:uppercase;margin-right:5px}button.btn{padding:12px 25px!important;font-size:12px;height:40px;border-radius:3px}button.btn-primary{background:var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000)}button.btn-primary:hover{background-color:var(--primary-color, #b82d2d);border-color:var(--primary-color, #b82d2d)}button.btn-light{background:#f7f8f9;border-color:#f7f8f9;color:#5a5a5a}button.btn-light:hover{background-color:#efefef;border-color:#efefef}.icon-btn{width:50px!important}.icon-btn img{width:20px}.dropdown-menu{padding:10px}.dropdown-menu .dropdown-item{height:46px}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i2$1.MatMenu, selector: "mat-menu", inputs: ["backdropClass", "aria-label", "aria-labelledby", "aria-describedby", "xPosition", "yPosition", "overlapTrigger", "hasBackdrop", "class", "classList"], outputs: ["closed", "close"], exportAs: ["matMenu"] }, { kind: "component", type: i2$1.MatMenuItem, selector: "[mat-menu-item]", inputs: ["role", "disabled", "disableRipple"], exportAs: ["matMenuItem"] }, { kind: "directive", type: i2$1.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", inputs: ["mat-menu-trigger-for", "matMenuTriggerFor", "matMenuTriggerData", "matMenuTriggerRestoreFocus"], outputs: ["menuOpened", "onMenuOpen", "menuClosed", "onMenuClose"], exportAs: ["matMenuTrigger"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellActionComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-action", template: "<span>\n    <span *ngFor=\"let buttonAction of cell.buttonActions\">\n        <span>\n            <button [disabled]=\"buttonAction.disabled\"\n                (click)=\"buttonAction.buttonAction(row)\" title=\"{{buttonAction.name }}\"\n                *ngIf=\"buttonAction.iconUrl\" class=\"btn btn-link btn-action icon-btn\">\n                <img [src]=\"buttonAction.iconUrl\" />\n            </button>\n            <button *ngIf=\"!buttonAction.iconUrl\" [disabled]=\"buttonAction.disabled\"\n                type=\"button\" class=\"btn btn-light btn-action\"\n                [ngClass]='\"btn-\" + buttonAction.name'\n                (click)=\"buttonAction.buttonAction(row)\">\n                {{buttonAction.name}}\n            </button>\n        </span>\n    </span>\n\n    <div class=\"btn-group\" *ngIf=\"cell.buttonDropdownActions.length\">\n        <div class=\"btn-group\">\n            <button mat-icon-button color=\"primary\" [matMenuTriggerFor]=\"moreActionsMenu\">\n                <mat-icon>more_vert</mat-icon>\n            </button>\n            <mat-menu #moreActionsMenu=\"matMenu\">\n                <button *ngFor=\"let btnAction of cell.buttonDropdownActions\" \n                    [disabled]=\"btnAction.disabled\" \n                    mat-menu-item \n                    (click)=\"btnAction.disabled!=true?btnAction.buttonAction(row):''\">\n                    {{btnAction.name}}\n                </button>\n            </mat-menu>\n        </div>\n    </div>\n</span>", styles: ["button.btn-action{padding:10px 0!important;width:80px;margin-top:5px;text-transform:uppercase;margin-right:5px}button.btn{padding:12px 25px!important;font-size:12px;height:40px;border-radius:3px}button.btn-primary{background:var(--primary-color, #9a0000);border-color:var(--primary-color, #9a0000)}button.btn-primary:hover{background-color:var(--primary-color, #b82d2d);border-color:var(--primary-color, #b82d2d)}button.btn-light{background:#f7f8f9;border-color:#f7f8f9;color:#5a5a5a}button.btn-light:hover{background-color:#efefef;border-color:#efefef}.icon-btn{width:50px!important}.icon-btn img{width:20px}.dropdown-menu{padding:10px}.dropdown-menu .dropdown-item{height:46px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }], row: [{
                type: Input
            }] } });

class CellCheckboxComp {
    constructor() {
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.rowUpdated = new EventEmitter();
        this.checked = false;
    }
    ngOnInit() {
        this.checked = this.cell.value.toLowerCase() === "true";
    }
    change() {
        this.cell.value = this.checked + "";
        this.rowUpdated.emit(this.row);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellCheckboxComp, selector: "via-mat-table-cell-checkbox", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, ngImport: i0, template: "<span class=\"cell-checkbox-wrapper\">\n    <!-- <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\"> -->\n    <mat-checkbox \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n    </mat-checkbox>\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "component", type: i2$2.MatCheckbox, selector: "mat-checkbox", inputs: ["aria-label", "aria-labelledby", "aria-describedby", "id", "required", "labelPosition", "name", "value", "disableRipple", "tabIndex", "color", "checked", "disabled", "indeterminate"], outputs: ["change", "indeterminateChange"], exportAs: ["matCheckbox"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-checkbox", template: "<span class=\"cell-checkbox-wrapper\">\n    <!-- <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\"> -->\n    <mat-checkbox \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n    </mat-checkbox>\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { row: [{
                type: Input
            }], cell: [{
                type: Input
            }], rowUpdated: [{
                type: Output
            }] } });

class MatFilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe }); }
    static { this.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, name: "matFilterColumn" }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, decorators: [{
            type: Pipe,
            args: [{ name: 'matFilterColumn' }]
        }] });

class MatTableComponent {
    constructor() {
        this.matTableView = undefined;
        this.tableOption = new TableOptionDto();
        this.dndSupport = false;
        this.iconFilterImg = undefined;
        this.visibleActionButtons = true;
        this.i18n = new TableI18nDto();
        this.sortColumn = new EventEmitter();
        this.selectColumn = new EventEmitter();
        this.pageChange = new EventEmitter();
        this.actionButton = new EventEmitter();
        this.filterOnColumn = new EventEmitter();
        this.openImagePreview = new EventEmitter();
        this.dndUpdated = new EventEmitter();
        this.rowUpdated = new EventEmitter();
        this.isShowFilter = false;
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        this.cellType = CELL_TYPE;
        this.pageSizeOptions = [5, 10, 25, 100];
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (this.dndSupport) {
            setTimeout(() => {
                let dnd = new DndSupport(this.matTableView.nativeElement, (dto) => {
                    this.doUpdateIndex(dto);
                });
                dnd.init();
            }, 0);
        }
    }
    getSibling(ui) {
        let text = "";
        if (ui.nextElementSibling && ui.nextElementSibling.cells) {
            text = ui.nextElementSibling.cells[1].innerText;
        }
        else if (ui.previousElementSibling && ui.previousElementSibling.cells) {
            text = ui.previousElementSibling.cells[1].innerText;
        }
        return text;
    }
    noDataFound() {
        return this.tableOption.tableRows.length === 0 && this.tableOption.currentPage <= 1;
    }
    hideFilter() {
        this.isShowFilter = false;
    }
    showFilter() {
        this.isShowFilter = true;
    }
    toggleFilter() {
        this.isShowFilter = !this.isShowFilter;
    }
    doAction(actionBtn) {
        this.actionButton?.emit(actionBtn);
    }
    doPageChange(pageEvent) {
        // let currentOffset = (this.tableOption.currentPage - 1) * this.tableOption.itemsPerPage;
        this.pageChange?.emit(pageEvent);
    }
    doSort(column) {
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        for (let i = 0; i < this.tableOption.tableColumns.length; i++) {
            let col = this.tableOption.tableColumns[i];
            if (col.name === column.name) {
                if (col.sort === SORT.ASC) {
                    col.sort = SORT.DESC;
                }
                else if (col.sort === SORT.DESC) {
                    col.sort = SORT.NONE;
                    col.sort = this.tableOption.defaultSort.sortColumnName === col.name ? SORT.ASC : SORT.NONE;
                    this.currentSortColumnName = this.tableOption.defaultSort.sortColumnName;
                    this.currentSortType = this.tableOption.defaultSort.sortType;
                }
                else if (col.sort === SORT.NONE) {
                    col.sort = SORT.ASC;
                }
            }
            else {
                col.sort = SORT.NONE;
            }
        }
        this.sortColumn?.emit(column);
    }
    doFilterOnColumn(req) {
        this.filterOnColumn?.emit(req);
    }
    doUpdateIndex(dragPostionObj) {
        this.dndUpdated?.emit(dragPostionObj);
    }
    editRow(row) {
        this.rowUpdated?.emit(row);
    }
    doOpenPreview(imgCode) {
        this.openImagePreview?.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatTableComponent, selector: "via-mat-table", inputs: { tableOption: "tableOption", dndSupport: "dndSupport", iconFilterImg: "iconFilterImg", visibleActionButtons: "visibleActionButtons", i18n: "i18n" }, outputs: { sortColumn: "sortColumn", selectColumn: "selectColumn", pageChange: "pageChange", actionButton: "actionButton", filterOnColumn: "filterOnColumn", openImagePreview: "openImagePreview", dndUpdated: "dndUpdated", rowUpdated: "rowUpdated" }, viewQueries: [{ propertyName: "matTableView", first: true, predicate: ["matTableView"], descendants: true }], ngImport: i0, template: "<div class=\"win-table-container mat-elevation-z8\">\n    <div class=\"win-table-wrapper\">\n        <div class=\"funtion-wrapper\">\n            <div class=\"action-wrapper\">\n                <div class=\"action-container\">\n                    <div class=\"filter\" *ngIf=\"tableOption.tableFilter.hasFilter\">\n                        <ul class=\"filter-container\">\n                            <li class=\"dropdown\" [ngClass]=\"isShowFilter ? 'open' : ''\">\n                                <span class=\"dropdown-content\">\n                                    <img *ngIf=\"iconFilterImg\" (click)=\"toggleFilter()\" [src]=\"iconFilterImg\" />\n                                    <mat-icon *ngIf=\"!iconFilterImg\" (click)=\"toggleFilter()\">filter_alt</mat-icon>\n                                    <span class=\"filter-text\" (click)=\"toggleFilter()\">{{i18n.filterBy}}</span>\n                                </span>\n                                <div (click)=\"hideFilter()\" class=\"filter-mask\"></div>\n                                <ul class=\"dropdown-menu-items z-depth-1\">\n                                    <li>\n                                        <ng-content select=\"[filter]\"></ng-content>\n                                    </li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"action-button\">\n                        <div *ngIf=\"tableOption.tableActions.length > 0 && visibleActionButtons\">\n                            <span *ngFor=\"let btn of tableOption.tableActions\">\n                                <button *ngIf=\"!btn.hided\" mat-flat-button color=\"primary\" class=\"uppercase-text\" [ngClass]=\"btn.css\" (click)=\"doAction(btn)\">\n                                    {{btn.name}}\n                                </button>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"paging\">\n                <div class=\"paging-container\">\n                    <div class=\"paging-wrapper\" *ngIf=\"!noDataFound()\">\n                        <mat-paginator \n                            [length]=\"tableOption.totalItems\"\n                            [pageSize]=\"tableOption.itemsPerPage\"\n                            [pageSizeOptions]=\"pageSizeOptions\"\n                            showFirstLastButtons\n                            (page)=\"doPageChange($event)\"\n                        ></mat-paginator>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <table #matTableView [ngClass]=\"dndSupport ? 'draggable-table' : ''\">\n            <thead *ngIf=\"!noDataFound()\">\n                <tr class=\"heading\">\n                    <th class='handle' *ngFor=\"let column of tableOption.tableColumns | matFilterColumn\" [ngClass]=\"[column.sortable ? 'sortable' : '', column.sort, column.css]\">\n                        <via-mat-table-column \n                            [column]=\"column\" \n                            [i18n]=\"i18n\" \n                            (sortColumn)=\"doSort($event)\"\n                            (dropdownFilter)=\"doFilterOnColumn($event)\">\n                        </via-mat-table-column>\n                    </th>\n                </tr>\n            </thead>\n            <tbody id=\"sortable\">\n                <ng-container *ngFor=\"let row of tableOption.tableRows; let i = index\">\n                    <tr [ngClass]=\"row.css\">\n                        <td class=\"tablerow\" *ngFor=\"let cell of row.tableCells\" [ngClass]=\"cell.type + ' ' + cell.css \" [ngStyle]=\"cell.style\">\n                            <!-- <via-mat-table-cell \n                                [dndSupport]=\"dndSupport\" \n                                [row]=\"row\"\n                                [cell]=\"cell\"\n                                (openImagePreview)=\"doOpenPreview($event)\"\n                                (rowUpdated)=\"editRow($event)\">\n                            </via-mat-table-cell> -->\n                            <ng-container [ngSwitch]=\"cell.type\">\n                                <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n                                    <via-mat-table-cell-image \n                                        [cell]=\"cell\"\n                                        (openImagePreview)=\"doOpenPreview($event)\">\n                                    </via-mat-table-cell-image>\n                                </ng-container>\n\n                                <ng-container *ngSwitchCase=cellType.INPUT>\n                                    <via-mat-table-cell-input\n                                        [row]=\"row\"\n                                        [cell]=\"cell\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-mat-table-cell-input>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.STATUS>\n                                    <via-mat-table-cell-status [cell]=\"cell\"></via-mat-table-cell-status>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.DATE_TIME>\n                                    <via-mat-table-cell-datetime [cell]=\"cell\"></via-mat-table-cell-datetime>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.ACTION>\n                                    <via-mat-table-cell-action \n                                        [cell]=\"cell\"\n                                        [row]=\"row\">\n                                    </via-mat-table-cell-action>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.CHECKBOX>\n                                    <via-mat-table-cell-checkbox \n                                        [cell]=\"cell\"\n                                        [row]=\"row\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-mat-table-cell-checkbox>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchDefault>\n                                    <via-mat-table-cell-default [cell]=\"cell\"></via-mat-table-cell-default>\n                                </ng-container>\n                            </ng-container>\n                        </td>\n                    </tr>\n                </ng-container>\n\n            </tbody>\n            <tfoot *ngIf=\"noDataFound()\">\n                <tr>\n                    <td [attr.colspan]=\"tableOption.tableColumns.length\">\n                        {{i18n.noDataFound}}\n                    </td>\n                </tr>\n            </tfoot>\n        </table>\n    </div>\n    <div class=\"spinner-wrapper\" *ngIf=\"tableOption.loading\">\n        <div class=\"overlay\"></div>\n        <mat-spinner strokeWidth=\"1\" diameter=\"30\"></mat-spinner>\n    </div>\n</div>", styles: [".win-table-container{position:relative;background-color:#fff}.win-table-container .win-table-wrapper{padding-bottom:40px;display:flex;flex-direction:column;align-items:center;width:100%;font-size:14px}.win-table-container .win-table-wrapper .funtion-wrapper{width:100%}.win-table-container .win-table-wrapper .action-wrapper{width:100%;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .action-wrapper .action-container{margin-top:20px;width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter{flex:3;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .filter-mask{position:fixed;inset:0;display:none}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .open .filter-mask{display:block}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container li{margin:0;padding:0}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown{width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content{display:flex;flex-direction:row;justify-content:flex-start;align-items:center}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content img{cursor:pointer;width:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content span{cursor:pointer}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content .filter-text{color:var(--primary-color, #9a0000);font-weight:700}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items{display:none;position:absolute;top:100%;list-style:none;width:250%;border-radius:3px;padding:10px 10px 30px;border:1px solid #EBEBEB;background:#fff;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items li{z-index:1;width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown.open .dropdown-menu-items{display:flex}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button{flex:7;display:flex;justify-content:flex-end;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button button{margin-left:10px}.win-table-container .win-table-wrapper .paging{width:100%;border-bottom:1px solid #F0F0F0;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .paging .paging-container{width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper{flex:1;display:flex;justify-content:flex-end;align-items:baseline;color:#aab2c0}.win-table-container .win-table-wrapper table{width:95%}.win-table-container .win-table-wrapper table.draggable-table{-webkit-user-select:none;user-select:none;background:#fff;cursor:grab}.win-table-container .win-table-wrapper table.draggable-table .draggable-drag{position:absolute;width:100%;border:1px solid var(--primary-color, #f1f1f1);z-index:10;cursor:grabbing;opacity:1}.win-table-container .win-table-wrapper table thead tr th{text-transform:uppercase;color:#b4bac6;font-size:12px;text-align:left;padding:30px 15px 10px 0}.win-table-container .win-table-wrapper table thead tr th.sortable{cursor:pointer}.win-table-container .win-table-wrapper table thead tr th.ACTION{text-align:center!important}.win-table-container .win-table-wrapper table tbody tr.dragging{background-color:var(--primary-lighter-color, rgba(124, 116, 116, .3))}.win-table-container .win-table-wrapper table tbody tr{min-height:40px;vertical-align:text-top}.win-table-container .win-table-wrapper table tbody tr:hover{background-color:#f5f5f5}.win-table-container .win-table-wrapper table tbody tr td{font-size:14px;color:#b4bac6;padding:10px 0}.win-table-container .win-table-wrapper table tbody tr td.bold{color:#354052;font-weight:700}.win-table-container .win-table-wrapper table tbody tr td.ellipsis-td{white-space:nowrap;max-width:200px;padding:0 10px!important;overflow:hidden;text-overflow:ellipsis}.win-table-container .win-table-wrapper table tbody tr td.wordwrap-td{display:block;max-width:200px;word-break:break-word}.win-table-container .win-table-wrapper table tfoot{text-align:center;font-size:24px;font-weight:700}.win-table-container .spinner-wrapper{position:absolute;width:100%;height:100%;top:0;z-index:9999}.win-table-container .spinner-wrapper .overlay{width:100%;height:100%;background:#000;opacity:.2;z-index:1;position:absolute}.win-table-container .spinner-wrapper mat-spinner{top:calc(50% - 30px);left:calc(50% - 30px);z-index:2}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "component", type: i2$3.MatPaginator, selector: "mat-paginator", inputs: ["color", "pageIndex", "length", "pageSize", "pageSizeOptions", "hidePageSize", "showFirstLastButtons", "selectConfig", "disabled"], outputs: ["page"], exportAs: ["matPaginator"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i4.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", exportAs: ["matButton"] }, { kind: "component", type: i5$1.MatProgressSpinner, selector: "mat-progress-spinner, mat-spinner", inputs: ["color", "mode", "value", "diameter", "strokeWidth"], exportAs: ["matProgressSpinner"] }, { kind: "component", type: ColumnComp, selector: "via-mat-table-column", inputs: ["column", "i18n"], outputs: ["dropdownFilter", "sortColumn"] }, { kind: "component", type: CellImageComp, selector: "via-mat-table-cell-image", inputs: ["cell"], outputs: ["openImagePreview"] }, { kind: "component", type: CellInputComp, selector: "via-mat-table-cell-input", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "component", type: CellDefaultComp, selector: "via-mat-table-cell-default", inputs: ["cell"] }, { kind: "component", type: CellStatusComp, selector: "via-mat-table-cell-status", inputs: ["cell"] }, { kind: "component", type: CellDateTimeComp, selector: "via-mat-table-cell-datetime", inputs: ["cell"] }, { kind: "component", type: CellActionComp, selector: "via-mat-table-cell-action", inputs: ["cell", "row"] }, { kind: "component", type: CellCheckboxComp, selector: "via-mat-table-cell-checkbox", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "pipe", type: MatFilterPipe, name: "matFilterColumn" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table", template: "<div class=\"win-table-container mat-elevation-z8\">\n    <div class=\"win-table-wrapper\">\n        <div class=\"funtion-wrapper\">\n            <div class=\"action-wrapper\">\n                <div class=\"action-container\">\n                    <div class=\"filter\" *ngIf=\"tableOption.tableFilter.hasFilter\">\n                        <ul class=\"filter-container\">\n                            <li class=\"dropdown\" [ngClass]=\"isShowFilter ? 'open' : ''\">\n                                <span class=\"dropdown-content\">\n                                    <img *ngIf=\"iconFilterImg\" (click)=\"toggleFilter()\" [src]=\"iconFilterImg\" />\n                                    <mat-icon *ngIf=\"!iconFilterImg\" (click)=\"toggleFilter()\">filter_alt</mat-icon>\n                                    <span class=\"filter-text\" (click)=\"toggleFilter()\">{{i18n.filterBy}}</span>\n                                </span>\n                                <div (click)=\"hideFilter()\" class=\"filter-mask\"></div>\n                                <ul class=\"dropdown-menu-items z-depth-1\">\n                                    <li>\n                                        <ng-content select=\"[filter]\"></ng-content>\n                                    </li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"action-button\">\n                        <div *ngIf=\"tableOption.tableActions.length > 0 && visibleActionButtons\">\n                            <span *ngFor=\"let btn of tableOption.tableActions\">\n                                <button *ngIf=\"!btn.hided\" mat-flat-button color=\"primary\" class=\"uppercase-text\" [ngClass]=\"btn.css\" (click)=\"doAction(btn)\">\n                                    {{btn.name}}\n                                </button>\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"paging\">\n                <div class=\"paging-container\">\n                    <div class=\"paging-wrapper\" *ngIf=\"!noDataFound()\">\n                        <mat-paginator \n                            [length]=\"tableOption.totalItems\"\n                            [pageSize]=\"tableOption.itemsPerPage\"\n                            [pageSizeOptions]=\"pageSizeOptions\"\n                            showFirstLastButtons\n                            (page)=\"doPageChange($event)\"\n                        ></mat-paginator>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <table #matTableView [ngClass]=\"dndSupport ? 'draggable-table' : ''\">\n            <thead *ngIf=\"!noDataFound()\">\n                <tr class=\"heading\">\n                    <th class='handle' *ngFor=\"let column of tableOption.tableColumns | matFilterColumn\" [ngClass]=\"[column.sortable ? 'sortable' : '', column.sort, column.css]\">\n                        <via-mat-table-column \n                            [column]=\"column\" \n                            [i18n]=\"i18n\" \n                            (sortColumn)=\"doSort($event)\"\n                            (dropdownFilter)=\"doFilterOnColumn($event)\">\n                        </via-mat-table-column>\n                    </th>\n                </tr>\n            </thead>\n            <tbody id=\"sortable\">\n                <ng-container *ngFor=\"let row of tableOption.tableRows; let i = index\">\n                    <tr [ngClass]=\"row.css\">\n                        <td class=\"tablerow\" *ngFor=\"let cell of row.tableCells\" [ngClass]=\"cell.type + ' ' + cell.css \" [ngStyle]=\"cell.style\">\n                            <!-- <via-mat-table-cell \n                                [dndSupport]=\"dndSupport\" \n                                [row]=\"row\"\n                                [cell]=\"cell\"\n                                (openImagePreview)=\"doOpenPreview($event)\"\n                                (rowUpdated)=\"editRow($event)\">\n                            </via-mat-table-cell> -->\n                            <ng-container [ngSwitch]=\"cell.type\">\n                                <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n                                    <via-mat-table-cell-image \n                                        [cell]=\"cell\"\n                                        (openImagePreview)=\"doOpenPreview($event)\">\n                                    </via-mat-table-cell-image>\n                                </ng-container>\n\n                                <ng-container *ngSwitchCase=cellType.INPUT>\n                                    <via-mat-table-cell-input\n                                        [row]=\"row\"\n                                        [cell]=\"cell\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-mat-table-cell-input>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.STATUS>\n                                    <via-mat-table-cell-status [cell]=\"cell\"></via-mat-table-cell-status>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.DATE_TIME>\n                                    <via-mat-table-cell-datetime [cell]=\"cell\"></via-mat-table-cell-datetime>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.ACTION>\n                                    <via-mat-table-cell-action \n                                        [cell]=\"cell\"\n                                        [row]=\"row\">\n                                    </via-mat-table-cell-action>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchCase=cellType.CHECKBOX>\n                                    <via-mat-table-cell-checkbox \n                                        [cell]=\"cell\"\n                                        [row]=\"row\"\n                                        (rowUpdated)=\"editRow($event)\">\n                                    </via-mat-table-cell-checkbox>\n                                </ng-container>\n                                \n                                <ng-container *ngSwitchDefault>\n                                    <via-mat-table-cell-default [cell]=\"cell\"></via-mat-table-cell-default>\n                                </ng-container>\n                            </ng-container>\n                        </td>\n                    </tr>\n                </ng-container>\n\n            </tbody>\n            <tfoot *ngIf=\"noDataFound()\">\n                <tr>\n                    <td [attr.colspan]=\"tableOption.tableColumns.length\">\n                        {{i18n.noDataFound}}\n                    </td>\n                </tr>\n            </tfoot>\n        </table>\n    </div>\n    <div class=\"spinner-wrapper\" *ngIf=\"tableOption.loading\">\n        <div class=\"overlay\"></div>\n        <mat-spinner strokeWidth=\"1\" diameter=\"30\"></mat-spinner>\n    </div>\n</div>", styles: [".win-table-container{position:relative;background-color:#fff}.win-table-container .win-table-wrapper{padding-bottom:40px;display:flex;flex-direction:column;align-items:center;width:100%;font-size:14px}.win-table-container .win-table-wrapper .funtion-wrapper{width:100%}.win-table-container .win-table-wrapper .action-wrapper{width:100%;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .action-wrapper .action-container{margin-top:20px;width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter{flex:3;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container{display:flex;list-style:none;margin:0;padding:0;flex:9;color:#999;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .filter-mask{position:fixed;inset:0;display:none}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .open .filter-mask{display:block}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container li{margin:0;padding:0}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown{width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content{display:flex;flex-direction:row;justify-content:flex-start;align-items:center}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content img{cursor:pointer;width:18px;margin-right:5px}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content span{cursor:pointer}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-content .filter-text{color:var(--primary-color, #9a0000);font-weight:700}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items{display:none;position:absolute;top:100%;list-style:none;width:250%;border-radius:3px;padding:10px 10px 30px;border:1px solid #EBEBEB;background:#fff;z-index:1}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown .dropdown-menu-items li{z-index:1;width:100%}.win-table-container .win-table-wrapper .action-wrapper .action-container .filter .filter-container .dropdown.open .dropdown-menu-items{display:flex}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button{flex:7;display:flex;justify-content:flex-end;align-items:baseline}.win-table-container .win-table-wrapper .action-wrapper .action-container .action-button button{margin-left:10px}.win-table-container .win-table-wrapper .paging{width:100%;border-bottom:1px solid #F0F0F0;display:flex;align-items:center;justify-content:center}.win-table-container .win-table-wrapper .paging .paging-container{width:95%;display:flex;align-items:baseline}.win-table-container .win-table-wrapper .paging .paging-container .paging-wrapper{flex:1;display:flex;justify-content:flex-end;align-items:baseline;color:#aab2c0}.win-table-container .win-table-wrapper table{width:95%}.win-table-container .win-table-wrapper table.draggable-table{-webkit-user-select:none;user-select:none;background:#fff;cursor:grab}.win-table-container .win-table-wrapper table.draggable-table .draggable-drag{position:absolute;width:100%;border:1px solid var(--primary-color, #f1f1f1);z-index:10;cursor:grabbing;opacity:1}.win-table-container .win-table-wrapper table thead tr th{text-transform:uppercase;color:#b4bac6;font-size:12px;text-align:left;padding:30px 15px 10px 0}.win-table-container .win-table-wrapper table thead tr th.sortable{cursor:pointer}.win-table-container .win-table-wrapper table thead tr th.ACTION{text-align:center!important}.win-table-container .win-table-wrapper table tbody tr.dragging{background-color:var(--primary-lighter-color, rgba(124, 116, 116, .3))}.win-table-container .win-table-wrapper table tbody tr{min-height:40px;vertical-align:text-top}.win-table-container .win-table-wrapper table tbody tr:hover{background-color:#f5f5f5}.win-table-container .win-table-wrapper table tbody tr td{font-size:14px;color:#b4bac6;padding:10px 0}.win-table-container .win-table-wrapper table tbody tr td.bold{color:#354052;font-weight:700}.win-table-container .win-table-wrapper table tbody tr td.ellipsis-td{white-space:nowrap;max-width:200px;padding:0 10px!important;overflow:hidden;text-overflow:ellipsis}.win-table-container .win-table-wrapper table tbody tr td.wordwrap-td{display:block;max-width:200px;word-break:break-word}.win-table-container .win-table-wrapper table tfoot{text-align:center;font-size:24px;font-weight:700}.win-table-container .spinner-wrapper{position:absolute;width:100%;height:100%;top:0;z-index:9999}.win-table-container .spinner-wrapper .overlay{width:100%;height:100%;background:#000;opacity:.2;z-index:1;position:absolute}.win-table-container .spinner-wrapper mat-spinner{top:calc(50% - 30px);left:calc(50% - 30px);z-index:2}\n"] }]
        }], ctorParameters: () => [], propDecorators: { matTableView: [{
                type: ViewChild,
                args: ["matTableView"]
            }], tableOption: [{
                type: Input
            }], dndSupport: [{
                type: Input
            }], iconFilterImg: [{
                type: Input
            }], visibleActionButtons: [{
                type: Input
            }], i18n: [{
                type: Input
            }], sortColumn: [{
                type: Output
            }], selectColumn: [{
                type: Output
            }], pageChange: [{
                type: Output
            }], actionButton: [{
                type: Output
            }], filterOnColumn: [{
                type: Output
            }], openImagePreview: [{
                type: Output
            }], dndUpdated: [{
                type: Output
            }], rowUpdated: [{
                type: Output
            }] } });

class CellComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.rowUpdated = new EventEmitter();
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.dndSupport = false;
        this.cellType = CELL_TYPE;
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
    editRow(str) {
        // this.cell.value = str;
        this.rowUpdated.emit(str);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellComp, selector: "via-mat-table-cell", inputs: { row: "row", cell: "cell", dndSupport: "dndSupport" }, outputs: { openImagePreview: "openImagePreview", rowUpdated: "rowUpdated" }, ngImport: i0, template: "<ng-container [ngSwitch]=\"cell.type\">\n    <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n        <via-mat-table-cell-image\n            [cell]=\"cell\"\n            (openImagePreview)=\"doOpenPreview($event)\">\n        </via-mat-table-cell-image>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.INPUT>\n        <via-mat-table-cell-input\n            [cell]=\"cell\"\n            (rowUpdated)=\"editRow($event)\">\n        </via-mat-table-cell-input>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.STATUS>\n        <via-mat-table-cell-status [cell]=\"cell\"></via-mat-table-cell-status>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.DATE_TIME>\n        <via-mat-table-cell-datetime [cell]=\"cell\"></via-mat-table-cell-datetime>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.ACTION>\n        <via-mat-table-cell-action [cell]=\"cell\"></via-mat-table-cell-action>\n    </ng-container>\n    \n    <ng-container *ngSwitchDefault>\n        <via-mat-table-cell-default [cell]=\"cell\"></via-mat-table-cell-default>\n    </ng-container>\n</ng-container>", styles: [""], dependencies: [{ kind: "directive", type: i1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "component", type: CellImageComp, selector: "via-mat-table-cell-image", inputs: ["cell"], outputs: ["openImagePreview"] }, { kind: "component", type: CellInputComp, selector: "via-mat-table-cell-input", inputs: ["row", "cell"], outputs: ["rowUpdated"] }, { kind: "component", type: CellDefaultComp, selector: "via-mat-table-cell-default", inputs: ["cell"] }, { kind: "component", type: CellStatusComp, selector: "via-mat-table-cell-status", inputs: ["cell"] }, { kind: "component", type: CellDateTimeComp, selector: "via-mat-table-cell-datetime", inputs: ["cell"] }, { kind: "component", type: CellActionComp, selector: "via-mat-table-cell-action", inputs: ["cell", "row"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell", template: "<ng-container [ngSwitch]=\"cell.type\">\n    <ng-container *ngSwitchCase=cellType.IMAGE_THUMBNAIL>\n        <via-mat-table-cell-image\n            [cell]=\"cell\"\n            (openImagePreview)=\"doOpenPreview($event)\">\n        </via-mat-table-cell-image>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.INPUT>\n        <via-mat-table-cell-input\n            [cell]=\"cell\"\n            (rowUpdated)=\"editRow($event)\">\n        </via-mat-table-cell-input>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.STATUS>\n        <via-mat-table-cell-status [cell]=\"cell\"></via-mat-table-cell-status>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.DATE_TIME>\n        <via-mat-table-cell-datetime [cell]=\"cell\"></via-mat-table-cell-datetime>\n    </ng-container>\n    \n    <ng-container *ngSwitchCase=cellType.ACTION>\n        <via-mat-table-cell-action [cell]=\"cell\"></via-mat-table-cell-action>\n    </ng-container>\n    \n    <ng-container *ngSwitchDefault>\n        <via-mat-table-cell-default [cell]=\"cell\"></via-mat-table-cell-default>\n    </ng-container>\n</ng-container>" }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], rowUpdated: [{
                type: Output
            }], row: [{
                type: Input
            }], cell: [{
                type: Input
            }], dndSupport: [{
                type: Input
            }] } });

// import { PaginatorI18n } from './i18n/paginator.i18n';
class MatTableModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, declarations: [MatFilterPipe,
            MatTableComponent,
            ColumnComp,
            ColumnFilterMultComp,
            ColumnFilterTextComp,
            CellComp,
            CellImageComp,
            CellInputComp,
            CellDefaultComp,
            CellStatusComp,
            CellDateTimeComp,
            CellActionComp,
            CellCheckboxComp], imports: [FormsModule,
            CommonModule,
            RouterModule,
            MatPaginatorModule,
            MatMenuModule,
            MatIconModule,
            MatButtonModule,
            MatBadgeModule,
            MatCheckboxModule,
            MatListModule,
            MatFormFieldModule,
            MatInputModule,
            MatProgressSpinnerModule], exports: [MatTableComponent,
            MatFilterPipe] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, imports: [FormsModule,
            CommonModule,
            RouterModule,
            MatPaginatorModule,
            MatMenuModule,
            MatIconModule,
            MatButtonModule,
            MatBadgeModule,
            MatCheckboxModule,
            MatListModule,
            MatFormFieldModule,
            MatInputModule,
            MatProgressSpinnerModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        FormsModule,
                        CommonModule,
                        RouterModule,
                        MatPaginatorModule,
                        MatMenuModule,
                        MatIconModule,
                        MatButtonModule,
                        MatBadgeModule,
                        MatCheckboxModule,
                        MatListModule,
                        MatFormFieldModule,
                        MatInputModule,
                        MatProgressSpinnerModule
                    ],
                    declarations: [
                        MatFilterPipe,
                        MatTableComponent,
                        ColumnComp,
                        ColumnFilterMultComp,
                        ColumnFilterTextComp,
                        CellComp,
                        CellImageComp,
                        CellInputComp,
                        CellDefaultComp,
                        CellStatusComp,
                        CellDateTimeComp,
                        CellActionComp,
                        CellCheckboxComp
                    ],
                    exports: [
                        MatTableComponent,
                        MatFilterPipe
                    ],
                    providers: [
                    // { provide: MatPaginatorIntl, useClass: PaginatorI18n }
                    ]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class TimepickerI18nDto {
    constructor() {
        this.label = "";
    }
}

class TimepickerComponent {
    constructor() {
        this.timepickerModelChange = new EventEmitter();
        this.timepickerModel = {
            hour: 0,
            minute: 0,
            second: 0,
        };
        this.showSeccond = false;
        this.showMeridian = false;
        this.disabled = false;
        this.spinners = false;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.timepickerId = "";
        this.timepickerId = "timepicker" + random();
    }
    ngOnInit() {
    }
    formControlInstance() {
        return this.viaControl;
    }
    timeSelect(time) {
        this.viaControl.setValue(time);
        this.timepickerModelChange?.emit(time);
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: TimepickerComponent, selector: "via-timepicker", inputs: { timepickerModel: "timepickerModel", showSeccond: "showSeccond", showMeridian: "showMeridian", disabled: "disabled", spinners: "spinners", viaControl: "viaControl", i18n: "i18n" }, outputs: { timepickerModelChange: "timepickerModelChange" }, ngImport: i0, template: "<div class=\"win-timepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"timepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <ngb-timepicker \n        [readonlyInputs]=\"disabled\" \n        [(ngModel)]=\"timepickerModel\" \n        [meridian]=\"showMeridian\" \n        (ngModelChange)=\"timeSelect($event)\" \n        [seconds]=\"showSeccond\"\n        [spinners]=\"spinners\">\n    </ngb-timepicker>\n</div>", styles: [".win-timepicker .danger-text{color:brown}\n"], dependencies: [{ kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i2.NgbTimepicker, selector: "ngb-timepicker", inputs: ["meridian", "spinners", "seconds", "hourStep", "minuteStep", "secondStep", "readonlyInputs", "size"], exportAs: ["ngbTimepicker"] }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-timepicker", template: "<div class=\"win-timepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"timepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <ngb-timepicker \n        [readonlyInputs]=\"disabled\" \n        [(ngModel)]=\"timepickerModel\" \n        [meridian]=\"showMeridian\" \n        (ngModelChange)=\"timeSelect($event)\" \n        [seconds]=\"showSeccond\"\n        [spinners]=\"spinners\">\n    </ngb-timepicker>\n</div>", styles: [".win-timepicker .danger-text{color:brown}\n"] }]
        }], ctorParameters: () => [], propDecorators: { timepickerModelChange: [{
                type: Output
            }], timepickerModel: [{
                type: Input
            }], showSeccond: [{
                type: Input
            }], showMeridian: [{
                type: Input
            }], disabled: [{
                type: Input
            }], spinners: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });

class TimepickerModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: TimepickerModule, declarations: [TimepickerComponent], imports: [ReactiveFormsModule,
            CommonModule,
            NgbTimepickerModule,
            FormsModule], exports: [TimepickerComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerModule, imports: [ReactiveFormsModule,
            CommonModule,
            NgbTimepickerModule,
            FormsModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [TimepickerComponent],
                    imports: [
                        ReactiveFormsModule,
                        CommonModule,
                        NgbTimepickerModule,
                        FormsModule,
                    ],
                    exports: [TimepickerComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class ToastComponent extends Toast {
    constructor(toastrService, toastPackage) {
        super(toastrService, toastPackage);
        this.toastrService = toastrService;
        this.toastPackage = toastPackage;
        this.listMessages = this.message ? this.message.toString().split("||") : [];
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ToastComponent, deps: [{ token: i1$4.ToastrService }, { token: i1$4.ToastPackage }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ToastComponent, selector: "[via-toast]", usesInheritance: true, ngImport: i0, template: "<div class=\"win-alert\">\n    <i class=\"fas fa-times action\"></i>\n    <div class=\"title\">\n        <span>{{title}}</span>\n    </div>\n    <div class=\"detail\">\n        <div *ngFor=\"let item of listMessages\">\n            <p *ngIf=\"item!='undefined'\" [innerHtml]=\"item\"></p>\n        </div>\n    </div>\n</div>", styles: [".win-alert{color:#fff;min-height:120px;padding:16px;border-radius:3px;min-width:400px;position:relative;margin:5px}.win-alert .action{text-align:right;position:absolute;right:15px;top:10px}.win-alert .title{font-size:20px;text-transform:uppercase}.win-alert .detail{font-size:16px}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], animations: [
            trigger('flyInOut', []),
        ] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ToastComponent, decorators: [{
            type: Component,
            args: [{ selector: '[via-toast]', animations: [
                        trigger('flyInOut', []),
                    ], template: "<div class=\"win-alert\">\n    <i class=\"fas fa-times action\"></i>\n    <div class=\"title\">\n        <span>{{title}}</span>\n    </div>\n    <div class=\"detail\">\n        <div *ngFor=\"let item of listMessages\">\n            <p *ngIf=\"item!='undefined'\" [innerHtml]=\"item\"></p>\n        </div>\n    </div>\n</div>", styles: [".win-alert{color:#fff;min-height:120px;padding:16px;border-radius:3px;min-width:400px;position:relative;margin:5px}.win-alert .action{text-align:right;position:absolute;right:15px;top:10px}.win-alert .title{font-size:20px;text-transform:uppercase}.win-alert .detail{font-size:16px}\n"] }]
        }], ctorParameters: () => [{ type: i1$4.ToastrService }, { type: i1$4.ToastPackage }] });

class ToastModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ToastModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: ToastModule, declarations: [ToastComponent], imports: [CommonModule,
            ToastrModule], exports: [ToastComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ToastModule, imports: [CommonModule,
            ToastrModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ToastModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [ToastComponent],
                    imports: [
                        CommonModule,
                        ToastrModule
                    ],
                    exports: [ToastComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class TypeaheadOptionDto {
    constructor(url = "", multiple = true, inlineSingleSearch = false) {
        this._url = url;
        this._multiple = multiple;
        this._inlineSingleSearch = inlineSingleSearch;
        this._debounceTime = 400;
        this._itemsSelected = new Array();
        this._items = new Array();
        this._errorMessage = "";
        this._isLoading = false;
        this._minLengthToLoad = 1;
        this._noDataFound = false;
        this._keepHistory = false;
    }
    /**
     * Getter url
     * return {string}
     */
    get url() {
        return this._url;
    }
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple() {
        return this._multiple;
    }
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch() {
        return this._inlineSingleSearch;
    }
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime() {
        return this._debounceTime;
    }
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected() {
        return this._itemsSelected;
    }
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items() {
        return this._items;
    }
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage() {
        return this._errorMessage;
    }
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad() {
        return this._minLengthToLoad;
    }
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound() {
        return this._noDataFound;
    }
    get keepHistory() {
        return this._keepHistory;
    }
    /**
     * Setter url
     * param {string} value
     */
    set url(value) {
        this._url = value;
    }
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value) {
        this._multiple = value;
    }
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value) {
        this._inlineSingleSearch = value;
    }
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value) {
        this._debounceTime = value;
    }
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value) {
        this._itemsSelected = value;
    }
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value) {
        this._items = value;
    }
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value) {
        this._errorMessage = value;
    }
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value) {
        this._isLoading = value;
    }
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value) {
        this._minLengthToLoad = value;
    }
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value) {
        this._noDataFound = value;
    }
    set keepHistory(_keepHistory) {
        this._keepHistory = _keepHistory;
    }
}

class TypeaheadI18nDto {
    constructor() {
        this.label = "";
        this.placeholder = "Please enter the text";
        this.noDataFound = "No items to select";
    }
}

class TypeaheadItemDto {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

class TypeaheadItemValueDto extends TypeaheadItemDto {
    constructor(name, value, data) {
        super(name, value);
        this.data = data;
    }
}

/*
 * Public API Surface of ngx-via
 */

class TypeaheadOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.typeaheadClickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.typeaheadClickOutSite && this.typeaheadClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1 || !this.el.nativeElement.contains($event.target)) {
                this.typeaheadClickOutSite.emit("");
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadOutSiteDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive }); }
    static { this.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "14.0.0", version: "17.3.5", type: TypeaheadOutSiteDirective, selector: "[typeaheadClickOutSite]", outputs: { typeaheadClickOutSite: "typeaheadClickOutSite" }, host: { listeners: { "document:click": "documentClick($event)" } }, ngImport: i0 }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadOutSiteDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: "[typeaheadClickOutSite]"
                }]
        }], ctorParameters: () => [{ type: i0.ElementRef }], propDecorators: { typeaheadClickOutSite: [{
                type: Output
            }], documentClick: [{
                type: HostListener,
                args: ["document:click", ["$event"]]
            }] } });

class TypeaheadComponent {
    constructor() {
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.options = new TypeaheadOptionDto();
        this.disabled = false;
        this.readOnly = false;
        this.i18n = new TypeaheadI18nDto();
        this.viaControl = new FormControl();
        this.searchFormControl = new FormControl();
        this.typeaheadId = "";
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
        this.typeaheadId = "typeahead" + random();
    }
    isEditable() {
        if (this.disabled)
            return false;
        if (this.readOnly)
            return false;
        return true;
    }
    formControlInstance() {
        return this.viaControl;
    }
    hide() {
        if (!this.options.keepHistory) {
            this.options.items = [];
        }
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        if (this.options.inlineSingleSearch == false) {
            //this.isSettingValue = true;
            this.searchFormControl.setValue("");
        }
        else if (this.isSettingValue == false && this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            //this.isSettingValue = true;
            //this.searchBox.setValue(this.options.itemsSelected[0].name);
        }
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    selected(items) {
        if (this.isSingleInlineSearch()) {
            this.options.itemsSelected = items.length > 0 ? [items[0]] : [];
        }
        else {
            this.options.itemsSelected = items;
        }
        this.viaControl.setValue(this.options.itemsSelected);
    }
    select(item) {
        this.options.items = this.options.items.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        if (!this.options.multiple) {
            this.options.itemsSelected = [];
        }
        this.options.itemsSelected.push(item);
        this.options.noDataFound = this.options.items.length === 0;
        this.viaControl.setValue(this.options.itemsSelected);
        this.typeaheadItemSelected.emit(item);
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            this.searchFormControl.setValue(item.name);
        }
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.viaControl.setValue(this.options.itemsSelected);
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        this.isSettingValue = true;
        this.searchFormControl.setValue("");
        this.options.itemsSelected = [];
        this.viaControl.setValue(this.options.itemsSelected);
        this.typeaheadItemSelected?.emit(new TypeaheadItemDto("", ""));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.searchFormControl.setValue(this.options.itemsSelected[0].name);
            this.viaControl.setValue(this.options.itemsSelected[0]);
            this.isSettingValue = true;
        }
        this.searchFormControl.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            if (searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                this.typeaheadLoadData?.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.searchFormControl.setValue(text);
        this.isSettingValue = true;
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: TypeaheadComponent, selector: "via-typeahead", inputs: { options: "options", disabled: "disabled", readOnly: "readOnly", i18n: "i18n", viaControl: "viaControl" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, ngImport: i0, template: "\n<div class=\"via-typeahead-wrapper\">\n  <label [attr.for]=\"typeaheadId\" *ngIf=\"i18n.label && i18n.label.length > 0\">\n    {{i18n.label}}\n    <span class=\"danger-text\"> {{required(viaControl)}}</span>\n  </label>\n  <div class=\"via-typeahead\" (typeaheadClickOutSite)=\"hide()\">\n    <ul class=\"typeahead-container\">\n      <li class=\"typeahead\">\n        <ul class=\"typeahead-items-selected-wrapper\">\n          <span *ngIf=\"!options.inlineSingleSearch\" class=\"typeahead-items-group\">\n            <li *ngFor=\"let item of options.itemsSelected\" class=\"typeahead-menu-item-selected\">\n              <span class=\"item-display\" title=\"{{item.name}}\">{{item.name}}</span>\n              <span class=\"ic-remove\" *ngIf=\"isEditable()\" (click)=\"remove(item)\">\n                  <i class=\"fas fa-times\" aria-hidden=\"true\"></i>\n              </span>\n            </li>\n          </span>\n          <span class=\"inline-disabled\" *ngIf=\"options.inlineSingleSearch && !isEditable() && options.itemsSelected.length\">{{options.itemsSelected[0].name}}</span>\n          <li class=\"typeahead-menu-item-input\">\n            <input *ngIf=\"isEditable()\" [attr.id]=\"typeaheadId\" autocomplete=\"off\" placeholder=\"{{!isEditable() ? '' : i18n.placeholder}}\" [formControl]=\"searchFormControl\" class=\"typeahead-input\">\n            <span class=\"ic-spinner\" *ngIf=\"options.isLoading\">\n                <i class=\"far fa-circle-notch fa-spin\" id=\"circle-o-notch-spin\" aria-hidden=\"true\"></i>\n            </span>\n            <span *ngIf=\"this.isSingleInlineSearch() && options.itemsSelected.length && options.isLoading == false && isEditable()\" (click)=\"removeSingleItem()\" class=\"ic-remove\">\n                <i class=\"fas fa-times\" aria-hidden=\"true\"></i>\n            </span>\n            <ul *ngIf=\"options.items.length > 0 || options.noDataFound\" class=\"typeahead-menu-items z-depth-1\">\n              <li *ngFor=\"let item of options.items\" class=\"typeahead-menu-item\" (click)=\"select(item)\">\n                  <span>{{item.name}}</span>\n              </li>\n              <li *ngIf=\"options.noDataFound\" class=\"typeahead-menu-item\">\n                  <span>{{i18n.noDataFound}}</span>\n              </li>\n            </ul>\n          </li>\n        </ul>\n      </li>\n    </ul>\n  </div>\n</div>", styles: [".via-typeahead-wrapper .danger-text{color:brown}.via-typeahead-wrapper .via-typeahead{width:100%;border:1px solid #EBEBEB;border-radius:3px;display:flex;flex-direction:row;background:#fff;cursor:pointer}.via-typeahead-wrapper .via-typeahead .inline-disabled{padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container{display:flex;list-style:none;margin:0;padding:0;color:#999;flex:1;z-index:1}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead{margin:0;width:100%}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper{list-style:none;display:flex;padding:0;flex-wrap:wrap}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-items-group{display:flex;flex-direction:row;justify-content:flex-start;align-items:flex-start;flex-wrap:wrap;max-width:100%}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected{padding:5px;border:1px solid #EBEBEB;margin:5px 0 5px 5px;border-radius:5px;background:#999;color:#fff;display:flex;flex-direction:row;max-width:200px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected .item-display{flex:1;width:100%;white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected .ic-remove{width:20px;font-size:14px;align-items:center;display:flex;justify-content:flex-end}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input{color:#999;border:0;display:flex;align-items:center;width:auto;margin:0;flex:1;position:relative}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .typeahead-input{flex:1;border:0;padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .ic-spinner{width:20px;font-size:14px;align-items:center;display:flex;justify-content:flex-end}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .ic-remove{padding:0 10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items{list-style:none;margin:0;padding:0;position:absolute;background:#fff;display:flex;flex-direction:column;top:100%;width:100%;border:1px solid #EBEBEB;border-radius:3px;max-height:300px;overflow:auto}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items .typeahead-menu-item{margin:0;padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items .typeahead-menu-item:hover{background:#f7f8f9}.via-typeahead-wrapper .via-typeahead .typeahead-arrow{flex:1;text-align:right}.via-typeahead-wrapper .via-typeahead .typeahead-arrow i{font-size:24px;color:#999}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.FormControlDirective, selector: "[formControl]", inputs: ["formControl", "disabled", "ngModel"], outputs: ["ngModelChange"], exportAs: ["ngForm"] }, { kind: "directive", type: TypeaheadOutSiteDirective, selector: "[typeaheadClickOutSite]", outputs: ["typeaheadClickOutSite"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-typeahead", template: "\n<div class=\"via-typeahead-wrapper\">\n  <label [attr.for]=\"typeaheadId\" *ngIf=\"i18n.label && i18n.label.length > 0\">\n    {{i18n.label}}\n    <span class=\"danger-text\"> {{required(viaControl)}}</span>\n  </label>\n  <div class=\"via-typeahead\" (typeaheadClickOutSite)=\"hide()\">\n    <ul class=\"typeahead-container\">\n      <li class=\"typeahead\">\n        <ul class=\"typeahead-items-selected-wrapper\">\n          <span *ngIf=\"!options.inlineSingleSearch\" class=\"typeahead-items-group\">\n            <li *ngFor=\"let item of options.itemsSelected\" class=\"typeahead-menu-item-selected\">\n              <span class=\"item-display\" title=\"{{item.name}}\">{{item.name}}</span>\n              <span class=\"ic-remove\" *ngIf=\"isEditable()\" (click)=\"remove(item)\">\n                  <i class=\"fas fa-times\" aria-hidden=\"true\"></i>\n              </span>\n            </li>\n          </span>\n          <span class=\"inline-disabled\" *ngIf=\"options.inlineSingleSearch && !isEditable() && options.itemsSelected.length\">{{options.itemsSelected[0].name}}</span>\n          <li class=\"typeahead-menu-item-input\">\n            <input *ngIf=\"isEditable()\" [attr.id]=\"typeaheadId\" autocomplete=\"off\" placeholder=\"{{!isEditable() ? '' : i18n.placeholder}}\" [formControl]=\"searchFormControl\" class=\"typeahead-input\">\n            <span class=\"ic-spinner\" *ngIf=\"options.isLoading\">\n                <i class=\"far fa-circle-notch fa-spin\" id=\"circle-o-notch-spin\" aria-hidden=\"true\"></i>\n            </span>\n            <span *ngIf=\"this.isSingleInlineSearch() && options.itemsSelected.length && options.isLoading == false && isEditable()\" (click)=\"removeSingleItem()\" class=\"ic-remove\">\n                <i class=\"fas fa-times\" aria-hidden=\"true\"></i>\n            </span>\n            <ul *ngIf=\"options.items.length > 0 || options.noDataFound\" class=\"typeahead-menu-items z-depth-1\">\n              <li *ngFor=\"let item of options.items\" class=\"typeahead-menu-item\" (click)=\"select(item)\">\n                  <span>{{item.name}}</span>\n              </li>\n              <li *ngIf=\"options.noDataFound\" class=\"typeahead-menu-item\">\n                  <span>{{i18n.noDataFound}}</span>\n              </li>\n            </ul>\n          </li>\n        </ul>\n      </li>\n    </ul>\n  </div>\n</div>", styles: [".via-typeahead-wrapper .danger-text{color:brown}.via-typeahead-wrapper .via-typeahead{width:100%;border:1px solid #EBEBEB;border-radius:3px;display:flex;flex-direction:row;background:#fff;cursor:pointer}.via-typeahead-wrapper .via-typeahead .inline-disabled{padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container{display:flex;list-style:none;margin:0;padding:0;color:#999;flex:1;z-index:1}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead{margin:0;width:100%}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper{list-style:none;display:flex;padding:0;flex-wrap:wrap}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-items-group{display:flex;flex-direction:row;justify-content:flex-start;align-items:flex-start;flex-wrap:wrap;max-width:100%}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected{padding:5px;border:1px solid #EBEBEB;margin:5px 0 5px 5px;border-radius:5px;background:#999;color:#fff;display:flex;flex-direction:row;max-width:200px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected .item-display{flex:1;width:100%;white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-selected .ic-remove{width:20px;font-size:14px;align-items:center;display:flex;justify-content:flex-end}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input{color:#999;border:0;display:flex;align-items:center;width:auto;margin:0;flex:1;position:relative}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .typeahead-input{flex:1;border:0;padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .ic-spinner{width:20px;font-size:14px;align-items:center;display:flex;justify-content:flex-end}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-items-selected-wrapper .typeahead-menu-item-input .ic-remove{padding:0 10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items{list-style:none;margin:0;padding:0;position:absolute;background:#fff;display:flex;flex-direction:column;top:100%;width:100%;border:1px solid #EBEBEB;border-radius:3px;max-height:300px;overflow:auto}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items .typeahead-menu-item{margin:0;padding:10px}.via-typeahead-wrapper .via-typeahead .typeahead-container .typeahead .typeahead-menu-items .typeahead-menu-item:hover{background:#f7f8f9}.via-typeahead-wrapper .via-typeahead .typeahead-arrow{flex:1;text-align:right}.via-typeahead-wrapper .via-typeahead .typeahead-arrow i{font-size:24px;color:#999}\n"] }]
        }], ctorParameters: () => [], propDecorators: { typeaheadLoadData: [{
                type: Output
            }], typeaheadItemSelected: [{
                type: Output
            }], options: [{
                type: Input
            }], disabled: [{
                type: Input
            }], readOnly: [{
                type: Input
            }], i18n: [{
                type: Input
            }], viaControl: [{
                type: Input
            }] } });

class TypeaheadModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadModule, declarations: [TypeaheadComponent,
            TypeaheadOutSiteDirective], imports: [CommonModule,
            FormsModule,
            ReactiveFormsModule], exports: [TypeaheadComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadModule, imports: [CommonModule,
            FormsModule,
            ReactiveFormsModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TypeaheadComponent,
                        TypeaheadOutSiteDirective
                    ],
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                    ],
                    exports: [
                        TypeaheadComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class MatTypeaheadComponent {
    constructor() {
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.options = new TypeaheadOptionDto();
        this.disabled = false;
        this.i18n = new TypeaheadI18nDto();
        this.viaControl = new FormControl('', []);
        this.typeaheadFormGroup = new FormGroup({
            searchFormControl: new FormControl('', [])
        });
        this.separatorKeysCodes = [ENTER, COMMA];
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
    }
    formControlInstance() {
        return this.viaControl;
    }
    hide() {
        if (!this.options.keepHistory) {
            this.options.items = [];
        }
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        if (this.typeaheadInput) {
            this.typeaheadInput.nativeElement.value = '';
        }
        this.typeaheadFormGroup.controls.searchFormControl.setValue(null);
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    selected(items) {
        if (this.isSingleInlineSearch()) {
            this.options.itemsSelected = items.length > 0 ? [items[0]] : [];
        }
        else {
            this.options.itemsSelected = items;
        }
        this.viaControl.setValue(this.options.itemsSelected);
    }
    select(event) {
        let item = event.option.value;
        if (item) {
            this.options.items = this.options.items.filter((filterItem) => {
                return filterItem.value !== item.value;
            });
            if (!this.options.multiple) {
                this.options.itemsSelected = [];
            }
            this.options.itemsSelected.push(item);
            this.viaControl.setValue(this.options.itemsSelected);
            this.options.noDataFound = this.options.items.length === 0;
            this.typeaheadItemSelected.emit(item);
        }
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            if (this.typeaheadInput) {
                this.typeaheadInput.nativeElement.value = item.name;
            }
            this.typeaheadFormGroup.controls.searchFormControl.setValue(item.name);
        }
        this.hide();
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.viaControl.setValue(this.options.itemsSelected);
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        this.isSettingValue = true;
        this.typeaheadFormGroup.controls.searchFormControl.setValue("");
        this.options.itemsSelected = [];
        this.viaControl.setValue(this.options.itemsSelected);
        this.typeaheadItemSelected?.emit(new TypeaheadItemDto("", ""));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.typeaheadFormGroup.controls.searchFormControl.setValue(this.options.itemsSelected[0].name);
            this.viaControl.setValue(this.options.itemsSelected);
            this.isSettingValue = true;
        }
        this.typeaheadFormGroup.controls.searchFormControl.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            if (searchText && searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                this.typeaheadLoadData?.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.typeaheadFormGroup.controls.searchFormControl.setValue(text);
        this.isSettingValue = true;
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatTypeaheadComponent, selector: "via-mat-typeahead", inputs: { options: "options", disabled: "disabled", i18n: "i18n", viaControl: "viaControl" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, viewQueries: [{ propertyName: "typeaheadInput", first: true, predicate: ["typeaheadInput"], descendants: true }], ngImport: i0, template: "<form [formGroup]=\"typeaheadFormGroup\">\n  <mat-form-field class=\"typeahead-wrapper\" appearance=\"outline\">\n    <mat-label *ngIf=\"i18n.label && i18n.label.length > 0\">\n      {{i18n.label}}\n      <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </mat-label>\n    <mat-chip-grid #typeaheadChips>\n      <mat-chip\n        *ngFor=\"let item of options.itemsSelected\"\n        [removable]=\"!disabled\"\n        (removed)=\"remove(item)\">\n          {{item.name}}\n        <mat-icon matChipRemove *ngIf=\"!disabled\">cancel</mat-icon>\n      </mat-chip>\n      <input\n        #typeaheadInput\n        *ngIf=\"disabled === false\" \n        placeholder=\"{{i18n.placeholder}}\"\n        formControlName=\"searchFormControl\"\n        [matAutocomplete]=\"auto\"\n        [matChipInputFor]=\"typeaheadChips\"\n        [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n      >\n      \n      <div matSuffix class=\"typeahead-suffix-wrapper\" *ngIf=\"this.isSingleInlineSearch() && options.itemsSelected.length && options.isLoading == false && !disabled\" (click)=\"removeSingleItem()\">\n        <mat-icon>clear</mat-icon>\n      </div>\n      <div matSuffix *ngIf=\"options.isLoading\">\n        <mat-spinner [diameter]=\"15\"></mat-spinner>\n      </div>\n    </mat-chip-grid>\n    <mat-autocomplete \n      #auto=\"matAutocomplete\" \n      [autoActiveFirstOption]=\"true\" \n      (optionSelected)=\"select($event)\"\n      showPanel = \"true\"\n    >\n      <mat-option *ngFor=\"let item of options.items\" [value]=\"item\" class=\"typeahead-menu-item\">\n        {{item.name}}\n      </mat-option>\n      <mat-option *ngIf=\"options.noDataFound\" [value]=\"null\" class=\"typeahead-menu-item\">\n        {{i18n.noDataFound}}\n      </mat-option>\n    </mat-autocomplete>\n  </mat-form-field>\n</form>", styles: [".typeahead-wrapper{width:100%}.typeahead-wrapper .danger-text{color:brown}.typeahead-wrapper .typeahead-suffix-wrapper{cursor:pointer}\n"], dependencies: [{ kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { kind: "directive", type: i1$1.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { kind: "directive", type: i1$1.FormControlName, selector: "[formControlName]", inputs: ["formControlName", "disabled", "ngModel"], outputs: ["ngModelChange"] }, { kind: "component", type: i3$1.MatChip, selector: "mat-basic-chip, [mat-basic-chip], mat-chip, [mat-chip]", inputs: ["role", "id", "aria-label", "aria-description", "value", "color", "removable", "highlighted", "disableRipple", "disabled", "tabIndex"], outputs: ["removed", "destroyed"], exportAs: ["matChip"] }, { kind: "component", type: i3$1.MatChipGrid, selector: "mat-chip-grid", inputs: ["disabled", "placeholder", "required", "value", "errorStateMatcher"], outputs: ["change", "valueChange"] }, { kind: "directive", type: i3$1.MatChipInput, selector: "input[matChipInputFor]", inputs: ["matChipInputFor", "matChipInputAddOnBlur", "matChipInputSeparatorKeyCodes", "placeholder", "id", "disabled"], outputs: ["matChipInputTokenEnd"], exportAs: ["matChipInput", "matChipInputFor"] }, { kind: "directive", type: i3$1.MatChipRemove, selector: "[matChipRemove]" }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "component", type: i5$2.MatAutocomplete, selector: "mat-autocomplete", inputs: ["aria-label", "aria-labelledby", "displayWith", "autoActiveFirstOption", "autoSelectActiveOption", "requireSelection", "panelWidth", "disableRipple", "class", "hideSingleSelectionIndicator"], outputs: ["optionSelected", "opened", "closed", "optionActivated"], exportAs: ["matAutocomplete"] }, { kind: "component", type: i6$1.MatOption, selector: "mat-option", inputs: ["value", "id", "disabled"], outputs: ["onSelectionChange"], exportAs: ["matOption"] }, { kind: "directive", type: i5$2.MatAutocompleteTrigger, selector: "input[matAutocomplete], textarea[matAutocomplete]", inputs: ["matAutocomplete", "matAutocompletePosition", "matAutocompleteConnectedTo", "autocomplete", "matAutocompleteDisabled"], exportAs: ["matAutocompleteTrigger"] }, { kind: "component", type: i7.MatFormField, selector: "mat-form-field", inputs: ["hideRequiredMarker", "color", "floatLabel", "appearance", "subscriptSizing", "hintLabel"], exportAs: ["matFormField"] }, { kind: "directive", type: i7.MatLabel, selector: "mat-label" }, { kind: "directive", type: i7.MatSuffix, selector: "[matSuffix], [matIconSuffix], [matTextSuffix]", inputs: ["matTextSuffix"] }, { kind: "component", type: i5$1.MatProgressSpinner, selector: "mat-progress-spinner, mat-spinner", inputs: ["color", "mode", "value", "diameter", "strokeWidth"], exportAs: ["matProgressSpinner"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-typeahead", template: "<form [formGroup]=\"typeaheadFormGroup\">\n  <mat-form-field class=\"typeahead-wrapper\" appearance=\"outline\">\n    <mat-label *ngIf=\"i18n.label && i18n.label.length > 0\">\n      {{i18n.label}}\n      <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </mat-label>\n    <mat-chip-grid #typeaheadChips>\n      <mat-chip\n        *ngFor=\"let item of options.itemsSelected\"\n        [removable]=\"!disabled\"\n        (removed)=\"remove(item)\">\n          {{item.name}}\n        <mat-icon matChipRemove *ngIf=\"!disabled\">cancel</mat-icon>\n      </mat-chip>\n      <input\n        #typeaheadInput\n        *ngIf=\"disabled === false\" \n        placeholder=\"{{i18n.placeholder}}\"\n        formControlName=\"searchFormControl\"\n        [matAutocomplete]=\"auto\"\n        [matChipInputFor]=\"typeaheadChips\"\n        [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n      >\n      \n      <div matSuffix class=\"typeahead-suffix-wrapper\" *ngIf=\"this.isSingleInlineSearch() && options.itemsSelected.length && options.isLoading == false && !disabled\" (click)=\"removeSingleItem()\">\n        <mat-icon>clear</mat-icon>\n      </div>\n      <div matSuffix *ngIf=\"options.isLoading\">\n        <mat-spinner [diameter]=\"15\"></mat-spinner>\n      </div>\n    </mat-chip-grid>\n    <mat-autocomplete \n      #auto=\"matAutocomplete\" \n      [autoActiveFirstOption]=\"true\" \n      (optionSelected)=\"select($event)\"\n      showPanel = \"true\"\n    >\n      <mat-option *ngFor=\"let item of options.items\" [value]=\"item\" class=\"typeahead-menu-item\">\n        {{item.name}}\n      </mat-option>\n      <mat-option *ngIf=\"options.noDataFound\" [value]=\"null\" class=\"typeahead-menu-item\">\n        {{i18n.noDataFound}}\n      </mat-option>\n    </mat-autocomplete>\n  </mat-form-field>\n</form>", styles: [".typeahead-wrapper{width:100%}.typeahead-wrapper .danger-text{color:brown}.typeahead-wrapper .typeahead-suffix-wrapper{cursor:pointer}\n"] }]
        }], ctorParameters: () => [], propDecorators: { typeaheadLoadData: [{
                type: Output
            }], typeaheadItemSelected: [{
                type: Output
            }], options: [{
                type: Input
            }], disabled: [{
                type: Input
            }], i18n: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], typeaheadInput: [{
                type: ViewChild,
                args: ["typeaheadInput"]
            }] } });

class MatTypeaheadModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadModule, declarations: [MatTypeaheadComponent], imports: [CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatChipsModule,
            MatIconModule,
            MatAutocompleteModule,
            MatFormFieldModule,
            MatProgressSpinnerModule,
            MatInputModule], exports: [MatTypeaheadComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadModule, imports: [CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatChipsModule,
            MatIconModule,
            MatAutocompleteModule,
            MatFormFieldModule,
            MatProgressSpinnerModule,
            MatInputModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTypeaheadModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        MatTypeaheadComponent
                    ],
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatChipsModule,
                        MatIconModule,
                        MatAutocompleteModule,
                        MatFormFieldModule,
                        MatProgressSpinnerModule,
                        MatInputModule,
                    ],
                    exports: [
                        MatTypeaheadComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class ImageOptionDto {
    constructor(text = "SIXTEEN_NINE", ratioStr = "16:9", ratio = 16 / 9, calculateHeightRatio = 9 / 16, resizeToWidth = 1024) {
        this.text = text;
        this.ratioStr = ratioStr;
        this.ratio = ratio;
        this.calculateHeightRatio = calculateHeightRatio;
        this.resizeToWidth = resizeToWidth;
    }
}

const UPLOAD_FILE_EVENT = {
    START_UPLOAD: "START_UPLOAD",
    UPLOAD_ERROR: "UPLOAD_ERROR",
    UPLOAD_SUCCESSED: "UPLOAD_SUCCESSED",
    FINISH_UPLOAD: "FINISH_UPLOAD"
};
const uploadType = {
    IMAGE: "IMAGE",
    FILE: "FILE",
    ALL: "ALL"
};
const allowedContentTypesUploadTxt = ["text/plain"];
const allowedContentTypesUploadImg = ["image/jpg", "image/jpeg", "image/png"];
const allowedContentTypesUploadExel = [".xlsx", ".csv", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
const SIXTEEN_NINE = new ImageOptionDto("SIXTEEN_NINE", "16:9", 16 / 9, 9 / 16, 1024);
const ONE_ONE = new ImageOptionDto("ONE_ONE", "1:1", 1, 1, 300);
const ONE_THREE = new ImageOptionDto("ONE_THREE", "1:3", 1 / 3, 3, 150);
const FOUR_ONE = new ImageOptionDto("FOUR_ONE", "4:1", 4, 1 / 4, 1024);

class UploadOptionDto {
    constructor(acceptFileType = [], uploadApi, downloadFileApi, language, authorization, placeHolderImg) {
        this._acceptFileType = acceptFileType;
        this._uploadApi = uploadApi;
        this._language = language;
        this._authorization = authorization;
        this._downloadFileApi = downloadFileApi;
        this._placeHolderImg = placeHolderImg;
    }
    /**
     * Getter acceptFileType
     * return {string[]}
     */
    get acceptFileType() {
        return this._acceptFileType;
    }
    /**
     * Getter uploadApi
     * return {string}
     */
    get uploadApi() {
        return this._uploadApi;
    }
    /**
     * Getter downloadFileApi
     * return {string}
     */
    get downloadFileApi() {
        return this._downloadFileApi;
    }
    /**
     * Getter language
     * return {string}
     */
    get language() {
        return this._language;
    }
    /**
     * Getter authorization
     * return {string}
     */
    get authorization() {
        return this._authorization;
    }
    /**
     * Getter placeHolderImg
     * return {string}
     */
    get placeHolderImg() {
        return this._placeHolderImg;
    }
    /**
     * Setter acceptFileType
     * param {string[]} value
     */
    set acceptFileType(value) {
        this._acceptFileType = value;
    }
    /**
     * Setter uploadApi
     * param {string} value
     */
    set uploadApi(value) {
        this._uploadApi = value;
    }
    /**
     * Setter downloadFileApi
     * param {string} value
     */
    set downloadFileApi(value) {
        this._downloadFileApi = value;
    }
    /**
     * Setter language
     * param {string} value
     */
    set language(value) {
        this._language = value;
    }
    /**
     * Setter authorization
     * param {string} value
     */
    set authorization(value) {
        this._authorization = value;
    }
    /**
     * Setter placeHolderImg
     * param {string} value
     */
    set placeHolderImg(value) {
        this._placeHolderImg = value;
    }
}

class UploadResponseDto {
    constructor(responseType = "", message = "", code = "") {
        this._message = message;
        this._responseType = responseType;
        this._code = code;
    }
    /**
     * Getter responseType
     * return {string}
     */
    get responseType() {
        return this._responseType;
    }
    /**
     * Getter message
     * return {string}
     */
    get message() {
        return this._message;
    }
    /**
     * Getter code
     * return {string}
     */
    get code() {
        return this._code;
    }
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value) {
        this._responseType = value;
    }
    /**
     * Setter message
     * param {string} value
     */
    set message(value) {
        this._message = value;
    }
    /**
     * Setter code
     * param {string} value
     */
    set code(value) {
        this._code = value;
    }
}

class UploadFileI18nDto {
    constructor() {
        this.label = "";
        this.yes = "Yes";
        this.cancel = "Cancel";
        this.dndFileHere = "Drag and drop file here";
        this.or = "or";
        this.change = "Change";
        this.browseFile = "Browser file";
        this.browseFiles = "Browser files";
    }
}

/*
 * Public API Surface of ngx-via
 */

class UploadErrorDto {
    constructor(field = "", message = "") {
        this.field = field;
        this.message = message;
    }
}

class UploadFileComponent {
    constructor(http) {
        this.http = http;
        this.imageCropper = undefined;
        this.multiple = false;
        this.hidePreview = false;
        this.uploadType = uploadType.IMAGE;
        this.fileName = "";
        this.fileSize = 1024 * 1024 * 10; //5MB
        this.uploadOption = undefined;
        this.i18n = new UploadFileI18nDto();
        this.imageOption = undefined;
        this.viaControl = new FormControl();
        this.response = new EventEmitter();
        this.uploadInput = new EventEmitter();
        this.options = undefined;
        this.formData = new FormData();
        this.files = [];
        this.humanizeBytes = humanizeBytes;
        this.dragOver = false;
        this.errorMessage = "";
        this.fileId = "";
        this.uploadFileId = "";
        this.uploadQueue = new Array();
        this.totalDoneUploaded = 0;
        this.currentKey = random();
        //crop img
        this.imageChangedEvent = '';
        this.originalImage = '';
        this.uploadFileId = "uploadfile" + random();
    }
    ngAfterViewInit() {
        setTimeout(() => {
            if (this.uploadOption) {
                this.options = {
                    concurrency: 1,
                    maxUploads: 9999,
                    allowedContentTypes: this.uploadOption.acceptFileType.length ? this.uploadOption.acceptFileType : allowedContentTypesUploadImg
                };
            }
        }, 0);
    }
    formControlInstance() {
        return this.viaControl;
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    parseErrorResponse(response) {
        let errors = new Array();
        if (this.isValid(response.error) && this.isValid(response.error.error) && response.error.error instanceof Array) {
            let arr = response.error.error;
            for (let i = 0; i < arr.length; i++) {
                errors.push(new UploadErrorDto(arr[i].field, arr[i].message));
            }
        }
        else if (response.status !== 401) {
            errors.push(new UploadErrorDto("", "serverError"));
        }
        return errors;
    }
    getRatio() {
        return this.imageOption ? this.imageOption.ratio : 1;
    }
    getImgUrl() {
        return this.fileName && this.fileName.length > 0 ? this.fileName + "&index=" + this.currentKey : "";
    }
    onUploadOutput(output) {
        if (output.type === "allAddedToQueue" && this.uploadQueue.length) {
            this.response.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.START_UPLOAD));
            this.uploadQueue.forEach((element, _index) => {
                this.startUpload(element);
            });
        }
        else if (output.type === "addedToQueue" && typeof output.file !== "undefined") {
            if (output.file.size > this.fileSize) {
                this.response.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
            }
            else {
                if (this.multiple) {
                    this.files.push(output.file);
                }
                else {
                    this.files = [output.file];
                }
                this.uploadQueue.push(output.file);
            }
        }
        else if (output.type === "uploading" && typeof output.file !== "undefined") {
            if (this.multiple) {
                const index = this.files.findIndex(file => typeof output.file !== "undefined" && file.id === output.file.id);
                this.files[index] = output.file;
            }
        }
        else if (output.type === "removed") {
            if (this.multiple) {
                this.files = this.files.filter((file) => file !== output.file);
            }
            else {
                this.files = [];
            }
        }
        else if (output.type === "dragOver") {
            this.dragOver = true;
        }
        else if (output.type === "dragOut") {
            this.dragOver = false;
        }
        else if (output.type === "drop") {
            this.dragOver = false;
        }
        else if (output.type === "done") {
            this.checkResponse(output);
        }
        else if (output.type === "rejected") {
            this.response.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.type"));
        }
    }
    checkResponse(output) {
        this.totalDoneUploaded = this.totalDoneUploaded + 1;
        if (output.file && this.isValid(output.file.response.error) && output.file.response.error instanceof Array && output.file.responseStatus !== 200) {
            let arr = output.file.response.error;
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, arr[0].code, output.file.responseStatus ? output.file.responseStatus.toString() : "400"));
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "400"));
            this.uploadQueue = [];
        }
        else if (output.file && (output.file.response.result || output.file.responseStatus === 200)) {
            if (!this.multiple) {
                this.uploadQueue = [];
                this.fileId = output.file.response.result;
                this.fileName = output.file.response.result;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, output.file.response.result, output.file.responseStatus ? output.file.responseStatus.toString() : "200"));
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "200"));
            }
            else if (this.totalDoneUploaded === this.uploadQueue.length) {
                this.uploadQueue = [];
                this.totalDoneUploaded = 0;
                let d = new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "200");
                this.viaControl.setValue(d);
                this.response?.emit(d);
            }
        }
        else {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error", (output.file && output.file.responseStatus) ? output.file.responseStatus.toString() : "400"));
            this.files = [];
            this.uploadQueue = [];
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", (output.file && output.file.responseStatus) ? output.file.responseStatus.toString() : "400"));
        }
    }
    isUploadFilesDone() {
        for (let i = 0; i < this.files.length; i++) {
            let file = this.files[i], progress = file.progress;
            if (progress.data) {
                return progress.data.percentage === 100;
            }
        }
        return false;
    }
    getFilesUploaded() {
        return this.files;
    }
    startUpload(file) {
        if (this.uploadOption) {
            this.fileId = "";
            const event = {
                type: "uploadFile",
                url: this.uploadOption.uploadApi.replace(":fileType", this.uploadType),
                method: "POST",
                file: file,
                headers: {
                    language: this.uploadOption.language,
                    Authorization: this.uploadOption.authorization
                }
            };
            this.uploadInput.emit(event);
        }
    }
    cancelUpload(id) {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "cancel", id: id });
    }
    removeFile(id) {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "remove", id: id });
    }
    removeAllFiles() {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "removeAll" });
    }
    // IMAGE CROPPER
    fileChangeEvent(event) {
        if (event.srcElement.files[0].size > this.fileSize) {
            this.response.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
        }
        else {
            this.imageChangedEvent = event;
            //this.originalImage = event.srcElement.files;
        }
    }
    imageCropped(_event) {
        // this.cropImgObject = event;
        // if (event.width > uploadRatio[this.ratio].resizeToWidth && this.imageCropper) {
        //   this.imageCropper.resizeToWidth = uploadRatio[this.ratio].resizeToWidth;
        //   this.imageCropper.crop();
        //   this.imageCropper.resizeToWidth = 0;
        // }
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }
    clearUploadCroppedImage() {
        //this.originalImage = new Array();
        // this.cropImgObject = undefined;
        this.imageChangedEvent = undefined;
    }
    doCrop() {
        if (this.imageCropper) {
            let event = this.imageCropper.crop('base64');
            let resizeToWidth = this.imageOption.resizeToWidth;
            if (event && event.base64) {
                if (resizeToWidth && event.width > resizeToWidth) {
                    let resizeToHeight = resizeToWidth * this.imageOption.calculateHeightRatio;
                    this.resizedataURL(event.base64, resizeToWidth, resizeToHeight).then((value) => {
                        event.base64 = value;
                        event.width = resizeToWidth;
                        event.height = resizeToHeight;
                        this.uploadCroppedImage(event);
                    });
                    // this.imageCropper.resizeToWidth = 0;
                }
                else {
                    this.uploadCroppedImage(event);
                }
            }
        }
    }
    uploadCroppedImage(event) {
        //upload cropped image
        if (this.uploadOption && event.base64) {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.START_UPLOAD));
            let formData = new FormData();
            formData.append('file', base64ToFile(event.base64));
            let headers = new HttpHeaders();
            headers = headers.append('language', this.uploadOption.language);
            headers = headers.append('Authorization', this.uploadOption.authorization);
            this.http.post(this.uploadOption.uploadApi.replace(":fileType", this.uploadType), formData, { headers: headers }).subscribe((result) => {
                let res = result;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, res.result));
                this.clearUploadCroppedImage();
                this.fileName = result.message;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            }, (errorRes) => {
                let errors = this.parseErrorResponse(errorRes);
                if (errors.length > 0 && errors[0].message.trim().length > 0) {
                    this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, errors[0].message.trim()));
                }
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            });
        }
    }
    resizedataURL(base64, resizeToWidth, resizeToHeight) {
        return new Promise(async (resolve, _reject) => {
            // We create an image to receive the Data URI
            let img = document.createElement('img');
            // When the event "onload" is triggered we can resize the image.
            img.onload = () => {
                // We create a canvas and get its context.
                let canvas = document.createElement('canvas');
                let ctx = canvas.getContext('2d');
                // We set the dimensions at the wanted size.
                canvas.width = resizeToWidth;
                canvas.height = resizeToHeight;
                // We resize the image with the canvas method drawImage();
                ctx?.drawImage(img, 0, 0, resizeToWidth, resizeToHeight);
                let dataURI = canvas.toDataURL();
                // This is the return of the Promise
                resolve(dataURI);
            };
            // We put the Data URI in the image's src attribute
            img.src = base64;
        });
    }
    percentage(file) {
        if (file && file.progress.data) {
            return file.progress.data.percentage;
        }
        return 0;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: UploadFileComponent, deps: [{ token: i1$5.HttpClient }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: UploadFileComponent, selector: "via-upload-file", inputs: { multiple: "multiple", hidePreview: "hidePreview", uploadType: "uploadType", fileName: "fileName", fileSize: "fileSize", uploadOption: "uploadOption", i18n: "i18n", imageOption: "imageOption", viaControl: "viaControl" }, outputs: { response: "response", uploadInput: "uploadInput" }, viewQueries: [{ propertyName: "imageCropper", first: true, predicate: ImageCropperComponent, descendants: true }], ngImport: i0, template: "<div *ngIf=\"imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- SUPPORT CROPPER IMAGE -->\n    <div class=\"upload-file ratio-{{imageOption.text}} cropper-upload\">\n        <div *ngIf=\"imageChangedEvent && options\" class=\"img-cropper-wrapper\" style=\"width: 100%;\">\n            <image-cropper \n                [maintainAspectRatio]=\"true\" \n                [imageQuality]=\"100\" \n                [imageChangedEvent]=\"imageChangedEvent\" \n                [autoCrop]=\"false\" \n                [maintainAspectRatio]=\"true\" \n                [aspectRatio]=\"getRatio()\" \n                (imageCropped)=\"imageCropped($event)\" \n                (imageLoaded)=\"imageLoaded()\"\n                (loadImageFailed)=\"loadImageFailed()\">\n            </image-cropper>\n            <div>\n                <button type=\"button\" class=\"btn uppercase-text btn-primary modal-action-btn\" (click)=\"doCrop()\">\n                    {{i18n.yes }}\n                </button>\n                <button type=\"button\" class=\"btn uppercase-text btn-light modal-action-btn\" (click)=\"clearUploadCroppedImage()\">\n                    {{i18n.cancel }}\n                </button>\n            </div>\n        </div>\n    \n        <div class=\"upload-item\" *ngIf=\"!imageChangedEvent && fileName\">\n            <div class=\"upload-item-content\">\n                <div class=\"image-uploaded\" *ngIf=\"uploadOption\">\n                    <img [src]=\"uploadOption.downloadFileApi + fileName\" (error)=\"$any(img).error=null;img.src = uploadOption.placeHolderImg\" #img/>\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"!imageChangedEvent && options\" class=\"upload-file-container\">\n            <label class=\"upload-button-file\">\n                <div *ngIf=\"!fileName\" id=\"upload-icon\"><i class=\"fas fa-cloud-upload-alt fa-5x\" aria-hidden=\"true\"></i></div>\n                <div class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n                </div>\n                <div class=\"message-or\">{{i18n.or }}</div>\n                <br>\n                <span class=\"btn-button-file\"> {{ i18n.browseFile }} </span>\n                <br>\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" type=\"file\" accept=\"{{options.allowedContentTypes}}\"\n                    (change)=\"fileChangeEvent($event)\">\n            </label>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"options && !imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- NORMAL UPLOAD -->\n    <div class=\" upload-file drop-container\" ngFileDrop [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" [ngClass]=\"{ 'is-drop-over': dragOver }\">\n        <div *ngIf=\"uploadType === 'IMAGE'\">\n            <div class=\"upload-item\" *ngFor=\"let f of files; let i = index;\">\n                <div class=\"upload-item-content\">\n                    <div class=\"progress-content\" *ngIf=\"!isUploadFilesDone()\">\n                        <ngb-progressbar [showValue]=\"true\" type=\"success\" [value]=\"percentage(f)\">\n                        </ngb-progressbar>\n                    </div>\n                </div>\n            </div>\n            <div class=\"upload-item\" *ngIf=\"!multiple && (fileId || fileName)\">\n                <div class=\"upload-item-content\">\n                    <div class=\"image-uploaded\">\n                        <img *ngIf=\"!hidePreview && uploadOption\" [src]=\"uploadOption.downloadFileApi + getImgUrl()\" (error)=\"$any(image).error=null;image.src = uploadOption.placeHolderImg\" #image/>\n                    </div>\n                    {{fileId}}\n                </div>\n            </div>\n        </div>\n\n        <div *ngIf=\"multiple\">\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n            </span>\n\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n\n            <label class=\"upload-button\">\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                    [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" multiple>\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.browseFiles }}\n                </span>\n\n                <span *ngIf=\"isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.change }} </span>\n            </label>\n        </div>\n        <div *ngIf=\"!multiple\" style=\"width: 100%;\">\n            <div *ngIf=\"uploadType === 'IMAGE'\">\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere\n                    }}\n                </span>\n\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n                <label class=\"upload-button\">\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                    <span *ngIf=\"!isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.browseFile }}\n                    </span>\n\n                    <span *ngIf=\"isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.change }} </span>\n                </label>\n            </div>\n            <div *ngIf=\"uploadType === 'ALL'\" class=\"upload-file-container\">\n                <label class=\"upload-button-file\">\n                    <span class=\"btn-button-file\"> {{ i18n.browseFile }} </span>\n                    {{fileId}}\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                </label>\n            </div>\n        </div>\n    </div>\n</div>", styles: ["upload-file{width:100%}.upload-file-wrapper .danger-text{color:brown}.upload-file-wrapper .upload-file{width:100%;overflow:hidden;border:2px dashed #C4CDD9;text-align:center;display:flex;flex-direction:column;align-items:center;border-radius:3px}.upload-file-wrapper .upload-file .modal-action-btn.btn-primary{margin-right:10px}.upload-file-wrapper .upload-file .drop-container{padding:30px 10px}.upload-file-wrapper .upload-file .message-drag-and-drop{font-weight:700}.upload-file-wrapper .upload-file .message-or{color:#999;display:block;margin-top:20px}.upload-file-wrapper .upload-file .upload-button{width:100%;display:flex;flex-direction:column;align-items:center}.upload-file-wrapper .upload-file .upload-button .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button .btn-button{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary-color, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-button-file{width:100%;display:flex;flex-direction:column;display:inline-block;text-align:center;margin:10px}.upload-file-wrapper .upload-file .upload-button-file .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button-file .btn-button-file{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary-color, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-file-container{padding-bottom:10px}.upload-file-wrapper .upload-file .message-error{width:80%;text-align:left;margin-bottom:5px}.upload-file-wrapper .upload-file .upload-item{width:100%;margin:20px auto}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded{padding:15px;text-align:center}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded img{width:100%}.upload-file-wrapper .upload-file .multiple-upload-thumbnail{width:100px;display:inline-block}.upload-file-wrapper .upload-file.cropper-upload{min-height:250px}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-button-file{margin:0;margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .message-or{margin-top:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-top:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container{padding:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container .upload-button-file{margin-bottom:10px}.upload-file-wrapper .upload-file.cropper-upload #upload-icon{margin-top:100px;color:#8080802e}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE{max-height:515px;width:100%}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper image-cropper{max-height:440px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .image-uploaded img{width:auto!important;max-width:100%;max-height:390px}.upload-file-wrapper .upload-file.ratio-ONE_ONE{max-height:730px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper image-cropper{max-height:550px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .image-uploaded img{width:auto!important;max-width:100%;max-height:600px}.upload-file-wrapper .upload-file.ratio-ONE_THREE{max-height:700px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper image-cropper{max-height:650px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .image-uploaded img{width:auto!important;max-width:100%;max-height:550px}\n"], dependencies: [{ kind: "component", type: i2.NgbProgressbar, selector: "ngb-progressbar", inputs: ["max", "animated", "ariaLabel", "striped", "showValue", "textType", "type", "value", "height"] }, { kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i4$1.ImageCropperComponent, selector: "image-cropper", inputs: ["imageChangedEvent", "imageURL", "imageBase64", "imageFile", "imageAltText", "cropperFrameAriaLabel", "output", "format", "transform", "maintainAspectRatio", "aspectRatio", "resetCropOnAspectRatioChange", "resizeToWidth", "resizeToHeight", "cropperMinWidth", "cropperMinHeight", "cropperMaxHeight", "cropperMaxWidth", "cropperStaticWidth", "cropperStaticHeight", "canvasRotation", "initialStepSize", "roundCropper", "onlyScaleDown", "imageQuality", "autoCrop", "backgroundColor", "containWithinAspectRatio", "hideResizeSquares", "allowMoveImage", "cropper", "alignImage", "disabled", "hidden"], outputs: ["imageCropped", "startCropImage", "imageLoaded", "cropperReady", "loadImageFailed", "transformChange"] }, { kind: "directive", type: i5$3.NgFileDropDirective, selector: "[ngFileDrop]", inputs: ["options", "uploadInput"], outputs: ["uploadOutput"] }, { kind: "directive", type: i5$3.NgFileSelectDirective, selector: "[ngFileSelect]", inputs: ["options", "uploadInput"], outputs: ["uploadOutput"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: UploadFileComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-upload-file", template: "<div *ngIf=\"imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- SUPPORT CROPPER IMAGE -->\n    <div class=\"upload-file ratio-{{imageOption.text}} cropper-upload\">\n        <div *ngIf=\"imageChangedEvent && options\" class=\"img-cropper-wrapper\" style=\"width: 100%;\">\n            <image-cropper \n                [maintainAspectRatio]=\"true\" \n                [imageQuality]=\"100\" \n                [imageChangedEvent]=\"imageChangedEvent\" \n                [autoCrop]=\"false\" \n                [maintainAspectRatio]=\"true\" \n                [aspectRatio]=\"getRatio()\" \n                (imageCropped)=\"imageCropped($event)\" \n                (imageLoaded)=\"imageLoaded()\"\n                (loadImageFailed)=\"loadImageFailed()\">\n            </image-cropper>\n            <div>\n                <button type=\"button\" class=\"btn uppercase-text btn-primary modal-action-btn\" (click)=\"doCrop()\">\n                    {{i18n.yes }}\n                </button>\n                <button type=\"button\" class=\"btn uppercase-text btn-light modal-action-btn\" (click)=\"clearUploadCroppedImage()\">\n                    {{i18n.cancel }}\n                </button>\n            </div>\n        </div>\n    \n        <div class=\"upload-item\" *ngIf=\"!imageChangedEvent && fileName\">\n            <div class=\"upload-item-content\">\n                <div class=\"image-uploaded\" *ngIf=\"uploadOption\">\n                    <img [src]=\"uploadOption.downloadFileApi + fileName\" (error)=\"$any(img).error=null;img.src = uploadOption.placeHolderImg\" #img/>\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"!imageChangedEvent && options\" class=\"upload-file-container\">\n            <label class=\"upload-button-file\">\n                <div *ngIf=\"!fileName\" id=\"upload-icon\"><i class=\"fas fa-cloud-upload-alt fa-5x\" aria-hidden=\"true\"></i></div>\n                <div class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n                </div>\n                <div class=\"message-or\">{{i18n.or }}</div>\n                <br>\n                <span class=\"btn-button-file\"> {{ i18n.browseFile }} </span>\n                <br>\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" type=\"file\" accept=\"{{options.allowedContentTypes}}\"\n                    (change)=\"fileChangeEvent($event)\">\n            </label>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"options && !imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- NORMAL UPLOAD -->\n    <div class=\" upload-file drop-container\" ngFileDrop [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" [ngClass]=\"{ 'is-drop-over': dragOver }\">\n        <div *ngIf=\"uploadType === 'IMAGE'\">\n            <div class=\"upload-item\" *ngFor=\"let f of files; let i = index;\">\n                <div class=\"upload-item-content\">\n                    <div class=\"progress-content\" *ngIf=\"!isUploadFilesDone()\">\n                        <ngb-progressbar [showValue]=\"true\" type=\"success\" [value]=\"percentage(f)\">\n                        </ngb-progressbar>\n                    </div>\n                </div>\n            </div>\n            <div class=\"upload-item\" *ngIf=\"!multiple && (fileId || fileName)\">\n                <div class=\"upload-item-content\">\n                    <div class=\"image-uploaded\">\n                        <img *ngIf=\"!hidePreview && uploadOption\" [src]=\"uploadOption.downloadFileApi + getImgUrl()\" (error)=\"$any(image).error=null;image.src = uploadOption.placeHolderImg\" #image/>\n                    </div>\n                    {{fileId}}\n                </div>\n            </div>\n        </div>\n\n        <div *ngIf=\"multiple\">\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n            </span>\n\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n\n            <label class=\"upload-button\">\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                    [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" multiple>\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.browseFiles }}\n                </span>\n\n                <span *ngIf=\"isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.change }} </span>\n            </label>\n        </div>\n        <div *ngIf=\"!multiple\" style=\"width: 100%;\">\n            <div *ngIf=\"uploadType === 'IMAGE'\">\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere\n                    }}\n                </span>\n\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n                <label class=\"upload-button\">\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                    <span *ngIf=\"!isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.browseFile }}\n                    </span>\n\n                    <span *ngIf=\"isUploadFilesDone()\" class=\"btn-button\"> {{ i18n.change }} </span>\n                </label>\n            </div>\n            <div *ngIf=\"uploadType === 'ALL'\" class=\"upload-file-container\">\n                <label class=\"upload-button-file\">\n                    <span class=\"btn-button-file\"> {{ i18n.browseFile }} </span>\n                    {{fileId}}\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                </label>\n            </div>\n        </div>\n    </div>\n</div>", styles: ["upload-file{width:100%}.upload-file-wrapper .danger-text{color:brown}.upload-file-wrapper .upload-file{width:100%;overflow:hidden;border:2px dashed #C4CDD9;text-align:center;display:flex;flex-direction:column;align-items:center;border-radius:3px}.upload-file-wrapper .upload-file .modal-action-btn.btn-primary{margin-right:10px}.upload-file-wrapper .upload-file .drop-container{padding:30px 10px}.upload-file-wrapper .upload-file .message-drag-and-drop{font-weight:700}.upload-file-wrapper .upload-file .message-or{color:#999;display:block;margin-top:20px}.upload-file-wrapper .upload-file .upload-button{width:100%;display:flex;flex-direction:column;align-items:center}.upload-file-wrapper .upload-file .upload-button .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button .btn-button{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary-color, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-button-file{width:100%;display:flex;flex-direction:column;display:inline-block;text-align:center;margin:10px}.upload-file-wrapper .upload-file .upload-button-file .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button-file .btn-button-file{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary-color, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-file-container{padding-bottom:10px}.upload-file-wrapper .upload-file .message-error{width:80%;text-align:left;margin-bottom:5px}.upload-file-wrapper .upload-file .upload-item{width:100%;margin:20px auto}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded{padding:15px;text-align:center}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded img{width:100%}.upload-file-wrapper .upload-file .multiple-upload-thumbnail{width:100px;display:inline-block}.upload-file-wrapper .upload-file.cropper-upload{min-height:250px}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-button-file{margin:0;margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .message-or{margin-top:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-top:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container{padding:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container .upload-button-file{margin-bottom:10px}.upload-file-wrapper .upload-file.cropper-upload #upload-icon{margin-top:100px;color:#8080802e}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE{max-height:515px;width:100%}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper image-cropper{max-height:440px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .image-uploaded img{width:auto!important;max-width:100%;max-height:390px}.upload-file-wrapper .upload-file.ratio-ONE_ONE{max-height:730px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper image-cropper{max-height:550px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .image-uploaded img{width:auto!important;max-width:100%;max-height:600px}.upload-file-wrapper .upload-file.ratio-ONE_THREE{max-height:700px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper image-cropper{max-height:650px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .image-uploaded img{width:auto!important;max-width:100%;max-height:550px}\n"] }]
        }], ctorParameters: () => [{ type: i1$5.HttpClient }], propDecorators: { imageCropper: [{
                type: ViewChild,
                args: [ImageCropperComponent]
            }], multiple: [{
                type: Input
            }], hidePreview: [{
                type: Input
            }], uploadType: [{
                type: Input
            }], fileName: [{
                type: Input
            }], fileSize: [{
                type: Input
            }], uploadOption: [{
                type: Input
            }], i18n: [{
                type: Input
            }], imageOption: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], response: [{
                type: Output
            }], uploadInput: [{
                type: Output
            }] } });

class UploadFileModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: UploadFileModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: UploadFileModule, declarations: [UploadFileComponent], imports: [ReactiveFormsModule,
            HttpClientModule,
            NgbProgressbarModule,
            CommonModule,
            ImageCropperModule,
            NgxUploaderModule], exports: [UploadFileComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: UploadFileModule, imports: [ReactiveFormsModule,
            HttpClientModule,
            NgbProgressbarModule,
            CommonModule,
            ImageCropperModule,
            NgxUploaderModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: UploadFileModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [UploadFileComponent],
                    imports: [
                        ReactiveFormsModule,
                        HttpClientModule,
                        NgbProgressbarModule,
                        CommonModule,
                        ImageCropperModule,
                        NgxUploaderModule
                    ],
                    exports: [UploadFileComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class MatUploadFileComponent {
    constructor(http) {
        this.http = http;
        this.imageCropper = undefined;
        this.multiple = false;
        this.hidePreview = false;
        this.uploadType = uploadType.IMAGE;
        this.fileName = "";
        this.fileSize = 1024 * 1024 * 10; //5MB
        this.uploadOption = undefined;
        this.i18n = new UploadFileI18nDto();
        this.imageOption = undefined;
        this.viaControl = new FormControl();
        this.response = new EventEmitter();
        this.uploadInput = new EventEmitter();
        this.options = undefined;
        this.formData = new FormData();
        this.files = [];
        this.humanizeBytes = humanizeBytes;
        this.dragOver = false;
        this.errorMessage = "";
        this.fileId = "";
        this.uploadQueue = new Array();
        this.totalDoneUploaded = 0;
        //crop img
        this.imageChangedEvent = undefined;
        this.originalImage = '';
        // cropImgObject?: ImageCroppedEvent = undefined;
        this.time = new Date().getTime();
        this.uploadFileId = "";
        this.uploadFileId = "uploadfile" + random();
    }
    ngAfterViewInit() {
        setTimeout(() => {
            if (this.uploadOption) {
                this.options = {
                    concurrency: 1,
                    maxUploads: 9999,
                    allowedContentTypes: this.uploadOption.acceptFileType.length ? this.uploadOption.acceptFileType : allowedContentTypesUploadImg
                };
            }
        }, 0);
    }
    formControlInstance() {
        return this.viaControl;
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    parseErrorResponse(response) {
        let errors = new Array();
        if (this.isValid(response.error) && this.isValid(response.error.error) && response.error.error instanceof Array) {
            let arr = response.error.error;
            for (let i = 0; i < arr.length; i++) {
                errors.push(new UploadErrorDto(arr[i].field, arr[i].message));
            }
        }
        else if (response.status !== 401) {
            errors.push(new UploadErrorDto("", "serverError"));
        }
        return errors;
    }
    getRatio() {
        return this.imageOption ? this.imageOption.ratio : 1;
    }
    getImgUrl() {
        return this.fileName && this.fileName.length > 0 ? this.fileName + "&" + this.time : "";
    }
    onUploadOutput(output) {
        if (output.type === "allAddedToQueue" && this.uploadQueue.length) {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.START_UPLOAD));
            this.uploadQueue.forEach((element, _index) => {
                this.startUpload(element);
            });
        }
        else if (output.type === "addedToQueue" && typeof output.file !== "undefined") {
            if (output.file.size > this.fileSize) {
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
            }
            else {
                if (this.multiple) {
                    this.files.push(output.file);
                }
                else {
                    this.files = [output.file];
                }
                this.uploadQueue.push(output.file);
            }
        }
        else if (output.type === "uploading" && typeof output.file !== "undefined") {
            if (this.multiple) {
                const index = this.files.findIndex(file => typeof output.file !== "undefined" && file.id === output.file.id);
                this.files[index] = output.file;
            }
        }
        else if (output.type === "removed") {
            if (this.multiple) {
                this.files = this.files.filter((file) => file !== output.file);
            }
            else {
                this.files = [];
            }
        }
        else if (output.type === "dragOver") {
            this.dragOver = true;
        }
        else if (output.type === "dragOut") {
            this.dragOver = false;
        }
        else if (output.type === "drop") {
            this.dragOver = false;
        }
        else if (output.type === "done") {
            this.checkResponse(output);
        }
        else if (output.type === "rejected") {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.type"));
        }
    }
    checkResponse(output) {
        this.totalDoneUploaded = this.totalDoneUploaded + 1;
        if (output.file && this.isValid(output.file.response.error) && output.file.response.error instanceof Array && output.file.responseStatus !== 200) {
            let arr = output.file.response.error;
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, arr[0].code, output.file.responseStatus ? output.file.responseStatus.toString() : "400"));
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "400"));
            this.uploadQueue = [];
        }
        else if (output.file && (output.file.response.result || output.file.responseStatus === 200)) {
            if (!this.multiple) {
                this.uploadQueue = [];
                this.fileId = output.file.response.result;
                this.fileName = output.file.response.result;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, output.file.response.result, output.file.responseStatus ? output.file.responseStatus.toString() : "200"));
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "200"));
            }
            else if (this.totalDoneUploaded === this.uploadQueue.length) {
                this.uploadQueue = [];
                this.totalDoneUploaded = 0;
                let d = new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus ? output.file.responseStatus.toString() : "200");
                this.viaControl.setValue(d);
                this.response?.emit(d);
            }
        }
        else {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error", (output.file && output.file.responseStatus) ? output.file.responseStatus.toString() : "400"));
            this.files = [];
            this.uploadQueue = [];
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", (output.file && output.file.responseStatus) ? output.file.responseStatus.toString() : "400"));
        }
    }
    isUploadFilesDone() {
        for (let i = 0; i < this.files.length; i++) {
            let file = this.files[i], progress = file.progress;
            if (progress.data) {
                return progress.data.percentage === 100;
            }
        }
        return false;
    }
    getFilesUploaded() {
        return this.files;
    }
    startUpload(file) {
        if (this.uploadOption) {
            this.fileId = "";
            const event = {
                type: "uploadFile",
                url: this.uploadOption.uploadApi.replace(":fileType", this.uploadType),
                method: "POST",
                file: file,
                headers: {
                    language: this.uploadOption.language,
                    Authorization: this.uploadOption.authorization
                }
            };
            this.uploadInput.emit(event);
        }
    }
    cancelUpload(id) {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "cancel", id: id });
    }
    removeFile(id) {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "remove", id: id });
    }
    removeAllFiles() {
        this.viaControl.setValue("");
        this.uploadInput.emit({ type: "removeAll" });
    }
    // IMAGE CROPPER
    fileChangeEvent(event) {
        if (event.srcElement.files[0].size > this.fileSize) {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
        }
        else {
            this.imageChangedEvent = event;
            //this.originalImage = event.srcElement.files;
        }
    }
    imageCropped(_event) {
        // this.cropImgObject = event;
        // if (event.width > uploadRatio[this.ratio].resizeToWidth && this.imageCropper) {
        //   this.imageCropper.resizeToWidth = uploadRatio[this.ratio].resizeToWidth;
        //   this.imageCropper.crop();
        //   this.imageCropper.resizeToWidth = 0;
        // }
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }
    clearUploadCroppedImage() {
        //this.originalImage = new Array();
        // this.cropImgObject = undefined;
        this.imageChangedEvent = undefined;
    }
    doCrop() {
        if (this.imageCropper) {
            let event = this.imageCropper.crop("base64");
            let resizeToWidth = this.imageOption.resizeToWidth;
            if (event && event.base64) {
                if (resizeToWidth && event.width > resizeToWidth) {
                    let resizeToHeight = resizeToWidth * this.imageOption.calculateHeightRatio;
                    this.resizedataURL(event.base64, resizeToWidth, resizeToHeight).then((value) => {
                        event.base64 = value;
                        event.width = resizeToWidth;
                        event.height = resizeToHeight;
                        this.uploadCroppedImage(event);
                    });
                    // this.imageCropper.resizeToWidth = 0;
                }
                else {
                    this.uploadCroppedImage(event);
                }
            }
        }
    }
    uploadCroppedImage(event) {
        //upload cropped image
        if (this.uploadOption && event.base64) {
            this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.START_UPLOAD));
            let formData = new FormData();
            formData.append('file', base64ToFile(event.base64));
            let headers = new HttpHeaders();
            headers.append('language', this.uploadOption.language);
            headers.append('Authorization', this.uploadOption.authorization);
            this.http.post(this.uploadOption.uploadApi.replace(":fileType", this.uploadType), formData, { headers: headers }).subscribe((result) => {
                let res = result;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, res.result));
                this.clearUploadCroppedImage();
                this.fileName = result.message;
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            }, (errorRes) => {
                let errors = this.parseErrorResponse(errorRes);
                if (errors.length > 0 && errors[0].message.trim().length > 0) {
                    this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.UPLOAD_ERROR, errors[0].message.trim()));
                }
                this.response?.emit(new UploadResponseDto(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            });
        }
    }
    resizedataURL(base64, resizeToWidth, resizeToHeight) {
        return new Promise(async (resolve, _reject) => {
            // We create an image to receive the Data URI
            let img = document.createElement('img');
            // When the event "onload" is triggered we can resize the image.
            img.onload = () => {
                // We create a canvas and get its context.
                let canvas = document.createElement('canvas');
                let ctx = canvas.getContext('2d');
                // We set the dimensions at the wanted size.
                canvas.width = resizeToWidth;
                canvas.height = resizeToHeight;
                // We resize the image with the canvas method drawImage();
                ctx?.drawImage(img, 0, 0, resizeToWidth, resizeToHeight);
                let dataURI = canvas.toDataURL();
                // This is the return of the Promise
                resolve(dataURI);
            };
            // We put the Data URI in the image's src attribute
            img.src = base64;
        });
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileComponent, deps: [{ token: i1$5.HttpClient }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatUploadFileComponent, selector: "via-mat-upload-file", inputs: { multiple: "multiple", hidePreview: "hidePreview", uploadType: "uploadType", fileName: "fileName", fileSize: "fileSize", uploadOption: "uploadOption", i18n: "i18n", imageOption: "imageOption", viaControl: "viaControl" }, outputs: { response: "response", uploadInput: "uploadInput" }, viewQueries: [{ propertyName: "imageCropper", first: true, predicate: ImageCropperComponent, descendants: true }], ngImport: i0, template: "<div *ngIf=\"imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- SUPPORT CROPPER IMAGE -->\n    <div class=\"upload-file ratio-{{imageOption.text}} cropper-upload\">\n        <div *ngIf=\"imageChangedEvent && options\" class=\"img-cropper-wrapper\" style=\"width: 100%;\">\n            <image-cropper \n                [maintainAspectRatio]=\"true\" \n                [imageQuality]=\"100\" \n                [imageChangedEvent]=\"imageChangedEvent\" \n                [autoCrop]=\"false\" \n                [maintainAspectRatio]=\"true\" \n                [aspectRatio]=\"getRatio()\"\n                (imageCropped)=\"imageCropped($event)\" \n                (imageLoaded)=\"imageLoaded()\"\n                (loadImageFailed)=\"loadImageFailed()\">\n            </image-cropper>\n            <div class=\"crop-btn-wrapper\">\n                <a mat-flat-button color=\"primary\" (click)=\"doCrop()\">\n                    {{i18n.yes }}\n                </a>\n                <a mat-button (click)=\"clearUploadCroppedImage()\">\n                    {{i18n.cancel }}\n                </a>\n            </div>\n        </div>\n    \n        <div class=\"upload-item\" *ngIf=\"!imageChangedEvent && fileName\">\n            <div class=\"upload-item-content\">\n                <div class=\"image-uploaded\" *ngIf=\"uploadOption\">\n                    <img [src]=\"uploadOption.downloadFileApi + fileName\" (error)=\"$any(img).error=null;img.src = uploadOption.placeHolderImg\" #img/>\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"!imageChangedEvent && options\" class=\"upload-file-container\">\n            <label class=\"upload-button-file\">\n                <div *ngIf=\"!fileName\" id=\"upload-icon\"><i class=\"fas fa-cloud-upload-alt fa-5x\" aria-hidden=\"true\"></i></div>\n                <div class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n                </div>\n                <div class=\"message-or\">{{i18n.or }}</div>\n                <br>\n                <a mat-flat-button color=\"primary\">\n                    {{ i18n.browseFile }}\n                </a>\n                <br>\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" type=\"file\" accept=\"{{options.allowedContentTypes}}\"\n                    (change)=\"fileChangeEvent($event)\">\n            </label>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"options && !imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- NORMAL UPLOAD -->\n    <div class=\"upload-file drop-container\" ngFileDrop [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" [ngClass]=\"{ 'is-drop-over': dragOver }\">\n        <div *ngIf=\"uploadType === 'IMAGE'\">\n            <div class=\"upload-item\" *ngFor=\"let f of files; let i = index;\">\n                <div class=\"upload-item-content\">\n                    <div class=\"progress-content\" *ngIf=\"!isUploadFilesDone()\">\n                        <mat-progress-bar mode=\"determinate\" value=\"f?.progress?.data?.percentage\"></mat-progress-bar>\n                    </div>\n                </div>\n            </div>\n            <div class=\"upload-item\" *ngIf=\"!multiple && (fileId || fileName)\">\n                <div class=\"upload-item-content\">\n                    <div class=\"image-uploaded\">\n                        <img *ngIf=\"!hidePreview && uploadOption\" [src]=\"uploadOption.downloadFileApi + getImgUrl()\" (error)=\"$any(image).error=null;image.src = uploadOption.placeHolderImg\" #image/>\n                    </div>\n                    {{fileId}}\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"multiple\">\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n            </span>\n    \n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n    \n            <label class=\"upload-button\">\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                    [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" multiple>\n    \n                <a mat-flat-button color=\"primary\" *ngIf=\"!isUploadFilesDone()\">{{ i18n.browseFiles }}</a>\n                <a mat-flat-button color=\"primary\" *ngIf=\"isUploadFilesDone()\">{{ i18n.change }}</a>\n            </label>\n        </div>\n        <div *ngIf=\"!multiple\" style=\"width: 100%;\">\n            <div *ngIf=\"uploadType === 'IMAGE'\">\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere\n                    }}\n                </span>\n    \n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n                <label class=\"upload-button\">\n                    <input class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n    \n                    <a mat-flat-button color=\"primary\" *ngIf=\"!isUploadFilesDone()\">{{ i18n.browseFile }}</a>\n                    <a mat-flat-button color=\"primary\" *ngIf=\"isUploadFilesDone()\">{{ i18n.change }}</a>\n                </label>\n            </div>\n            <div *ngIf=\"uploadType === 'ALL'\" class=\"upload-file-container\">\n                <label class=\"upload-button-file\">\n                    <a mat-flat-button color=\"primary\">{{ i18n.browseFile }}</a>\n    \n                    {{fileId}}\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                </label>\n            </div>\n        </div>\n    </div>\n</div>", styles: ["upload-file{width:100%}.upload-file-wrapper .danger-text{color:brown}.upload-file-wrapper .upload-file{width:100%;overflow:hidden;border:2px dashed #C4CDD9;text-align:center;display:flex;flex-direction:column;align-items:center;border-radius:3px}.upload-file-wrapper .upload-file .crop-btn-wrapper{margin-top:10px}.upload-file-wrapper .upload-file .crop-btn-wrapper a{text-transform:uppercase}.upload-file-wrapper .upload-file .crop-btn-wrapper a:nth-child(1){margin-right:10px}.upload-file-wrapper .upload-file .drop-container{padding:30px 10px}.upload-file-wrapper .upload-file .message-drag-and-drop{font-weight:700}.upload-file-wrapper .upload-file .message-or{color:#999;display:block;margin-top:20px}.upload-file-wrapper .upload-file .upload-button{width:100%;display:flex;flex-direction:column;align-items:center}.upload-file-wrapper .upload-file .upload-button .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button .btn-button{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-button-file{width:100%;display:flex;flex-direction:column;display:inline-block;text-align:center;margin:10px}.upload-file-wrapper .upload-file .upload-button-file .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button-file .btn-button-file{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-file-container{padding-bottom:10px}.upload-file-wrapper .upload-file .message-error{width:80%;text-align:left;margin-bottom:5px}.upload-file-wrapper .upload-file .upload-item{width:100%;margin:20px auto}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded{padding:15px;text-align:center}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded img{width:100%}.upload-file-wrapper .upload-file .multiple-upload-thumbnail{width:100px;display:inline-block}.upload-file-wrapper .upload-file.cropper-upload{min-height:250px}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-button-file{margin:0;margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .message-or{margin-top:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-top:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container{padding:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container .upload-button-file{margin-bottom:10px}.upload-file-wrapper .upload-file.cropper-upload #upload-icon{margin-top:100px;color:#8080802e}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE{max-height:515px;width:100%}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper image-cropper{max-height:440px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .image-uploaded img{width:auto!important;max-width:100%;max-height:390px}.upload-file-wrapper .upload-file.ratio-ONE_ONE{max-height:730px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper image-cropper{max-height:550px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .image-uploaded img{width:auto!important;max-width:100%;max-height:600px}.upload-file-wrapper .upload-file.ratio-ONE_THREE{max-height:700px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper image-cropper{max-height:650px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .image-uploaded img{width:auto!important;max-width:100%;max-height:550px}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i4$1.ImageCropperComponent, selector: "image-cropper", inputs: ["imageChangedEvent", "imageURL", "imageBase64", "imageFile", "imageAltText", "cropperFrameAriaLabel", "output", "format", "transform", "maintainAspectRatio", "aspectRatio", "resetCropOnAspectRatioChange", "resizeToWidth", "resizeToHeight", "cropperMinWidth", "cropperMinHeight", "cropperMaxHeight", "cropperMaxWidth", "cropperStaticWidth", "cropperStaticHeight", "canvasRotation", "initialStepSize", "roundCropper", "onlyScaleDown", "imageQuality", "autoCrop", "backgroundColor", "containWithinAspectRatio", "hideResizeSquares", "allowMoveImage", "cropper", "alignImage", "disabled", "hidden"], outputs: ["imageCropped", "startCropImage", "imageLoaded", "cropperReady", "loadImageFailed", "transformChange"] }, { kind: "directive", type: i5$3.NgFileDropDirective, selector: "[ngFileDrop]", inputs: ["options", "uploadInput"], outputs: ["uploadOutput"] }, { kind: "directive", type: i5$3.NgFileSelectDirective, selector: "[ngFileSelect]", inputs: ["options", "uploadInput"], outputs: ["uploadOutput"] }, { kind: "component", type: i5$4.MatProgressBar, selector: "mat-progress-bar", inputs: ["color", "value", "bufferValue", "mode"], outputs: ["animationEnd"], exportAs: ["matProgressBar"] }, { kind: "component", type: i4.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-flat-button], a[mat-stroked-button]", exportAs: ["matButton", "matAnchor"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-upload-file", template: "<div *ngIf=\"imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- SUPPORT CROPPER IMAGE -->\n    <div class=\"upload-file ratio-{{imageOption.text}} cropper-upload\">\n        <div *ngIf=\"imageChangedEvent && options\" class=\"img-cropper-wrapper\" style=\"width: 100%;\">\n            <image-cropper \n                [maintainAspectRatio]=\"true\" \n                [imageQuality]=\"100\" \n                [imageChangedEvent]=\"imageChangedEvent\" \n                [autoCrop]=\"false\" \n                [maintainAspectRatio]=\"true\" \n                [aspectRatio]=\"getRatio()\"\n                (imageCropped)=\"imageCropped($event)\" \n                (imageLoaded)=\"imageLoaded()\"\n                (loadImageFailed)=\"loadImageFailed()\">\n            </image-cropper>\n            <div class=\"crop-btn-wrapper\">\n                <a mat-flat-button color=\"primary\" (click)=\"doCrop()\">\n                    {{i18n.yes }}\n                </a>\n                <a mat-button (click)=\"clearUploadCroppedImage()\">\n                    {{i18n.cancel }}\n                </a>\n            </div>\n        </div>\n    \n        <div class=\"upload-item\" *ngIf=\"!imageChangedEvent && fileName\">\n            <div class=\"upload-item-content\">\n                <div class=\"image-uploaded\" *ngIf=\"uploadOption\">\n                    <img [src]=\"uploadOption.downloadFileApi + fileName\" (error)=\"$any(img).error=null;img.src = uploadOption.placeHolderImg\" #img/>\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"!imageChangedEvent && options\" class=\"upload-file-container\">\n            <label class=\"upload-button-file\">\n                <div *ngIf=\"!fileName\" id=\"upload-icon\"><i class=\"fas fa-cloud-upload-alt fa-5x\" aria-hidden=\"true\"></i></div>\n                <div class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n                </div>\n                <div class=\"message-or\">{{i18n.or }}</div>\n                <br>\n                <a mat-flat-button color=\"primary\">\n                    {{ i18n.browseFile }}\n                </a>\n                <br>\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" type=\"file\" accept=\"{{options.allowedContentTypes}}\"\n                    (change)=\"fileChangeEvent($event)\">\n            </label>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"options && !imageOption\" class=\"upload-file-wrapper form-group\">\n    <label *ngIf=\"i18n.label && i18n.label.length > 0\" [attr.for]=\"uploadFileId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <!-- NORMAL UPLOAD -->\n    <div class=\"upload-file drop-container\" ngFileDrop [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" [ngClass]=\"{ 'is-drop-over': dragOver }\">\n        <div *ngIf=\"uploadType === 'IMAGE'\">\n            <div class=\"upload-item\" *ngFor=\"let f of files; let i = index;\">\n                <div class=\"upload-item-content\">\n                    <div class=\"progress-content\" *ngIf=\"!isUploadFilesDone()\">\n                        <mat-progress-bar mode=\"determinate\" value=\"f?.progress?.data?.percentage\"></mat-progress-bar>\n                    </div>\n                </div>\n            </div>\n            <div class=\"upload-item\" *ngIf=\"!multiple && (fileId || fileName)\">\n                <div class=\"upload-item-content\">\n                    <div class=\"image-uploaded\">\n                        <img *ngIf=\"!hidePreview && uploadOption\" [src]=\"uploadOption.downloadFileApi + getImgUrl()\" (error)=\"$any(image).error=null;image.src = uploadOption.placeHolderImg\" #image/>\n                    </div>\n                    {{fileId}}\n                </div>\n            </div>\n        </div>\n    \n        <div *ngIf=\"multiple\">\n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere }}\n            </span>\n    \n            <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n    \n            <label class=\"upload-button\">\n                <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                    [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\" multiple>\n    \n                <a mat-flat-button color=\"primary\" *ngIf=\"!isUploadFilesDone()\">{{ i18n.browseFiles }}</a>\n                <a mat-flat-button color=\"primary\" *ngIf=\"isUploadFilesDone()\">{{ i18n.change }}</a>\n            </label>\n        </div>\n        <div *ngIf=\"!multiple\" style=\"width: 100%;\">\n            <div *ngIf=\"uploadType === 'IMAGE'\">\n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-drag-and-drop\">{{ i18n.dndFileHere\n                    }}\n                </span>\n    \n                <span *ngIf=\"!isUploadFilesDone()\" class=\"message-or\">{{i18n.or }}</span>\n                <label class=\"upload-button\">\n                    <input class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n    \n                    <a mat-flat-button color=\"primary\" *ngIf=\"!isUploadFilesDone()\">{{ i18n.browseFile }}</a>\n                    <a mat-flat-button color=\"primary\" *ngIf=\"isUploadFilesDone()\">{{ i18n.change }}</a>\n                </label>\n            </div>\n            <div *ngIf=\"uploadType === 'ALL'\" class=\"upload-file-container\">\n                <label class=\"upload-button-file\">\n                    <a mat-flat-button color=\"primary\">{{ i18n.browseFile }}</a>\n    \n                    {{fileId}}\n                    <input [attr.id]=\"uploadFileId\" class=\"input-upload\" onclick=\"this.value=null\" accept=\"{{options.allowedContentTypes}}\" type=\"file\" ngFileSelect\n                        [options]=\"options\" (uploadOutput)=\"onUploadOutput($event)\" [uploadInput]=\"uploadInput\">\n                </label>\n            </div>\n        </div>\n    </div>\n</div>", styles: ["upload-file{width:100%}.upload-file-wrapper .danger-text{color:brown}.upload-file-wrapper .upload-file{width:100%;overflow:hidden;border:2px dashed #C4CDD9;text-align:center;display:flex;flex-direction:column;align-items:center;border-radius:3px}.upload-file-wrapper .upload-file .crop-btn-wrapper{margin-top:10px}.upload-file-wrapper .upload-file .crop-btn-wrapper a{text-transform:uppercase}.upload-file-wrapper .upload-file .crop-btn-wrapper a:nth-child(1){margin-right:10px}.upload-file-wrapper .upload-file .drop-container{padding:30px 10px}.upload-file-wrapper .upload-file .message-drag-and-drop{font-weight:700}.upload-file-wrapper .upload-file .message-or{color:#999;display:block;margin-top:20px}.upload-file-wrapper .upload-file .upload-button{width:100%;display:flex;flex-direction:column;align-items:center}.upload-file-wrapper .upload-file .upload-button .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button .btn-button{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-button-file{width:100%;display:flex;flex-direction:column;display:inline-block;text-align:center;margin:10px}.upload-file-wrapper .upload-file .upload-button-file .input-upload{visibility:hidden;height:0;width:0}.upload-file-wrapper .upload-file .upload-button-file .btn-button-file{border-radius:.25rem;padding:10px;max-width:200px;background:var(--primary, #9a0000);color:#fff;font-weight:700;font-size:12px;cursor:pointer}.upload-file-wrapper .upload-file .upload-file-container{padding-bottom:10px}.upload-file-wrapper .upload-file .message-error{width:80%;text-align:left;margin-bottom:5px}.upload-file-wrapper .upload-file .upload-item{width:100%;margin:20px auto}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded{padding:15px;text-align:center}.upload-file-wrapper .upload-file .upload-item .upload-item-content .image-uploaded img{width:100%}.upload-file-wrapper .upload-file .multiple-upload-thumbnail{width:100px;display:inline-block}.upload-file-wrapper .upload-file.cropper-upload{min-height:250px}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-button-file{margin:0;margin-bottom:0!important}.upload-file-wrapper .upload-file.cropper-upload .message-or{margin-top:0!important}.upload-file-wrapper .upload-file.cropper-upload .upload-item{margin-top:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container{padding:0}.upload-file-wrapper .upload-file.cropper-upload .upload-file-container .upload-button-file{margin-bottom:10px}.upload-file-wrapper .upload-file.cropper-upload #upload-icon{margin-top:100px;color:#8080802e}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE{max-height:515px;width:100%}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .img-cropper-wrapper image-cropper{max-height:440px}.upload-file-wrapper .upload-file.ratio-SIXTEEN_NINE .image-uploaded img{width:auto!important;max-width:100%;max-height:390px}.upload-file-wrapper .upload-file.ratio-ONE_ONE{max-height:730px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .img-cropper-wrapper image-cropper{max-height:550px}.upload-file-wrapper .upload-file.ratio-ONE_ONE .image-uploaded img{width:auto!important;max-width:100%;max-height:600px}.upload-file-wrapper .upload-file.ratio-ONE_THREE{max-height:700px;width:100%}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper{height:100%;padding-bottom:10px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .img-cropper-wrapper image-cropper{max-height:650px}.upload-file-wrapper .upload-file.ratio-ONE_THREE .image-uploaded img{width:auto!important;max-width:100%;max-height:550px}\n"] }]
        }], ctorParameters: () => [{ type: i1$5.HttpClient }], propDecorators: { imageCropper: [{
                type: ViewChild,
                args: [ImageCropperComponent]
            }], multiple: [{
                type: Input
            }], hidePreview: [{
                type: Input
            }], uploadType: [{
                type: Input
            }], fileName: [{
                type: Input
            }], fileSize: [{
                type: Input
            }], uploadOption: [{
                type: Input
            }], i18n: [{
                type: Input
            }], imageOption: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], response: [{
                type: Output
            }], uploadInput: [{
                type: Output
            }] } });

class MatUploadFileModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileModule, declarations: [MatUploadFileComponent], imports: [HttpClientModule,
            CommonModule,
            ImageCropperModule,
            NgxUploaderModule,
            MatProgressBarModule,
            MatButtonModule], exports: [MatUploadFileComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileModule, imports: [HttpClientModule,
            CommonModule,
            ImageCropperModule,
            NgxUploaderModule,
            MatProgressBarModule,
            MatButtonModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatUploadFileModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MatUploadFileComponent],
                    imports: [
                        HttpClientModule,
                        CommonModule,
                        ImageCropperModule,
                        NgxUploaderModule,
                        MatProgressBarModule,
                        MatButtonModule
                    ],
                    exports: [MatUploadFileComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

const mapConfig = {
    defaultLattitude: 10.8230989,
    defaultLongtitude: 106.6296638,
    defaultTitle: "Ho CHi Minh City",
    mapEventNames: {
        search: "search",
        myLocation: "myLocation",
        mapClick: "mapClick",
        markerDrag: "markerDrag"
    },
    myLocation: "MAP_MY_LOCATION"
};

class CordinateDto {
    constructor(latitude, longtitude, title, eventName, zoomLevel) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.title = title;
        this.eventName = eventName;
        this.zoomLevel = zoomLevel;
    }
}

class MapI18nDto {
    constructor() {
        this.label = "";
    }
}

/*
 * Public API Surface of ngx-via
 */

class MapComponent {
    constructor() {
        this.searchInput = undefined;
        this.googleMap = undefined;
        this.cordinate = new CordinateDto();
        this.searchPlaceholer = "";
        this.markerDraggable = true;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.requestLocation = true;
        this.loading = false;
        this.myLocationMarker = 'assets/icons/green_pin.svg';
        this.markerIcon = 'assets/icons/red_pin.svg';
        this.shouldClusterer = false;
        this.clustererImagepath = "assets/markerclusterer/m";
        this.iconMyLocationImg = undefined;
        this.iconSearchImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.boundsChanged = new EventEmitter();
        this.centerChanged = new EventEmitter();
        this.clickOnMap = new EventEmitter();
        this.dblClick = new EventEmitter();
        this.drag = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.idle = new EventEmitter();
        this.mouseMove = new EventEmitter();
        this.mouseOut = new EventEmitter();
        this.mouseOver = new EventEmitter();
        this.tilesLoaded = new EventEmitter();
        this.zoomChanged = new EventEmitter();
        this.latlng = undefined;
        this.myLatLng = undefined;
        this.map = undefined;
        this.myLocation = undefined;
        this.marker = undefined;
        this.searchBox = undefined;
        this.markers = [];
        this.activeInfoWindow = undefined;
        this.circle = undefined;
        this.myLocaltionLoaded = false;
        this.mapLoaded = false;
        this.markerCluster = undefined;
        this.mapId = "";
        this.mapId = "dropdown" + random();
        this.geocoder = new google.maps.Geocoder();
    }
    instance() {
        return this.map;
    }
    formControlInstance() {
        return this.viaControl;
    }
    getCenterMapLocation() {
        return this.instance()?.getCenter();
    }
    getMarkerCluster() {
        return this.markerCluster;
    }
    ngAfterViewInit() {
        let lat = this.cordinate.latitude;
        let lng = this.cordinate.longtitude;
        if (lat && lng) {
            this.initMap();
        }
        else {
            if (this.requestLocation && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    this.cordinate.latitude = location.coords.latitude;
                    this.cordinate.longtitude = location.coords.longitude;
                    this.initMap();
                    this.myLocaltionLoaded = true;
                }, () => {
                    this.cordinate.latitude = 10.762622;
                    this.cordinate.longtitude = 106.660172;
                    this.initMap();
                });
            }
            else {
                this.cordinate.latitude = 10.762622;
                this.cordinate.longtitude = 106.660172;
                this.initMap();
            }
        }
    }
    bindingCordinate() {
        if (this.cordinate.title === mapConfig.myLocation) {
            this.myLocation = new google.maps.Marker({
                position: this.myLatLng,
                icon: {
                    url: this.myLocationMarker, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map,
                draggable: false
            });
        }
        else {
            this.marker = new google.maps.Marker({
                position: this.myLatLng, icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map,
                draggable: this.markerDraggable,
                animation: google.maps.Animation.DROP,
                title: this.cordinate.title
            });
            this.addClickListener();
            this.addDragendListener();
        }
    }
    initMap() {
        if (this.googleMap) {
            this.myLatLng = new google.maps.LatLng(this.cordinate.latitude, this.cordinate.longtitude);
            let mapConfigObject = {
                zoom: 15,
                center: this.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            this.map = new google.maps.Map(this.googleMap.nativeElement, mapConfigObject);
            if (!this.hideSearchTextBox && this.searchInput) {
                this.searchBox = new google.maps.places.SearchBox(this.searchInput.nativeElement);
                this.addSearchBoxEvent();
            }
            this.bindingEvent();
            this.bindingCordinate();
            this.geocodePosition(this.myLatLng);
            if (this.shouldClusterer) {
                // this.markerCluster = new MarkerClusterer(this.map, [], {
                //   imagePath: this.clustererImagepath,
                // });
                //use the svg path for image
                this.markerCluster = new MarkerClusterer({ map: this.map, markers: [] });
            }
        }
    }
    bindingEvent() {
        google.maps.event.addListener(this.map, 'bounds_changed', () => {
            let bounds = this.map.getBounds();
            if (bounds) {
                if (!this.hideSearchTextBox && this.searchInput) {
                    this.searchBox.setBounds(bounds);
                }
                this.boundsChanged?.emit(bounds);
            }
        });
        google.maps.event.addListener(this.map, 'center_changed', () => {
            let center = this.map.getCenter();
            this.centerChanged?.emit(center);
        });
        google.maps.event.addListener(this.map, 'click', (event) => {
            this.clickOnMap?.emit(event);
        });
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
            this.dblClick?.emit(event);
        });
        google.maps.event.addListener(this.map, 'drag', () => {
            this.drag?.emit("drag");
        });
        google.maps.event.addListener(this.map, 'dragend', () => {
            this.dragEnd?.emit("dragend");
        });
        google.maps.event.addListener(this.map, 'dragstart', () => {
            this.dragStart?.emit("dragstart");
        });
        google.maps.event.addListener(this.map, 'idle', () => {
            this.mapLoaded = true;
            this.idle?.emit("idle");
        });
        google.maps.event.addListener(this.map, 'mousemove', (event) => {
            this.mouseMove?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseout', (event) => {
            this.mouseOut?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseover', (event) => {
            this.mouseOver?.emit(event);
        });
        google.maps.event.addListener(this.map, "tilesloaded", () => {
            this.tilesLoaded?.emit("tilesloaded");
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.zoomChanged?.emit(this.map.getZoom());
        });
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        if (this.map && this.map.getBounds()) {
            let sW = this.map.getBounds().getSouthWest();
            let nE = this.map.getBounds().getNorthEast();
            corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
            corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
            corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
            corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
            cornersArray[corner1] = {
                "lat": sW.lat(),
                "lng": sW.lng()
            };
            cornersArray[corner2] = {
                "lat": nE.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner3] = {
                "lat": sW.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner4] = {
                "lat": nE.lat(),
                "lng": sW.lng()
            };
            farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
            cordinate = new CordinateDto(farestPoint.lat, farestPoint.lng, "", eventName);
            this.mapRadius.emit(cordinate);
        }
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
    }
    //listLocation[0]: code
    //listLocation[1]: Latitude (pointY)
    //listLocation[2]: Longtitude (pointX)
    //listLocation[3]: index
    //listLocation[4]: label
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                map: this.map
            };
            markObject["icon"] = {
                url: this.markerIcon, // url
                scaledSize: new google.maps.Size(30, 30), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //get address
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    //@ts-ignore
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick.emit(listLocation[i][3]);
                        }
                    });
                };
            })(marker, i));
        }
        // for (let index = 0; index < this.markers.length; index++) {
        //   bounds.extend(this.markers[index].getPosition());
        // }
        // if (self.marker) {
        //   bounds.extend(self.marker.getPosition());
        // }
        // if (self.myLocation) {
        //   bounds.extend(self.myLocation.getPosition());
        // }
        //this.map.fitBounds(bounds);
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map?.setZoom(zoomNumber);
        this.map?.panTo(myLatLng);
    }
    addMaker(cordinate) {
        if (cordinate.latitude && cordinate.longtitude) {
            let markObject = {
                position: new google.maps.LatLng(cordinate.latitude, cordinate.longtitude),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map
            };
            let marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            this.panTo(cordinate.latitude, cordinate.longtitude, this.map?.getZoom());
        }
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        //@ts-ignore
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = undefined;
                this.cordinate.longtitude = undefined;
            }
            this.triggerUpdateData(eventName);
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        this.searchBox?.addListener('places_changed', () => {
            if (this.marker) {
                this.marker.setMap(null);
            }
            //self.deleteMarkers();
            let places = this.searchBox.getPlaces();
            if (Array.isArray(places) && places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                let markerObj = {
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry?.location
                };
                if (!place.geometry || !place.geometry.location) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                this.marker = new google.maps.Marker(markerObj);
                // self.markers.push(self.marker);
                this.addDragendListener();
                this.searchEvent.emit(new CordinateDto(markerObj.position.lat(), markerObj.position.lng()));
                this.cordinate.latitude = place.geometry.location.lat();
                this.cordinate.longtitude = place.geometry.location.lng();
                this.cordinate.title = place.formatted_address;
                this.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map?.fitBounds(bounds);
            this.map?.setZoom(12);
            if (this.marker && this.marker.getPosition()) {
                //@ts-ignore
                this.map.panTo(this.marker.getPosition());
            }
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            // event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition()?.lat();
            this.cordinate.longtitude = this.marker.getPosition()?.lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.viaControl.setValue(this.cordinate);
        this.cordinateChange?.emit(this.cordinate);
    }
    moveToMyLocation() {
        let self = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((location) => {
                if (self.marker) {
                    self.marker.setMap(null);
                }
                // self.deleteMarkers();
                self.cordinate.latitude = location.coords.latitude;
                self.cordinate.longtitude = location.coords.longitude;
                this.myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
                if (self.myLocation) {
                    self.myLocation.setMap(null);
                }
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    map: self.map,
                    draggable: false,
                    title: self.cordinate.title
                });
                self.geocodePosition(self.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                //self.addDragendListener();
                self.getCurrentLocationEvent.emit(new CordinateDto(location.coords.latitude, location.coords.longitude));
                this.map.setCenter(this.myLatLng);
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getRadius(distance = 100) {
        if (this.myLocation) {
            let w = this.map.getBounds()?.getSouthWest();
            let centerLng = this.myLocation.getPosition();
            if (centerLng && w) {
                distance = google.maps.geometry.spherical.computeDistanceBetween(centerLng, w);
            }
        }
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        // Get map bounds
        let bounds = this.map.getBounds();
        this.getTopCordinatorEvent.emit(new CordinateDto(bounds?.getNorthEast().lat(), myLatLng.lng(), "", eventName));
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle?.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MapComponent, selector: "via-map", inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox", requestLocation: "requestLocation", loading: "loading", myLocationMarker: "myLocationMarker", markerIcon: "markerIcon", shouldClusterer: "shouldClusterer", clustererImagepath: "clustererImagepath", iconMyLocationImg: "iconMyLocationImg", iconSearchImg: "iconSearchImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent", boundsChanged: "boundsChanged", centerChanged: "centerChanged", clickOnMap: "clickOnMap", dblClick: "dblClick", drag: "drag", dragEnd: "dragEnd", dragStart: "dragStart", idle: "idle", mouseMove: "mouseMove", mouseOut: "mouseOut", mouseOver: "mouseOver", tilesLoaded: "tilesLoaded", zoomChanged: "zoomChanged" }, viewQueries: [{ propertyName: "searchInput", first: true, predicate: ["searchInput"], descendants: true }, { propertyName: "googleMap", first: true, predicate: ["googleMap"], descendants: true }], ngImport: i0, template: "<div class=\"row\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"col-sm-12 map-search-container\">\n        <div class=\"form-group\">\n            <label *ngIf=\"i18n\" [attr.for]=\"mapId\">\n                {{i18n.label}}\n                <span class=\"danger-text\"> {{required(viaControl)}}</span>\n            </label>\n            <div class=\"via-map-search\">\n                <input \n                    type=\"text\" \n                    [attr.id]=\"mapId\"\n                    class=\"form-control\" \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <img *ngIf=\"iconSearchImg\" [src]=\"iconSearchImg\" class=\"input-search-icon\">\n                <i *ngIf=\"!iconSearchImg\"class=\"fas fa-search input-search-icon\" aria-hidden=\"true\"></i>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-sm-12\" style=\"position: relative;\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button  *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <i *ngIf=\"!iconMyLocationImg\" class=\"fas fa-location-arrow\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n</div>", styles: [".google-maps{height:300px;border-radius:3px}.my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:22px;bottom:110px}.my-location-btn img,.my-location-btn i{width:20px}.map-search-container{margin-bottom:10px}.map-search-container .danger-text{color:brown}.map-search-container .via-map-search{position:relative}.map-search-container .via-map-search input{padding-right:35px}.map-search-container .input-search-icon{width:20px;position:absolute;top:10px;right:10px}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-map', template: "<div class=\"row\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"col-sm-12 map-search-container\">\n        <div class=\"form-group\">\n            <label *ngIf=\"i18n\" [attr.for]=\"mapId\">\n                {{i18n.label}}\n                <span class=\"danger-text\"> {{required(viaControl)}}</span>\n            </label>\n            <div class=\"via-map-search\">\n                <input \n                    type=\"text\" \n                    [attr.id]=\"mapId\"\n                    class=\"form-control\" \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <img *ngIf=\"iconSearchImg\" [src]=\"iconSearchImg\" class=\"input-search-icon\">\n                <i *ngIf=\"!iconSearchImg\"class=\"fas fa-search input-search-icon\" aria-hidden=\"true\"></i>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-sm-12\" style=\"position: relative;\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button  *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <i *ngIf=\"!iconMyLocationImg\" class=\"fas fa-location-arrow\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n</div>", styles: [".google-maps{height:300px;border-radius:3px}.my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:22px;bottom:110px}.my-location-btn img,.my-location-btn i{width:20px}.map-search-container{margin-bottom:10px}.map-search-container .danger-text{color:brown}.map-search-container .via-map-search{position:relative}.map-search-container .via-map-search input{padding-right:35px}.map-search-container .input-search-icon{width:20px;position:absolute;top:10px;right:10px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { searchInput: [{
                type: ViewChild,
                args: ["searchInput"]
            }], googleMap: [{
                type: ViewChild,
                args: ["googleMap"]
            }], cordinate: [{
                type: Input
            }], searchPlaceholer: [{
                type: Input
            }], markerDraggable: [{
                type: Input
            }], showMyLocation: [{
                type: Input
            }], hideSearchTextBox: [{
                type: Input
            }], requestLocation: [{
                type: Input
            }], loading: [{
                type: Input
            }], myLocationMarker: [{
                type: Input
            }], markerIcon: [{
                type: Input
            }], shouldClusterer: [{
                type: Input
            }], clustererImagepath: [{
                type: Input
            }], iconMyLocationImg: [{
                type: Input
            }], iconSearchImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }], cordinateChange: [{
                type: Output
            }], markerClick: [{
                type: Output
            }], mapRadius: [{
                type: Output
            }], searchEvent: [{
                type: Output
            }], getTopCordinatorEvent: [{
                type: Output
            }], getCurrentLocationEvent: [{
                type: Output
            }], boundsChanged: [{
                type: Output
            }], centerChanged: [{
                type: Output
            }], clickOnMap: [{
                type: Output
            }], dblClick: [{
                type: Output
            }], drag: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], idle: [{
                type: Output
            }], mouseMove: [{
                type: Output
            }], mouseOut: [{
                type: Output
            }], mouseOver: [{
                type: Output
            }], tilesLoaded: [{
                type: Output
            }], zoomChanged: [{
                type: Output
            }] } });

class MapModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MapModule, declarations: [MapComponent], imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule], exports: [MapComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapModule, imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MapComponent],
                    imports: [
                        FormsModule,
                        CommonModule,
                        ReactiveFormsModule
                    ],
                    exports: [MapComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class MatMapComponent {
    constructor() {
        this.searchInput = undefined;
        this.googleMap = undefined;
        this.cordinate = new CordinateDto();
        this.searchPlaceholer = "";
        this.markerDraggable = true;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.requestLocation = true;
        this.loading = false;
        this.myLocationMarker = 'assets/icons/green_pin.svg';
        this.markerIcon = 'assets/icons/red_pin.svg';
        this.shouldClusterer = false;
        this.clustererImagepath = "assets/markerclusterer/m";
        this.iconMyLocationImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.boundsChanged = new EventEmitter();
        this.centerChanged = new EventEmitter();
        this.clickOnMap = new EventEmitter();
        this.dblClick = new EventEmitter();
        this.drag = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.idle = new EventEmitter();
        this.mouseMove = new EventEmitter();
        this.mouseOut = new EventEmitter();
        this.mouseOver = new EventEmitter();
        this.tilesLoaded = new EventEmitter();
        this.zoomChanged = new EventEmitter();
        this.latlng = undefined;
        this.myLatLng = undefined;
        this.map = undefined;
        this.myLocation = undefined;
        this.marker = undefined;
        this.searchBox = undefined;
        this.markers = [];
        this.activeInfoWindow = undefined;
        this.circle = undefined;
        this.myLocaltionLoaded = false;
        this.mapLoaded = false;
        this.markerCluster = undefined;
        this.geocoder = new google.maps.Geocoder();
    }
    instance() {
        return this.map;
    }
    formControlInstance() {
        return this.viaControl;
    }
    getCenterMapLocation() {
        return this.instance()?.getCenter();
    }
    getMarkerCluster() {
        return this.markerCluster;
    }
    ngAfterViewInit() {
        if (this.cordinate.latitude && this.cordinate.longtitude) {
            this.initMap();
        }
        else {
            if (this.requestLocation && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    this.cordinate.latitude = location.coords.latitude;
                    this.cordinate.longtitude = location.coords.longitude;
                    this.initMap();
                    this.myLocaltionLoaded = true;
                }, () => {
                    this.cordinate.latitude = 10.762622;
                    this.cordinate.longtitude = 106.660172;
                    this.initMap();
                });
            }
            else {
                this.cordinate.latitude = 10.762622;
                this.cordinate.longtitude = 106.660172;
                this.initMap();
            }
        }
    }
    bindingCordinate(cordinate) {
        if (cordinate.title === mapConfig.myLocation) {
            this.myLocation?.setMap(null);
            this.myLocation = undefined;
            setTimeout(() => {
                this.myLocation = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    map: this.map,
                    draggable: false
                });
            }, 0);
        }
        else {
            this.marker?.setMap(null);
            this.marker = undefined;
            setTimeout(() => {
                this.marker = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.markerIcon, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    map: this.map,
                    draggable: this.markerDraggable,
                    animation: google.maps.Animation.DROP,
                    title: cordinate.title
                });
            }, 0);
            this.addClickListener();
            this.addDragendListener();
        }
    }
    initMap() {
        if (this.googleMap) {
            this.myLatLng = new google.maps.LatLng(this.cordinate.latitude, this.cordinate.longtitude);
            let mapConfigObject = {
                zoom: 15,
                center: this.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            this.map = new google.maps.Map(this.googleMap.nativeElement, mapConfigObject);
            if (!this.hideSearchTextBox && this.searchInput) {
                this.searchBox = new google.maps.places.SearchBox(this.searchInput.nativeElement);
                this.addSearchBoxEvent();
            }
            this.bindingEvent();
            this.bindingCordinate(this.cordinate);
            this.geocodePosition(this.myLatLng);
            if (this.shouldClusterer) {
                // this.markerCluster = new MarkerClusterer(this.map, [], {
                //   imagePath: this.clustererImagepath,
                // });
                //use the svg path for image
                this.markerCluster = new MarkerClusterer({ map: this.map, markers: [] });
            }
        }
    }
    bindingEvent() {
        google.maps.event.addListener(this.map, 'bounds_changed', () => {
            let bounds = this.map.getBounds();
            if (bounds) {
                if (!this.hideSearchTextBox && this.searchInput) {
                    this.searchBox.setBounds(bounds);
                }
                this.boundsChanged?.emit(bounds);
            }
        });
        google.maps.event.addListener(this.map, 'center_changed', () => {
            let center = this.map.getCenter();
            this.centerChanged?.emit(center);
        });
        google.maps.event.addListener(this.map, 'click', (event) => {
            this.clickOnMap?.emit(event);
        });
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
            this.dblClick?.emit(event);
        });
        google.maps.event.addListener(this.map, 'drag', () => {
            this.drag?.emit("drag");
        });
        google.maps.event.addListener(this.map, 'dragend', () => {
            this.dragEnd?.emit("dragend");
        });
        google.maps.event.addListener(this.map, 'dragstart', () => {
            this.dragStart?.emit("dragstart");
        });
        google.maps.event.addListener(this.map, 'idle', () => {
            this.mapLoaded = true;
            this.idle?.emit("idle");
        });
        google.maps.event.addListener(this.map, 'mousemove', (event) => {
            this.mouseMove?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseout', (event) => {
            this.mouseOut?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseover', (event) => {
            this.mouseOver?.emit(event);
        });
        google.maps.event.addListener(this.map, "tilesloaded", () => {
            this.tilesLoaded?.emit("tilesloaded");
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.zoomChanged?.emit(this.map.getZoom());
        });
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        if (this.map && this.map.getBounds()) {
            let sW = this.map.getBounds().getSouthWest();
            let nE = this.map.getBounds().getNorthEast();
            corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
            corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
            corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
            corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
            cornersArray[corner1] = {
                "lat": sW.lat(),
                "lng": sW.lng()
            };
            cornersArray[corner2] = {
                "lat": nE.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner3] = {
                "lat": sW.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner4] = {
                "lat": nE.lat(),
                "lng": sW.lng()
            };
            farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
            cordinate = new CordinateDto(farestPoint.lat, farestPoint.lng, "", eventName);
            this.mapRadius?.emit(cordinate);
        }
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        if (this.shouldClusterer) {
            this.markerCluster?.clearMarkers();
        }
        this.markers = [];
    }
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                map: this.map
            };
            markObject["icon"] = {
                url: this.markerIcon, // url
                scaledSize: new google.maps.Size(30, 30), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (marker, i) => {
                return function () {
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    //@ts-ignore
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick?.emit(listLocation[i][3]);
                        }
                    });
                };
            });
        }
        if (this.shouldClusterer) {
            this.markerCluster?.addMarkers(this.markers);
        }
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map?.setZoom(zoomNumber);
        this.map?.panTo(myLatLng);
    }
    addMaker(cordinate) {
        if (cordinate.latitude && cordinate.longtitude) {
            let markObject = {
                position: new google.maps.LatLng(cordinate.latitude, cordinate.longtitude),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map
            };
            let marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            this.panTo(cordinate.latitude, cordinate.longtitude, this.map?.getZoom());
        }
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        //@ts-ignore
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = undefined;
                this.cordinate.longtitude = undefined;
            }
            this.triggerUpdateData(eventName);
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        this.searchBox.addListener('places_changed', () => {
            if (this.marker) {
                this.marker.setMap(null);
            }
            let places = this.searchBox.getPlaces();
            if (Array.isArray(places) && places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry || !place.geometry.location) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                let markerObj = {
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry ? place.geometry.location : undefined
                };
                this.marker = new google.maps.Marker(markerObj);
                this.addDragendListener();
                this.searchEvent?.emit(new CordinateDto(markerObj.position?.lat(), markerObj.position?.lng()));
                this.cordinate.latitude = place.geometry.location.lat();
                this.cordinate.longtitude = place.geometry.location.lng();
                this.cordinate.title = place.formatted_address;
                this.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map.fitBounds(bounds);
            this.map.setZoom(12);
            if (this.marker && this.marker.getPosition()) {
                //@ts-ignore
                this.map.panTo(this.marker.getPosition());
            }
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            // event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition()?.lat();
            this.cordinate.longtitude = this.marker.getPosition()?.lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.viaControl.setValue(this.cordinate);
        this.cordinateChange?.emit(this.cordinate);
    }
    moveToMyLocation() {
        if (!this.requestLocation) {
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((location) => {
                if (this.marker) {
                    this.marker.setMap(null);
                }
                // self.deleteMarkers();
                this.cordinate.latitude = location.coords.latitude;
                this.cordinate.longtitude = location.coords.longitude;
                this.myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
                if (this.myLocation) {
                    this.myLocation.setMap(null);
                }
                this.myLocation = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    map: this.map,
                    draggable: false,
                    title: this.cordinate.title
                });
                this.geocodePosition(this.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                this.getCurrentLocationEvent?.emit(new CordinateDto(location.coords.latitude, location.coords.longitude));
                this.map.setCenter(this.myLatLng);
                this.myLocaltionLoaded = true;
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        let bounds = this.map.getBounds();
        this.getTopCordinatorEvent?.emit(new CordinateDto(bounds?.getNorthEast().lat(), myLatLng.lng(), "", eventName));
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle?.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatMapComponent, selector: "via-mat-map", inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox", requestLocation: "requestLocation", loading: "loading", myLocationMarker: "myLocationMarker", markerIcon: "markerIcon", shouldClusterer: "shouldClusterer", clustererImagepath: "clustererImagepath", iconMyLocationImg: "iconMyLocationImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent", boundsChanged: "boundsChanged", centerChanged: "centerChanged", clickOnMap: "clickOnMap", dblClick: "dblClick", drag: "drag", dragEnd: "dragEnd", dragStart: "dragStart", idle: "idle", mouseMove: "mouseMove", mouseOut: "mouseOut", mouseOver: "mouseOver", tilesLoaded: "tilesLoaded", zoomChanged: "zoomChanged" }, viewQueries: [{ propertyName: "searchInput", first: true, predicate: ["searchInput"], descendants: true }, { propertyName: "googleMap", first: true, predicate: ["googleMap"], descendants: true }], ngImport: i0, template: "<div class=\"via-map-wrapper\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"map-search-container\">\n        <div id=\"via-map-search\">\n            <mat-form-field appearance=\"outline\">\n                <mat-label *ngIf=\"i18n\">{{i18n.label}} {{required(viaControl)}}</mat-label>\n                <input \n                    matInput \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <button matSuffix mat-icon-button>\n                  <mat-icon>search</mat-icon>\n                </button>\n            </mat-form-field>\n        </div>\n    </div>\n    <div class=\"\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <mat-icon *ngIf=\"!iconMyLocationImg\">my_location</mat-icon>\n        </button>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"loading\">\n        <mat-spinner strokeWidth=\"1\" diameter=\"20\"></mat-spinner>\n    </div>\n</div>", styles: [".via-map-wrapper{position:relative;display:flex;flex-direction:column}.via-map-wrapper .google-maps{height:300px;border-radius:3px}.via-map-wrapper mat-form-field{width:100%}.via-map-wrapper .my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:10px;bottom:110px}.via-map-wrapper .my-location-btn img{width:20px}.via-map-wrapper .map-search-container .input-search-icon{width:20px;position:absolute;top:13px;right:21px}.via-map-wrapper .spinner-wrapper{position:absolute;bottom:20px;right:60px;z-index:9999}.via-map-wrapper .spinner-wrapper mat-spinner{z-index:2}\n"], dependencies: [{ kind: "directive", type: i1$1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1$1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1$1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i7.MatFormField, selector: "mat-form-field", inputs: ["hideRequiredMarker", "color", "floatLabel", "appearance", "subscriptSizing", "hintLabel"], exportAs: ["matFormField"] }, { kind: "directive", type: i7.MatLabel, selector: "mat-label" }, { kind: "directive", type: i7.MatSuffix, selector: "[matSuffix], [matIconSuffix], [matTextSuffix]", inputs: ["matTextSuffix"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }, { kind: "component", type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "directive", type: i7$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["disabled", "id", "placeholder", "name", "required", "type", "errorStateMatcher", "aria-describedby", "value", "readonly"], exportAs: ["matInput"] }, { kind: "component", type: i5$1.MatProgressSpinner, selector: "mat-progress-spinner, mat-spinner", inputs: ["color", "mode", "value", "diameter", "strokeWidth"], exportAs: ["matProgressSpinner"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-mat-map', template: "<div class=\"via-map-wrapper\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"map-search-container\">\n        <div id=\"via-map-search\">\n            <mat-form-field appearance=\"outline\">\n                <mat-label *ngIf=\"i18n\">{{i18n.label}} {{required(viaControl)}}</mat-label>\n                <input \n                    matInput \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <button matSuffix mat-icon-button>\n                  <mat-icon>search</mat-icon>\n                </button>\n            </mat-form-field>\n        </div>\n    </div>\n    <div class=\"\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <mat-icon *ngIf=\"!iconMyLocationImg\">my_location</mat-icon>\n        </button>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"loading\">\n        <mat-spinner strokeWidth=\"1\" diameter=\"20\"></mat-spinner>\n    </div>\n</div>", styles: [".via-map-wrapper{position:relative;display:flex;flex-direction:column}.via-map-wrapper .google-maps{height:300px;border-radius:3px}.via-map-wrapper mat-form-field{width:100%}.via-map-wrapper .my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:10px;bottom:110px}.via-map-wrapper .my-location-btn img{width:20px}.via-map-wrapper .map-search-container .input-search-icon{width:20px;position:absolute;top:13px;right:21px}.via-map-wrapper .spinner-wrapper{position:absolute;bottom:20px;right:60px;z-index:9999}.via-map-wrapper .spinner-wrapper mat-spinner{z-index:2}\n"] }]
        }], ctorParameters: () => [], propDecorators: { searchInput: [{
                type: ViewChild,
                args: ["searchInput"]
            }], googleMap: [{
                type: ViewChild,
                args: ["googleMap"]
            }], cordinate: [{
                type: Input
            }], searchPlaceholer: [{
                type: Input
            }], markerDraggable: [{
                type: Input
            }], showMyLocation: [{
                type: Input
            }], hideSearchTextBox: [{
                type: Input
            }], requestLocation: [{
                type: Input
            }], loading: [{
                type: Input
            }], myLocationMarker: [{
                type: Input
            }], markerIcon: [{
                type: Input
            }], shouldClusterer: [{
                type: Input
            }], clustererImagepath: [{
                type: Input
            }], iconMyLocationImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }], cordinateChange: [{
                type: Output
            }], markerClick: [{
                type: Output
            }], mapRadius: [{
                type: Output
            }], searchEvent: [{
                type: Output
            }], getTopCordinatorEvent: [{
                type: Output
            }], getCurrentLocationEvent: [{
                type: Output
            }], boundsChanged: [{
                type: Output
            }], centerChanged: [{
                type: Output
            }], clickOnMap: [{
                type: Output
            }], dblClick: [{
                type: Output
            }], drag: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], idle: [{
                type: Output
            }], mouseMove: [{
                type: Output
            }], mouseOut: [{
                type: Output
            }], mouseOver: [{
                type: Output
            }], tilesLoaded: [{
                type: Output
            }], zoomChanged: [{
                type: Output
            }] } });

class MatMapModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatMapModule, declarations: [MatMapComponent], imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule,
            MatFormFieldModule,
            MatButtonModule,
            MatIconModule,
            MatInputModule,
            MatProgressSpinnerModule], exports: [MatMapComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapModule, imports: [FormsModule,
            CommonModule,
            ReactiveFormsModule,
            MatFormFieldModule,
            MatButtonModule,
            MatIconModule,
            MatInputModule,
            MatProgressSpinnerModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MatMapComponent],
                    imports: [
                        FormsModule,
                        CommonModule,
                        ReactiveFormsModule,
                        MatFormFieldModule,
                        MatButtonModule,
                        MatIconModule,
                        MatInputModule,
                        MatProgressSpinnerModule
                    ],
                    exports: [MatMapComponent]
                }]
        }] });

/*
 * Public API Surface of ngx-via
 */

class CarouselOptionDto {
    constructor() {
        this.autoAdjustSlidesPerView = true;
        this.centered = false;
        this.scaleCenter = false;
        /*
        breakpoints: {
            '(min-width: 720px) and (max-width: 1000px)': {
            loop: false,
            },
        } */
        this.breakpoints = null;
        this.controls = true;
        this.dragSpeed = 1;
        this.friction = 0.0025;
        this.loop = false;
        this.initial = 0;
        this.duration = 500;
        this.preventEvent = 'data-via-carousel-pe';
        this.slides = '.via-carousel-item';
        this.vertical = false;
        this.resetSlide = false;
        this.slidesPerView = 1;
        this.spacing = 0;
        this.mode = 'snap';
        this.rtl = false;
        this.rubberband = true;
        this.cancelOnLeave = true;
        this.nav = false;
        this.dots = false;
        this.autoInit = false;
    }
}

"use strict";
if (!Math.sign) {
    Math.sign = function (x) {
        return (x > 0) - (x < 0) || +x;
    };
}

function ViaCarousel(initialContainer, initialOptions = {}) {
    const attributeMoving = 'data-via-carousel-moves';
    const attributeVertical = 'data-via-carousel-v';
    let container;
    let events = [];
    let touchControls;
    let length;
    let origin;
    let slides;
    let width;
    let slidesPerView;
    let spacing;
    let resizeLastWidth;
    let breakpointCurrent = null;
    let optionsChanged = false;
    let sliderCreated = false;
    let trackCurrentIdx;
    let trackPosition = 0;
    let trackMeasurePoints = [];
    let trackDirection;
    let trackMeasureTimeout;
    let trackSpeed;
    let trackSlidePositions;
    let trackProgress;
    let options;
    // touch/swipe helper
    let touchIndexStart;
    let touchActive;
    let touchIdentifier;
    let touchLastX;
    let touchLastClientX;
    let touchLastClientY;
    let touchMultiplicator;
    let touchJustStarted;
    let touchSumDistance;
    let touchChecked;
    // animation
    let reqId;
    let startTime;
    let moveDistance;
    let moveDuration;
    let moveEasing;
    let moved;
    let moveForceFinish;
    let moveCallBack;
    function eventAdd(element, event, handler, options = {}) {
        element.addEventListener(event, handler, options);
        events.push([element, event, handler, options]);
    }
    function eventDrag(e) {
        if (!touchActive ||
            touchIdentifier !== eventGetIdentifier(e) ||
            !isTouchable())
            return;
        const x = eventGetX(e).x;
        if (!eventIsSlide(e) && touchJustStarted) {
            return eventDragStop(e);
        }
        if (touchJustStarted) {
            trackMeasureReset();
            touchLastX = x;
            touchJustStarted = false;
        }
        if (e.cancelable)
            e.preventDefault();
        const touchDistance = touchLastX - x;
        touchSumDistance += Math.abs(touchDistance);
        if (!touchChecked && touchSumDistance > 5) {
            touchChecked = true;
            container.setAttribute(attributeMoving, true);
        }
        trackAdd(touchMultiplicator(touchDistance, pubfuncs) * (!isRtl() ? 1 : -1), e.timeStamp);
        touchLastX = x;
    }
    function eventDragStart(e) {
        if (touchActive || !isTouchable() || eventIsIgnoreTarget(e.target))
            return;
        touchActive = true;
        touchJustStarted = true;
        touchIdentifier = eventGetIdentifier(e);
        touchChecked = false;
        touchSumDistance = 0;
        eventIsSlide(e);
        moveAnimateAbort();
        touchIndexStart = trackCurrentIdx;
        touchLastX = eventGetX(e).x;
        trackAdd(0, e.timeStamp);
        hook('dragStart');
    }
    function eventDragStop(e) {
        if (!touchActive ||
            touchIdentifier !== eventGetIdentifier(e, true) ||
            !isTouchable())
            return;
        container.removeAttribute(attributeMoving);
        touchActive = false;
        moveWithSpeed();
        hook('dragEnd');
    }
    function eventGetChangedTouches(e) {
        return e.changedTouches;
    }
    function eventGetIdentifier(e, changedTouches = false) {
        const touches = changedTouches
            ? eventGetChangedTouches(e)
            : eventGetTargetTouches(e);
        return !touches ? 'default' : touches[0] ? touches[0].identifier : 'error';
    }
    function eventGetTargetTouches(e) {
        return e.targetTouches;
    }
    function eventGetX(e) {
        const touches = eventGetTargetTouches(e);
        return {
            x: isVerticalSlider()
                ? !touches
                    ? e.pageY
                    : touches[0].screenY
                : !touches
                    ? e.pageX
                    : touches[0].screenX,
            timestamp: e.timeStamp,
        };
    }
    function eventIsIgnoreTarget(target) {
        return target.hasAttribute(options.preventEvent);
    }
    function eventIsSlide(e) {
        const touches = eventGetTargetTouches(e);
        if (!touches)
            return true;
        const touch = touches[0];
        const x = isVerticalSlider() ? touch.clientY : touch.clientX;
        const y = isVerticalSlider() ? touch.clientX : touch.clientY;
        const isSlide = touchLastClientX !== undefined &&
            touchLastClientY !== undefined &&
            Math.abs(touchLastClientY - y) <= Math.abs(touchLastClientX - x);
        touchLastClientX = x;
        touchLastClientY = y;
        return isSlide;
    }
    function eventWheel(e) {
        if (!isTouchable())
            return;
        if (touchActive)
            e.preventDefault();
    }
    function eventsAdd() {
        eventAdd(window, 'orientationchange', sliderResizeFix);
        eventAdd(window, 'resize', () => sliderResize());
        eventAdd(container, 'dragstart', function (e) {
            if (!isTouchable())
                return;
            e.preventDefault();
        });
        eventAdd(container, 'mousedown', eventDragStart);
        eventAdd(options.cancelOnLeave ? container : window, 'mousemove', eventDrag);
        if (options.cancelOnLeave)
            eventAdd(container, 'mouseleave', eventDragStop);
        eventAdd(window, 'mouseup', eventDragStop);
        eventAdd(container, 'touchstart', eventDragStart, {
            passive: true,
        });
        eventAdd(container, 'touchmove', eventDrag, {
            passive: false,
        });
        eventAdd(container, 'touchend', eventDragStop, {
            passive: true,
        });
        eventAdd(container, 'touchcancel', eventDragStop, {
            passive: true,
        });
        eventAdd(window, 'wheel', eventWheel, {
            passive: false,
        });
    }
    function eventsRemove() {
        events.forEach(event => {
            event[0].removeEventListener(event[1], event[2], event[3]);
        });
        events = [];
    }
    function hook(hook) {
        if (options[hook])
            options[hook](pubfuncs);
    }
    function isCenterMode() {
        return options.centered;
    }
    function isTouchable() {
        return touchControls !== undefined ? touchControls : options.controls;
    }
    function isLoop() {
        return options.loop && length > 1;
    }
    function isRtl() {
        return options.rtl;
    }
    function isRubberband() {
        return !options.loop && options.rubberband;
    }
    function isVerticalSlider() {
        return !!options.vertical;
    }
    function moveAnimate() {
        reqId = window.requestAnimationFrame(moveAnimateUpdate);
    }
    function moveAnimateAbort() {
        if (reqId) {
            window.cancelAnimationFrame(reqId);
            reqId = null;
        }
        startTime = null;
    }
    function moveAnimateUpdate(timestamp) {
        if (!startTime)
            startTime = timestamp;
        const duration = timestamp - startTime;
        let add = moveCalcValue(duration);
        if (duration >= moveDuration) {
            trackAdd(moveDistance - moved, false);
            if (moveCallBack)
                return moveCallBack();
            hook('afterChange');
            return;
        }
        const offset = trackCalculateOffset(add);
        if (offset !== 0 && !isLoop() && !isRubberband() && !moveForceFinish) {
            trackAdd(add - offset, false);
            return;
        }
        if (offset !== 0 && isRubberband() && !moveForceFinish) {
            return moveRubberband(Math.sign(offset));
        }
        moved += add;
        trackAdd(add, false);
        moveAnimate();
    }
    function moveCalcValue(progress) {
        const value = moveDistance * moveEasing(progress / moveDuration) - moved;
        return value;
    }
    function moveWithSpeed() {
        hook('beforeChange');
        switch (options.mode) {
            case 'free':
                moveFree();
                break;
            case 'free-snap':
                moveSnapFree();
                break;
            case 'snap':
            default:
                moveSnapOne();
                break;
        }
    }
    function moveSnapOne() {
        const startIndex = slidesPerView === 1 && trackDirection !== 0
            ? touchIndexStart
            : trackCurrentIdx;
        moveToIdx(startIndex + Math.sign(trackDirection));
    }
    function moveToIdx(idx, forceFinish, duration = options.duration, relative = false, nearest = false) {
        // forceFinish is used to ignore boundaries when rubberband movement is active
        idx = trackGetIdx(idx, relative, nearest);
        const easing = t => 1 + --t * t * t * t * t;
        moveTo(trackGetIdxDistance(idx), duration, easing, forceFinish);
    }
    function moveFree() {
        // todo: refactor!
        if (trackSpeed === 0)
            return trackCalculateOffset(0) && !isLoop()
                ? moveToIdx(trackCurrentIdx)
                : false;
        const friction = options.friction / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const duration = Math.abs(trackSpeed / friction) * 6;
        const easing = function (t) {
            return 1 - Math.pow(1 - t, 5);
        };
        moveTo(distance, duration, easing);
    }
    function moveSnapFree() {
        // todo: refactor!
        if (trackSpeed === 0)
            return moveToIdx(trackCurrentIdx);
        const friction = options.friction / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const duration = Math.abs(trackSpeed / friction) * 6;
        const easing = function (t) {
            return 1 - Math.pow(1 - t, 5);
        };
        const idx_trend = (trackPosition + distance) / (width / slidesPerView);
        const idx = trackDirection === -1 ? Math.floor(idx_trend) : Math.ceil(idx_trend);
        moveTo(idx * (width / slidesPerView) - trackPosition, duration, easing);
    }
    function moveRubberband() {
        moveAnimateAbort();
        // todo: refactor!
        if (trackSpeed === 0)
            return moveToIdx(trackCurrentIdx, true);
        const friction = 0.04 / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const easing = function (t) {
            return --t * t * t + 1;
        };
        const speed = trackSpeed;
        const cb = () => {
            moveTo(trackGetIdxDistance(trackGetIdx(trackCurrentIdx)), 500, easing, true);
        };
        moveTo(distance, Math.abs(speed / friction) * 3, easing, true, cb);
    }
    function moveTo(distance, duration, easing, forceFinish, cb) {
        moveAnimateAbort();
        moveDistance = distance;
        moved = 0;
        moveDuration = duration;
        moveEasing = easing;
        moveForceFinish = forceFinish;
        moveCallBack = cb;
        startTime = null;
        moveAnimate();
    }
    function sliderBind(force_resize) {
        let _container = getElements(initialContainer);
        if (!_container.length)
            return;
        container = _container[0];
        sliderResize(force_resize);
        eventsAdd();
        hook('mounted');
    }
    function sliderCheckBreakpoint() {
        const breakpoints = initialOptions.breakpoints || [];
        let lastValid;
        for (let value in breakpoints) {
            if (window.matchMedia(value).matches)
                lastValid = value;
        }
        if (lastValid === breakpointCurrent)
            return true;
        breakpointCurrent = lastValid;
        const _options = breakpointCurrent
            ? breakpoints[breakpointCurrent]
            : initialOptions;
        if (_options.breakpoints && breakpointCurrent)
            delete _options.breakpoints;
        options = { ...defaultOptions, ...initialOptions, ..._options };
        optionsChanged = true;
        resizeLastWidth = null;
        hook('optionsChanged');
        sliderRebind();
    }
    function sliderGetSlidesPerView(option) {
        if (typeof option === 'function')
            return option();
        const adjust = options.autoAdjustSlidesPerView;
        if (!adjust)
            length = Math.max(option, length);
        const max = isLoop() && adjust ? length - 1 : length;
        return clampValue(option, 1, Math.max(max, 1));
    }
    function sliderInit() {
        sliderCheckBreakpoint();
        sliderCreated = true;
        hook('created');
    }
    function sliderRebind(new_options, force_resize) {
        if (new_options)
            initialOptions = new_options;
        if (force_resize)
            breakpointCurrent = null;
        sliderUnbind();
        sliderBind(force_resize);
    }
    function sliderResize(force) {
        const windowWidth = window.innerWidth;
        if (!sliderCheckBreakpoint() || (windowWidth === resizeLastWidth && !force))
            return;
        resizeLastWidth = windowWidth;
        const optionSlides = options.slides;
        if (typeof optionSlides === 'number') {
            slides = null;
            length = optionSlides;
        }
        else {
            slides = getElements(optionSlides, container);
            length = slides ? slides.length : 0;
        }
        const dragSpeed = options.dragSpeed;
        touchMultiplicator =
            typeof dragSpeed === 'function' ? dragSpeed : val => val * dragSpeed;
        width = isVerticalSlider() ? container.offsetHeight : container.offsetWidth;
        slidesPerView = sliderGetSlidesPerView(options.slidesPerView);
        spacing = clampValue(options.spacing, 0, width / (slidesPerView - 1) - 1);
        width += spacing;
        origin = isCenterMode()
            ? (width / 2 - width / slidesPerView / 2) / width
            : 0;
        slidesSetWidths();
        const currentIdx = !sliderCreated || (optionsChanged && options.resetSlide)
            ? options.initial
            : trackCurrentIdx;
        trackSetPositionByIdx(isLoop() ? currentIdx : trackClampIndex(currentIdx));
        if (isVerticalSlider()) {
            container.setAttribute(attributeVertical, true);
        }
        optionsChanged = false;
    }
    function sliderResizeFix(force) {
        sliderResize();
        setTimeout(sliderResize, 500);
        setTimeout(sliderResize, 2000);
    }
    function sliderUnbind() {
        eventsRemove();
        slidesRemoveStyles();
        if (container && container.hasAttribute(attributeVertical))
            container.removeAttribute(attributeVertical);
        hook('destroyed');
    }
    function slidesSetPositions() {
        if (!slides)
            return;
        slides.forEach((slide, idx) => {
            const absoluteDistance = trackSlidePositions[idx].distance * width;
            const pos = absoluteDistance -
                idx *
                    (width / slidesPerView -
                        spacing / slidesPerView -
                        (spacing / slidesPerView) * (slidesPerView - 1));
            const x = isVerticalSlider() ? 0 : pos;
            const y = isVerticalSlider() ? pos : 0;
            const transformString = (options.scaleCenter && idx !== ((trackCurrentIdx % length) + length) % length) ? `translate3d(${x}px, ${y}px, 0) scale(0.8)` : `translate3d(${x}px, ${y}px, 0)`;
            slide.style.transform = transformString;
            slide.style.transition = 'all 0.4s ease';
            slide.style['-webkit-transform'] = transformString;
        });
    }
    function slidesSetWidths() {
        if (!slides)
            return;
        slides.forEach(slide => {
            const style = `calc(${100 / slidesPerView}% - ${(spacing / slidesPerView) * (slidesPerView - 1)}px)`;
            if (isVerticalSlider()) {
                slide.style['min-height'] = style;
                slide.style['max-height'] = style;
            }
            else {
                slide.style['min-width'] = style;
                slide.style['max-width'] = style;
            }
        });
    }
    function slidesRemoveStyles() {
        if (!slides)
            return;
        let styles = ['transform', '-webkit-transform'];
        styles = isVerticalSlider
            ? [...styles, 'min-height', 'max-height']
            : [...styles, 'min-width', 'max-width'];
        slides.forEach(slide => {
            styles.forEach(style => {
                slide.style.removeProperty(style);
            });
        });
    }
    function trackAdd(val, drag = true, timestamp = Date.now()) {
        trackMeasure(val, timestamp);
        if (drag)
            val = trackrubberband(val);
        trackPosition += val;
        trackMove();
    }
    function trackCalculateOffset(add) {
        const trackLength = (width * (length - 1 * (isCenterMode() ? 1 : slidesPerView))) /
            slidesPerView;
        const position = trackPosition + add;
        return position > trackLength
            ? position - trackLength
            : position < 0
                ? position
                : 0;
    }
    function trackClampIndex(idx) {
        return clampValue(idx, 0, length - 1 - (isCenterMode() ? 0 : slidesPerView - 1));
    }
    function trackGetDetails() {
        const trackProgressAbs = Math.abs(trackProgress);
        const progress = trackPosition < 0 ? 1 - trackProgressAbs : trackProgressAbs;
        return {
            direction: trackDirection,
            progressTrack: progress,
            progressSlides: (progress * length) / (length - 1),
            positions: trackSlidePositions,
            position: trackPosition,
            speed: trackSpeed,
            relativeSlide: ((trackCurrentIdx % length) + length) % length,
            absoluteSlide: trackCurrentIdx,
            size: length,
            slidesPerView,
            widthOrHeight: width,
        };
    }
    function trackGetIdx(idx, relative = false, nearest = false) {
        return !isLoop()
            ? trackClampIndex(idx)
            : !relative
                ? idx
                : trackGetRelativeIdx(idx, nearest);
    }
    function trackGetIdxDistance(idx) {
        return -(-((width / slidesPerView) * idx) + trackPosition);
    }
    function trackGetRelativeIdx(idx, nearest) {
        idx = ((idx % length) + length) % length;
        const current = ((trackCurrentIdx % length) + length) % length;
        const left = current < idx ? -current - length + idx : -(current - idx);
        const right = current > idx ? length - current + idx : idx - current;
        const add = nearest
            ? Math.abs(left) <= right
                ? left
                : right
            : idx < current
                ? left
                : right;
        return trackCurrentIdx + add;
    }
    function trackMeasure(val, timestamp) {
        // todo - improve measurement - it could be better for ios
        clearTimeout(trackMeasureTimeout);
        const direction = Math.sign(val);
        if (direction !== trackDirection)
            trackMeasureReset();
        trackDirection = direction;
        trackMeasurePoints.push({
            distance: val,
            time: timestamp,
        });
        trackMeasureTimeout = setTimeout(() => {
            trackMeasurePoints = [];
            trackSpeed = 0;
        }, 50);
        trackMeasurePoints = trackMeasurePoints.slice(-6);
        if (trackMeasurePoints.length <= 1 || trackDirection === 0)
            return (trackSpeed = 0);
        const distance = trackMeasurePoints
            .slice(0, -1)
            .reduce((acc, next) => acc + next.distance, 0);
        const end = trackMeasurePoints[trackMeasurePoints.length - 1].time;
        const start = trackMeasurePoints[0].time;
        trackSpeed = clampValue(distance / (end - start), -10, 10);
    }
    function trackMeasureReset() {
        trackMeasurePoints = [];
    }
    // todo - option for not calculating slides that are not in sight
    function trackMove() {
        trackProgress = isLoop()
            ? (trackPosition % ((width * length) / slidesPerView)) /
                ((width * length) / slidesPerView)
            : trackPosition / ((width * length) / slidesPerView);
        trackSetCurrentIdx();
        const slidePositions = [];
        for (let idx = 0; idx < length; idx++) {
            let distance = (((1 / length) * idx -
                (trackProgress < 0 && isLoop() ? trackProgress + 1 : trackProgress)) *
                length) /
                slidesPerView +
                origin;
            if (isLoop())
                distance +=
                    distance > (length - 1) / slidesPerView
                        ? -(length / slidesPerView)
                        : distance < -(length / slidesPerView) + 1
                            ? length / slidesPerView
                            : 0;
            const slideWidth = 1 / slidesPerView;
            const left = distance + slideWidth;
            const portion = left < slideWidth
                ? left / slideWidth
                : left > 1
                    ? 1 - ((left - 1) * slidesPerView) / 1
                    : 1;
            slidePositions.push({
                portion: portion < 0 || portion > 1 ? 0 : portion,
                distance: !isRtl() ? distance : distance * -1 + 1 - slideWidth,
            });
        }
        trackSlidePositions = slidePositions;
        slidesSetPositions();
        hook('move');
    }
    function trackrubberband(add) {
        if (isLoop())
            return add;
        const offset = trackCalculateOffset(add);
        if (!isRubberband())
            return add - offset;
        if (offset === 0)
            return add;
        const easing = t => (1 - Math.abs(t)) * (1 - Math.abs(t));
        return add * easing(offset / width);
    }
    function trackSetCurrentIdx() {
        const new_idx = Math.round(trackPosition / (width / slidesPerView));
        if (new_idx === trackCurrentIdx)
            return;
        if (!isLoop() && (new_idx < 0 || new_idx > length - 1))
            return;
        trackCurrentIdx = new_idx;
        hook('slideChanged');
    }
    function trackSetPositionByIdx(idx) {
        hook('beforeChange');
        trackAdd(trackGetIdxDistance(idx), false);
        hook('afterChange');
    }
    const defaultOptions = {
        autoAdjustSlidesPerView: true,
        centered: false,
        scaleCenter: false,
        breakpoints: null,
        controls: true,
        dragSpeed: 1,
        friction: 0.0025,
        loop: false,
        initial: 0,
        duration: 500,
        preventEvent: 'data-via-carousel-pe',
        slides: '.via-carousel__slide',
        vertical: false,
        resetSlide: false,
        slidesPerView: 1,
        spacing: 0,
        mode: 'snap',
        rtl: false,
        rubberband: true,
        cancelOnLeave: true,
    };
    const pubfuncs = {
        controls: active => {
            touchControls = active;
        },
        destroy: sliderUnbind,
        refresh(options) {
            sliderRebind(options, true);
        },
        next() {
            moveToIdx(trackCurrentIdx + 1, true);
        },
        prev() {
            moveToIdx(trackCurrentIdx - 1, true);
        },
        moveToSlide(idx, duration) {
            moveToIdx(idx, true, duration);
        },
        moveToSlideRelative(idx, nearest = false, duration) {
            moveToIdx(idx, true, duration, true, nearest);
        },
        resize() {
            sliderResize(true);
        },
        details() {
            return trackGetDetails();
        },
        options() {
            const opt = { ...options };
            delete opt.breakpoints;
            return opt;
        },
    };
    sliderInit();
    return pubfuncs;
}
// helper functions
function convertToArray(nodeList) {
    return Array.prototype.slice.call(nodeList);
}
function getElements(element, wrapper = document) {
    return typeof element === 'function'
        ? convertToArray(element())
        : typeof element === 'string'
            ? convertToArray(wrapper.querySelectorAll(element))
            : element instanceof HTMLElement !== false
                ? [element]
                : element instanceof NodeList !== false
                    ? element
                    : [];
}
function clampValue(value, min, max) {
    return Math.min(Math.max(value, min), max);
}

class CarouselComponent {
    constructor() {
        this.carouselElement = undefined;
        this.option = new CarouselOptionDto();
        this.afterChange = new EventEmitter();
        this.beforeChange = new EventEmitter();
        this.mounted = new EventEmitter();
        this.created = new EventEmitter();
        this.slideChanged = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.move = new EventEmitter();
        this.destroyed = new EventEmitter();
        this.carousel = undefined;
        this.activeIndex = 1;
        this.dotHelper = [];
    }
    ngOnInit() {
        this.activeIndex = this.option.initial;
    }
    ngAfterViewInit() {
        setTimeout(() => {
            if (this.option.autoInit) {
                this.init();
            }
        }, 0);
    }
    instance() {
        return this.carousel;
    }
    init() {
        this.carousel = ViaCarousel(this.carouselElement.nativeElement, {
            autoAdjustSlidesPerView: this.option.autoAdjustSlidesPerView,
            centered: this.option.centered,
            scaleCenter: this.option.scaleCenter,
            breakpoints: this.option.breakpoints,
            controls: this.option.controls,
            dragSpeed: this.option.dragSpeed,
            friction: this.option.friction,
            loop: this.option.loop,
            initial: this.option.initial,
            duration: this.option.duration,
            preventEvent: this.option.preventEvent,
            slides: this.option.slides,
            vertical: this.option.vertical,
            resetSlide: this.option.resetSlide,
            slidesPerView: this.option.slidesPerView,
            spacing: this.option.spacing,
            mode: this.option.mode,
            rtl: this.option.rtl,
            rubberband: this.option.rubberband,
            cancelOnLeave: this.option.cancelOnLeave,
            afterChange: (s) => {
                this.afterChange?.emit(s);
            },
            beforeChange: (s) => {
                this.beforeChange?.emit(s);
            },
            mounted: (s) => {
                this.mounted?.emit(s);
            },
            created: (s) => {
                this.created?.emit(s);
            },
            slideChanged: (s) => {
                this.dotHelper = [...Array(s.details().size).keys()];
                this.activeIndex = s.details().relativeSlide;
                this.slideChanged?.emit(s);
            },
            dragEnd: (s) => {
                this.dragEnd?.emit(s);
            },
            dragStart: (s) => {
                this.dragStart?.emit(s);
            },
            move: (s) => {
                this.move?.emit(s);
            },
            destroyed: (s) => {
                this.destroyed?.emit(s);
            }
        });
    }
    shouldShowDots() {
        return this.carousel && this.option.dots;
    }
    shouldShowNav() {
        return this.carousel && this.option.nav;
    }
    ngOnDestroy() {
        this.carousel?.destroy();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CarouselComponent, selector: "via-carousel", inputs: { option: "option" }, outputs: { afterChange: "afterChange", beforeChange: "beforeChange", mounted: "mounted", created: "created", slideChanged: "slideChanged", dragEnd: "dragEnd", dragStart: "dragStart", move: "move", destroyed: "destroyed" }, viewQueries: [{ propertyName: "carouselElement", first: true, predicate: ["carouselElement"], descendants: true }], ngImport: i0, template: "<div class=\"via-carousel-wrapper\">\n    <div class=\"via-carousel\" #carouselElement>\n        <ng-content></ng-content>\n    </div>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--left ' + (activeIndex === 0 ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.prev()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path\n        d=\"M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z\"/>\n    </svg>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--right ' +\n        (carousel!.details().size - 1 === activeIndex ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.next()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path d=\"M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z\" />\n    </svg>\n    <div class=\"dots\" *ngIf=\"shouldShowDots()\">\n        <button\n            *ngFor=\"let slide of dotHelper; let i = index\"\n            (click)=\"carousel!.moveToSlideRelative(i)\"\n            [class]=\"'dot ' + (i === activeIndex ? 'active' : '')\"\n        ></button>\n    </div>\n</div>", styles: [".via-carousel-wrapper{width:100%;height:100%;position:relative}.via-carousel-wrapper .via-carousel{display:flex;overflow:hidden;position:relative;-webkit-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}.via-carousel-wrapper .via-carousel-item{position:relative;overflow:hidden;width:100%;min-height:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-v]{flex-wrap:wrap}.via-carousel-wrapper .via-carousel[data-via-carousel-v] .via-carousel-wrapper .via-carousel-item{width:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-moves] *{pointer-events:none}.via-carousel-wrapper .dots{display:flex;padding:10px 0;justify-content:center}.via-carousel-wrapper .dot{border:none;width:10px;height:10px;background:#c5c5c5;border-radius:50%;margin:0 5px;padding:5px;cursor:pointer}.via-carousel-wrapper .dot:focus{outline:none}.via-carousel-wrapper .dot.active{background:var(--primary-color, #9a0000)}.via-carousel-wrapper .arrow{width:15px;height:15px;position:absolute;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);fill:gray;cursor:pointer}.via-carousel-wrapper .arrow--left{left:5px}.via-carousel-wrapper .arrow--right{left:auto;right:5px}.via-carousel-wrapper .arrow--disabled{fill:#8080804d}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-carousel", template: "<div class=\"via-carousel-wrapper\">\n    <div class=\"via-carousel\" #carouselElement>\n        <ng-content></ng-content>\n    </div>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--left ' + (activeIndex === 0 ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.prev()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path\n        d=\"M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z\"/>\n    </svg>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--right ' +\n        (carousel!.details().size - 1 === activeIndex ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.next()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path d=\"M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z\" />\n    </svg>\n    <div class=\"dots\" *ngIf=\"shouldShowDots()\">\n        <button\n            *ngFor=\"let slide of dotHelper; let i = index\"\n            (click)=\"carousel!.moveToSlideRelative(i)\"\n            [class]=\"'dot ' + (i === activeIndex ? 'active' : '')\"\n        ></button>\n    </div>\n</div>", styles: [".via-carousel-wrapper{width:100%;height:100%;position:relative}.via-carousel-wrapper .via-carousel{display:flex;overflow:hidden;position:relative;-webkit-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}.via-carousel-wrapper .via-carousel-item{position:relative;overflow:hidden;width:100%;min-height:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-v]{flex-wrap:wrap}.via-carousel-wrapper .via-carousel[data-via-carousel-v] .via-carousel-wrapper .via-carousel-item{width:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-moves] *{pointer-events:none}.via-carousel-wrapper .dots{display:flex;padding:10px 0;justify-content:center}.via-carousel-wrapper .dot{border:none;width:10px;height:10px;background:#c5c5c5;border-radius:50%;margin:0 5px;padding:5px;cursor:pointer}.via-carousel-wrapper .dot:focus{outline:none}.via-carousel-wrapper .dot.active{background:var(--primary-color, #9a0000)}.via-carousel-wrapper .arrow{width:15px;height:15px;position:absolute;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);fill:gray;cursor:pointer}.via-carousel-wrapper .arrow--left{left:5px}.via-carousel-wrapper .arrow--right{left:auto;right:5px}.via-carousel-wrapper .arrow--disabled{fill:#8080804d}\n"] }]
        }], ctorParameters: () => [], propDecorators: { carouselElement: [{
                type: ViewChild,
                args: ["carouselElement"]
            }], option: [{
                type: Input
            }], afterChange: [{
                type: Output
            }], beforeChange: [{
                type: Output
            }], mounted: [{
                type: Output
            }], created: [{
                type: Output
            }], slideChanged: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], move: [{
                type: Output
            }], destroyed: [{
                type: Output
            }] } });

class CarouselModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: CarouselModule, declarations: [CarouselComponent], imports: [CommonModule], exports: [CarouselComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselModule, imports: [CommonModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        CarouselComponent
                    ],
                    imports: [
                        CommonModule,
                    ],
                    exports: [
                        CarouselComponent,
                    ]
                }]
        }] });

class CarouselInfoDto {
    constructor() {
        this.direction = 0;
        this.progressTrack = 0;
        this.progressSlides = 0;
        this.positions = [];
        this.position = 0;
        this.speed = 0;
        this.relativeSlide = 0;
        this.absoluteSlide = 0;
        this.size = 0;
        this.widthOrHeight = 0;
    }
}

class CarouselPosition {
    constructor() {
        this.distance = 0;
        this.portion = 0;
    }
}

class CarouselInstanceDto {
    constructor() {
        this.controls = () => { };
        this.destroy = () => { };
        this.refresh = () => { };
        this.next = () => { };
        this.prev = () => { };
        this.moveToSlide = () => { };
        this.moveToSlideRelative = () => { };
        this.resize = () => { };
        this.details = () => new CarouselInfoDto();
        this.options = () => new CarouselOptionDto();
    }
}

let dto = new CarouselOptionDto();
dto.slidesPerView = 3;
dto.initial = 1;
dto.mode = "free-snap";
dto.spacing = 10;
dto.centered = true;
dto.scaleCenter = true;
dto.loop = false;
dto.nav = true;
dto.autoInit = true;
dto.breakpoints = {
    '(max-width: 641px)': {
        slidesPerView: 1,
    },
};
const CAROUSEL_CENTER_MODE = dto;

//wrapper of keen-slider

/*
 * Public API Surface of ngx-via
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ACTION_TYPE, CAROUSEL_CENTER_MODE, CELL_TYPE, COLUMN_TYPE, CarouselComponent, CarouselInfoDto, CarouselInstanceDto, CarouselModule, CarouselOptionDto, CarouselPosition, ClickOutSiteDirective, ClickOutSiteModule, ColumnFilterBaseDto, ColumnFilterMultDto, ColumnFilterTextDto, CordinateDto, DATE_FORMAT, DATE_FORMAT_WITHOUT_TIME, DateAdapter, DateParserFormatter, DatepickerComponent, DatepickerI18n, DatepickerI18nDto, DatepickerModule, DropdownComponent, DropdownI18nDto, DropdownItemDto, DropdownItemValueDto, DropdownModule, EntryPerpageDto, FILTER_ON_COLUMN_TYPE, FOUR_ONE, FilterPipe, FullTextSearchPipe, GoogleAnalyticsService, I18N_VALUES, I18nService, Image360Component, Image360Module, ImageOptionDto, ItemDto, MapComponent, MapI18nDto, MapModule, MatFilterPipe, MatImage360Component, MatImage360Module, MatMapComponent, MatMapModule, MatModalComponent, MatModalModule, MatTableComponent, MatTableModule, MatTypeaheadComponent, MatTypeaheadModule, MatUploadFileComponent, MatUploadFileModule, ModalComponent, ModalDto, ModalModule, ONE_ONE, ONE_THREE, SIXTEEN_NINE, SORT, TABLE_DATE_FORMAT, TableActionDto, TableBtnActionDto, TableCellDto, TableColumnDto, TableComponent, TableDndResultDto, TableFilterDto, TableI18nDto, TableModule, TableOptionDto, TableRowDto, TableRowValueDto, TableSortDto, TimepickerComponent, TimepickerI18nDto, TimepickerModule, ToastComponent, ToastModule, TypeaheadComponent, TypeaheadI18nDto, TypeaheadItemDto, TypeaheadItemValueDto, TypeaheadModule, TypeaheadOptionDto, UPLOAD_FILE_EVENT, UploadFileComponent, UploadFileI18nDto, UploadFileModule, UploadOptionDto, UploadResponseDto, allowedContentTypesUploadExel, allowedContentTypesUploadImg, allowedContentTypesUploadTxt, mapConfig, modalConfigs, uploadType };
//# sourceMappingURL=ngx-via.mjs.map
