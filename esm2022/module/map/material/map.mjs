import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { mapConfig } from '../constant';
import { CordinateDto } from "../dto/cordinate.dto";
import { MarkerClusterer } from '@googlemaps/markerclusterer';
import { FormControl } from '@angular/forms';
import { FormUtils } from '../../../utils/form.utils';
import { MapI18nDto } from '../dto/map.i18n.dto';
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/form-field";
import * as i4 from "@angular/material/button";
import * as i5 from "@angular/material/icon";
import * as i6 from "@angular/material/input";
import * as i7 from "@angular/material/progress-spinner";
export class MatMapComponent {
    constructor() {
        this.searchInput = undefined;
        this.googleMap = undefined;
        this.cordinate = new CordinateDto();
        this.searchPlaceholer = "";
        this.markerDraggable = true;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.requestLocation = true;
        this.loading = false;
        this.myLocationMarker = 'assets/icons/green_pin.svg';
        this.markerIcon = 'assets/icons/red_pin.svg';
        this.shouldClusterer = false;
        this.clustererImagepath = "assets/markerclusterer/m";
        this.iconMyLocationImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.boundsChanged = new EventEmitter();
        this.centerChanged = new EventEmitter();
        this.clickOnMap = new EventEmitter();
        this.dblClick = new EventEmitter();
        this.drag = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.idle = new EventEmitter();
        this.mouseMove = new EventEmitter();
        this.mouseOut = new EventEmitter();
        this.mouseOver = new EventEmitter();
        this.tilesLoaded = new EventEmitter();
        this.zoomChanged = new EventEmitter();
        this.latlng = undefined;
        this.myLatLng = undefined;
        this.map = undefined;
        this.myLocation = undefined;
        this.marker = undefined;
        this.searchBox = undefined;
        this.markers = [];
        this.activeInfoWindow = undefined;
        this.circle = undefined;
        this.myLocaltionLoaded = false;
        this.mapLoaded = false;
        this.markerCluster = undefined;
        this.geocoder = new google.maps.Geocoder();
    }
    instance() {
        return this.map;
    }
    formControlInstance() {
        return this.viaControl;
    }
    getCenterMapLocation() {
        return this.instance()?.getCenter();
    }
    getMarkerCluster() {
        return this.markerCluster;
    }
    ngAfterViewInit() {
        if (this.cordinate.latitude && this.cordinate.longtitude) {
            this.initMap();
        }
        else {
            if (this.requestLocation && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    this.cordinate.latitude = location.coords.latitude;
                    this.cordinate.longtitude = location.coords.longitude;
                    this.initMap();
                    this.myLocaltionLoaded = true;
                }, () => {
                    this.cordinate.latitude = 10.762622;
                    this.cordinate.longtitude = 106.660172;
                    this.initMap();
                });
            }
            else {
                this.cordinate.latitude = 10.762622;
                this.cordinate.longtitude = 106.660172;
                this.initMap();
            }
        }
    }
    bindingCordinate(cordinate) {
        if (cordinate.title === mapConfig.myLocation) {
            this.myLocation?.setMap(null);
            this.myLocation = undefined;
            setTimeout(() => {
                this.myLocation = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    map: this.map,
                    draggable: false
                });
            }, 0);
        }
        else {
            this.marker?.setMap(null);
            this.marker = undefined;
            setTimeout(() => {
                this.marker = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.markerIcon, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    map: this.map,
                    draggable: this.markerDraggable,
                    animation: google.maps.Animation.DROP,
                    title: cordinate.title
                });
            }, 0);
            this.addClickListener();
            this.addDragendListener();
        }
    }
    initMap() {
        if (this.googleMap) {
            this.myLatLng = new google.maps.LatLng(this.cordinate.latitude, this.cordinate.longtitude);
            let mapConfigObject = {
                zoom: 15,
                center: this.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            this.map = new google.maps.Map(this.googleMap.nativeElement, mapConfigObject);
            if (!this.hideSearchTextBox && this.searchInput) {
                this.searchBox = new google.maps.places.SearchBox(this.searchInput.nativeElement);
                this.addSearchBoxEvent();
            }
            this.bindingEvent();
            this.bindingCordinate(this.cordinate);
            this.geocodePosition(this.myLatLng);
            if (this.shouldClusterer) {
                // this.markerCluster = new MarkerClusterer(this.map, [], {
                //   imagePath: this.clustererImagepath,
                // });
                //use the svg path for image
                this.markerCluster = new MarkerClusterer({ map: this.map, markers: [] });
            }
        }
    }
    bindingEvent() {
        google.maps.event.addListener(this.map, 'bounds_changed', () => {
            let bounds = this.map.getBounds();
            if (bounds) {
                if (!this.hideSearchTextBox && this.searchInput) {
                    this.searchBox.setBounds(bounds);
                }
                this.boundsChanged?.emit(bounds);
            }
        });
        google.maps.event.addListener(this.map, 'center_changed', () => {
            let center = this.map.getCenter();
            this.centerChanged?.emit(center);
        });
        google.maps.event.addListener(this.map, 'click', (event) => {
            this.clickOnMap?.emit(event);
        });
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
            this.dblClick?.emit(event);
        });
        google.maps.event.addListener(this.map, 'drag', () => {
            this.drag?.emit("drag");
        });
        google.maps.event.addListener(this.map, 'dragend', () => {
            this.dragEnd?.emit("dragend");
        });
        google.maps.event.addListener(this.map, 'dragstart', () => {
            this.dragStart?.emit("dragstart");
        });
        google.maps.event.addListener(this.map, 'idle', () => {
            this.mapLoaded = true;
            this.idle?.emit("idle");
        });
        google.maps.event.addListener(this.map, 'mousemove', (event) => {
            this.mouseMove?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseout', (event) => {
            this.mouseOut?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseover', (event) => {
            this.mouseOver?.emit(event);
        });
        google.maps.event.addListener(this.map, "tilesloaded", () => {
            this.tilesLoaded?.emit("tilesloaded");
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.zoomChanged?.emit(this.map.getZoom());
        });
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        if (this.map && this.map.getBounds()) {
            let sW = this.map.getBounds().getSouthWest();
            let nE = this.map.getBounds().getNorthEast();
            corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
            corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
            corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
            corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
            cornersArray[corner1] = {
                "lat": sW.lat(),
                "lng": sW.lng()
            };
            cornersArray[corner2] = {
                "lat": nE.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner3] = {
                "lat": sW.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner4] = {
                "lat": nE.lat(),
                "lng": sW.lng()
            };
            farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
            cordinate = new CordinateDto(farestPoint.lat, farestPoint.lng, "", eventName);
            this.mapRadius?.emit(cordinate);
        }
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        if (this.shouldClusterer) {
            this.markerCluster?.clearMarkers();
        }
        this.markers = [];
    }
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                map: this.map
            };
            markObject["icon"] = {
                url: this.markerIcon, // url
                scaledSize: new google.maps.Size(30, 30), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (marker, i) => {
                return function () {
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    //@ts-ignore
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick?.emit(listLocation[i][3]);
                        }
                    });
                };
            });
        }
        if (this.shouldClusterer) {
            this.markerCluster?.addMarkers(this.markers);
        }
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map?.setZoom(zoomNumber);
        this.map?.panTo(myLatLng);
    }
    addMaker(cordinate) {
        if (cordinate.latitude && cordinate.longtitude) {
            let markObject = {
                position: new google.maps.LatLng(cordinate.latitude, cordinate.longtitude),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map
            };
            let marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            this.panTo(cordinate.latitude, cordinate.longtitude, this.map?.getZoom());
        }
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        //@ts-ignore
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = undefined;
                this.cordinate.longtitude = undefined;
            }
            this.triggerUpdateData(eventName);
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        this.searchBox.addListener('places_changed', () => {
            if (this.marker) {
                this.marker.setMap(null);
            }
            let places = this.searchBox.getPlaces();
            if (Array.isArray(places) && places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry || !place.geometry.location) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                let markerObj = {
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry ? place.geometry.location : undefined
                };
                this.marker = new google.maps.Marker(markerObj);
                this.addDragendListener();
                this.searchEvent?.emit(new CordinateDto(markerObj.position?.lat(), markerObj.position?.lng()));
                this.cordinate.latitude = place.geometry.location.lat();
                this.cordinate.longtitude = place.geometry.location.lng();
                this.cordinate.title = place.formatted_address;
                this.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map.fitBounds(bounds);
            this.map.setZoom(12);
            if (this.marker && this.marker.getPosition()) {
                //@ts-ignore
                this.map.panTo(this.marker.getPosition());
            }
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            // event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition()?.lat();
            this.cordinate.longtitude = this.marker.getPosition()?.lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.viaControl.setValue(this.cordinate);
        this.cordinateChange?.emit(this.cordinate);
    }
    moveToMyLocation() {
        if (!this.requestLocation) {
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((location) => {
                if (this.marker) {
                    this.marker.setMap(null);
                }
                // self.deleteMarkers();
                this.cordinate.latitude = location.coords.latitude;
                this.cordinate.longtitude = location.coords.longitude;
                this.myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
                if (this.myLocation) {
                    this.myLocation.setMap(null);
                }
                this.myLocation = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    map: this.map,
                    draggable: false,
                    title: this.cordinate.title
                });
                this.geocodePosition(this.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                this.getCurrentLocationEvent?.emit(new CordinateDto(location.coords.latitude, location.coords.longitude));
                this.map.setCenter(this.myLatLng);
                this.myLocaltionLoaded = true;
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        let bounds = this.map.getBounds();
        this.getTopCordinatorEvent?.emit(new CordinateDto(bounds?.getNorthEast().lat(), myLatLng.lng(), "", eventName));
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle?.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MatMapComponent, selector: "via-mat-map", inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox", requestLocation: "requestLocation", loading: "loading", myLocationMarker: "myLocationMarker", markerIcon: "markerIcon", shouldClusterer: "shouldClusterer", clustererImagepath: "clustererImagepath", iconMyLocationImg: "iconMyLocationImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent", boundsChanged: "boundsChanged", centerChanged: "centerChanged", clickOnMap: "clickOnMap", dblClick: "dblClick", drag: "drag", dragEnd: "dragEnd", dragStart: "dragStart", idle: "idle", mouseMove: "mouseMove", mouseOut: "mouseOut", mouseOver: "mouseOver", tilesLoaded: "tilesLoaded", zoomChanged: "zoomChanged" }, viewQueries: [{ propertyName: "searchInput", first: true, predicate: ["searchInput"], descendants: true }, { propertyName: "googleMap", first: true, predicate: ["googleMap"], descendants: true }], ngImport: i0, template: "<div class=\"via-map-wrapper\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"map-search-container\">\n        <div id=\"via-map-search\">\n            <mat-form-field appearance=\"outline\">\n                <mat-label *ngIf=\"i18n\">{{i18n.label}} {{required(viaControl)}}</mat-label>\n                <input \n                    matInput \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <button matSuffix mat-icon-button>\n                  <mat-icon>search</mat-icon>\n                </button>\n            </mat-form-field>\n        </div>\n    </div>\n    <div class=\"\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <mat-icon *ngIf=\"!iconMyLocationImg\">my_location</mat-icon>\n        </button>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"loading\">\n        <mat-spinner strokeWidth=\"1\" diameter=\"20\"></mat-spinner>\n    </div>\n</div>", styles: [".via-map-wrapper{position:relative;display:flex;flex-direction:column}.via-map-wrapper .google-maps{height:300px;border-radius:3px}.via-map-wrapper mat-form-field{width:100%}.via-map-wrapper .my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:10px;bottom:110px}.via-map-wrapper .my-location-btn img{width:20px}.via-map-wrapper .map-search-container .input-search-icon{width:20px;position:absolute;top:13px;right:21px}.via-map-wrapper .spinner-wrapper{position:absolute;bottom:20px;right:60px;z-index:9999}.via-map-wrapper .spinner-wrapper mat-spinner{z-index:2}\n"], dependencies: [{ kind: "directive", type: i1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i3.MatFormField, selector: "mat-form-field", inputs: ["hideRequiredMarker", "color", "floatLabel", "appearance", "subscriptSizing", "hintLabel"], exportAs: ["matFormField"] }, { kind: "directive", type: i3.MatLabel, selector: "mat-label" }, { kind: "directive", type: i3.MatSuffix, selector: "[matSuffix], [matIconSuffix], [matTextSuffix]", inputs: ["matTextSuffix"] }, { kind: "component", type: i4.MatIconButton, selector: "button[mat-icon-button]", exportAs: ["matButton"] }, { kind: "component", type: i5.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "directive", type: i6.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["disabled", "id", "placeholder", "name", "required", "type", "errorStateMatcher", "aria-describedby", "value", "readonly"], exportAs: ["matInput"] }, { kind: "component", type: i7.MatProgressSpinner, selector: "mat-progress-spinner, mat-spinner", inputs: ["color", "mode", "value", "diameter", "strokeWidth"], exportAs: ["matProgressSpinner"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatMapComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-mat-map', template: "<div class=\"via-map-wrapper\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"map-search-container\">\n        <div id=\"via-map-search\">\n            <mat-form-field appearance=\"outline\">\n                <mat-label *ngIf=\"i18n\">{{i18n.label}} {{required(viaControl)}}</mat-label>\n                <input \n                    matInput \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <button matSuffix mat-icon-button>\n                  <mat-icon>search</mat-icon>\n                </button>\n            </mat-form-field>\n        </div>\n    </div>\n    <div class=\"\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <mat-icon *ngIf=\"!iconMyLocationImg\">my_location</mat-icon>\n        </button>\n    </div>\n\n    <div class=\"spinner-wrapper\" *ngIf=\"loading\">\n        <mat-spinner strokeWidth=\"1\" diameter=\"20\"></mat-spinner>\n    </div>\n</div>", styles: [".via-map-wrapper{position:relative;display:flex;flex-direction:column}.via-map-wrapper .google-maps{height:300px;border-radius:3px}.via-map-wrapper mat-form-field{width:100%}.via-map-wrapper .my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:10px;bottom:110px}.via-map-wrapper .my-location-btn img{width:20px}.via-map-wrapper .map-search-container .input-search-icon{width:20px;position:absolute;top:13px;right:21px}.via-map-wrapper .spinner-wrapper{position:absolute;bottom:20px;right:60px;z-index:9999}.via-map-wrapper .spinner-wrapper mat-spinner{z-index:2}\n"] }]
        }], ctorParameters: () => [], propDecorators: { searchInput: [{
                type: ViewChild,
                args: ["searchInput"]
            }], googleMap: [{
                type: ViewChild,
                args: ["googleMap"]
            }], cordinate: [{
                type: Input
            }], searchPlaceholer: [{
                type: Input
            }], markerDraggable: [{
                type: Input
            }], showMyLocation: [{
                type: Input
            }], hideSearchTextBox: [{
                type: Input
            }], requestLocation: [{
                type: Input
            }], loading: [{
                type: Input
            }], myLocationMarker: [{
                type: Input
            }], markerIcon: [{
                type: Input
            }], shouldClusterer: [{
                type: Input
            }], clustererImagepath: [{
                type: Input
            }], iconMyLocationImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }], cordinateChange: [{
                type: Output
            }], markerClick: [{
                type: Output
            }], mapRadius: [{
                type: Output
            }], searchEvent: [{
                type: Output
            }], getTopCordinatorEvent: [{
                type: Output
            }], getCurrentLocationEvent: [{
                type: Output
            }], boundsChanged: [{
                type: Output
            }], centerChanged: [{
                type: Output
            }], clickOnMap: [{
                type: Output
            }], dblClick: [{
                type: Output
            }], drag: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], idle: [{
                type: Output
            }], mouseMove: [{
                type: Output
            }], mouseOut: [{
                type: Output
            }], mouseOver: [{
                type: Output
            }], tilesLoaded: [{
                type: Output
            }], zoomChanged: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL21hcC9tYXRlcmlhbC9tYXAudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvbWFwL21hdGVyaWFsL21hcC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7Ozs7OztBQU9qRCxNQUFNLE9BQU8sZUFBZTtJQXNEMUI7UUFwRDBCLGdCQUFXLEdBQWtDLFNBQVMsQ0FBQztRQUN6RCxjQUFTLEdBQTZCLFNBQVMsQ0FBQztRQUUvRCxjQUFTLEdBQWlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0MscUJBQWdCLEdBQVcsRUFBRSxDQUFDO1FBQzlCLG9CQUFlLEdBQVksSUFBSSxDQUFDO1FBQ2hDLG1CQUFjLEdBQVksSUFBSSxDQUFDO1FBQy9CLHNCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNuQyxvQkFBZSxHQUFZLElBQUksQ0FBQztRQUNoQyxZQUFPLEdBQVksS0FBSyxDQUFDO1FBQ3pCLHFCQUFnQixHQUFXLDRCQUE0QixDQUFDO1FBQ3hELGVBQVUsR0FBVywwQkFBMEIsQ0FBQztRQUNoRCxvQkFBZSxHQUFZLEtBQUssQ0FBQztRQUNqQyx1QkFBa0IsR0FBVywwQkFBMEIsQ0FBQztRQUN4RCxzQkFBaUIsR0FBWSxTQUFTLENBQUM7UUFDdkMsZUFBVSxHQUFnQixJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQzVDLFNBQUksR0FBZ0IsU0FBUyxDQUFDO1FBRTdCLG9CQUFlLEdBQStCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakUsZ0JBQVcsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RCxjQUFTLEdBQStCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0QsZ0JBQVcsR0FBK0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3RCwwQkFBcUIsR0FBK0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RSw0QkFBdUIsR0FBK0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RSxrQkFBYSxHQUEyQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNFLGtCQUFhLEdBQXFDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckUsZUFBVSxHQUE0QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pFLGFBQVEsR0FBNEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RSxTQUFJLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDaEQsWUFBTyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ25ELGNBQVMsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyRCxTQUFJLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDaEQsY0FBUyxHQUE0QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3hFLGFBQVEsR0FBNEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RSxjQUFTLEdBQTRDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDeEUsZ0JBQVcsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RCxnQkFBVyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBRWpFLFdBQU0sR0FBd0IsU0FBUyxDQUFDO1FBRXhDLGFBQVEsR0FBd0IsU0FBUyxDQUFDO1FBQzFDLFFBQUcsR0FBcUIsU0FBUyxDQUFDO1FBQ2xDLGVBQVUsR0FBd0IsU0FBUyxDQUFDO1FBQzVDLFdBQU0sR0FBd0IsU0FBUyxDQUFDO1FBQ3hDLGNBQVMsR0FBa0MsU0FBUyxDQUFDO1FBQ3JELFlBQU8sR0FBOEIsRUFBRSxDQUFDO1FBQ3hDLHFCQUFnQixHQUE0QixTQUFTLENBQUM7UUFDdEQsV0FBTSxHQUF3QixTQUFTLENBQUM7UUFDeEMsc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0Isa0JBQWEsR0FBcUIsU0FBUyxDQUFDO1FBRzFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFFRCxRQUFRO1FBQ04sT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xCLENBQUM7SUFFRCxtQkFBbUI7UUFDakIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELGdCQUFnQjtRQUNkLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN6RCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDakIsQ0FBQzthQUFNLENBQUM7WUFDTixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNsRCxTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO29CQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztvQkFDdEQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUNmLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLENBQUMsRUFBRSxHQUFHLEVBQUU7b0JBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO29CQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDakIsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO2lCQUFNLENBQUM7Z0JBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNqQixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxTQUF1QjtRQUN0QyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1lBQzVCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLElBQUksRUFBRTt3QkFDSixHQUFHLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLE1BQU07d0JBQ2xDLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO3dCQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUzt3QkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7cUJBQzlDO29CQUNELEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsS0FBSztpQkFDakIsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ1IsQ0FBQzthQUFNLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztZQUN4QixVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbkMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixJQUFJLEVBQUU7d0JBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTTt3QkFDNUIsVUFBVSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLGNBQWM7d0JBQ3hELE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxTQUFTO3dCQUM5QyxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUztxQkFDOUM7b0JBQ0QsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO29CQUNiLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZTtvQkFDL0IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7b0JBQ3JDLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSztpQkFDdkIsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDNUIsQ0FBQztJQUNILENBQUM7SUFFRCxPQUFPO1FBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVyxDQUFDLENBQUM7WUFDN0YsSUFBSSxlQUFlLEdBQUc7Z0JBQ3BCLElBQUksRUFBRSxFQUFFO2dCQUNSLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUTtnQkFDckIsaUJBQWlCLEVBQUUsS0FBSztnQkFDeEIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87Z0JBQ3hDLGNBQWMsRUFBRSxLQUFLO2FBQ3RCLENBQUM7WUFFRixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFFOUUsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDM0IsQ0FBQztZQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXRDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3BDLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN6QiwyREFBMkQ7Z0JBQzNELHdDQUF3QztnQkFDeEMsTUFBTTtnQkFFTiw0QkFBNEI7Z0JBQzVCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFlLENBQUMsRUFBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQztZQUN6RSxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFRCxZQUFZO1FBQ1YsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFO1lBQzlELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDbkMsSUFBSSxNQUFNLEVBQUUsQ0FBQztnQkFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDaEQsSUFBSSxDQUFDLFNBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3BDLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkMsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFO1lBQzlELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQyxLQUFnQyxFQUFFLEVBQUU7WUFDckYsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxVQUFVLEVBQUUsQ0FBQyxLQUFnQyxFQUFFLEVBQUU7WUFDeEYsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFO1lBQ3BELElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRTtZQUN2RCxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLFdBQVcsRUFBRSxHQUFHLEVBQUU7WUFDekQsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFO1lBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3pGLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3hGLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3pGLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsYUFBYSxFQUFFLEdBQUcsRUFBRTtZQUMzRCxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLGNBQWMsRUFBRSxHQUFHLEVBQUU7WUFDNUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQzlDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUE0QixDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsU0FBaUI7UUFDdEUsSUFBSSxTQUF1QixFQUFFLFlBQVksR0FBUSxFQUFFLEVBQUUsT0FBZSxFQUFFLE9BQWUsRUFBRSxPQUFlLEVBQUUsT0FBZSxFQUFFLFdBQTBDLENBQUM7UUFDcEssSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQztZQUNyQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzlDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFHLENBQUMsWUFBWSxFQUFFLENBQUM7WUFFOUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5SSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDOUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTlJLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUVELFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLFNBQVMsR0FBRyxJQUFJLFlBQVksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQzlFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xDLENBQUM7SUFDSCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUN0RSxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3ZJLENBQUM7SUFFRCxtRUFBbUU7SUFDbkUsYUFBYTtRQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsYUFBYSxFQUFFLFlBQVksRUFBRSxDQUFDO1FBQ3JDLENBQUM7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsc0JBQXNCLENBQUMsZUFBMkIsRUFBRTtRQUNsRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUM1QyxJQUFJLE1BQTBCLEVBQUUsQ0FBUyxDQUFDO1FBQzFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN6QyxlQUFlO1lBQ2YsSUFBSSxVQUFVLEdBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxFQUFFO2dCQUNWLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRzthQUNkLENBQUM7WUFFRixVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUc7Z0JBQ25CLEdBQUcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07Z0JBQzVCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO2dCQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUztnQkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7YUFDOUMsQ0FBQztZQUNGLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLENBQUMsTUFBMEIsRUFBRSxDQUFTLEVBQUUsRUFBRTtnQkFDdkYsT0FBTztvQkFDTCxJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUUsWUFBWTtvQkFDWixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFNBQXlCLEVBQUUsRUFBRTt3QkFDeEUsSUFBSSxPQUFlLENBQUM7d0JBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDeEIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztnQ0FDdEMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzs0QkFDM0MsQ0FBQztpQ0FBTSxDQUFDO2dDQUNOLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQzs0QkFDekQsQ0FBQzt3QkFDSCxDQUFDOzZCQUFNLENBQUM7NEJBQ04sT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDL0IsQ0FBQzt3QkFFRCxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUN2RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDO3dCQUNuQyxVQUFVLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUMvQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBRWxDLHlDQUF5Qzt3QkFDekMsSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDdkIsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzdDLENBQUM7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDO1FBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9DLENBQUM7SUFDSCxDQUFDO0lBRUQsS0FBSyxDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsYUFBcUIsRUFBRTtRQUNyRCxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsUUFBUSxDQUFDLFNBQXVCO1FBQzlCLElBQUksU0FBUyxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDL0MsSUFBSSxVQUFVLEdBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQztnQkFDMUUsVUFBVSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQztnQkFDeEMsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFO29CQUNKLEdBQUcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07b0JBQzVCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO29CQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUztvQkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7aUJBQzlDO2dCQUNELEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRzthQUNkLENBQUM7WUFFRixJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUM1RSxDQUFDO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxHQUFRLEVBQUUsWUFBZ0MsU0FBUyxFQUFFLE9BQXdCLElBQUk7UUFDL0YsWUFBWTtRQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsU0FBeUIsRUFBRSxFQUFFO1lBQ3JFLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ25FLENBQUM7aUJBQU0sQ0FBQztnQkFDTixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyw0Q0FBNEMsQ0FBQztnQkFDcEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7WUFDeEMsQ0FBQztZQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNsQyxJQUFJLElBQUksRUFBRSxDQUFDO2dCQUNULElBQUksRUFBRSxDQUFDO1lBQ1QsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlCQUFpQjtRQUNmLElBQUksQ0FBQyxTQUFVLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLEdBQUcsRUFBRTtZQUNqRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0IsQ0FBQztZQUVELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFVLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDekMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFLENBQUM7Z0JBQ2hELE9BQU87WUFDVCxDQUFDO1lBRUQsSUFBSSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzVDLE1BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7b0JBQ25ELE9BQU87Z0JBQ1QsQ0FBQztnQkFFRCxJQUFJLFNBQVMsR0FBRztvQkFDZCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUMvQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUk7b0JBQ2pCLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJO29CQUNyQyxRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVM7aUJBQy9ELENBQUM7Z0JBRUYsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUVoRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQkFFMUIsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFFL0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUMxRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsaUJBQWlCLENBQUM7Z0JBRS9DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUV2RCxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQzVCLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEMsQ0FBQztxQkFBTSxDQUFDO29CQUNOLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDekMsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLEdBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLEdBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQztnQkFDN0MsWUFBWTtnQkFDWixJQUFJLENBQUMsR0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDN0MsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdCQUFnQjtRQUNkLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLEtBQWdDLEVBQUUsRUFBRTtZQUNyRix5QkFBeUI7WUFDekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtnQkFDeEUsSUFBSSxDQUFDLE1BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbkMsUUFBUSxFQUFFLEtBQUssQ0FBQyxNQUFNO29CQUN0QixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUUvQixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO2lCQUM1QixDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFPLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRTtZQUMxRCwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3JGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFDNUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNoRSxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxTQUE2QjtRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDckMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUMxQixPQUFPO1FBQ1QsQ0FBQztRQUNELElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQzFCLFNBQVMsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDcEQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQixDQUFDO2dCQUNELHdCQUF3QjtnQkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUV0RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFFNUYsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQixDQUFDO2dCQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDdkMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixJQUFJLEVBQUU7d0JBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNO3dCQUNsQyxVQUFVLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsY0FBYzt3QkFDeEQsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLFNBQVM7d0JBQzlDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTO3FCQUNoRDtvQkFDRCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLEtBQUs7b0JBQ2hCLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUs7aUJBQzVCLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDeEYsSUFBSSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7Z0JBQ3pHLElBQUksQ0FBQyxHQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7YUFBTSxDQUFDO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1FBQy9ELENBQUM7SUFDSCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxTQUFrQjtRQUNuRSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDakIsT0FBTztRQUNULENBQUM7UUFDRCxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBRUQsVUFBVSxDQUFDLFNBQWlCLEVBQUUsU0FBaUIsRUFBRSxRQUFnQixFQUFFLFFBQWdCLEVBQUUsWUFBb0IsTUFBTTtRQUM3RyxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM1RCxpQkFBaUI7UUFDakIsSUFBSSxLQUFLLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDdkQsSUFBSSxhQUFhLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMzRixJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDM0IsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ25DLFdBQVcsRUFBRSxNQUFNO1lBQ25CLGFBQWEsRUFBRSxHQUFHO1lBQ2xCLFlBQVksRUFBRSxDQUFDO1lBQ2YsU0FBUyxFQUFFLFNBQVM7WUFDcEIsV0FBVyxFQUFFLElBQUk7WUFDakIsUUFBUSxFQUFFLElBQUk7WUFDZCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7WUFDYixNQUFNLEVBQUUsUUFBUTtZQUNoQixNQUFNLEVBQUUsTUFBTTtTQUNmLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRLENBQUMsSUFBaUI7UUFDeEIsT0FBTyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hELENBQUM7OEdBOWhCVSxlQUFlO2tHQUFmLGVBQWUsaXVDQ2I1Qiw4ckNBMkJNOzsyRkRkTyxlQUFlO2tCQUwzQixTQUFTOytCQUNFLGFBQWE7d0RBTUcsV0FBVztzQkFBcEMsU0FBUzt1QkFBQyxhQUFhO2dCQUNBLFNBQVM7c0JBQWhDLFNBQVM7dUJBQUMsV0FBVztnQkFFYixTQUFTO3NCQUFqQixLQUFLO2dCQUNHLGdCQUFnQjtzQkFBeEIsS0FBSztnQkFDRyxlQUFlO3NCQUF2QixLQUFLO2dCQUNHLGNBQWM7c0JBQXRCLEtBQUs7Z0JBQ0csaUJBQWlCO3NCQUF6QixLQUFLO2dCQUNHLGVBQWU7c0JBQXZCLEtBQUs7Z0JBQ0csT0FBTztzQkFBZixLQUFLO2dCQUNHLGdCQUFnQjtzQkFBeEIsS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLGVBQWU7c0JBQXZCLEtBQUs7Z0JBQ0csa0JBQWtCO3NCQUExQixLQUFLO2dCQUNHLGlCQUFpQjtzQkFBekIsS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFFSSxlQUFlO3NCQUF4QixNQUFNO2dCQUNHLFdBQVc7c0JBQXBCLE1BQU07Z0JBQ0csU0FBUztzQkFBbEIsTUFBTTtnQkFDRyxXQUFXO3NCQUFwQixNQUFNO2dCQUNHLHFCQUFxQjtzQkFBOUIsTUFBTTtnQkFDRyx1QkFBdUI7c0JBQWhDLE1BQU07Z0JBQ0csYUFBYTtzQkFBdEIsTUFBTTtnQkFDRyxhQUFhO3NCQUF0QixNQUFNO2dCQUNHLFVBQVU7c0JBQW5CLE1BQU07Z0JBQ0csUUFBUTtzQkFBakIsTUFBTTtnQkFDRyxJQUFJO3NCQUFiLE1BQU07Z0JBQ0csT0FBTztzQkFBaEIsTUFBTTtnQkFDRyxTQUFTO3NCQUFsQixNQUFNO2dCQUNHLElBQUk7c0JBQWIsTUFBTTtnQkFDRyxTQUFTO3NCQUFsQixNQUFNO2dCQUNHLFFBQVE7c0JBQWpCLE1BQU07Z0JBQ0csU0FBUztzQkFBbEIsTUFBTTtnQkFDRyxXQUFXO3NCQUFwQixNQUFNO2dCQUNHLFdBQVc7c0JBQXBCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBtYXBDb25maWcgfSBmcm9tICcuLi9jb25zdGFudCc7XG5pbXBvcnQgeyBDb3JkaW5hdGVEdG8gfSBmcm9tIFwiLi4vZHRvL2NvcmRpbmF0ZS5kdG9cIjtcbmltcG9ydCB7IE1hcmtlckNsdXN0ZXJlciB9IGZyb20gJ0Bnb29nbGVtYXBzL21hcmtlcmNsdXN0ZXJlcic7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEZvcm1VdGlscyB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2Zvcm0udXRpbHMnO1xuaW1wb3J0IHsgTWFwSTE4bkR0byB9IGZyb20gJy4uL2R0by9tYXAuaTE4bi5kdG8nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd2aWEtbWF0LW1hcCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9tYXAuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21hcC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTWF0TWFwQ29tcG9uZW50IHtcblxuICBAVmlld0NoaWxkKFwic2VhcmNoSW5wdXRcIikgc2VhcmNoSW5wdXQ/OiBFbGVtZW50UmVmPEhUTUxJbnB1dEVsZW1lbnQ+ID0gdW5kZWZpbmVkO1xuICBAVmlld0NoaWxkKFwiZ29vZ2xlTWFwXCIpIGdvb2dsZU1hcD86IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+ID0gdW5kZWZpbmVkO1xuXG4gIEBJbnB1dCgpIGNvcmRpbmF0ZTogQ29yZGluYXRlRHRvID0gbmV3IENvcmRpbmF0ZUR0bygpO1xuICBASW5wdXQoKSBzZWFyY2hQbGFjZWhvbGVyOiBzdHJpbmcgPSBcIlwiO1xuICBASW5wdXQoKSBtYXJrZXJEcmFnZ2FibGU6IGJvb2xlYW4gPSB0cnVlO1xuICBASW5wdXQoKSBzaG93TXlMb2NhdGlvbjogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIGhpZGVTZWFyY2hUZXh0Qm94OiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIHJlcXVlc3RMb2NhdGlvbjogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgbXlMb2NhdGlvbk1hcmtlcjogc3RyaW5nID0gJ2Fzc2V0cy9pY29ucy9ncmVlbl9waW4uc3ZnJztcbiAgQElucHV0KCkgbWFya2VySWNvbjogc3RyaW5nID0gJ2Fzc2V0cy9pY29ucy9yZWRfcGluLnN2Zyc7XG4gIEBJbnB1dCgpIHNob3VsZENsdXN0ZXJlcjogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBjbHVzdGVyZXJJbWFnZXBhdGg6IHN0cmluZyA9IFwiYXNzZXRzL21hcmtlcmNsdXN0ZXJlci9tXCI7XG4gIEBJbnB1dCgpIGljb25NeUxvY2F0aW9uSW1nPzogc3RyaW5nID0gdW5kZWZpbmVkO1xuICBASW5wdXQoKSB2aWFDb250cm9sOiBGb3JtQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xuICBASW5wdXQoKSBpMThuPzogTWFwSTE4bkR0byA9IHVuZGVmaW5lZDtcblxuICBAT3V0cHV0KCkgY29yZGluYXRlQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q29yZGluYXRlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1hcmtlckNsaWNrOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1hcFJhZGl1czogRXZlbnRFbWl0dGVyPENvcmRpbmF0ZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBzZWFyY2hFdmVudDogRXZlbnRFbWl0dGVyPENvcmRpbmF0ZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBnZXRUb3BDb3JkaW5hdG9yRXZlbnQ6IEV2ZW50RW1pdHRlcjxDb3JkaW5hdGVEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZ2V0Q3VycmVudExvY2F0aW9uRXZlbnQ6IEV2ZW50RW1pdHRlcjxDb3JkaW5hdGVEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgYm91bmRzQ2hhbmdlZDogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjZW50ZXJDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTGF0TG5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNsaWNrT25NYXA6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGRibENsaWNrOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBkcmFnOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGRyYWdFbmQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZHJhZ1N0YXJ0OiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGlkbGU6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgbW91c2VNb3ZlOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtb3VzZU91dDogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgbW91c2VPdmVyOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSB0aWxlc0xvYWRlZDogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSB6b29tQ2hhbmdlZDogRXZlbnRFbWl0dGVyPG51bWJlcj4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgbGF0bG5nPzogZ29vZ2xlLm1hcHMuTGF0TG5nID0gdW5kZWZpbmVkO1xuICBnZW9jb2RlcjogZ29vZ2xlLm1hcHMuR2VvY29kZXI7XG4gIG15TGF0TG5nPzogZ29vZ2xlLm1hcHMuTGF0TG5nID0gdW5kZWZpbmVkO1xuICBtYXA/OiBnb29nbGUubWFwcy5NYXAgPSB1bmRlZmluZWQ7XG4gIG15TG9jYXRpb24/OiBnb29nbGUubWFwcy5NYXJrZXIgPSB1bmRlZmluZWQ7XG4gIG1hcmtlcj86IGdvb2dsZS5tYXBzLk1hcmtlciA9IHVuZGVmaW5lZDtcbiAgc2VhcmNoQm94PzogZ29vZ2xlLm1hcHMucGxhY2VzLlNlYXJjaEJveCA9IHVuZGVmaW5lZDtcbiAgbWFya2VyczogQXJyYXk8Z29vZ2xlLm1hcHMuTWFya2VyPiA9IFtdO1xuICBhY3RpdmVJbmZvV2luZG93PzogZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyA9IHVuZGVmaW5lZDtcbiAgY2lyY2xlPzogZ29vZ2xlLm1hcHMuQ2lyY2xlID0gdW5kZWZpbmVkO1xuICBteUxvY2FsdGlvbkxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBtYXBMb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgbWFya2VyQ2x1c3Rlcj86IE1hcmtlckNsdXN0ZXJlciA9IHVuZGVmaW5lZDtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyKCk7XG4gIH1cblxuICBpbnN0YW5jZSgpOiBnb29nbGUubWFwcy5NYXAgfCB1bmRlZmluZWQge1xuICAgIHJldHVybiB0aGlzLm1hcDtcbiAgfVxuXG4gIGZvcm1Db250cm9sSW5zdGFuY2UoKSB7XG4gICAgcmV0dXJuIHRoaXMudmlhQ29udHJvbDtcbiAgfVxuXG4gIGdldENlbnRlck1hcExvY2F0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLmluc3RhbmNlKCk/LmdldENlbnRlcigpO1xuICB9XG5cbiAgZ2V0TWFya2VyQ2x1c3RlcigpIHtcbiAgICByZXR1cm4gdGhpcy5tYXJrZXJDbHVzdGVyO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIGlmICh0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSAmJiB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlKSB7XG4gICAgICB0aGlzLmluaXRNYXAoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMucmVxdWVzdExvY2F0aW9uICYmIG5hdmlnYXRvci5nZW9sb2NhdGlvbikge1xuICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChsb2NhdGlvbikgPT4ge1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxhdGl0dWRlO1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuICAgICAgICAgIHRoaXMuaW5pdE1hcCgpO1xuICAgICAgICAgIHRoaXMubXlMb2NhbHRpb25Mb2FkZWQgPSB0cnVlO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSAxMC43NjI2MjI7XG4gICAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IDEwNi42NjAxNzI7XG4gICAgICAgICAgdGhpcy5pbml0TWFwKCk7XG4gICAgICAgIH0pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IDEwLjc2MjYyMjtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IDEwNi42NjAxNzI7XG4gICAgICAgIHRoaXMuaW5pdE1hcCgpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGJpbmRpbmdDb3JkaW5hdGUoY29yZGluYXRlOiBDb3JkaW5hdGVEdG8pIHsgXG4gICAgaWYgKGNvcmRpbmF0ZS50aXRsZSA9PT0gbWFwQ29uZmlnLm15TG9jYXRpb24pIHtcbiAgICAgIHRoaXMubXlMb2NhdGlvbj8uc2V0TWFwKG51bGwpO1xuICAgICAgdGhpcy5teUxvY2F0aW9uID0gdW5kZWZpbmVkO1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7IFxuICAgICAgICB0aGlzLm15TG9jYXRpb24gPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICBwb3NpdGlvbjogdGhpcy5teUxhdExuZyxcbiAgICAgICAgICBpY29uOiB7XG4gICAgICAgICAgICB1cmw6IHRoaXMubXlMb2NhdGlvbk1hcmtlciwgLy8gdXJsXG4gICAgICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLCAvLyBzY2FsZWQgc2l6ZVxuICAgICAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksIC8vIG9yaWdpblxuICAgICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCkgLy8gYW5jaG9yXG4gICAgICAgICAgfSxcbiAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgIGRyYWdnYWJsZTogZmFsc2VcbiAgICAgICAgfSk7XG4gICAgICB9LCAwKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tYXJrZXI/LnNldE1hcChudWxsKTtcbiAgICAgIHRoaXMubWFya2VyID0gdW5kZWZpbmVkO1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7IFxuICAgICAgICB0aGlzLm1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgIHBvc2l0aW9uOiB0aGlzLm15TGF0TG5nLFxuICAgICAgICAgIGljb246IHtcbiAgICAgICAgICAgIHVybDogdGhpcy5tYXJrZXJJY29uLCAvLyB1cmxcbiAgICAgICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDMwLCAzMCksIC8vIHNjYWxlZCBzaXplXG4gICAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgICAgICB9LFxuICAgICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiB0aGlzLm1hcmtlckRyYWdnYWJsZSxcbiAgICAgICAgICBhbmltYXRpb246IGdvb2dsZS5tYXBzLkFuaW1hdGlvbi5EUk9QLFxuICAgICAgICAgIHRpdGxlOiBjb3JkaW5hdGUudGl0bGVcbiAgICAgICAgfSk7XG4gICAgICB9LCAwKTtcbiAgICAgIHRoaXMuYWRkQ2xpY2tMaXN0ZW5lcigpO1xuICAgICAgdGhpcy5hZGREcmFnZW5kTGlzdGVuZXIoKTtcbiAgICB9XG4gIH1cblxuICBpbml0TWFwKCkge1xuICAgIGlmICh0aGlzLmdvb2dsZU1hcCkge1xuICAgICAgdGhpcy5teUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcodGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUhLCB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlISk7XG4gICAgICBsZXQgbWFwQ29uZmlnT2JqZWN0ID0ge1xuICAgICAgICB6b29tOiAxNSxcbiAgICAgICAgY2VudGVyOiB0aGlzLm15TGF0TG5nLFxuICAgICAgICBzdHJlZXRWaWV3Q29udHJvbDogZmFsc2UsXG4gICAgICAgIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlJPQURNQVAsXG4gICAgICAgIG1hcFR5cGVDb250cm9sOiBmYWxzZVxuICAgICAgfTtcbiAgXG4gICAgICB0aGlzLm1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAodGhpcy5nb29nbGVNYXAubmF0aXZlRWxlbWVudCwgbWFwQ29uZmlnT2JqZWN0KTtcbiAgXG4gICAgICBpZiAoIXRoaXMuaGlkZVNlYXJjaFRleHRCb3ggJiYgdGhpcy5zZWFyY2hJbnB1dCkge1xuICAgICAgICB0aGlzLnNlYXJjaEJveCA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94KHRoaXMuc2VhcmNoSW5wdXQubmF0aXZlRWxlbWVudCk7XG4gICAgICAgIHRoaXMuYWRkU2VhcmNoQm94RXZlbnQoKTtcbiAgICAgIH1cbiAgXG4gICAgICB0aGlzLmJpbmRpbmdFdmVudCgpO1xuICAgICAgdGhpcy5iaW5kaW5nQ29yZGluYXRlKHRoaXMuY29yZGluYXRlKTtcbiAgICAgIFxuICAgICAgdGhpcy5nZW9jb2RlUG9zaXRpb24odGhpcy5teUxhdExuZyk7XG4gICAgICBpZiAodGhpcy5zaG91bGRDbHVzdGVyZXIpIHtcbiAgICAgICAgLy8gdGhpcy5tYXJrZXJDbHVzdGVyID0gbmV3IE1hcmtlckNsdXN0ZXJlcih0aGlzLm1hcCwgW10sIHtcbiAgICAgICAgLy8gICBpbWFnZVBhdGg6IHRoaXMuY2x1c3RlcmVySW1hZ2VwYXRoLFxuICAgICAgICAvLyB9KTtcblxuICAgICAgICAvL3VzZSB0aGUgc3ZnIHBhdGggZm9yIGltYWdlXG4gICAgICAgIHRoaXMubWFya2VyQ2x1c3RlciA9IG5ldyBNYXJrZXJDbHVzdGVyZXIoe21hcDogdGhpcy5tYXAsIG1hcmtlcnM6IFtdfSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgYmluZGluZ0V2ZW50KCkge1xuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2JvdW5kc19jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgbGV0IGJvdW5kcyA9IHRoaXMubWFwIS5nZXRCb3VuZHMoKTtcbiAgICAgIGlmIChib3VuZHMpIHtcbiAgICAgICAgaWYgKCF0aGlzLmhpZGVTZWFyY2hUZXh0Qm94ICYmIHRoaXMuc2VhcmNoSW5wdXQpIHtcbiAgICAgICAgICB0aGlzLnNlYXJjaEJveCEuc2V0Qm91bmRzKGJvdW5kcyk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5ib3VuZHNDaGFuZ2VkPy5lbWl0KGJvdW5kcyk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdjZW50ZXJfY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIGxldCBjZW50ZXIgPSB0aGlzLm1hcCEuZ2V0Q2VudGVyKCk7XG4gICAgICB0aGlzLmNlbnRlckNoYW5nZWQ/LmVtaXQoY2VudGVyKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2NsaWNrJywgKGV2ZW50OiBnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLmNsaWNrT25NYXA/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnZGJsY2xpY2snLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMuZGJsQ2xpY2s/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnZHJhZycsICgpID0+IHtcbiAgICAgIHRoaXMuZHJhZz8uZW1pdChcImRyYWdcIik7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdkcmFnZW5kJywgKCkgPT4ge1xuICAgICAgdGhpcy5kcmFnRW5kPy5lbWl0KFwiZHJhZ2VuZFwiKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2RyYWdzdGFydCcsICgpID0+IHtcbiAgICAgIHRoaXMuZHJhZ1N0YXJ0Py5lbWl0KFwiZHJhZ3N0YXJ0XCIpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnaWRsZScsICgpID0+IHtcbiAgICAgIHRoaXMubWFwTG9hZGVkID0gdHJ1ZTtcbiAgICAgIHRoaXMuaWRsZT8uZW1pdChcImlkbGVcIik7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdtb3VzZW1vdmUnLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMubW91c2VNb3ZlPy5lbWl0KGV2ZW50KTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ21vdXNlb3V0JywgKGV2ZW50OiBnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm1vdXNlT3V0Py5lbWl0KGV2ZW50KTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ21vdXNlb3ZlcicsIChldmVudDogZ29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudCkgPT4ge1xuICAgICAgdGhpcy5tb3VzZU92ZXI/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCBcInRpbGVzbG9hZGVkXCIsICgpID0+IHtcbiAgICAgIHRoaXMudGlsZXNMb2FkZWQ/LmVtaXQoXCJ0aWxlc2xvYWRlZFwiKTtcbiAgICB9KTtcbiAgICBcbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICd6b29tX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICB0aGlzLnpvb21DaGFuZ2VkPy5lbWl0KHRoaXMubWFwIS5nZXRab29tKCkpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0RnVydGhlc3RQb2ludEZyb21Mb2NhdGlvbihsYXQ6IG51bWJlciwgbG5nOiBudW1iZXIsIGV2ZW50TmFtZTogc3RyaW5nKSB7XG4gICAgbGV0IGNvcmRpbmF0ZTogQ29yZGluYXRlRHRvLCBjb3JuZXJzQXJyYXk6IGFueSA9IHt9LCBjb3JuZXIxOiBudW1iZXIsIGNvcm5lcjI6IG51bWJlciwgY29ybmVyMzogbnVtYmVyLCBjb3JuZXI0OiBudW1iZXIsIGZhcmVzdFBvaW50OiB7IGxhdDogbnVtYmVyOyBsbmc6IG51bWJlcjsgfTtcbiAgICBpZiAodGhpcy5tYXAgJiYgdGhpcy5tYXAuZ2V0Qm91bmRzKCkpIHtcbiAgICAgIGxldCBzVyA9IHRoaXMubWFwLmdldEJvdW5kcygpIS5nZXRTb3V0aFdlc3QoKTtcbiAgICAgIGxldCBuRSA9IHRoaXMubWFwLmdldEJvdW5kcygpIS5nZXROb3J0aEVhc3QoKTtcbiAgXG4gICAgICBjb3JuZXIxID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcoc1cubGF0KCksIHNXLmxuZygpKSk7XG4gICAgICBjb3JuZXIyID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcobkUubGF0KCksIG5FLmxuZygpKSk7XG4gICAgICBjb3JuZXIzID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcoc1cubGF0KCksIG5FLmxuZygpKSk7XG4gICAgICBjb3JuZXI0ID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcobkUubGF0KCksIHNXLmxuZygpKSk7XG4gIFxuICAgICAgY29ybmVyc0FycmF5W2Nvcm5lcjFdID0ge1xuICAgICAgICBcImxhdFwiOiBzVy5sYXQoKSxcbiAgICAgICAgXCJsbmdcIjogc1cubG5nKClcbiAgICAgIH1cbiAgICAgIGNvcm5lcnNBcnJheVtjb3JuZXIyXSA9IHtcbiAgICAgICAgXCJsYXRcIjogbkUubGF0KCksXG4gICAgICAgIFwibG5nXCI6IG5FLmxuZygpXG4gICAgICB9XG4gICAgICBjb3JuZXJzQXJyYXlbY29ybmVyM10gPSB7XG4gICAgICAgIFwibGF0XCI6IHNXLmxhdCgpLFxuICAgICAgICBcImxuZ1wiOiBuRS5sbmcoKVxuICAgICAgfVxuICAgICAgY29ybmVyc0FycmF5W2Nvcm5lcjRdID0ge1xuICAgICAgICBcImxhdFwiOiBuRS5sYXQoKSxcbiAgICAgICAgXCJsbmdcIjogc1cubG5nKClcbiAgICAgIH1cbiAgXG4gICAgICBmYXJlc3RQb2ludCA9IGNvcm5lcnNBcnJheVtNYXRoLm1heChjb3JuZXIxLCBjb3JuZXIyLCBjb3JuZXIzLCBjb3JuZXI0KV07XG4gICAgICBjb3JkaW5hdGUgPSBuZXcgQ29yZGluYXRlRHRvKGZhcmVzdFBvaW50LmxhdCwgZmFyZXN0UG9pbnQubG5nLCBcIlwiLCBldmVudE5hbWUpO1xuICAgICAgdGhpcy5tYXBSYWRpdXM/LmVtaXQoY29yZGluYXRlKTtcbiAgICB9XG4gIH1cblxuICBjYWxjdWxhdGVEaXN0YW5jZShhTGF0OiBudW1iZXIsIGFMbmc6IG51bWJlciwgYkxhdDogbnVtYmVyLCBiTG5nOiBudW1iZXIpIHtcbiAgICByZXR1cm4gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhhTGF0LCBhTG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhiTGF0LCBiTG5nKSk7XG4gIH1cblxuICAvLyBEZWxldGVzIGFsbCBtYXJrZXJzIGluIHRoZSBhcnJheSBieSByZW1vdmluZyByZWZlcmVuY2VzIHRvIHRoZW0uXG4gIGRlbGV0ZU1hcmtlcnMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMubWFya2Vyc1tpXS5zZXRNYXAobnVsbCk7XG4gICAgfVxuICAgIGlmICh0aGlzLnNob3VsZENsdXN0ZXJlcikge1xuICAgICAgdGhpcy5tYXJrZXJDbHVzdGVyPy5jbGVhck1hcmtlcnMoKTtcbiAgICB9XG4gICAgdGhpcy5tYXJrZXJzID0gW107XG4gIH1cblxuICBzaG93TXVsdGl0cGxlTG9jYXRpb25zKGxpc3RMb2NhdGlvbjogQXJyYXk8YW55PiA9IFtdKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCBpbmZvd2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3c7XG4gICAgbGV0IG1hcmtlcjogZ29vZ2xlLm1hcHMuTWFya2VyLCBpOiBudW1iZXI7XG4gICAgdGhpcy5kZWxldGVNYXJrZXJzKCk7XG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgbGlzdExvY2F0aW9uLmxlbmd0aDsgaSsrKSB7XG4gICAgICAvL21hcmtlciBDT05GSUdcbiAgICAgIGxldCBtYXJrT2JqZWN0OiBhbnkgPSB7XG4gICAgICAgIHBvc2l0aW9uOiBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxpc3RMb2NhdGlvbltpXVsxXSwgbGlzdExvY2F0aW9uW2ldWzJdKSxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSxcbiAgICAgICAgekluZGV4OiAxMCxcbiAgICAgICAgbWFwOiB0aGlzLm1hcFxuICAgICAgfTtcblxuICAgICAgbWFya09iamVjdFtcImljb25cIl0gPSB7XG4gICAgICAgIHVybDogdGhpcy5tYXJrZXJJY29uLCAvLyB1cmxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksIC8vIG9yaWdpblxuICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgIH07XG4gICAgICBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtPYmplY3QpO1xuICAgICAgdGhpcy5tYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKG1hcmtlciwgJ2NsaWNrJywgKG1hcmtlcjogZ29vZ2xlLm1hcHMuTWFya2VyLCBpOiBudW1iZXIpID0+IHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBsZXQgbGF0bG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsaXN0TG9jYXRpb25baV1bMV0sIGxpc3RMb2NhdGlvbltpXVsyXSk7XG4gICAgICAgICAgLy9AdHMtaWdub3JlXG4gICAgICAgICAgc2VsZi5nZW9jb2Rlci5nZW9jb2RlKHsgJ2xhdExuZyc6IGxhdGxuZyB9LCAocmVzcG9uc2VzOiBzdHJpbmcgfCBhbnlbXSkgPT4ge1xuICAgICAgICAgICAgbGV0IGFkZHJlc3M6IHN0cmluZztcbiAgICAgICAgICAgIGlmICghbGlzdExvY2F0aW9uW2ldWzRdKSB7XG4gICAgICAgICAgICAgIGlmIChyZXNwb25zZXMgJiYgcmVzcG9uc2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBhZGRyZXNzID0gcmVzcG9uc2VzWzBdLmZvcm1hdHRlZF9hZGRyZXNzO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFkZHJlc3MgPSBcIkNhbm5vdCBkZXRlcm1pbmUgYWRkcmVzcyBhdCB0aGlzIGxvY2F0aW9uLlwiO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBhZGRyZXNzID0gbGlzdExvY2F0aW9uW2ldWzRdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzZWxmLmFjdGl2ZUluZm9XaW5kb3cgJiYgc2VsZi5hY3RpdmVJbmZvV2luZG93LmNsb3NlKCk7XG4gICAgICAgICAgICBzZWxmLmFjdGl2ZUluZm9XaW5kb3cgPSBpbmZvd2luZG93O1xuICAgICAgICAgICAgaW5mb3dpbmRvdy5zZXRDb250ZW50KGFkZHJlc3MpO1xuICAgICAgICAgICAgaW5mb3dpbmRvdy5vcGVuKHNlbGYubWFwLCBtYXJrZXIpO1xuXG4gICAgICAgICAgICAvL21hcmtlciBvYmplY3QgW3RpdGxlLCBsYXQsIGxvbmcsIGluZGV4XVxuICAgICAgICAgICAgaWYgKGxpc3RMb2NhdGlvbltpXVszXSkge1xuICAgICAgICAgICAgICBzZWxmLm1hcmtlckNsaWNrPy5lbWl0KGxpc3RMb2NhdGlvbltpXVszXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAodGhpcy5zaG91bGRDbHVzdGVyZXIpIHtcbiAgICAgIHRoaXMubWFya2VyQ2x1c3Rlcj8uYWRkTWFya2Vycyh0aGlzLm1hcmtlcnMpO1xuICAgIH1cbiAgfVxuXG4gIHBhblRvKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgem9vbU51bWJlcjogbnVtYmVyID0gMTIpIHtcbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKTtcbiAgICB0aGlzLm1hcD8uc2V0Wm9vbSh6b29tTnVtYmVyKTtcbiAgICB0aGlzLm1hcD8ucGFuVG8obXlMYXRMbmcpO1xuICB9XG5cbiAgYWRkTWFrZXIoY29yZGluYXRlOiBDb3JkaW5hdGVEdG8pIHtcbiAgICBpZiAoY29yZGluYXRlLmxhdGl0dWRlICYmIGNvcmRpbmF0ZS5sb25ndGl0dWRlKSB7IFxuICAgICAgbGV0IG1hcmtPYmplY3Q6IGFueSA9IHtcbiAgICAgICAgcG9zaXRpb246IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoY29yZGluYXRlLmxhdGl0dWRlLCBjb3JkaW5hdGUubG9uZ3RpdHVkZSksXG4gICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDMwLCAzMCksXG4gICAgICAgIHpJbmRleDogMTAsXG4gICAgICAgIGljb246IHtcbiAgICAgICAgICB1cmw6IHRoaXMubWFya2VySWNvbiwgLy8gdXJsXG4gICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCkgLy8gYW5jaG9yXG4gICAgICAgIH0sXG4gICAgICAgIG1hcDogdGhpcy5tYXBcbiAgICAgIH07XG4gICAgICBcbiAgICAgIGxldCBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtPYmplY3QpO1xuICAgICAgdGhpcy5tYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgIHRoaXMucGFuVG8oY29yZGluYXRlLmxhdGl0dWRlLCBjb3JkaW5hdGUubG9uZ3RpdHVkZSwgdGhpcy5tYXA/LmdldFpvb20oKSk7XG4gICAgfVxuICB9XG5cbiAgZ2VvY29kZVBvc2l0aW9uKHBvczogYW55LCBldmVudE5hbWU6IHN0cmluZyB8IHVuZGVmaW5lZCA9IHVuZGVmaW5lZCwgZnVuYzogRnVuY3Rpb24gfCBudWxsID0gbnVsbCk6IHZvaWQge1xuICAgIC8vQHRzLWlnbm9yZVxuICAgIHRoaXMuZ2VvY29kZXIuZ2VvY29kZSh7ICdsYXRMbmcnOiBwb3MgfSwgKHJlc3BvbnNlczogc3RyaW5nIHwgYW55W10pID0+IHtcbiAgICAgIGlmIChyZXNwb25zZXMgJiYgcmVzcG9uc2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUudGl0bGUgPSByZXNwb25zZXNbMF0uZm9ybWF0dGVkX2FkZHJlc3M7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gcmVzcG9uc2VzWzBdLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gcmVzcG9uc2VzWzBdLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUudGl0bGUgPSBcIkNhbm5vdCBkZXRlcm1pbmUgYWRkcmVzcyBhdCB0aGlzIGxvY2F0aW9uLlwiO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHVuZGVmaW5lZDtcbiAgICAgIH1cbiAgICAgIHRoaXMudHJpZ2dlclVwZGF0ZURhdGEoZXZlbnROYW1lKTtcbiAgICAgIGlmIChmdW5jKSB7XG4gICAgICAgIGZ1bmMoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGFkZFNlYXJjaEJveEV2ZW50KCk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoQm94IS5hZGRMaXN0ZW5lcigncGxhY2VzX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5tYXJrZXIpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuc2V0TWFwKG51bGwpO1xuICAgICAgfVxuXG4gICAgICBsZXQgcGxhY2VzID0gdGhpcy5zZWFyY2hCb3ghLmdldFBsYWNlcygpO1xuICAgICAgaWYgKEFycmF5LmlzQXJyYXkocGxhY2VzKSAmJiBwbGFjZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBsZXQgYm91bmRzID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcygpO1xuICAgICAgcGxhY2VzIS5mb3JFYWNoKChwbGFjZSkgPT4ge1xuICAgICAgICBpZiAoIXBsYWNlLmdlb21ldHJ5IHx8ICFwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmV0dXJuZWQgcGxhY2UgY29udGFpbnMgbm8gZ2VvbWV0cnlcIik7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IG1hcmtlck9iaiA9IHtcbiAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgIGRyYWdnYWJsZTogdGhpcy5tYXJrZXJEcmFnZ2FibGUsXG4gICAgICAgICAgdGl0bGU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgYW5pbWF0aW9uOiBnb29nbGUubWFwcy5BbmltYXRpb24uRFJPUCxcbiAgICAgICAgICBwb3NpdGlvbjogcGxhY2UuZ2VvbWV0cnkgPyBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbiA6IHVuZGVmaW5lZFxuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcihtYXJrZXJPYmopO1xuXG4gICAgICAgIHRoaXMuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG5cbiAgICAgICAgdGhpcy5zZWFyY2hFdmVudD8uZW1pdChuZXcgQ29yZGluYXRlRHRvKG1hcmtlck9iai5wb3NpdGlvbj8ubGF0KCksIG1hcmtlck9iai5wb3NpdGlvbj8ubG5nKCkpKTtcblxuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gcGxhY2UuZ2VvbWV0cnkubG9jYXRpb24ubG5nKCk7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLnRpdGxlID0gcGxhY2UuZm9ybWF0dGVkX2FkZHJlc3M7XG5cbiAgICAgICAgdGhpcy50cmlnZ2VyVXBkYXRlRGF0YShtYXBDb25maWcubWFwRXZlbnROYW1lcy5zZWFyY2gpO1xuXG4gICAgICAgIGlmIChwbGFjZS5nZW9tZXRyeS52aWV3cG9ydCkge1xuICAgICAgICAgIGJvdW5kcy51bmlvbihwbGFjZS5nZW9tZXRyeS52aWV3cG9ydCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYm91bmRzLmV4dGVuZChwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgdGhpcy5tYXAhLmZpdEJvdW5kcyhib3VuZHMpO1xuICAgICAgdGhpcy5tYXAhLnNldFpvb20oMTIpO1xuICAgICAgaWYgKHRoaXMubWFya2VyICYmIHRoaXMubWFya2VyLmdldFBvc2l0aW9uKCkpIHtcbiAgICAgICAgLy9AdHMtaWdub3JlXG4gICAgICAgIHRoaXMubWFwIS5wYW5Ubyh0aGlzLm1hcmtlci5nZXRQb3NpdGlvbigpKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGFkZENsaWNrTGlzdGVuZXIoKTogdm9pZCB7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnY2xpY2snLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHRoaXMuZ2VvY29kZVBvc2l0aW9uKGV2ZW50LmxhdExuZywgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubWFwQ2xpY2ssICgpID0+IHtcbiAgICAgICAgdGhpcy5tYXJrZXIhLnNldE1hcChudWxsKTtcbiAgICAgICAgdGhpcy5tYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICBwb3NpdGlvbjogZXZlbnQubGF0TG5nLFxuICAgICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiB0aGlzLm1hcmtlckRyYWdnYWJsZSxcblxuICAgICAgICAgIHRpdGxlOiB0aGlzLmNvcmRpbmF0ZS50aXRsZVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5hZGREcmFnZW5kTGlzdGVuZXIoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgYWRkRHJhZ2VuZExpc3RlbmVyKCk6IHZvaWQge1xuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFya2VyISwgJ2RyYWdlbmQnLCAoKSA9PiB7XG4gICAgICAvLyBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdGhpcy5kZWxldGVNYXJrZXJzKCk7XG4gICAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbih0aGlzLm1hcmtlciEuZ2V0UG9zaXRpb24oKSwgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubWFya2VyRHJhZyk7XG4gICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHRoaXMubWFya2VyIS5nZXRQb3NpdGlvbigpPy5sYXQoKTtcbiAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSB0aGlzLm1hcmtlciEuZ2V0UG9zaXRpb24oKT8ubG5nKCk7XG4gICAgfSk7XG4gIH1cblxuICB0cmlnZ2VyVXBkYXRlRGF0YShldmVudE5hbWU6IHN0cmluZyB8IHVuZGVmaW5lZCk6IHZvaWQge1xuICAgIHRoaXMuY29yZGluYXRlLmV2ZW50TmFtZSA9IGV2ZW50TmFtZTtcbiAgICB0aGlzLnZpYUNvbnRyb2wuc2V0VmFsdWUodGhpcy5jb3JkaW5hdGUpO1xuICAgIHRoaXMuY29yZGluYXRlQ2hhbmdlPy5lbWl0KHRoaXMuY29yZGluYXRlKTtcbiAgfVxuXG4gIG1vdmVUb015TG9jYXRpb24oKSB7XG4gICAgaWYgKCF0aGlzLnJlcXVlc3RMb2NhdGlvbikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpZiAobmF2aWdhdG9yLmdlb2xvY2F0aW9uKSB7XG4gICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChsb2NhdGlvbikgPT4ge1xuICAgICAgICBpZiAodGhpcy5tYXJrZXIpIHtcbiAgICAgICAgICB0aGlzLm1hcmtlci5zZXRNYXAobnVsbCk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gc2VsZi5kZWxldGVNYXJrZXJzKCk7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxhdGl0dWRlO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxvbmdpdHVkZTtcblxuICAgICAgICB0aGlzLm15TGF0TG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxvY2F0aW9uLmNvb3Jkcy5sb25naXR1ZGUpO1xuXG4gICAgICAgIGlmICh0aGlzLm15TG9jYXRpb24pIHtcbiAgICAgICAgICB0aGlzLm15TG9jYXRpb24uc2V0TWFwKG51bGwpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubXlMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgIHBvc2l0aW9uOiB0aGlzLm15TGF0TG5nLFxuICAgICAgICAgIGljb246IHtcbiAgICAgICAgICAgIHVybDogdGhpcy5teUxvY2F0aW9uTWFya2VyLCAvLyB1cmxcbiAgICAgICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDMwLCAzMCksIC8vIHNjYWxlZCBzaXplXG4gICAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgxNSwgMTUpIC8vIGFuY2hvclxuICAgICAgICAgIH0sXG4gICAgICAgICAgbWFwOiB0aGlzLm1hcCxcbiAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlLFxuICAgICAgICAgIHRpdGxlOiB0aGlzLmNvcmRpbmF0ZS50aXRsZVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbih0aGlzLm15TG9jYXRpb24uZ2V0UG9zaXRpb24oKSwgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubXlMb2NhdGlvbik7XG4gICAgICAgIHRoaXMuZ2V0Q3VycmVudExvY2F0aW9uRXZlbnQ/LmVtaXQobmV3IENvcmRpbmF0ZUR0byhsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxvY2F0aW9uLmNvb3Jkcy5sb25naXR1ZGUpKVxuICAgICAgICB0aGlzLm1hcCEuc2V0Q2VudGVyKHRoaXMubXlMYXRMbmcpO1xuICAgICAgICB0aGlzLm15TG9jYWx0aW9uTG9hZGVkID0gdHJ1ZTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhcIkdlb2xvY2F0aW9uIGlzIG5vdCBzdXBwb3J0ZWQgYnkgdGhpcyBicm93c2VyLlwiKTtcbiAgICB9XG4gIH1cblxuICBnZXRUb3BEaXN0YW5jZUZyb21NYXJrZXIobGF0OiBudW1iZXIsIGxuZzogbnVtYmVyLCBldmVudE5hbWU/OiBzdHJpbmcpIHtcbiAgICBpZiAoIWxhdCB8fCAhbG5nKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBteUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpO1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcCEuZ2V0Qm91bmRzKCk7XG4gICAgdGhpcy5nZXRUb3BDb3JkaW5hdG9yRXZlbnQ/LmVtaXQobmV3IENvcmRpbmF0ZUR0byhib3VuZHM/LmdldE5vcnRoRWFzdCgpLmxhdCgpLCBteUxhdExuZy5sbmcoKSwgXCJcIiwgZXZlbnROYW1lKSk7XG4gIH1cblxuICBkcmF3Q2lyY2xlKGNlbnRlckxhdDogbnVtYmVyLCBjZW50ZXJMbmc6IG51bWJlciwgcG9pbnRMYXQ6IG51bWJlciwgcG9pbnRMbmc6IG51bWJlciwgZmlsbENvbG9yOiBzdHJpbmcgPSBcIiNGRjBcIikge1xuICAgIGxldCBteUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoY2VudGVyTGF0LCBjZW50ZXJMbmcpO1xuICAgIC8vIEdldCBtYXAgYm91bmRzXG4gICAgbGV0IHBvaW50ID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhwb2ludExhdCwgcG9pbnRMbmcpO1xuICAgIGxldCBkaXN0YW5jZVRvVG9wID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obXlMYXRMbmcsIHBvaW50KTtcbiAgICBsZXQgcmFkaXVzID0gZGlzdGFuY2VUb1RvcDtcbiAgICB0aGlzLmNpcmNsZT8uc2V0TWFwKG51bGwpO1xuICAgIHRoaXMuY2lyY2xlID0gbmV3IGdvb2dsZS5tYXBzLkNpcmNsZSh7XG4gICAgICBzdHJva2VDb2xvcjogJyNGRjAnLFxuICAgICAgc3Ryb2tlT3BhY2l0eTogMC44LFxuICAgICAgc3Ryb2tlV2VpZ2h0OiAyLFxuICAgICAgZmlsbENvbG9yOiBmaWxsQ29sb3IsXG4gICAgICBmaWxsT3BhY2l0eTogMC4zNSxcbiAgICAgIGVkaXRhYmxlOiB0cnVlLFxuICAgICAgbWFwOiB0aGlzLm1hcCxcbiAgICAgIGNlbnRlcjogbXlMYXRMbmcsXG4gICAgICByYWRpdXM6IHJhZGl1c1xuICAgIH0pO1xuICB9XG5cbiAgcmVxdWlyZWQoZm9ybTogRm9ybUNvbnRyb2wpIHtcbiAgICByZXR1cm4gRm9ybVV0aWxzLmdldEluc3RhbmNlKCkucmVxdWlyZWQoZm9ybSk7XG4gIH1cbn0iLCI8ZGl2IGNsYXNzPVwidmlhLW1hcC13cmFwcGVyXCI+XG4gICAgPGRpdiAqbmdJZj1cIiFoaWRlU2VhcmNoVGV4dEJveFwiIGNsYXNzPVwibWFwLXNlYXJjaC1jb250YWluZXJcIj5cbiAgICAgICAgPGRpdiBpZD1cInZpYS1tYXAtc2VhcmNoXCI+XG4gICAgICAgICAgICA8bWF0LWZvcm0tZmllbGQgYXBwZWFyYW5jZT1cIm91dGxpbmVcIj5cbiAgICAgICAgICAgICAgICA8bWF0LWxhYmVsICpuZ0lmPVwiaTE4blwiPnt7aTE4bi5sYWJlbH19IHt7cmVxdWlyZWQodmlhQ29udHJvbCl9fTwvbWF0LWxhYmVsPlxuICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXQgXG4gICAgICAgICAgICAgICAgICAgICNzZWFyY2hJbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgW3BsYWNlaG9sZGVyXT1cInNlYXJjaFBsYWNlaG9sZXJcIlxuICAgICAgICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cImNvcmRpbmF0ZS50aXRsZVwiPlxuICAgICAgICAgICAgICAgIDxidXR0b24gbWF0U3VmZml4IG1hdC1pY29uLWJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbj5zZWFyY2g8L21hdC1pY29uPlxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cIlwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiZ29vZ2xlLW1hcHNcIiAjZ29vZ2xlTWFwPjwvZGl2PlxuICAgICAgICA8YnV0dG9uICpuZ0lmPVwic2hvd015TG9jYXRpb24gJiYgbWFwTG9hZGVkICYmIG15TG9jYWx0aW9uTG9hZGVkXCIgKGNsaWNrKT1cIm1vdmVUb015TG9jYXRpb24oKVwiIGNsYXNzPVwibXktbG9jYXRpb24tYnRuXCI+XG4gICAgICAgICAgICA8aW1nICpuZ0lmPVwiaWNvbk15TG9jYXRpb25JbWdcIiBbc3JjXT1cImljb25NeUxvY2F0aW9uSW1nXCI+XG4gICAgICAgICAgICA8bWF0LWljb24gKm5nSWY9XCIhaWNvbk15TG9jYXRpb25JbWdcIj5teV9sb2NhdGlvbjwvbWF0LWljb24+XG4gICAgICAgIDwvYnV0dG9uPlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz1cInNwaW5uZXItd3JhcHBlclwiICpuZ0lmPVwibG9hZGluZ1wiPlxuICAgICAgICA8bWF0LXNwaW5uZXIgc3Ryb2tlV2lkdGg9XCIxXCIgZGlhbWV0ZXI9XCIyMFwiPjwvbWF0LXNwaW5uZXI+XG4gICAgPC9kaXY+XG48L2Rpdj4iXX0=