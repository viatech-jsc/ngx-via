export class CordinateDto {
    constructor(latitude, longtitude, title, eventName, zoomLevel) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.title = title;
        this.eventName = eventName;
        this.zoomLevel = zoomLevel;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZGluYXRlLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS9tYXAvZHRvL2NvcmRpbmF0ZS5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxPQUFPLFlBQVk7SUFPckIsWUFDSSxRQUFpQixFQUNqQixVQUFtQixFQUNuQixLQUFjLEVBQ2QsU0FBa0IsRUFDbEIsU0FBa0I7UUFFbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDL0IsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIENvcmRpbmF0ZUR0byB7XG4gICAgbGF0aXR1ZGU/OiBudW1iZXI7XG4gICAgbG9uZ3RpdHVkZT86IG51bWJlcjtcbiAgICB0aXRsZT86IHN0cmluZztcbiAgICBldmVudE5hbWU/OiBzdHJpbmc7XG4gICAgem9vbUxldmVsPzogbnVtYmVyO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIGxhdGl0dWRlPzogbnVtYmVyLFxuICAgICAgICBsb25ndGl0dWRlPzogbnVtYmVyLFxuICAgICAgICB0aXRsZT86IHN0cmluZyxcbiAgICAgICAgZXZlbnROYW1lPzogc3RyaW5nLFxuICAgICAgICB6b29tTGV2ZWw/OiBudW1iZXJcbiAgICApIHtcbiAgICAgICAgdGhpcy5sYXRpdHVkZSA9IGxhdGl0dWRlO1xuICAgICAgICB0aGlzLmxvbmd0aXR1ZGUgPSBsb25ndGl0dWRlO1xuICAgICAgICB0aGlzLnRpdGxlID0gdGl0bGU7XG4gICAgICAgIHRoaXMuZXZlbnROYW1lID0gZXZlbnROYW1lO1xuICAgICAgICB0aGlzLnpvb21MZXZlbCA9IHpvb21MZXZlbDtcbiAgICB9XG59XG4iXX0=