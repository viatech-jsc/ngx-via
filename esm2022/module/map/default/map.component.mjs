import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MarkerClusterer } from '@googlemaps/markerclusterer';
import random from "lodash/random";
import { mapConfig } from '../constant';
import { CordinateDto } from '../dto/cordinate.dto';
import { MapI18nDto } from '../dto/map.i18n.dto';
import { FormUtils } from '../../../utils/form.utils';
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/common";
export class MapComponent {
    constructor() {
        this.searchInput = undefined;
        this.googleMap = undefined;
        this.cordinate = new CordinateDto();
        this.searchPlaceholer = "";
        this.markerDraggable = true;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.requestLocation = true;
        this.loading = false;
        this.myLocationMarker = 'assets/icons/green_pin.svg';
        this.markerIcon = 'assets/icons/red_pin.svg';
        this.shouldClusterer = false;
        this.clustererImagepath = "assets/markerclusterer/m";
        this.iconMyLocationImg = undefined;
        this.iconSearchImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.boundsChanged = new EventEmitter();
        this.centerChanged = new EventEmitter();
        this.clickOnMap = new EventEmitter();
        this.dblClick = new EventEmitter();
        this.drag = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.idle = new EventEmitter();
        this.mouseMove = new EventEmitter();
        this.mouseOut = new EventEmitter();
        this.mouseOver = new EventEmitter();
        this.tilesLoaded = new EventEmitter();
        this.zoomChanged = new EventEmitter();
        this.latlng = undefined;
        this.myLatLng = undefined;
        this.map = undefined;
        this.myLocation = undefined;
        this.marker = undefined;
        this.searchBox = undefined;
        this.markers = [];
        this.activeInfoWindow = undefined;
        this.circle = undefined;
        this.myLocaltionLoaded = false;
        this.mapLoaded = false;
        this.markerCluster = undefined;
        this.mapId = "";
        this.mapId = "dropdown" + random();
        this.geocoder = new google.maps.Geocoder();
    }
    instance() {
        return this.map;
    }
    formControlInstance() {
        return this.viaControl;
    }
    getCenterMapLocation() {
        return this.instance()?.getCenter();
    }
    getMarkerCluster() {
        return this.markerCluster;
    }
    ngAfterViewInit() {
        let lat = this.cordinate.latitude;
        let lng = this.cordinate.longtitude;
        if (lat && lng) {
            this.initMap();
        }
        else {
            if (this.requestLocation && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    this.cordinate.latitude = location.coords.latitude;
                    this.cordinate.longtitude = location.coords.longitude;
                    this.initMap();
                    this.myLocaltionLoaded = true;
                }, () => {
                    this.cordinate.latitude = 10.762622;
                    this.cordinate.longtitude = 106.660172;
                    this.initMap();
                });
            }
            else {
                this.cordinate.latitude = 10.762622;
                this.cordinate.longtitude = 106.660172;
                this.initMap();
            }
        }
    }
    bindingCordinate() {
        if (this.cordinate.title === mapConfig.myLocation) {
            this.myLocation = new google.maps.Marker({
                position: this.myLatLng,
                icon: {
                    url: this.myLocationMarker, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map,
                draggable: false
            });
        }
        else {
            this.marker = new google.maps.Marker({
                position: this.myLatLng, icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map,
                draggable: this.markerDraggable,
                animation: google.maps.Animation.DROP,
                title: this.cordinate.title
            });
            this.addClickListener();
            this.addDragendListener();
        }
    }
    initMap() {
        if (this.googleMap) {
            this.myLatLng = new google.maps.LatLng(this.cordinate.latitude, this.cordinate.longtitude);
            let mapConfigObject = {
                zoom: 15,
                center: this.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            this.map = new google.maps.Map(this.googleMap.nativeElement, mapConfigObject);
            if (!this.hideSearchTextBox && this.searchInput) {
                this.searchBox = new google.maps.places.SearchBox(this.searchInput.nativeElement);
                this.addSearchBoxEvent();
            }
            this.bindingEvent();
            this.bindingCordinate();
            this.geocodePosition(this.myLatLng);
            if (this.shouldClusterer) {
                // this.markerCluster = new MarkerClusterer(this.map, [], {
                //   imagePath: this.clustererImagepath,
                // });
                //use the svg path for image
                this.markerCluster = new MarkerClusterer({ map: this.map, markers: [] });
            }
        }
    }
    bindingEvent() {
        google.maps.event.addListener(this.map, 'bounds_changed', () => {
            let bounds = this.map.getBounds();
            if (bounds) {
                if (!this.hideSearchTextBox && this.searchInput) {
                    this.searchBox.setBounds(bounds);
                }
                this.boundsChanged?.emit(bounds);
            }
        });
        google.maps.event.addListener(this.map, 'center_changed', () => {
            let center = this.map.getCenter();
            this.centerChanged?.emit(center);
        });
        google.maps.event.addListener(this.map, 'click', (event) => {
            this.clickOnMap?.emit(event);
        });
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
            this.dblClick?.emit(event);
        });
        google.maps.event.addListener(this.map, 'drag', () => {
            this.drag?.emit("drag");
        });
        google.maps.event.addListener(this.map, 'dragend', () => {
            this.dragEnd?.emit("dragend");
        });
        google.maps.event.addListener(this.map, 'dragstart', () => {
            this.dragStart?.emit("dragstart");
        });
        google.maps.event.addListener(this.map, 'idle', () => {
            this.mapLoaded = true;
            this.idle?.emit("idle");
        });
        google.maps.event.addListener(this.map, 'mousemove', (event) => {
            this.mouseMove?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseout', (event) => {
            this.mouseOut?.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseover', (event) => {
            this.mouseOver?.emit(event);
        });
        google.maps.event.addListener(this.map, "tilesloaded", () => {
            this.tilesLoaded?.emit("tilesloaded");
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.zoomChanged?.emit(this.map.getZoom());
        });
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        if (this.map && this.map.getBounds()) {
            let sW = this.map.getBounds().getSouthWest();
            let nE = this.map.getBounds().getNorthEast();
            corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
            corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
            corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
            corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
            cornersArray[corner1] = {
                "lat": sW.lat(),
                "lng": sW.lng()
            };
            cornersArray[corner2] = {
                "lat": nE.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner3] = {
                "lat": sW.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner4] = {
                "lat": nE.lat(),
                "lng": sW.lng()
            };
            farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
            cordinate = new CordinateDto(farestPoint.lat, farestPoint.lng, "", eventName);
            this.mapRadius.emit(cordinate);
        }
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
    }
    //listLocation[0]: code
    //listLocation[1]: Latitude (pointY)
    //listLocation[2]: Longtitude (pointX)
    //listLocation[3]: index
    //listLocation[4]: label
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                map: this.map
            };
            markObject["icon"] = {
                url: this.markerIcon, // url
                scaledSize: new google.maps.Size(30, 30), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //get address
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    //@ts-ignore
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick.emit(listLocation[i][3]);
                        }
                    });
                };
            })(marker, i));
        }
        // for (let index = 0; index < this.markers.length; index++) {
        //   bounds.extend(this.markers[index].getPosition());
        // }
        // if (self.marker) {
        //   bounds.extend(self.marker.getPosition());
        // }
        // if (self.myLocation) {
        //   bounds.extend(self.myLocation.getPosition());
        // }
        //this.map.fitBounds(bounds);
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map?.setZoom(zoomNumber);
        this.map?.panTo(myLatLng);
    }
    addMaker(cordinate) {
        if (cordinate.latitude && cordinate.longtitude) {
            let markObject = {
                position: new google.maps.LatLng(cordinate.latitude, cordinate.longtitude),
                scaledSize: new google.maps.Size(30, 30),
                zIndex: 10,
                icon: {
                    url: this.markerIcon, // url
                    scaledSize: new google.maps.Size(30, 30), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                map: this.map
            };
            let marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            this.panTo(cordinate.latitude, cordinate.longtitude, this.map?.getZoom());
        }
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        //@ts-ignore
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = undefined;
                this.cordinate.longtitude = undefined;
            }
            this.triggerUpdateData(eventName);
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        this.searchBox?.addListener('places_changed', () => {
            if (this.marker) {
                this.marker.setMap(null);
            }
            //self.deleteMarkers();
            let places = this.searchBox.getPlaces();
            if (Array.isArray(places) && places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                let markerObj = {
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry?.location
                };
                if (!place.geometry || !place.geometry.location) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                this.marker = new google.maps.Marker(markerObj);
                // self.markers.push(self.marker);
                this.addDragendListener();
                this.searchEvent.emit(new CordinateDto(markerObj.position.lat(), markerObj.position.lng()));
                this.cordinate.latitude = place.geometry.location.lat();
                this.cordinate.longtitude = place.geometry.location.lng();
                this.cordinate.title = place.formatted_address;
                this.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map?.fitBounds(bounds);
            this.map?.setZoom(12);
            if (this.marker && this.marker.getPosition()) {
                //@ts-ignore
                this.map.panTo(this.marker.getPosition());
            }
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            // event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition()?.lat();
            this.cordinate.longtitude = this.marker.getPosition()?.lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.viaControl.setValue(this.cordinate);
        this.cordinateChange?.emit(this.cordinate);
    }
    moveToMyLocation() {
        let self = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((location) => {
                if (self.marker) {
                    self.marker.setMap(null);
                }
                // self.deleteMarkers();
                self.cordinate.latitude = location.coords.latitude;
                self.cordinate.longtitude = location.coords.longitude;
                this.myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
                if (self.myLocation) {
                    self.myLocation.setMap(null);
                }
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.myLocationMarker, // url
                        scaledSize: new google.maps.Size(30, 30), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    map: self.map,
                    draggable: false,
                    title: self.cordinate.title
                });
                self.geocodePosition(self.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                //self.addDragendListener();
                self.getCurrentLocationEvent.emit(new CordinateDto(location.coords.latitude, location.coords.longitude));
                this.map.setCenter(this.myLatLng);
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getRadius(distance = 100) {
        if (this.myLocation) {
            let w = this.map.getBounds()?.getSouthWest();
            let centerLng = this.myLocation.getPosition();
            if (centerLng && w) {
                distance = google.maps.geometry.spherical.computeDistanceBetween(centerLng, w);
            }
        }
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        // Get map bounds
        let bounds = this.map.getBounds();
        this.getTopCordinatorEvent.emit(new CordinateDto(bounds?.getNorthEast().lat(), myLatLng.lng(), "", eventName));
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle?.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: MapComponent, selector: "via-map", inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox", requestLocation: "requestLocation", loading: "loading", myLocationMarker: "myLocationMarker", markerIcon: "markerIcon", shouldClusterer: "shouldClusterer", clustererImagepath: "clustererImagepath", iconMyLocationImg: "iconMyLocationImg", iconSearchImg: "iconSearchImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent", boundsChanged: "boundsChanged", centerChanged: "centerChanged", clickOnMap: "clickOnMap", dblClick: "dblClick", drag: "drag", dragEnd: "dragEnd", dragStart: "dragStart", idle: "idle", mouseMove: "mouseMove", mouseOut: "mouseOut", mouseOver: "mouseOver", tilesLoaded: "tilesLoaded", zoomChanged: "zoomChanged" }, viewQueries: [{ propertyName: "searchInput", first: true, predicate: ["searchInput"], descendants: true }, { propertyName: "googleMap", first: true, predicate: ["googleMap"], descendants: true }], ngImport: i0, template: "<div class=\"row\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"col-sm-12 map-search-container\">\n        <div class=\"form-group\">\n            <label *ngIf=\"i18n\" [attr.for]=\"mapId\">\n                {{i18n.label}}\n                <span class=\"danger-text\"> {{required(viaControl)}}</span>\n            </label>\n            <div class=\"via-map-search\">\n                <input \n                    type=\"text\" \n                    [attr.id]=\"mapId\"\n                    class=\"form-control\" \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <img *ngIf=\"iconSearchImg\" [src]=\"iconSearchImg\" class=\"input-search-icon\">\n                <i *ngIf=\"!iconSearchImg\"class=\"fas fa-search input-search-icon\" aria-hidden=\"true\"></i>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-sm-12\" style=\"position: relative;\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button  *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <i *ngIf=\"!iconMyLocationImg\" class=\"fas fa-location-arrow\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n</div>", styles: [".google-maps{height:300px;border-radius:3px}.my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:22px;bottom:110px}.my-location-btn img,.my-location-btn i{width:20px}.map-search-container{margin-bottom:10px}.map-search-container .danger-text{color:brown}.map-search-container .via-map-search{position:relative}.map-search-container .via-map-search input{padding-right:35px}.map-search-container .input-search-icon{width:20px;position:absolute;top:10px;right:10px}\n"], dependencies: [{ kind: "directive", type: i1.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "directive", type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MapComponent, decorators: [{
            type: Component,
            args: [{ selector: 'via-map', template: "<div class=\"row\">\n    <div *ngIf=\"!hideSearchTextBox\" class=\"col-sm-12 map-search-container\">\n        <div class=\"form-group\">\n            <label *ngIf=\"i18n\" [attr.for]=\"mapId\">\n                {{i18n.label}}\n                <span class=\"danger-text\"> {{required(viaControl)}}</span>\n            </label>\n            <div class=\"via-map-search\">\n                <input \n                    type=\"text\" \n                    [attr.id]=\"mapId\"\n                    class=\"form-control\" \n                    #searchInput \n                    [placeholder]=\"searchPlaceholer\"\n                    [(ngModel)]=\"cordinate.title\">\n                <img *ngIf=\"iconSearchImg\" [src]=\"iconSearchImg\" class=\"input-search-icon\">\n                <i *ngIf=\"!iconSearchImg\"class=\"fas fa-search input-search-icon\" aria-hidden=\"true\"></i>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-sm-12\" style=\"position: relative;\">\n        <div class=\"google-maps\" #googleMap></div>\n        <button  *ngIf=\"showMyLocation && mapLoaded && myLocaltionLoaded\" (click)=\"moveToMyLocation()\" class=\"my-location-btn\">\n            <img *ngIf=\"iconMyLocationImg\" [src]=\"iconMyLocationImg\">\n            <i *ngIf=\"!iconMyLocationImg\" class=\"fas fa-location-arrow\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n</div>", styles: [".google-maps{height:300px;border-radius:3px}.my-location-btn{cursor:pointer;background:#fff;border-style:none;width:41px;height:41px;text-align:center;border-radius:2px;padding:0!important;box-shadow:#0000004d 0 1px 4px -1px;position:absolute;right:22px;bottom:110px}.my-location-btn img,.my-location-btn i{width:20px}.map-search-container{margin-bottom:10px}.map-search-container .danger-text{color:brown}.map-search-container .via-map-search{position:relative}.map-search-container .via-map-search input{padding-right:35px}.map-search-container .input-search-icon{width:20px;position:absolute;top:10px;right:10px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { searchInput: [{
                type: ViewChild,
                args: ["searchInput"]
            }], googleMap: [{
                type: ViewChild,
                args: ["googleMap"]
            }], cordinate: [{
                type: Input
            }], searchPlaceholer: [{
                type: Input
            }], markerDraggable: [{
                type: Input
            }], showMyLocation: [{
                type: Input
            }], hideSearchTextBox: [{
                type: Input
            }], requestLocation: [{
                type: Input
            }], loading: [{
                type: Input
            }], myLocationMarker: [{
                type: Input
            }], markerIcon: [{
                type: Input
            }], shouldClusterer: [{
                type: Input
            }], clustererImagepath: [{
                type: Input
            }], iconMyLocationImg: [{
                type: Input
            }], iconSearchImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }], cordinateChange: [{
                type: Output
            }], markerClick: [{
                type: Output
            }], mapRadius: [{
                type: Output
            }], searchEvent: [{
                type: Output
            }], getTopCordinatorEvent: [{
                type: Output
            }], getCurrentLocationEvent: [{
                type: Output
            }], boundsChanged: [{
                type: Output
            }], centerChanged: [{
                type: Output
            }], clickOnMap: [{
                type: Output
            }], dblClick: [{
                type: Output
            }], drag: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], idle: [{
                type: Output
            }], mouseMove: [{
                type: Output
            }], mouseOut: [{
                type: Output
            }], mouseOver: [{
                type: Output
            }], tilesLoaded: [{
                type: Output
            }], zoomChanged: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS9tYXAvZGVmYXVsdC9tYXAuY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL21hcC9kZWZhdWx0L21hcC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzlELE9BQU8sTUFBTSxNQUFNLGVBQWUsQ0FBQztBQUNuQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDJCQUEyQixDQUFDOzs7O0FBT3RELE1BQU0sT0FBTyxZQUFZO0lBd0R2QjtRQXREMEIsZ0JBQVcsR0FBa0MsU0FBUyxDQUFDO1FBQ3pELGNBQVMsR0FBNkIsU0FBUyxDQUFDO1FBRS9ELGNBQVMsR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QyxxQkFBZ0IsR0FBVyxFQUFFLENBQUM7UUFDOUIsb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFDaEMsbUJBQWMsR0FBWSxJQUFJLENBQUM7UUFDL0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFlLEdBQVksSUFBSSxDQUFDO1FBQ2hDLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDekIscUJBQWdCLEdBQVcsNEJBQTRCLENBQUM7UUFDeEQsZUFBVSxHQUFXLDBCQUEwQixDQUFDO1FBQ2hELG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLHVCQUFrQixHQUFXLDBCQUEwQixDQUFDO1FBQ3hELHNCQUFpQixHQUFZLFNBQVMsQ0FBQztRQUN2QyxrQkFBYSxHQUFZLFNBQVMsQ0FBQztRQUNuQyxlQUFVLEdBQWdCLElBQUksV0FBVyxFQUFFLENBQUM7UUFDNUMsU0FBSSxHQUFnQixTQUFTLENBQUM7UUFFN0Isb0JBQWUsR0FBK0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqRSxnQkFBVyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZELGNBQVMsR0FBK0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzRCxnQkFBVyxHQUErQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzdELDBCQUFxQixHQUErQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZFLDRCQUF1QixHQUErQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pFLGtCQUFhLEdBQTJDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0Usa0JBQWEsR0FBcUMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyRSxlQUFVLEdBQTRDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDekUsYUFBUSxHQUE0QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZFLFNBQUksR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNoRCxZQUFPLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDbkQsY0FBUyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3JELFNBQUksR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNoRCxjQUFTLEdBQTRDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDeEUsYUFBUSxHQUE0QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZFLGNBQVMsR0FBNEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN4RSxnQkFBVyxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZELGdCQUFXLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFakUsV0FBTSxHQUF3QixTQUFTLENBQUM7UUFFeEMsYUFBUSxHQUF3QixTQUFTLENBQUM7UUFDMUMsUUFBRyxHQUFxQixTQUFTLENBQUM7UUFDbEMsZUFBVSxHQUF3QixTQUFTLENBQUM7UUFDNUMsV0FBTSxHQUF3QixTQUFTLENBQUM7UUFDeEMsY0FBUyxHQUFrQyxTQUFTLENBQUM7UUFDckQsWUFBTyxHQUE4QixFQUFFLENBQUM7UUFDeEMscUJBQWdCLEdBQTRCLFNBQVMsQ0FBQztRQUN0RCxXQUFNLEdBQXdCLFNBQVMsQ0FBQztRQUN4QyxzQkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixrQkFBYSxHQUFxQixTQUFTLENBQUM7UUFDNUMsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUdqQixJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM3QyxDQUFDO0lBRUQsUUFBUTtRQUNOLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNsQixDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN6QixDQUFDO0lBRUQsb0JBQW9CO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLFNBQVMsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztRQUNsQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztRQUNwQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNmLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNqQixDQUFDO2FBQU0sQ0FBQztZQUNOLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2xELFNBQVMsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtvQkFDcEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7b0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO29CQUN0RCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ2YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDaEMsQ0FBQyxFQUFFLEdBQUcsRUFBRTtvQkFDTixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNqQixDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7aUJBQU0sQ0FBQztnQkFDTixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDdkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2pCLENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVELGdCQUFnQjtRQUNkLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDdkMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUN2QixJQUFJLEVBQUU7b0JBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNO29CQUNsQyxVQUFVLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsY0FBYztvQkFDeEQsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLFNBQVM7b0JBQzlDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTO2lCQUM5QztnQkFDRCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2IsU0FBUyxFQUFFLEtBQUs7YUFDakIsQ0FBQyxDQUFDO1FBQ0wsQ0FBQzthQUFNLENBQUM7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQ25DLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLElBQUksRUFBRTtvQkFDNUIsR0FBRyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTTtvQkFDNUIsVUFBVSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLGNBQWM7b0JBQ3hELE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxTQUFTO29CQUM5QyxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUztpQkFDOUM7Z0JBQ0QsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO2dCQUNiLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZTtnQkFDL0IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7Z0JBQ3JDLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUs7YUFDNUIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDNUIsQ0FBQztJQUNILENBQUM7SUFFRCxPQUFPO1FBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVyxDQUFDLENBQUM7WUFDN0YsSUFBSSxlQUFlLEdBQUc7Z0JBQ3BCLElBQUksRUFBRSxFQUFFO2dCQUNSLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUTtnQkFDckIsaUJBQWlCLEVBQUUsS0FBSztnQkFDeEIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87Z0JBQ3hDLGNBQWMsRUFBRSxLQUFLO2FBQ3RCLENBQUM7WUFFRixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFFOUUsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDM0IsQ0FBQztZQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUV4QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDekIsMkRBQTJEO2dCQUMzRCx3Q0FBd0M7Z0JBQ3hDLE1BQU07Z0JBRU4sNEJBQTRCO2dCQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksZUFBZSxDQUFDLEVBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBQyxDQUFDLENBQUM7WUFDekUsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRUQsWUFBWTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLGdCQUFnQixFQUFFLEdBQUcsRUFBRTtZQUM5RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ25DLElBQUksTUFBTSxFQUFFLENBQUM7Z0JBQ1gsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ2hELElBQUksQ0FBQyxTQUFVLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQyxDQUFDO2dCQUNELElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLGdCQUFnQixFQUFFLEdBQUcsRUFBRTtZQUM5RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsT0FBTyxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3JGLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3hGLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRTtZQUNwRCxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUU7WUFDdkQsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxXQUFXLEVBQUUsR0FBRyxFQUFFO1lBQ3pELElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRTtZQUNwRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLFdBQVcsRUFBRSxDQUFDLEtBQWdDLEVBQUUsRUFBRTtZQUN6RixJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLFVBQVUsRUFBRSxDQUFDLEtBQWdDLEVBQUUsRUFBRTtZQUN4RixJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLFdBQVcsRUFBRSxDQUFDLEtBQWdDLEVBQUUsRUFBRTtZQUN6RixJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBSSxFQUFFLGFBQWEsRUFBRSxHQUFHLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUksRUFBRSxjQUFjLEVBQUUsR0FBRyxFQUFFO1lBQzVELElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUM5QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0QkFBNEIsQ0FBQyxHQUFXLEVBQUUsR0FBVyxFQUFFLFNBQWlCO1FBQ3RFLElBQUksU0FBdUIsRUFBRSxZQUFZLEdBQVEsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUM7UUFDckcsSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQztZQUNyQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBSSxDQUFDLFNBQVMsRUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQy9DLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFJLENBQUMsU0FBUyxFQUFHLENBQUMsWUFBWSxFQUFFLENBQUM7WUFFL0MsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzlJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM5SSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDOUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRTlJLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztnQkFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7YUFDaEIsQ0FBQTtZQUNELFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3pFLFNBQVMsR0FBRyxJQUFJLFlBQVksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQzlFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7SUFDSCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUN0RSxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3ZJLENBQUM7SUFFRCxtRUFBbUU7SUFDbkUsYUFBYTtRQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsdUJBQXVCO0lBQ3ZCLG9DQUFvQztJQUNwQyxzQ0FBc0M7SUFDdEMsd0JBQXdCO0lBQ3hCLHdCQUF3QjtJQUV4QixzQkFBc0IsQ0FBQyxlQUEyQixFQUFFO1FBQ2xELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzVDLElBQUksTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNkLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQixLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN6QyxlQUFlO1lBQ2YsSUFBSSxVQUFVLEdBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxFQUFFO2dCQUNWLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRzthQUNkLENBQUM7WUFFRixVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUc7Z0JBQ25CLEdBQUcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07Z0JBQzVCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO2dCQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUztnQkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7YUFDOUMsQ0FBQztZQUNGLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLENBQUMsVUFBVSxNQUFNLEVBQUUsQ0FBQztnQkFDakUsT0FBTztvQkFDTCxhQUFhO29CQUNiLElBQUksTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxZQUFZO29CQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUU7d0JBQ3hELElBQUksT0FBTyxDQUFDO3dCQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDeEIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztnQ0FDdEMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzs0QkFDM0MsQ0FBQztpQ0FBTSxDQUFDO2dDQUNOLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQzs0QkFDekQsQ0FBQzt3QkFDSCxDQUFDOzZCQUFNLENBQUM7NEJBQ04sT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDL0IsQ0FBQzt3QkFFRCxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUN2RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDO3dCQUNuQyxVQUFVLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUMvQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBRWxDLHlDQUF5Qzt3QkFDekMsSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzVDLENBQUM7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQztRQUVELDhEQUE4RDtRQUM5RCxzREFBc0Q7UUFDdEQsSUFBSTtRQUNKLHFCQUFxQjtRQUNyQiw4Q0FBOEM7UUFDOUMsSUFBSTtRQUNKLHlCQUF5QjtRQUN6QixrREFBa0Q7UUFDbEQsSUFBSTtRQUVKLDZCQUE2QjtJQUMvQixDQUFDO0lBRUQsS0FBSyxDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsYUFBcUIsRUFBRTtRQUNyRCxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsUUFBUSxDQUFDLFNBQXVCO1FBQzlCLElBQUksU0FBUyxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDL0MsSUFBSSxVQUFVLEdBQVE7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQztnQkFDMUUsVUFBVSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQztnQkFDeEMsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFO29CQUNKLEdBQUcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU07b0JBQzVCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO29CQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUztvQkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7aUJBQzlDO2dCQUNELEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRzthQUNkLENBQUM7WUFFRixJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUM1RSxDQUFDO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxHQUFRLEVBQUUsWUFBZ0MsU0FBUyxFQUFFLE9BQXdCLElBQUk7UUFDL0YsWUFBWTtRQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDckQsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztnQkFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO2dCQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDbkUsQ0FBQztpQkFBTSxDQUFDO2dCQUNOLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLDRDQUE0QyxDQUFDO2dCQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztZQUN4QyxDQUFDO1lBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksSUFBSSxFQUFFLENBQUM7Z0JBQ1QsSUFBSSxFQUFFLENBQUM7WUFDVCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQixDQUFDO1lBRUQsdUJBQXVCO1lBQ3ZCLElBQUksTUFBTSxHQUErQyxJQUFJLENBQUMsU0FBVSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3JGLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNoRCxPQUFPO1lBQ1QsQ0FBQztZQUVELElBQUksTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUM1QyxNQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3hCLElBQUksU0FBUyxHQUFRO29CQUNuQixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUMvQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUk7b0JBQ2pCLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJO29CQUNyQyxRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxRQUFRO2lCQUNuQyxDQUFDO2dCQUVGLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO29CQUNuRCxPQUFPO2dCQUNULENBQUM7Z0JBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUVoRCxrQ0FBa0M7Z0JBQ2xDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUUxQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUU1RixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQzFELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztnQkFFL0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXZELElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDNUIsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDO3FCQUFNLENBQUM7b0JBQ04sTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN0QixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDO2dCQUM3QyxZQUFZO2dCQUNaLElBQUksQ0FBQyxHQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM3QyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFJLEVBQUUsT0FBTyxFQUFFLENBQUMsS0FBZ0MsRUFBRSxFQUFFO1lBQ3JGLHlCQUF5QjtZQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO2dCQUN4RSxJQUFJLENBQUMsTUFBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUNuQyxRQUFRLEVBQUUsS0FBSyxDQUFDLE1BQU07b0JBQ3RCLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWU7b0JBRS9CLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUs7aUJBQzVCLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGtCQUFrQjtRQUNoQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU8sRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFO1lBQzFELDBCQUEwQjtZQUMxQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDckYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQztZQUM1RCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlCQUFpQixDQUFDLFNBQTZCO1FBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDMUIsU0FBUyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUNwRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNCLENBQUM7Z0JBQ0Qsd0JBQXdCO2dCQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7Z0JBRXRELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUU1RixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLENBQUM7Z0JBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLElBQUksRUFBRTt3QkFDSixHQUFHLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLE1BQU07d0JBQ2xDLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxjQUFjO3dCQUN4RCxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsU0FBUzt3QkFDOUMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVM7cUJBQ2hEO29CQUNELEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsS0FBSztvQkFDaEIsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSztpQkFDNUIsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4Riw0QkFBNEI7Z0JBQzVCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUN6RyxJQUFJLENBQUMsR0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDO2FBQU0sQ0FBQztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0NBQStDLENBQUMsQ0FBQztRQUMvRCxDQUFDO0lBQ0gsQ0FBQztJQUVELFNBQVMsQ0FBQyxXQUFtQixHQUFHO1FBQzlCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsWUFBWSxFQUFFLENBQUM7WUFDOUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxJQUFJLFNBQVMsSUFBSSxDQUFDLEVBQUUsQ0FBQztnQkFDbkIsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDakYsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxTQUFrQjtRQUNuRSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDakIsT0FBTztRQUNULENBQUM7UUFDRCxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRCxpQkFBaUI7UUFDakIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksWUFBWSxDQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxRQUFRLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDakgsQ0FBQztJQUVELFVBQVUsQ0FBQyxTQUFpQixFQUFFLFNBQWlCLEVBQUUsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLFlBQW9CLE1BQU07UUFDN0csSUFBSSxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDNUQsaUJBQWlCO1FBQ2pCLElBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0YsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxXQUFXLEVBQUUsTUFBTTtZQUNuQixhQUFhLEVBQUUsR0FBRztZQUNsQixZQUFZLEVBQUUsQ0FBQztZQUNmLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO1lBQ2IsTUFBTSxFQUFFLFFBQVE7WUFDaEIsTUFBTSxFQUFFLE1BQU07U0FDZixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsUUFBUSxDQUFDLElBQWlCO1FBQ3hCLE9BQU8sU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoRCxDQUFDOzhHQWpqQlUsWUFBWTtrR0FBWixZQUFZLDZ2Q0NkekIsNjJDQTJCTTs7MkZEYk8sWUFBWTtrQkFMeEIsU0FBUzsrQkFDRSxTQUFTO3dEQU1PLFdBQVc7c0JBQXBDLFNBQVM7dUJBQUMsYUFBYTtnQkFDQSxTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBRWIsU0FBUztzQkFBakIsS0FBSztnQkFDRyxnQkFBZ0I7c0JBQXhCLEtBQUs7Z0JBQ0csZUFBZTtzQkFBdkIsS0FBSztnQkFDRyxjQUFjO3NCQUF0QixLQUFLO2dCQUNHLGlCQUFpQjtzQkFBekIsS0FBSztnQkFDRyxlQUFlO3NCQUF2QixLQUFLO2dCQUNHLE9BQU87c0JBQWYsS0FBSztnQkFDRyxnQkFBZ0I7c0JBQXhCLEtBQUs7Z0JBQ0csVUFBVTtzQkFBbEIsS0FBSztnQkFDRyxlQUFlO3NCQUF2QixLQUFLO2dCQUNHLGtCQUFrQjtzQkFBMUIsS0FBSztnQkFDRyxpQkFBaUI7c0JBQXpCLEtBQUs7Z0JBQ0csYUFBYTtzQkFBckIsS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFFSSxlQUFlO3NCQUF4QixNQUFNO2dCQUNHLFdBQVc7c0JBQXBCLE1BQU07Z0JBQ0csU0FBUztzQkFBbEIsTUFBTTtnQkFDRyxXQUFXO3NCQUFwQixNQUFNO2dCQUNHLHFCQUFxQjtzQkFBOUIsTUFBTTtnQkFDRyx1QkFBdUI7c0JBQWhDLE1BQU07Z0JBQ0csYUFBYTtzQkFBdEIsTUFBTTtnQkFDRyxhQUFhO3NCQUF0QixNQUFNO2dCQUNHLFVBQVU7c0JBQW5CLE1BQU07Z0JBQ0csUUFBUTtzQkFBakIsTUFBTTtnQkFDRyxJQUFJO3NCQUFiLE1BQU07Z0JBQ0csT0FBTztzQkFBaEIsTUFBTTtnQkFDRyxTQUFTO3NCQUFsQixNQUFNO2dCQUNHLElBQUk7c0JBQWIsTUFBTTtnQkFDRyxTQUFTO3NCQUFsQixNQUFNO2dCQUNHLFFBQVE7c0JBQWpCLE1BQU07Z0JBQ0csU0FBUztzQkFBbEIsTUFBTTtnQkFDRyxXQUFXO3NCQUFwQixNQUFNO2dCQUNHLFdBQVc7c0JBQXBCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE1hcmtlckNsdXN0ZXJlciB9IGZyb20gJ0Bnb29nbGVtYXBzL21hcmtlcmNsdXN0ZXJlcic7XG5pbXBvcnQgcmFuZG9tIGZyb20gXCJsb2Rhc2gvcmFuZG9tXCI7XG5pbXBvcnQgeyBtYXBDb25maWcgfSBmcm9tICcuLi9jb25zdGFudCc7XG5pbXBvcnQgeyBDb3JkaW5hdGVEdG8gfSBmcm9tICcuLi9kdG8vY29yZGluYXRlLmR0byc7XG5pbXBvcnQgeyBNYXBJMThuRHRvIH0gZnJvbSAnLi4vZHRvL21hcC5pMThuLmR0byc7XG5pbXBvcnQgeyBGb3JtVXRpbHMgfSBmcm9tICcuLi8uLi8uLi91dGlscy9mb3JtLnV0aWxzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndmlhLW1hcCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9tYXAuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21hcC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTWFwQ29tcG9uZW50IHtcbiAgXG4gIEBWaWV3Q2hpbGQoXCJzZWFyY2hJbnB1dFwiKSBzZWFyY2hJbnB1dD86IEVsZW1lbnRSZWY8SFRNTElucHV0RWxlbWVudD4gPSB1bmRlZmluZWQ7XG4gIEBWaWV3Q2hpbGQoXCJnb29nbGVNYXBcIikgZ29vZ2xlTWFwPzogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4gPSB1bmRlZmluZWQ7XG5cbiAgQElucHV0KCkgY29yZGluYXRlOiBDb3JkaW5hdGVEdG8gPSBuZXcgQ29yZGluYXRlRHRvKCk7XG4gIEBJbnB1dCgpIHNlYXJjaFBsYWNlaG9sZXI6IHN0cmluZyA9IFwiXCI7XG4gIEBJbnB1dCgpIG1hcmtlckRyYWdnYWJsZTogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIHNob3dNeUxvY2F0aW9uOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgaGlkZVNlYXJjaFRleHRCb3g6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgcmVxdWVzdExvY2F0aW9uOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBteUxvY2F0aW9uTWFya2VyOiBzdHJpbmcgPSAnYXNzZXRzL2ljb25zL2dyZWVuX3Bpbi5zdmcnO1xuICBASW5wdXQoKSBtYXJrZXJJY29uOiBzdHJpbmcgPSAnYXNzZXRzL2ljb25zL3JlZF9waW4uc3ZnJztcbiAgQElucHV0KCkgc2hvdWxkQ2x1c3RlcmVyOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIGNsdXN0ZXJlckltYWdlcGF0aDogc3RyaW5nID0gXCJhc3NldHMvbWFya2VyY2x1c3RlcmVyL21cIjtcbiAgQElucHV0KCkgaWNvbk15TG9jYXRpb25JbWc/OiBzdHJpbmcgPSB1bmRlZmluZWQ7XG4gIEBJbnB1dCgpIGljb25TZWFyY2hJbWc/OiBzdHJpbmcgPSB1bmRlZmluZWQ7XG4gIEBJbnB1dCgpIHZpYUNvbnRyb2w6IEZvcm1Db250cm9sID0gbmV3IEZvcm1Db250cm9sKCk7XG4gIEBJbnB1dCgpIGkxOG4/OiBNYXBJMThuRHRvID0gdW5kZWZpbmVkO1xuXG4gIEBPdXRwdXQoKSBjb3JkaW5hdGVDaGFuZ2U6IEV2ZW50RW1pdHRlcjxDb3JkaW5hdGVEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgbWFya2VyQ2xpY2s6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgbWFwUmFkaXVzOiBFdmVudEVtaXR0ZXI8Q29yZGluYXRlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIHNlYXJjaEV2ZW50OiBFdmVudEVtaXR0ZXI8Q29yZGluYXRlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGdldFRvcENvcmRpbmF0b3JFdmVudDogRXZlbnRFbWl0dGVyPENvcmRpbmF0ZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBnZXRDdXJyZW50TG9jYXRpb25FdmVudDogRXZlbnRFbWl0dGVyPENvcmRpbmF0ZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBib3VuZHNDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTGF0TG5nQm91bmRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNlbnRlckNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5MYXRMbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2xpY2tPbk1hcDogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZGJsQ2xpY2s6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGRyYWc6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZHJhZ0VuZDogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBkcmFnU3RhcnQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgaWRsZTogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtb3VzZU1vdmU6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1vdXNlT3V0OiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtb3VzZU92ZXI6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIHRpbGVzTG9hZGVkOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIHpvb21DaGFuZ2VkOiBFdmVudEVtaXR0ZXI8bnVtYmVyPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBsYXRsbmc/OiBnb29nbGUubWFwcy5MYXRMbmcgPSB1bmRlZmluZWQ7XG4gIGdlb2NvZGVyOiBnb29nbGUubWFwcy5HZW9jb2RlcjtcbiAgbXlMYXRMbmc/OiBnb29nbGUubWFwcy5MYXRMbmcgPSB1bmRlZmluZWQ7XG4gIG1hcD86IGdvb2dsZS5tYXBzLk1hcCA9IHVuZGVmaW5lZDtcbiAgbXlMb2NhdGlvbj86IGdvb2dsZS5tYXBzLk1hcmtlciA9IHVuZGVmaW5lZDtcbiAgbWFya2VyPzogZ29vZ2xlLm1hcHMuTWFya2VyID0gdW5kZWZpbmVkO1xuICBzZWFyY2hCb3g/OiBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94ID0gdW5kZWZpbmVkO1xuICBtYXJrZXJzOiBBcnJheTxnb29nbGUubWFwcy5NYXJrZXI+ID0gW107XG4gIGFjdGl2ZUluZm9XaW5kb3c/OiBnb29nbGUubWFwcy5JbmZvV2luZG93ID0gdW5kZWZpbmVkO1xuICBjaXJjbGU/OiBnb29nbGUubWFwcy5DaXJjbGUgPSB1bmRlZmluZWQ7XG4gIG15TG9jYWx0aW9uTG9hZGVkOiBib29sZWFuID0gZmFsc2U7XG4gIG1hcExvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBtYXJrZXJDbHVzdGVyPzogTWFya2VyQ2x1c3RlcmVyID0gdW5kZWZpbmVkO1xuICBtYXBJZDogc3RyaW5nID0gXCJcIjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLm1hcElkID0gXCJkcm9wZG93blwiICsgcmFuZG9tKCk7XG4gICAgdGhpcy5nZW9jb2RlciA9IG5ldyBnb29nbGUubWFwcy5HZW9jb2RlcigpO1xuICB9XG5cbiAgaW5zdGFuY2UoKTogZ29vZ2xlLm1hcHMuTWFwIHwgdW5kZWZpbmVkIHtcbiAgICByZXR1cm4gdGhpcy5tYXA7XG4gIH1cblxuICBmb3JtQ29udHJvbEluc3RhbmNlKCkge1xuICAgIHJldHVybiB0aGlzLnZpYUNvbnRyb2w7XG4gIH1cblxuICBnZXRDZW50ZXJNYXBMb2NhdGlvbigpIHtcbiAgICByZXR1cm4gdGhpcy5pbnN0YW5jZSgpPy5nZXRDZW50ZXIoKTtcbiAgfVxuXG4gIGdldE1hcmtlckNsdXN0ZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFya2VyQ2x1c3RlcjtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICBsZXQgbGF0ID0gdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGU7XG4gICAgbGV0IGxuZyA9IHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGU7XG4gICAgaWYgKGxhdCAmJiBsbmcpIHsgXG4gICAgICB0aGlzLmluaXRNYXAoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMucmVxdWVzdExvY2F0aW9uICYmIG5hdmlnYXRvci5nZW9sb2NhdGlvbikge1xuICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChsb2NhdGlvbikgPT4ge1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxhdGl0dWRlO1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuICAgICAgICAgIHRoaXMuaW5pdE1hcCgpO1xuICAgICAgICAgIHRoaXMubXlMb2NhbHRpb25Mb2FkZWQgPSB0cnVlO1xuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSAxMC43NjI2MjI7XG4gICAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IDEwNi42NjAxNzI7XG4gICAgICAgICAgdGhpcy5pbml0TWFwKCk7XG4gICAgICAgIH0pXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IDEwLjc2MjYyMjtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IDEwNi42NjAxNzI7XG4gICAgICAgIHRoaXMuaW5pdE1hcCgpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGJpbmRpbmdDb3JkaW5hdGUoKSB7IFxuICAgIGlmICh0aGlzLmNvcmRpbmF0ZS50aXRsZSA9PT0gbWFwQ29uZmlnLm15TG9jYXRpb24pIHtcbiAgICAgIHRoaXMubXlMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICBwb3NpdGlvbjogdGhpcy5teUxhdExuZyxcbiAgICAgICAgaWNvbjoge1xuICAgICAgICAgIHVybDogdGhpcy5teUxvY2F0aW9uTWFya2VyLCAvLyB1cmxcbiAgICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLCAvLyBzY2FsZWQgc2l6ZVxuICAgICAgICAgIG9yaWdpbjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApLCAvLyBvcmlnaW5cbiAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgICAgfSxcbiAgICAgICAgbWFwOiB0aGlzLm1hcCxcbiAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgIHBvc2l0aW9uOiB0aGlzLm15TGF0TG5nLGljb246IHtcbiAgICAgICAgICB1cmw6IHRoaXMubWFya2VySWNvbiwgLy8gdXJsXG4gICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCkgLy8gYW5jaG9yXG4gICAgICAgIH0sXG4gICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgIGRyYWdnYWJsZTogdGhpcy5tYXJrZXJEcmFnZ2FibGUsXG4gICAgICAgIGFuaW1hdGlvbjogZ29vZ2xlLm1hcHMuQW5pbWF0aW9uLkRST1AsXG4gICAgICAgIHRpdGxlOiB0aGlzLmNvcmRpbmF0ZS50aXRsZVxuICAgICAgfSk7XG4gICAgICB0aGlzLmFkZENsaWNrTGlzdGVuZXIoKTtcbiAgICAgIHRoaXMuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG4gICAgfVxuICB9XG5cbiAgaW5pdE1hcCgpIHtcbiAgICBpZiAodGhpcy5nb29nbGVNYXApIHtcbiAgICAgIHRoaXMubXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHRoaXMuY29yZGluYXRlLmxhdGl0dWRlISwgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSEpO1xuICAgICAgbGV0IG1hcENvbmZpZ09iamVjdCA9IHtcbiAgICAgICAgem9vbTogMTUsXG4gICAgICAgIGNlbnRlcjogdGhpcy5teUxhdExuZyxcbiAgICAgICAgc3RyZWV0Vmlld0NvbnRyb2w6IGZhbHNlLFxuICAgICAgICBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQLFxuICAgICAgICBtYXBUeXBlQ29udHJvbDogZmFsc2VcbiAgICAgIH07XG4gIFxuICAgICAgdGhpcy5tYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKHRoaXMuZ29vZ2xlTWFwLm5hdGl2ZUVsZW1lbnQsIG1hcENvbmZpZ09iamVjdCk7XG4gIFxuICAgICAgaWYgKCF0aGlzLmhpZGVTZWFyY2hUZXh0Qm94ICYmIHRoaXMuc2VhcmNoSW5wdXQpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hCb3ggPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlNlYXJjaEJveCh0aGlzLnNlYXJjaElucHV0Lm5hdGl2ZUVsZW1lbnQpO1xuICAgICAgICB0aGlzLmFkZFNlYXJjaEJveEV2ZW50KCk7XG4gICAgICB9XG4gIFxuICAgICAgdGhpcy5iaW5kaW5nRXZlbnQoKTtcbiAgICAgIHRoaXMuYmluZGluZ0NvcmRpbmF0ZSgpO1xuICAgICAgXG4gICAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbih0aGlzLm15TGF0TG5nKTtcbiAgICAgIGlmICh0aGlzLnNob3VsZENsdXN0ZXJlcikge1xuICAgICAgICAvLyB0aGlzLm1hcmtlckNsdXN0ZXIgPSBuZXcgTWFya2VyQ2x1c3RlcmVyKHRoaXMubWFwLCBbXSwge1xuICAgICAgICAvLyAgIGltYWdlUGF0aDogdGhpcy5jbHVzdGVyZXJJbWFnZXBhdGgsXG4gICAgICAgIC8vIH0pO1xuXG4gICAgICAgIC8vdXNlIHRoZSBzdmcgcGF0aCBmb3IgaW1hZ2VcbiAgICAgICAgdGhpcy5tYXJrZXJDbHVzdGVyID0gbmV3IE1hcmtlckNsdXN0ZXJlcih7bWFwOiB0aGlzLm1hcCwgbWFya2VyczogW119KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBiaW5kaW5nRXZlbnQoKSB7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnYm91bmRzX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICBsZXQgYm91bmRzID0gdGhpcy5tYXAhLmdldEJvdW5kcygpO1xuICAgICAgaWYgKGJvdW5kcykge1xuICAgICAgICBpZiAoIXRoaXMuaGlkZVNlYXJjaFRleHRCb3ggJiYgdGhpcy5zZWFyY2hJbnB1dCkge1xuICAgICAgICAgIHRoaXMuc2VhcmNoQm94IS5zZXRCb3VuZHMoYm91bmRzKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmJvdW5kc0NoYW5nZWQ/LmVtaXQoYm91bmRzKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2NlbnRlcl9jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgbGV0IGNlbnRlciA9IHRoaXMubWFwIS5nZXRDZW50ZXIoKTtcbiAgICAgIHRoaXMuY2VudGVyQ2hhbmdlZD8uZW1pdChjZW50ZXIpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnY2xpY2snLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMuY2xpY2tPbk1hcD8uZW1pdChldmVudCk7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdkYmxjbGljaycsIChldmVudDogZ29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudCkgPT4ge1xuICAgICAgdGhpcy5kYmxDbGljaz8uZW1pdChldmVudCk7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdkcmFnJywgKCkgPT4ge1xuICAgICAgdGhpcy5kcmFnPy5lbWl0KFwiZHJhZ1wiKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2RyYWdlbmQnLCAoKSA9PiB7XG4gICAgICB0aGlzLmRyYWdFbmQ/LmVtaXQoXCJkcmFnZW5kXCIpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnZHJhZ3N0YXJ0JywgKCkgPT4ge1xuICAgICAgdGhpcy5kcmFnU3RhcnQ/LmVtaXQoXCJkcmFnc3RhcnRcIik7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsICdpZGxlJywgKCkgPT4ge1xuICAgICAgdGhpcy5tYXBMb2FkZWQgPSB0cnVlO1xuICAgICAgdGhpcy5pZGxlPy5lbWl0KFwiaWRsZVwiKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ21vdXNlbW92ZScsIChldmVudDogZ29vZ2xlLm1hcHMuTWFwTW91c2VFdmVudCkgPT4ge1xuICAgICAgdGhpcy5tb3VzZU1vdmU/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnbW91c2VvdXQnLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1hcE1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMubW91c2VPdXQ/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAhLCAnbW91c2VvdmVyJywgKGV2ZW50OiBnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm1vdXNlT3Zlcj8uZW1pdChldmVudCk7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCEsIFwidGlsZXNsb2FkZWRcIiwgKCkgPT4ge1xuICAgICAgdGhpcy50aWxlc0xvYWRlZD8uZW1pdChcInRpbGVzbG9hZGVkXCIpO1xuICAgIH0pO1xuICAgIFxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ3pvb21fY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIHRoaXMuem9vbUNoYW5nZWQ/LmVtaXQodGhpcy5tYXAhLmdldFpvb20oKSk7XG4gICAgfSk7XG4gIH1cblxuICBnZXRGdXJ0aGVzdFBvaW50RnJvbUxvY2F0aW9uKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgZXZlbnROYW1lOiBzdHJpbmcpIHtcbiAgICBsZXQgY29yZGluYXRlOiBDb3JkaW5hdGVEdG8sIGNvcm5lcnNBcnJheTogYW55ID0ge30sIGNvcm5lcjEsIGNvcm5lcjIsIGNvcm5lcjMsIGNvcm5lcjQsIGZhcmVzdFBvaW50O1xuICAgIGlmICh0aGlzLm1hcCAmJiB0aGlzLm1hcC5nZXRCb3VuZHMoKSkge1xuICAgICAgbGV0IHNXID0gdGhpcy5tYXAhLmdldEJvdW5kcygpIS5nZXRTb3V0aFdlc3QoKTtcbiAgICAgIGxldCBuRSA9IHRoaXMubWFwIS5nZXRCb3VuZHMoKSEuZ2V0Tm9ydGhFYXN0KCk7XG5cbiAgICAgIGNvcm5lcjEgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhzVy5sYXQoKSwgc1cubG5nKCkpKTtcbiAgICAgIGNvcm5lcjIgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhuRS5sYXQoKSwgbkUubG5nKCkpKTtcbiAgICAgIGNvcm5lcjMgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhzVy5sYXQoKSwgbkUubG5nKCkpKTtcbiAgICAgIGNvcm5lcjQgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhuRS5sYXQoKSwgc1cubG5nKCkpKTtcbiAgXG4gICAgICBjb3JuZXJzQXJyYXlbY29ybmVyMV0gPSB7XG4gICAgICAgIFwibGF0XCI6IHNXLmxhdCgpLFxuICAgICAgICBcImxuZ1wiOiBzVy5sbmcoKVxuICAgICAgfVxuICAgICAgY29ybmVyc0FycmF5W2Nvcm5lcjJdID0ge1xuICAgICAgICBcImxhdFwiOiBuRS5sYXQoKSxcbiAgICAgICAgXCJsbmdcIjogbkUubG5nKClcbiAgICAgIH1cbiAgICAgIGNvcm5lcnNBcnJheVtjb3JuZXIzXSA9IHtcbiAgICAgICAgXCJsYXRcIjogc1cubGF0KCksXG4gICAgICAgIFwibG5nXCI6IG5FLmxuZygpXG4gICAgICB9XG4gICAgICBjb3JuZXJzQXJyYXlbY29ybmVyNF0gPSB7XG4gICAgICAgIFwibGF0XCI6IG5FLmxhdCgpLFxuICAgICAgICBcImxuZ1wiOiBzVy5sbmcoKVxuICAgICAgfVxuICAgICAgZmFyZXN0UG9pbnQgPSBjb3JuZXJzQXJyYXlbTWF0aC5tYXgoY29ybmVyMSwgY29ybmVyMiwgY29ybmVyMywgY29ybmVyNCldO1xuICAgICAgY29yZGluYXRlID0gbmV3IENvcmRpbmF0ZUR0byhmYXJlc3RQb2ludC5sYXQsIGZhcmVzdFBvaW50LmxuZywgXCJcIiwgZXZlbnROYW1lKTtcbiAgICAgIHRoaXMubWFwUmFkaXVzLmVtaXQoY29yZGluYXRlKTtcbiAgICB9XG4gIH1cblxuICBjYWxjdWxhdGVEaXN0YW5jZShhTGF0OiBudW1iZXIsIGFMbmc6IG51bWJlciwgYkxhdDogbnVtYmVyLCBiTG5nOiBudW1iZXIpIHtcbiAgICByZXR1cm4gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhhTGF0LCBhTG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhiTGF0LCBiTG5nKSk7XG4gIH1cblxuICAvLyBEZWxldGVzIGFsbCBtYXJrZXJzIGluIHRoZSBhcnJheSBieSByZW1vdmluZyByZWZlcmVuY2VzIHRvIHRoZW0uXG4gIGRlbGV0ZU1hcmtlcnMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMubWFya2Vyc1tpXS5zZXRNYXAobnVsbCk7XG4gICAgfVxuICAgIHRoaXMubWFya2VycyA9IFtdO1xuICB9XG5cbiAgLy9saXN0TG9jYXRpb25bMF06IGNvZGVcbiAgLy9saXN0TG9jYXRpb25bMV06IExhdGl0dWRlIChwb2ludFkpXG4gIC8vbGlzdExvY2F0aW9uWzJdOiBMb25ndGl0dWRlIChwb2ludFgpXG4gIC8vbGlzdExvY2F0aW9uWzNdOiBpbmRleFxuICAvL2xpc3RMb2NhdGlvbls0XTogbGFiZWxcblxuICBzaG93TXVsdGl0cGxlTG9jYXRpb25zKGxpc3RMb2NhdGlvbjogQXJyYXk8YW55PiA9IFtdKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCBpbmZvd2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3c7XG4gICAgbGV0IG1hcmtlciwgaTtcbiAgICB0aGlzLmRlbGV0ZU1hcmtlcnMoKTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBsaXN0TG9jYXRpb24ubGVuZ3RoOyBpKyspIHtcbiAgICAgIC8vbWFya2VyIENPTkZJR1xuICAgICAgbGV0IG1hcmtPYmplY3Q6IGFueSA9IHtcbiAgICAgICAgcG9zaXRpb246IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGlzdExvY2F0aW9uW2ldWzFdLCBsaXN0TG9jYXRpb25baV1bMl0pLFxuICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLFxuICAgICAgICB6SW5kZXg6IDEwLFxuICAgICAgICBtYXA6IHRoaXMubWFwXG4gICAgICB9O1xuXG4gICAgICBtYXJrT2JqZWN0W1wiaWNvblwiXSA9IHtcbiAgICAgICAgdXJsOiB0aGlzLm1hcmtlckljb24sIC8vIHVybFxuICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLCAvLyBzY2FsZWQgc2l6ZVxuICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgIGFuY2hvcjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApIC8vIGFuY2hvclxuICAgICAgfTtcbiAgICAgIG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIobWFya09iamVjdCk7XG4gICAgICB0aGlzLm1hcmtlcnMucHVzaChtYXJrZXIpO1xuICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFya2VyLCAnY2xpY2snLCAoZnVuY3Rpb24gKG1hcmtlciwgaSkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIC8vZ2V0IGFkZHJlc3NcbiAgICAgICAgICBsZXQgbGF0bG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsaXN0TG9jYXRpb25baV1bMV0sIGxpc3RMb2NhdGlvbltpXVsyXSk7XG4gICAgICAgICAgLy9AdHMtaWdub3JlXG4gICAgICAgICAgc2VsZi5nZW9jb2Rlci5nZW9jb2RlKHsgJ2xhdExuZyc6IGxhdGxuZyB9LCAocmVzcG9uc2VzKSA9PiB7XG4gICAgICAgICAgICBsZXQgYWRkcmVzcztcbiAgICAgICAgICAgIGlmICghbGlzdExvY2F0aW9uW2ldWzRdKSB7XG4gICAgICAgICAgICAgIGlmIChyZXNwb25zZXMgJiYgcmVzcG9uc2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBhZGRyZXNzID0gcmVzcG9uc2VzWzBdLmZvcm1hdHRlZF9hZGRyZXNzO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGFkZHJlc3MgPSBcIkNhbm5vdCBkZXRlcm1pbmUgYWRkcmVzcyBhdCB0aGlzIGxvY2F0aW9uLlwiO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBhZGRyZXNzID0gbGlzdExvY2F0aW9uW2ldWzRdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzZWxmLmFjdGl2ZUluZm9XaW5kb3cgJiYgc2VsZi5hY3RpdmVJbmZvV2luZG93LmNsb3NlKCk7XG4gICAgICAgICAgICBzZWxmLmFjdGl2ZUluZm9XaW5kb3cgPSBpbmZvd2luZG93O1xuICAgICAgICAgICAgaW5mb3dpbmRvdy5zZXRDb250ZW50KGFkZHJlc3MpO1xuICAgICAgICAgICAgaW5mb3dpbmRvdy5vcGVuKHNlbGYubWFwLCBtYXJrZXIpO1xuXG4gICAgICAgICAgICAvL21hcmtlciBvYmplY3QgW3RpdGxlLCBsYXQsIGxvbmcsIGluZGV4XVxuICAgICAgICAgICAgaWYgKGxpc3RMb2NhdGlvbltpXVszXSkge1xuICAgICAgICAgICAgICBzZWxmLm1hcmtlckNsaWNrLmVtaXQobGlzdExvY2F0aW9uW2ldWzNdKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSkobWFya2VyLCBpKSk7XG4gICAgfVxuXG4gICAgLy8gZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHRoaXMubWFya2Vycy5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAvLyAgIGJvdW5kcy5leHRlbmQodGhpcy5tYXJrZXJzW2luZGV4XS5nZXRQb3NpdGlvbigpKTtcbiAgICAvLyB9XG4gICAgLy8gaWYgKHNlbGYubWFya2VyKSB7XG4gICAgLy8gICBib3VuZHMuZXh0ZW5kKHNlbGYubWFya2VyLmdldFBvc2l0aW9uKCkpO1xuICAgIC8vIH1cbiAgICAvLyBpZiAoc2VsZi5teUxvY2F0aW9uKSB7XG4gICAgLy8gICBib3VuZHMuZXh0ZW5kKHNlbGYubXlMb2NhdGlvbi5nZXRQb3NpdGlvbigpKTtcbiAgICAvLyB9XG5cbiAgICAvL3RoaXMubWFwLmZpdEJvdW5kcyhib3VuZHMpO1xuICB9XG5cbiAgcGFuVG8obGF0OiBudW1iZXIsIGxuZzogbnVtYmVyLCB6b29tTnVtYmVyOiBudW1iZXIgPSAxMikge1xuICAgIGxldCBteUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpO1xuICAgIHRoaXMubWFwPy5zZXRab29tKHpvb21OdW1iZXIpO1xuICAgIHRoaXMubWFwPy5wYW5UbyhteUxhdExuZyk7XG4gIH1cblxuICBhZGRNYWtlcihjb3JkaW5hdGU6IENvcmRpbmF0ZUR0bykge1xuICAgIGlmIChjb3JkaW5hdGUubGF0aXR1ZGUgJiYgY29yZGluYXRlLmxvbmd0aXR1ZGUpIHsgXG4gICAgICBsZXQgbWFya09iamVjdDogYW55ID0ge1xuICAgICAgICBwb3NpdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhjb3JkaW5hdGUubGF0aXR1ZGUsIGNvcmRpbmF0ZS5sb25ndGl0dWRlKSxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSxcbiAgICAgICAgekluZGV4OiAxMCxcbiAgICAgICAgaWNvbjoge1xuICAgICAgICAgIHVybDogdGhpcy5tYXJrZXJJY29uLCAvLyB1cmxcbiAgICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLCAvLyBzY2FsZWQgc2l6ZVxuICAgICAgICAgIG9yaWdpbjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApLCAvLyBvcmlnaW5cbiAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgICAgfSxcbiAgICAgICAgbWFwOiB0aGlzLm1hcFxuICAgICAgfTtcbiAgICAgIFxuICAgICAgbGV0IG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIobWFya09iamVjdCk7XG4gICAgICB0aGlzLm1hcmtlcnMucHVzaChtYXJrZXIpO1xuICAgICAgdGhpcy5wYW5Ubyhjb3JkaW5hdGUubGF0aXR1ZGUsIGNvcmRpbmF0ZS5sb25ndGl0dWRlLCB0aGlzLm1hcD8uZ2V0Wm9vbSgpKTtcbiAgICB9XG4gIH1cblxuICBnZW9jb2RlUG9zaXRpb24ocG9zOiBhbnksIGV2ZW50TmFtZTogc3RyaW5nIHwgdW5kZWZpbmVkID0gdW5kZWZpbmVkLCBmdW5jOiBGdW5jdGlvbiB8IG51bGwgPSBudWxsKTogdm9pZCB7XG4gICAgLy9AdHMtaWdub3JlXG4gICAgdGhpcy5nZW9jb2Rlci5nZW9jb2RlKHsgJ2xhdExuZyc6IHBvcyB9LCAocmVzcG9uc2VzKSA9PiB7XG4gICAgICBpZiAocmVzcG9uc2VzICYmIHJlc3BvbnNlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLnRpdGxlID0gcmVzcG9uc2VzWzBdLmZvcm1hdHRlZF9hZGRyZXNzO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHJlc3BvbnNlc1swXS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHJlc3BvbnNlc1swXS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLnRpdGxlID0gXCJDYW5ub3QgZGV0ZXJtaW5lIGFkZHJlc3MgYXQgdGhpcyBsb2NhdGlvbi5cIjtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSB1bmRlZmluZWQ7XG4gICAgICB9XG4gICAgICB0aGlzLnRyaWdnZXJVcGRhdGVEYXRhKGV2ZW50TmFtZSk7XG4gICAgICBpZiAoZnVuYykge1xuICAgICAgICBmdW5jKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBhZGRTZWFyY2hCb3hFdmVudCgpOiB2b2lkIHtcbiAgICB0aGlzLnNlYXJjaEJveD8uYWRkTGlzdGVuZXIoJ3BsYWNlc19jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgaWYgKHRoaXMubWFya2VyKSB7XG4gICAgICAgIHRoaXMubWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgIH1cblxuICAgICAgLy9zZWxmLmRlbGV0ZU1hcmtlcnMoKTtcbiAgICAgIGxldCBwbGFjZXM6IGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZVJlc3VsdFtdfHVuZGVmaW5lZCA9IHRoaXMuc2VhcmNoQm94IS5nZXRQbGFjZXMoKTtcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHBsYWNlcykgJiYgcGxhY2VzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgbGV0IGJvdW5kcyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmdCb3VuZHMoKTtcbiAgICAgIHBsYWNlcyEuZm9yRWFjaCgocGxhY2UpID0+IHtcbiAgICAgICAgbGV0IG1hcmtlck9iajogYW55ID0ge1xuICAgICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiB0aGlzLm1hcmtlckRyYWdnYWJsZSxcbiAgICAgICAgICB0aXRsZTogcGxhY2UubmFtZSxcbiAgICAgICAgICBhbmltYXRpb246IGdvb2dsZS5tYXBzLkFuaW1hdGlvbi5EUk9QLFxuICAgICAgICAgIHBvc2l0aW9uOiBwbGFjZS5nZW9tZXRyeT8ubG9jYXRpb25cbiAgICAgICAgfTtcblxuICAgICAgICBpZiAoIXBsYWNlLmdlb21ldHJ5IHx8ICFwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbikge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmV0dXJuZWQgcGxhY2UgY29udGFpbnMgbm8gZ2VvbWV0cnlcIik7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5tYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtlck9iaik7XG5cbiAgICAgICAgLy8gc2VsZi5tYXJrZXJzLnB1c2goc2VsZi5tYXJrZXIpO1xuICAgICAgICB0aGlzLmFkZERyYWdlbmRMaXN0ZW5lcigpO1xuXG4gICAgICAgIHRoaXMuc2VhcmNoRXZlbnQuZW1pdChuZXcgQ29yZGluYXRlRHRvKG1hcmtlck9iai5wb3NpdGlvbi5sYXQoKSwgbWFya2VyT2JqLnBvc2l0aW9uLmxuZygpKSk7XG5cbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS50aXRsZSA9IHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzO1xuXG4gICAgICAgIHRoaXMudHJpZ2dlclVwZGF0ZURhdGEobWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMuc2VhcmNoKTtcblxuICAgICAgICBpZiAocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpIHtcbiAgICAgICAgICBib3VuZHMudW5pb24ocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGJvdW5kcy5leHRlbmQocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHRoaXMubWFwPy5maXRCb3VuZHMoYm91bmRzKTtcbiAgICAgIHRoaXMubWFwPy5zZXRab29tKDEyKTtcbiAgICAgIGlmICh0aGlzLm1hcmtlciAmJiB0aGlzLm1hcmtlci5nZXRQb3NpdGlvbigpKSB7XG4gICAgICAgIC8vQHRzLWlnbm9yZVxuICAgICAgICB0aGlzLm1hcCEucGFuVG8odGhpcy5tYXJrZXIuZ2V0UG9zaXRpb24oKSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBhZGRDbGlja0xpc3RlbmVyKCk6IHZvaWQge1xuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwISwgJ2NsaWNrJywgKGV2ZW50OiBnb29nbGUubWFwcy5NYXBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICAvL2V2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbihldmVudC5sYXRMbmcsIG1hcENvbmZpZy5tYXBFdmVudE5hbWVzLm1hcENsaWNrLCAoKSA9PiB7XG4gICAgICAgIHRoaXMubWFya2VyIS5zZXRNYXAobnVsbCk7XG4gICAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgcG9zaXRpb246IGV2ZW50LmxhdExuZyxcbiAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgIGRyYWdnYWJsZTogdGhpcy5tYXJrZXJEcmFnZ2FibGUsXG5cbiAgICAgICAgICB0aXRsZTogdGhpcy5jb3JkaW5hdGUudGl0bGVcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGFkZERyYWdlbmRMaXN0ZW5lcigpOiB2b2lkIHtcbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcmtlciEsICdkcmFnZW5kJywgKCkgPT4ge1xuICAgICAgLy8gZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHRoaXMuZGVsZXRlTWFya2VycygpO1xuICAgICAgdGhpcy5nZW9jb2RlUG9zaXRpb24odGhpcy5tYXJrZXIhLmdldFBvc2l0aW9uKCksIG1hcENvbmZpZy5tYXBFdmVudE5hbWVzLm1hcmtlckRyYWcpO1xuICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSB0aGlzLm1hcmtlciEuZ2V0UG9zaXRpb24oKT8ubGF0KCk7XG4gICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gdGhpcy5tYXJrZXIhLmdldFBvc2l0aW9uKCk/LmxuZygpO1xuICAgIH0pO1xuICB9XG5cbiAgdHJpZ2dlclVwZGF0ZURhdGEoZXZlbnROYW1lOiBzdHJpbmcgfCB1bmRlZmluZWQpOiB2b2lkIHtcbiAgICB0aGlzLmNvcmRpbmF0ZS5ldmVudE5hbWUgPSBldmVudE5hbWU7XG4gICAgdGhpcy52aWFDb250cm9sLnNldFZhbHVlKHRoaXMuY29yZGluYXRlKTtcbiAgICB0aGlzLmNvcmRpbmF0ZUNoYW5nZT8uZW1pdCh0aGlzLmNvcmRpbmF0ZSk7XG4gIH1cblxuICBtb3ZlVG9NeUxvY2F0aW9uKCkge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBpZiAobmF2aWdhdG9yLmdlb2xvY2F0aW9uKSB7XG4gICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChsb2NhdGlvbikgPT57XG4gICAgICAgIGlmIChzZWxmLm1hcmtlcikge1xuICAgICAgICAgIHNlbGYubWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzZWxmLmRlbGV0ZU1hcmtlcnMoKTtcbiAgICAgICAgc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGU7XG4gICAgICAgIHNlbGYuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuXG4gICAgICAgIHRoaXMubXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxvY2F0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG9jYXRpb24uY29vcmRzLmxvbmdpdHVkZSk7XG5cbiAgICAgICAgaWYgKHNlbGYubXlMb2NhdGlvbikge1xuICAgICAgICAgIHNlbGYubXlMb2NhdGlvbi5zZXRNYXAobnVsbCk7XG4gICAgICAgIH1cbiAgICAgICAgc2VsZi5teUxvY2F0aW9uID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgcG9zaXRpb246IHNlbGYubXlMYXRMbmcsXG4gICAgICAgICAgaWNvbjoge1xuICAgICAgICAgICAgdXJsOiBzZWxmLm15TG9jYXRpb25NYXJrZXIsIC8vIHVybFxuICAgICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgICAgIG9yaWdpbjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApLCAvLyBvcmlnaW5cbiAgICAgICAgICAgIGFuY2hvcjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDE1LCAxNSkgLy8gYW5jaG9yXG4gICAgICAgICAgfSxcbiAgICAgICAgICBtYXA6IHNlbGYubWFwLFxuICAgICAgICAgIGRyYWdnYWJsZTogZmFsc2UsXG4gICAgICAgICAgdGl0bGU6IHNlbGYuY29yZGluYXRlLnRpdGxlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHNlbGYuZ2VvY29kZVBvc2l0aW9uKHNlbGYubXlMb2NhdGlvbi5nZXRQb3NpdGlvbigpLCBtYXBDb25maWcubWFwRXZlbnROYW1lcy5teUxvY2F0aW9uKTtcbiAgICAgICAgLy9zZWxmLmFkZERyYWdlbmRMaXN0ZW5lcigpO1xuICAgICAgICBzZWxmLmdldEN1cnJlbnRMb2NhdGlvbkV2ZW50LmVtaXQobmV3IENvcmRpbmF0ZUR0byhsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxvY2F0aW9uLmNvb3Jkcy5sb25naXR1ZGUpKTtcbiAgICAgICAgdGhpcy5tYXAhLnNldENlbnRlcih0aGlzLm15TGF0TG5nKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhcIkdlb2xvY2F0aW9uIGlzIG5vdCBzdXBwb3J0ZWQgYnkgdGhpcyBicm93c2VyLlwiKTtcbiAgICB9XG4gIH1cblxuICBnZXRSYWRpdXMoZGlzdGFuY2U6IG51bWJlciA9IDEwMCkge1xuICAgIGlmICh0aGlzLm15TG9jYXRpb24pIHtcbiAgICAgIGxldCB3ID0gdGhpcy5tYXAhLmdldEJvdW5kcygpPy5nZXRTb3V0aFdlc3QoKTtcbiAgICAgIGxldCBjZW50ZXJMbmcgPSB0aGlzLm15TG9jYXRpb24uZ2V0UG9zaXRpb24oKTtcbiAgICAgIGlmIChjZW50ZXJMbmcgJiYgdykge1xuICAgICAgICBkaXN0YW5jZSA9IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKGNlbnRlckxuZywgdyk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZ2V0VG9wRGlzdGFuY2VGcm9tTWFya2VyKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgZXZlbnROYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKCFsYXQgfHwgIWxuZykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKTtcbiAgICAvLyBHZXQgbWFwIGJvdW5kc1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcCEuZ2V0Qm91bmRzKCk7XG4gICAgdGhpcy5nZXRUb3BDb3JkaW5hdG9yRXZlbnQuZW1pdChuZXcgQ29yZGluYXRlRHRvKGJvdW5kcz8uZ2V0Tm9ydGhFYXN0KCkubGF0KCksIG15TGF0TG5nLmxuZygpLCBcIlwiLCBldmVudE5hbWUpKTtcbiAgfVxuXG4gIGRyYXdDaXJjbGUoY2VudGVyTGF0OiBudW1iZXIsIGNlbnRlckxuZzogbnVtYmVyLCBwb2ludExhdDogbnVtYmVyLCBwb2ludExuZzogbnVtYmVyLCBmaWxsQ29sb3I6IHN0cmluZyA9IFwiI0ZGMFwiKSB7XG4gICAgbGV0IG15TGF0TG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhjZW50ZXJMYXQsIGNlbnRlckxuZyk7XG4gICAgLy8gR2V0IG1hcCBib3VuZHNcbiAgICBsZXQgcG9pbnQgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHBvaW50TGF0LCBwb2ludExuZyk7XG4gICAgbGV0IGRpc3RhbmNlVG9Ub3AgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihteUxhdExuZywgcG9pbnQpO1xuICAgIGxldCByYWRpdXMgPSBkaXN0YW5jZVRvVG9wO1xuICAgIHRoaXMuY2lyY2xlPy5zZXRNYXAobnVsbCk7XG4gICAgdGhpcy5jaXJjbGUgPSBuZXcgZ29vZ2xlLm1hcHMuQ2lyY2xlKHtcbiAgICAgIHN0cm9rZUNvbG9yOiAnI0ZGMCcsXG4gICAgICBzdHJva2VPcGFjaXR5OiAwLjgsXG4gICAgICBzdHJva2VXZWlnaHQ6IDIsXG4gICAgICBmaWxsQ29sb3I6IGZpbGxDb2xvcixcbiAgICAgIGZpbGxPcGFjaXR5OiAwLjM1LFxuICAgICAgZWRpdGFibGU6IHRydWUsXG4gICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgY2VudGVyOiBteUxhdExuZyxcbiAgICAgIHJhZGl1czogcmFkaXVzXG4gICAgfSk7XG4gIH1cblxuICByZXF1aXJlZChmb3JtOiBGb3JtQ29udHJvbCkge1xuICAgIHJldHVybiBGb3JtVXRpbHMuZ2V0SW5zdGFuY2UoKS5yZXF1aXJlZChmb3JtKTtcbiAgfVxufSIsIjxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICA8ZGl2ICpuZ0lmPVwiIWhpZGVTZWFyY2hUZXh0Qm94XCIgY2xhc3M9XCJjb2wtc20tMTIgbWFwLXNlYXJjaC1jb250YWluZXJcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cbiAgICAgICAgICAgIDxsYWJlbCAqbmdJZj1cImkxOG5cIiBbYXR0ci5mb3JdPVwibWFwSWRcIj5cbiAgICAgICAgICAgICAgICB7e2kxOG4ubGFiZWx9fVxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZGFuZ2VyLXRleHRcIj4ge3tyZXF1aXJlZCh2aWFDb250cm9sKX19PC9zcGFuPlxuICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ2aWEtbWFwLXNlYXJjaFwiPlxuICAgICAgICAgICAgICAgIDxpbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiBcbiAgICAgICAgICAgICAgICAgICAgW2F0dHIuaWRdPVwibWFwSWRcIlxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiIFxuICAgICAgICAgICAgICAgICAgICAjc2VhcmNoSW5wdXQgXG4gICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJzZWFyY2hQbGFjZWhvbGVyXCJcbiAgICAgICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJjb3JkaW5hdGUudGl0bGVcIj5cbiAgICAgICAgICAgICAgICA8aW1nICpuZ0lmPVwiaWNvblNlYXJjaEltZ1wiIFtzcmNdPVwiaWNvblNlYXJjaEltZ1wiIGNsYXNzPVwiaW5wdXQtc2VhcmNoLWljb25cIj5cbiAgICAgICAgICAgICAgICA8aSAqbmdJZj1cIiFpY29uU2VhcmNoSW1nXCJjbGFzcz1cImZhcyBmYS1zZWFyY2ggaW5wdXQtc2VhcmNoLWljb25cIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz1cImNvbC1zbS0xMlwiIHN0eWxlPVwicG9zaXRpb246IHJlbGF0aXZlO1wiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiZ29vZ2xlLW1hcHNcIiAjZ29vZ2xlTWFwPjwvZGl2PlxuICAgICAgICA8YnV0dG9uICAqbmdJZj1cInNob3dNeUxvY2F0aW9uICYmIG1hcExvYWRlZCAmJiBteUxvY2FsdGlvbkxvYWRlZFwiIChjbGljayk9XCJtb3ZlVG9NeUxvY2F0aW9uKClcIiBjbGFzcz1cIm15LWxvY2F0aW9uLWJ0blwiPlxuICAgICAgICAgICAgPGltZyAqbmdJZj1cImljb25NeUxvY2F0aW9uSW1nXCIgW3NyY109XCJpY29uTXlMb2NhdGlvbkltZ1wiPlxuICAgICAgICAgICAgPGkgKm5nSWY9XCIhaWNvbk15TG9jYXRpb25JbWdcIiBjbGFzcz1cImZhcyBmYS1sb2NhdGlvbi1hcnJvd1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgPC9idXR0b24+XG4gICAgPC9kaXY+XG48L2Rpdj4iXX0=