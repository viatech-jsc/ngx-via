import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FullTextSearchPipe {
    constructor() { }
    transform(value, keys, term) {
        if (!term)
            return value;
        //@ts-ignore
        return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe }); }
    static { this.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, name: "fullTextSearch", pure: false }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: FullTextSearchPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'fullTextSearch',
                    pure: false
                }]
        }], ctorParameters: () => [] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS9kcm9wZG93bi9waXBlL2Ryb3Bkb3duLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBTXBELE1BQU0sT0FBTyxrQkFBa0I7SUFFM0IsZ0JBQWdCLENBQUM7SUFFVixTQUFTLENBQUMsS0FBVSxFQUFFLElBQVksRUFBRSxJQUFZO1FBQ25ELElBQUksQ0FBQyxJQUFJO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDeEIsWUFBWTtRQUNaLE9BQU8sQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pJLENBQUM7OEdBUlEsa0JBQWtCOzRHQUFsQixrQkFBa0I7OzJGQUFsQixrQkFBa0I7a0JBSjlCLElBQUk7bUJBQUM7b0JBQ0YsSUFBSSxFQUFFLGdCQUFnQjtvQkFDdEIsSUFBSSxFQUFFLEtBQUs7aUJBQ2QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgICBuYW1lOiAnZnVsbFRleHRTZWFyY2gnLFxuICAgIHB1cmU6IGZhbHNlXG59KVxuZXhwb3J0IGNsYXNzIEZ1bGxUZXh0U2VhcmNoUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIHB1YmxpYyB0cmFuc2Zvcm0odmFsdWU6IGFueSwga2V5czogc3RyaW5nLCB0ZXJtOiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKCF0ZXJtKSByZXR1cm4gdmFsdWU7XG4gICAgICAgIC8vQHRzLWlnbm9yZVxuICAgICAgICByZXR1cm4gKHZhbHVlIHx8IFtdKS5maWx0ZXIoaXRlbSA9PiBrZXlzLnNwbGl0KCcsJykuc29tZShrZXkgPT4gaXRlbS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIG5ldyBSZWdFeHAodGVybSwgJ2dpJykudGVzdChpdGVtW2tleV0pKSk7XG4gICAgfVxufSJdfQ==