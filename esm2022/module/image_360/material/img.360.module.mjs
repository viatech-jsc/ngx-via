import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatImage360Component } from './img.360';
import * as i0 from "@angular/core";
export class MatImage360Module {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, declarations: [MatImage360Component], imports: [MatDialogModule], exports: [MatImage360Component] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, imports: [MatDialogModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatImage360Module, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [MatImage360Component],
                    imports: [
                        MatDialogModule
                    ],
                    exports: [MatImage360Component]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nLjM2MC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvaW1hZ2VfMzYwL21hdGVyaWFsL2ltZy4zNjAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLFdBQVcsQ0FBQzs7QUFTakQsTUFBTSxPQUFPLGlCQUFpQjs4R0FBakIsaUJBQWlCOytHQUFqQixpQkFBaUIsaUJBTmIsb0JBQW9CLGFBRWpDLGVBQWUsYUFFUCxvQkFBb0I7K0dBRW5CLGlCQUFpQixZQUoxQixlQUFlOzsyRkFJTixpQkFBaUI7a0JBUDdCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQ3BDLE9BQU8sRUFBRTt3QkFDUCxlQUFlO3FCQUNoQjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0RGlhbG9nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcbmltcG9ydCB7IE1hdEltYWdlMzYwQ29tcG9uZW50IH0gZnJvbSAnLi9pbWcuMzYwJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbTWF0SW1hZ2UzNjBDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgTWF0RGlhbG9nTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtNYXRJbWFnZTM2MENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTWF0SW1hZ2UzNjBNb2R1bGUgeyB9XG4iXX0=