import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { Image360Component } from './img.360';
import * as i0 from "@angular/core";
export class Image360Module {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, declarations: [Image360Component], imports: [NgbModalModule], exports: [Image360Component] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, imports: [NgbModalModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: Image360Module, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [Image360Component],
                    imports: [
                        NgbModalModule
                    ],
                    exports: [Image360Component]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nLjM2MC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvaW1hZ2VfMzYwL2RlZmF1bHQvaW1nLjM2MC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sV0FBVyxDQUFDOztBQVM5QyxNQUFNLE9BQU8sY0FBYzs4R0FBZCxjQUFjOytHQUFkLGNBQWMsaUJBTlYsaUJBQWlCLGFBRTlCLGNBQWMsYUFFTixpQkFBaUI7K0dBRWhCLGNBQWMsWUFKdkIsY0FBYzs7MkZBSUwsY0FBYztrQkFQMUIsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakMsT0FBTyxFQUFFO3dCQUNQLGNBQWM7cUJBQ2Y7b0JBQ0QsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQzdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5nYk1vZGFsTW9kdWxlIH0gZnJvbSAnQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAnO1xuaW1wb3J0IHsgSW1hZ2UzNjBDb21wb25lbnQgfSBmcm9tICcuL2ltZy4zNjAnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtJbWFnZTM2MENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBOZ2JNb2RhbE1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbSW1hZ2UzNjBDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEltYWdlMzYwTW9kdWxlIHsgfVxuIl19