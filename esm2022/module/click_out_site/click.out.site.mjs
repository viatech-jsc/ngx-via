import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export class ClickOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.viaClickOutSite = new EventEmitter();
        this.clickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.viaClickOutSite && this.viaClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1) {
                this.viaClickOutSite.emit("");
            }
        }
        else if (this.clickOutSite && this.clickOutSite.observers.length > 0) {
            if (!this.el.nativeElement.contains($event.target)) {
                this.clickOutSite.emit("");
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive }); }
    static { this.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "14.0.0", version: "17.3.5", type: ClickOutSiteDirective, selector: "[viaClickOutSite]", outputs: { viaClickOutSite: "viaClickOutSite", clickOutSite: "clickOutSite" }, host: { listeners: { "document:click": "documentClick($event)" } }, ngImport: i0 }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: "[viaClickOutSite]"
                }]
        }], ctorParameters: () => [{ type: i0.ElementRef }], propDecorators: { viaClickOutSite: [{
                type: Output
            }], clickOutSite: [{
                type: Output
            }], documentClick: [{
                type: HostListener,
                args: ["document:click", ["$event"]]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2sub3V0LnNpdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvY2xpY2tfb3V0X3NpdGUvY2xpY2sub3V0LnNpdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxZQUFZLEVBQ1osVUFBVSxFQUNWLE1BQU0sRUFDTixZQUFZLEVBQ2IsTUFBTSxlQUFlLENBQUM7O0FBS3ZCLE1BQU0sT0FBTyxxQkFBcUI7SUFJaEMsWUFBb0IsRUFBYztRQUFkLE9BQUUsR0FBRixFQUFFLENBQVk7UUFIeEIsb0JBQWUsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzRCxpQkFBWSxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO0lBRTdCLENBQUM7SUFHdEMsYUFBYSxDQUFDLE1BQVc7UUFDdkIsSUFBRyxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUMsQ0FBQztZQUNwRSxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUM1RSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNoQyxDQUFDO1FBQ0gsQ0FBQzthQUFNLElBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDOzhHQWpCVSxxQkFBcUI7a0dBQXJCLHFCQUFxQjs7MkZBQXJCLHFCQUFxQjtrQkFIakMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2lCQUM5QjsrRUFFVyxlQUFlO3NCQUF4QixNQUFNO2dCQUNHLFlBQVk7c0JBQXJCLE1BQU07Z0JBS1AsYUFBYTtzQkFEWixZQUFZO3VCQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgRGlyZWN0aXZlLFxuICBIb3N0TGlzdGVuZXIsXG4gIEVsZW1lbnRSZWYsXG4gIE91dHB1dCxcbiAgRXZlbnRFbWl0dGVyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogXCJbdmlhQ2xpY2tPdXRTaXRlXVwiXG59KVxuZXhwb3J0IGNsYXNzIENsaWNrT3V0U2l0ZURpcmVjdGl2ZSB7XG4gIEBPdXRwdXQoKSB2aWFDbGlja091dFNpdGU6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2xpY2tPdXRTaXRlOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsOiBFbGVtZW50UmVmKSB7fVxuXG4gIEBIb3N0TGlzdGVuZXIoXCJkb2N1bWVudDpjbGlja1wiLCBbXCIkZXZlbnRcIl0pXG4gIGRvY3VtZW50Q2xpY2soJGV2ZW50OiBhbnkpIHtcbiAgICBpZih0aGlzLnZpYUNsaWNrT3V0U2l0ZSAmJiB0aGlzLnZpYUNsaWNrT3V0U2l0ZS5vYnNlcnZlcnMubGVuZ3RoID4gMCl7XG4gICAgICBpZiAodGhpcy5lbC5uYXRpdmVFbGVtZW50LmlubmVySFRNTC5pbmRleE9mKCRldmVudC50YXJnZXQuaW5uZXJIVE1MKSA9PT0gLTEpIHtcbiAgICAgICAgdGhpcy52aWFDbGlja091dFNpdGUuZW1pdChcIlwiKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYodGhpcy5jbGlja091dFNpdGUgJiYgdGhpcy5jbGlja091dFNpdGUub2JzZXJ2ZXJzLmxlbmd0aCA+IDApe1xuICAgICAgaWYgKCF0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuY29udGFpbnMoJGV2ZW50LnRhcmdldCkpIHtcbiAgICAgICAgdGhpcy5jbGlja091dFNpdGUuZW1pdChcIlwiKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==