import { ClickOutSiteDirective } from "./click.out.site";
import { NgModule } from "@angular/core";
import * as i0 from "@angular/core";
export class ClickOutSiteModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, declarations: [ClickOutSiteDirective], exports: [ClickOutSiteDirective] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ClickOutSiteModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [],
                    declarations: [ClickOutSiteDirective],
                    exports: [ClickOutSiteDirective]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2sub3V0LnNpdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL2NsaWNrX291dF9zaXRlL2NsaWNrLm91dC5zaXRlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU96QyxNQUFNLE9BQU8sa0JBQWtCOzhHQUFsQixrQkFBa0I7K0dBQWxCLGtCQUFrQixpQkFIZCxxQkFBcUIsYUFDMUIscUJBQXFCOytHQUVwQixrQkFBa0I7OzJGQUFsQixrQkFBa0I7a0JBTDlCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7b0JBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO2lCQUNqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENsaWNrT3V0U2l0ZURpcmVjdGl2ZSB9IGZyb20gXCIuL2NsaWNrLm91dC5zaXRlXCI7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtdLFxuICBkZWNsYXJhdGlvbnM6IFtDbGlja091dFNpdGVEaXJlY3RpdmVdLFxuICBleHBvcnRzOiBbQ2xpY2tPdXRTaXRlRGlyZWN0aXZlXVxufSlcbmV4cG9ydCBjbGFzcyBDbGlja091dFNpdGVNb2R1bGUge31cbiJdfQ==