export class TypeaheadOptionDto {
    constructor(url = "", multiple = true, inlineSingleSearch = false) {
        this._url = url;
        this._multiple = multiple;
        this._inlineSingleSearch = inlineSingleSearch;
        this._debounceTime = 400;
        this._itemsSelected = new Array();
        this._items = new Array();
        this._errorMessage = "";
        this._isLoading = false;
        this._minLengthToLoad = 1;
        this._noDataFound = false;
        this._keepHistory = false;
    }
    /**
     * Getter url
     * return {string}
     */
    get url() {
        return this._url;
    }
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple() {
        return this._multiple;
    }
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch() {
        return this._inlineSingleSearch;
    }
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime() {
        return this._debounceTime;
    }
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected() {
        return this._itemsSelected;
    }
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items() {
        return this._items;
    }
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage() {
        return this._errorMessage;
    }
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad() {
        return this._minLengthToLoad;
    }
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound() {
        return this._noDataFound;
    }
    get keepHistory() {
        return this._keepHistory;
    }
    /**
     * Setter url
     * param {string} value
     */
    set url(value) {
        this._url = value;
    }
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value) {
        this._multiple = value;
    }
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value) {
        this._inlineSingleSearch = value;
    }
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value) {
        this._debounceTime = value;
    }
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value) {
        this._itemsSelected = value;
    }
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value) {
        this._items = value;
    }
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value) {
        this._errorMessage = value;
    }
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value) {
        this._isLoading = value;
    }
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value) {
        this._minLengthToLoad = value;
    }
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value) {
        this._noDataFound = value;
    }
    set keepHistory(_keepHistory) {
        this._keepHistory = _keepHistory;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLm9wdGlvbi5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdHlwZWFoZWFkL2R0by90eXBlYWhlYWQub3B0aW9uLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8sa0JBQWtCO0lBYzNCLFlBQ0ksTUFBYyxFQUFFLEVBQ2hCLFdBQW9CLElBQUksRUFDeEIscUJBQThCLEtBQUs7UUFFbkMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGtCQUFrQixDQUFDO1FBQzlDLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxLQUFLLEVBQW9CLENBQUM7UUFDcEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBQztRQUM1QyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLEdBQUc7UUFDVixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUTtRQUNmLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxrQkFBa0I7UUFDekIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsWUFBWTtRQUNuQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsYUFBYTtRQUNwQixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsS0FBSztRQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxTQUFTO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxlQUFlO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFdBQVc7UUFDbEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCxJQUFXLFdBQVc7UUFDbEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLEdBQUcsQ0FBQyxLQUFhO1FBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFFBQVEsQ0FBQyxLQUFjO1FBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGtCQUFrQixDQUFDLEtBQWM7UUFDeEMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztJQUNyQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxhQUFhLENBQUMsS0FBeUI7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsS0FBSyxDQUFDLEtBQXlCO1FBQ3RDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVksQ0FBQyxLQUFhO1FBQ2pDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFNBQVMsQ0FBQyxLQUFjO1FBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGVBQWUsQ0FBQyxLQUFhO1FBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsV0FBVyxDQUFDLEtBQWM7UUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVELElBQVcsV0FBVyxDQUFDLFlBQXFCO1FBQ3hDLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO0lBQ3JDLENBQUM7Q0FFSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFR5cGVhaGVhZEl0ZW1EdG8gfSBmcm9tIFwiLi90eXBlYWhlYWQuaXRlbS5kdG9cIjtcblxuZXhwb3J0IGNsYXNzIFR5cGVhaGVhZE9wdGlvbkR0byB7XG4gICAgcHJpdmF0ZSBfdXJsOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfbXVsdGlwbGU6IGJvb2xlYW47XG4gICAgLy9pbmxpbmUgc2VhcmNoIHdpdGhvdXQgY2FyZCBvbiBzZWFyY2ggYm94XG4gICAgcHJpdmF0ZSBfaW5saW5lU2luZ2xlU2VhcmNoOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2RlYm91bmNlVGltZTogbnVtYmVyO1xuICAgIHByaXZhdGUgX2l0ZW1zU2VsZWN0ZWQ6IEFycmF5PFR5cGVhaGVhZEl0ZW1EdG8+O1xuICAgIHByaXZhdGUgX2l0ZW1zOiBBcnJheTxUeXBlYWhlYWRJdGVtRHRvPjtcbiAgICBwcml2YXRlIF9lcnJvck1lc3NhZ2U6IHN0cmluZztcbiAgICBwcml2YXRlIF9pc0xvYWRpbmc6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfbWluTGVuZ3RoVG9Mb2FkOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfbm9EYXRhRm91bmQ6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfa2VlcEhpc3Rvcnk6IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgdXJsOiBzdHJpbmcgPSBcIlwiLFxuICAgICAgICBtdWx0aXBsZTogYm9vbGVhbiA9IHRydWUsXG4gICAgICAgIGlubGluZVNpbmdsZVNlYXJjaDogYm9vbGVhbiA9IGZhbHNlXG4gICAgKSB7XG4gICAgICAgIHRoaXMuX3VybCA9IHVybDtcbiAgICAgICAgdGhpcy5fbXVsdGlwbGUgPSBtdWx0aXBsZTtcbiAgICAgICAgdGhpcy5faW5saW5lU2luZ2xlU2VhcmNoID0gaW5saW5lU2luZ2xlU2VhcmNoO1xuICAgICAgICB0aGlzLl9kZWJvdW5jZVRpbWUgPSA0MDA7XG4gICAgICAgIHRoaXMuX2l0ZW1zU2VsZWN0ZWQgPSBuZXcgQXJyYXk8VHlwZWFoZWFkSXRlbUR0bz4oKTtcbiAgICAgICAgdGhpcy5faXRlbXMgPSBuZXcgQXJyYXk8VHlwZWFoZWFkSXRlbUR0bz4oKTtcbiAgICAgICAgdGhpcy5fZXJyb3JNZXNzYWdlID0gXCJcIjtcbiAgICAgICAgdGhpcy5faXNMb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX21pbkxlbmd0aFRvTG9hZCA9IDE7XG4gICAgICAgIHRoaXMuX25vRGF0YUZvdW5kID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2tlZXBIaXN0b3J5ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHVybFxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgdXJsKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl91cmw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG11bHRpcGxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbXVsdGlwbGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9tdWx0aXBsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaW5saW5lU2luZ2xlU2VhcmNoXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgaW5saW5lU2luZ2xlU2VhcmNoKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faW5saW5lU2luZ2xlU2VhcmNoO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWJvdW5jZVRpbWVcbiAgICAgKiByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGRlYm91bmNlVGltZSgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5fZGVib3VuY2VUaW1lO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpdGVtc1NlbGVjdGVkXG4gICAgICogcmV0dXJuIHtUeXBlYWhlYWRJdGVtW119XG4gICAgICovXG4gICAgcHVibGljIGdldCBpdGVtc1NlbGVjdGVkKCk6IFR5cGVhaGVhZEl0ZW1EdG9bXSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtc1NlbGVjdGVkO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpdGVtc1xuICAgICAqIHJldHVybiB7VHlwZWFoZWFkSXRlbVtdfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgaXRlbXMoKTogVHlwZWFoZWFkSXRlbUR0b1tdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2l0ZW1zO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBlcnJvck1lc3NhZ2VcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGVycm9yTWVzc2FnZSgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5fZXJyb3JNZXNzYWdlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpc0xvYWRpbmdcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBpc0xvYWRpbmcoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0xvYWRpbmc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG1pbkxlbmd0aFRvTG9hZFxuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbWluTGVuZ3RoVG9Mb2FkKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9taW5MZW5ndGhUb0xvYWQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG5vRGF0YUZvdW5kXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbm9EYXRhRm91bmQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9ub0RhdGFGb3VuZDtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGtlZXBIaXN0b3J5KCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fa2VlcEhpc3Rvcnk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHVybFxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCB1cmwodmFsdWU6IHN0cmluZykge1xuICAgICAgICB0aGlzLl91cmwgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbXVsdGlwbGVcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IG11bHRpcGxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX211bHRpcGxlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGlubGluZVNpbmdsZVNlYXJjaFxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaW5saW5lU2luZ2xlU2VhcmNoKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2lubGluZVNpbmdsZVNlYXJjaCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkZWJvdW5jZVRpbWVcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZGVib3VuY2VUaW1lKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fZGVib3VuY2VUaW1lID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGl0ZW1zU2VsZWN0ZWRcbiAgICAgKiBwYXJhbSB7VHlwZWFoZWFkSXRlbVtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXRlbXNTZWxlY3RlZCh2YWx1ZTogVHlwZWFoZWFkSXRlbUR0b1tdKSB7XG4gICAgICAgIHRoaXMuX2l0ZW1zU2VsZWN0ZWQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgaXRlbXNcbiAgICAgKiBwYXJhbSB7VHlwZWFoZWFkSXRlbVtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXRlbXModmFsdWU6IFR5cGVhaGVhZEl0ZW1EdG9bXSkge1xuICAgICAgICB0aGlzLl9pdGVtcyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBlcnJvck1lc3NhZ2VcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZXJyb3JNZXNzYWdlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fZXJyb3JNZXNzYWdlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGlzTG9hZGluZ1xuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXNMb2FkaW5nKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2lzTG9hZGluZyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBtaW5MZW5ndGhUb0xvYWRcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgbWluTGVuZ3RoVG9Mb2FkKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fbWluTGVuZ3RoVG9Mb2FkID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG5vRGF0YUZvdW5kXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBub0RhdGFGb3VuZCh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9ub0RhdGFGb3VuZCA9IHZhbHVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZXQga2VlcEhpc3RvcnkoX2tlZXBIaXN0b3J5OiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2tlZXBIaXN0b3J5ID0gX2tlZXBIaXN0b3J5O1xuICAgIH1cblxufSJdfQ==