import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export class TypeaheadOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.typeaheadClickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.typeaheadClickOutSite && this.typeaheadClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1 || !this.el.nativeElement.contains($event.target)) {
                this.typeaheadClickOutSite.emit("");
            }
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadOutSiteDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive }); }
    static { this.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "14.0.0", version: "17.3.5", type: TypeaheadOutSiteDirective, selector: "[typeaheadClickOutSite]", outputs: { typeaheadClickOutSite: "typeaheadClickOutSite" }, host: { listeners: { "document:click": "documentClick($event)" } }, ngImport: i0 }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TypeaheadOutSiteDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: "[typeaheadClickOutSite]"
                }]
        }], ctorParameters: () => [{ type: i0.ElementRef }], propDecorators: { typeaheadClickOutSite: [{
                type: Output
            }], documentClick: [{
                type: HostListener,
                args: ["document:click", ["$event"]]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2sub3V0LnNpdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdHlwZWFoZWFkL2NsaWNrLm91dC5zaXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsWUFBWSxFQUNaLFVBQVUsRUFDVixNQUFNLEVBQ04sWUFBWSxFQUNiLE1BQU0sZUFBZSxDQUFDOztBQUt2QixNQUFNLE9BQU8seUJBQXlCO0lBR3BDLFlBQW9CLEVBQWM7UUFBZCxPQUFFLEdBQUYsRUFBRSxDQUFZO1FBRnhCLDBCQUFxQixHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO0lBRXRDLENBQUM7SUFHdEMsYUFBYSxDQUFDLE1BQVc7UUFDdkIsSUFBRyxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDLENBQUM7WUFDaEYsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7Z0JBQzlILElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdEMsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDOzhHQVpVLHlCQUF5QjtrR0FBekIseUJBQXlCOzsyRkFBekIseUJBQXlCO2tCQUhyQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx5QkFBeUI7aUJBQ3BDOytFQUVXLHFCQUFxQjtzQkFBOUIsTUFBTTtnQkFLUCxhQUFhO3NCQURaLFlBQVk7dUJBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIEhvc3RMaXN0ZW5lcixcbiAgRWxlbWVudFJlZixcbiAgT3V0cHV0LFxuICBFdmVudEVtaXR0ZXJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiBcIlt0eXBlYWhlYWRDbGlja091dFNpdGVdXCJcbn0pXG5leHBvcnQgY2xhc3MgVHlwZWFoZWFkT3V0U2l0ZURpcmVjdGl2ZSB7XG4gIEBPdXRwdXQoKSB0eXBlYWhlYWRDbGlja091dFNpdGU6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHt9XG5cbiAgQEhvc3RMaXN0ZW5lcihcImRvY3VtZW50OmNsaWNrXCIsIFtcIiRldmVudFwiXSlcbiAgZG9jdW1lbnRDbGljaygkZXZlbnQ6IGFueSkge1xuICAgIGlmKHRoaXMudHlwZWFoZWFkQ2xpY2tPdXRTaXRlICYmIHRoaXMudHlwZWFoZWFkQ2xpY2tPdXRTaXRlLm9ic2VydmVycy5sZW5ndGggPiAwKXtcbiAgICAgIGlmICh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MLmluZGV4T2YoJGV2ZW50LnRhcmdldC5pbm5lckhUTUwpID09PSAtMSB8fCAhdGhpcy5lbC5uYXRpdmVFbGVtZW50LmNvbnRhaW5zKCRldmVudC50YXJnZXQpKSB7XG4gICAgICAgIHRoaXMudHlwZWFoZWFkQ2xpY2tPdXRTaXRlLmVtaXQoXCJcIik7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=