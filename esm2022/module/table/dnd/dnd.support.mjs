import { TableDndResultDto } from "../dto/table.dnd.result.dto";
export class DndSupport {
    constructor(table, swap) {
        this.currRow = null;
        this.dragElem = null;
        this.mouseDownX = 0;
        this.mouseDownY = 0;
        this.mouseX = 0;
        this.mouseY = 0;
        this.mouseDrag = false;
        this.dndResult = new TableDndResultDto();
        this.swap = (_dto) => { };
        this.swap = swap;
        this.table = table;
        this.tbody = this.table.querySelector('tbody');
    }
    init() {
        this._bindMouse();
    }
    _bindMouse() {
        this.table.addEventListener('mousedown', (event) => {
            if (event.button != 0)
                return;
            let target = this._getTargetRow(event.target);
            if (target) {
                this.currRow = target;
                this.dndResult.fromIndex = Array.from(this.tbody.children).indexOf(this.currRow);
                this._addDraggableRow(target);
                this.currRow.classList.add('dragging');
                let coords = this._getMouseCoords(event);
                this.mouseDownX = coords.x;
                this.mouseDownY = coords.y;
                this.mouseDrag = true;
            }
        });
        this.table.addEventListener('mousemove', (event) => {
            if (!this.mouseDrag)
                return;
            let coords = this._getMouseCoords(event);
            this.mouseX = coords.x - this.mouseDownX;
            this.mouseY = coords.y - this.mouseDownY;
            this._moveRow(this.mouseX, this.mouseY);
        });
        this.table.addEventListener('mouseup', (_event) => {
            if (!this.mouseDrag)
                return;
            this.currRow.classList.remove('dragging');
            this.table.removeChild(this.dragElem);
            this.dragElem = null;
            this.mouseDrag = false;
            this.dndResult.toIndex = Array.from(this.tbody.children).indexOf(this.currRow);
            if (this.dndResult.fromIndex !== this.dndResult.toIndex) {
                this.swap(this.dndResult);
            }
            this.dndResult = new TableDndResultDto();
        });
    }
    _swapRow(row, index) {
        let currIndex = Array.from(this.tbody.children).indexOf(this.currRow), row1 = currIndex > index ? this.currRow : row, row2 = currIndex > index ? row : this.currRow;
        if (currIndex != -1) {
            this.tbody.insertBefore(row1, row2);
        }
        else {
            console.log("remove drag element");
            this.currRow.classList.remove('dragging');
            this.table.removeChild(this.dragElem);
            this.dragElem = null;
        }
    }
    _moveRow(x, y) {
        this.dragElem.style.transform = "translate3d(" + x + "px, " + y + "px, 0)";
        let dPos = this.dragElem.getBoundingClientRect(), currStartY = dPos.y, currEndY = currStartY + dPos.height, rows = this._getRows();
        for (var i = 0; i < rows.length; i++) {
            let rowElem = rows[i], rowSize = rowElem.getBoundingClientRect(), rowStartY = rowSize.y, rowEndY = rowStartY + rowSize.height;
            if (this.currRow !== rowElem && this._isIntersecting(currStartY, currEndY, rowStartY, rowEndY)) {
                if (Math.abs(currStartY - rowStartY) < rowSize.height / 2) {
                    this._swapRow(rowElem, i);
                }
            }
        }
    }
    _addDraggableRow(target) {
        this.dragElem = target.cloneNode(true);
        this.dragElem.classList.add('draggable-drag');
        //@ts-ignore
        this.dragElem.style.height = this._getStyle(target, 'height');
        this.dragElem.style.width = this._getStyle(target, 'width');
        //@ts-ignore
        this.dragElem.style.backgroundColor = this._getStyle(target, 'backgroundColor');
        for (var i = 0; i < target.children.length; i++) {
            let oldTD = target.children[i], newTD = this.dragElem.children[i];
            newTD.style.width = this._getStyle(oldTD, 'width');
            newTD.style.height = this._getStyle(oldTD, 'height');
            newTD.style.padding = this._getStyle(oldTD, 'padding');
            newTD.style.margin = this._getStyle(oldTD, 'margin');
        }
        this.table.appendChild(this.dragElem);
        let tPos = target.getBoundingClientRect(), dPos = this.dragElem.getBoundingClientRect();
        this.dragElem.style.bottom = ((dPos.y - tPos.y) - tPos.height / 3) + "px";
        this.dragElem.style.left = "-1px";
        this.table.dispatchEvent(new MouseEvent('mousemove', { view: window, cancelable: true, bubbles: true }));
    }
    _getRows() {
        return this.table.querySelectorAll('tbody tr');
    }
    _getTargetRow(target) {
        if (target) {
            let elemName = target.tagName.toLowerCase();
            if (elemName == 'tr')
                return target;
            if (elemName == 'td')
                return target.closest('tr');
        }
        return undefined;
    }
    _getMouseCoords(event) {
        return {
            x: event.clientX,
            y: event.clientY
        };
    }
    _getStyle(target, styleName) {
        let compStyle = getComputedStyle(target), 
        //@ts-ignore
        style = compStyle[styleName];
        return style ? style : "none";
    }
    _isIntersecting(min0, max0, min1, max1) {
        return Math.max(min0, max0) >= Math.min(min1, max1)
            && Math.min(min0, max0) <= Math.max(min1, max1);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG5kLnN1cHBvcnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZG5kL2RuZC5zdXBwb3J0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBRWhFLE1BQU0sT0FBTyxVQUFVO0lBZW5CLFlBQVksS0FBa0IsRUFBRSxJQUFzQztRQVZ0RSxZQUFPLEdBQXVCLElBQUksQ0FBQztRQUNuQyxhQUFRLEdBQXVCLElBQUksQ0FBQztRQUNwQyxlQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGVBQVUsR0FBVyxDQUFDLENBQUM7UUFDdkIsV0FBTSxHQUFXLENBQUMsQ0FBQztRQUNuQixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBQ25CLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsY0FBUyxHQUFzQixJQUFJLGlCQUFpQixFQUFFLENBQUM7UUFDdkQsU0FBSSxHQUFhLENBQUMsSUFBdUIsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRzlDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFFLENBQUM7SUFDcEQsQ0FBQztJQUVELElBQUk7UUFDQSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELFVBQVU7UUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQWlCLEVBQUUsRUFBRTtZQUMzRCxJQUFHLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFBRSxPQUFPO1lBRTdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQXFCLENBQUMsQ0FBQztZQUM3RCxJQUFHLE1BQU0sRUFBRSxDQUFDO2dCQUNSLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO2dCQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFRLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QixJQUFJLENBQUMsT0FBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRXhDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUUzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUMxQixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQWlCLEVBQUUsRUFBRTtZQUMzRCxJQUFHLENBQUMsSUFBSSxDQUFDLFNBQVM7Z0JBQUUsT0FBTztZQUUzQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBRXpDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDNUMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQzlDLElBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUztnQkFBRSxPQUFPO1lBRTNCLElBQUksQ0FBQyxPQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsS0FBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBUSxDQUFDLENBQUM7WUFDaEYsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM5QixDQUFDO1lBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsUUFBUSxDQUFDLEdBQWdCLEVBQUUsS0FBYTtRQUNwQyxJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFRLENBQUMsRUFDbEUsSUFBSSxHQUFnQixTQUFTLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQzNELElBQUksR0FBZ0IsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBUSxDQUFDO1FBQ2hFLElBQUksU0FBUyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hDLENBQUM7YUFBTSxDQUFDO1lBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxPQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsS0FBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDekIsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRLENBQUMsQ0FBUyxFQUFFLENBQVM7UUFDekIsSUFBSSxDQUFDLFFBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLGNBQWMsR0FBRyxDQUFDLEdBQUcsTUFBTSxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUM7UUFFNUUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVMsQ0FBQyxxQkFBcUIsRUFBRSxFQUM3QyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRSxRQUFRLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQ3hELElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFM0IsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNsQyxJQUFJLE9BQU8sR0FBWSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQzlCLE9BQU8sR0FBRyxPQUFPLENBQUMscUJBQXFCLEVBQUUsRUFDekMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxHQUFHLFNBQVMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBRTVELElBQUcsSUFBSSxDQUFDLE9BQU8sS0FBSyxPQUFPLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsRUFBRSxDQUFDO2dCQUM1RixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUM7b0JBQ3hELElBQUksQ0FBQyxRQUFRLENBQUMsT0FBc0IsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDN0MsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELGdCQUFnQixDQUFDLE1BQW1CO1FBQ2hDLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQWdCLENBQUM7UUFDdEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDOUMsWUFBWTtRQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUQsWUFBWTtRQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2hGLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLElBQUksS0FBSyxHQUFnQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBZ0IsRUFDdEQsS0FBSyxHQUFnQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQWdCLENBQUM7WUFDbEUsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbkQsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDckQsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDdkQsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDekQsQ0FBQztRQUVELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUV0QyxJQUFJLElBQUksR0FBRyxNQUFNLENBQUMscUJBQXFCLEVBQUUsRUFDckMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQzFFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFFbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxVQUFVLENBQUMsV0FBVyxFQUMvQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQ3BELENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxRQUFRO1FBQ0osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxhQUFhLENBQUMsTUFBMEI7UUFDcEMsSUFBSSxNQUFNLEVBQUUsQ0FBQztZQUNULElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFNUMsSUFBRyxRQUFRLElBQUksSUFBSTtnQkFBRSxPQUFPLE1BQU0sQ0FBQztZQUNuQyxJQUFJLFFBQVEsSUFBSSxJQUFJO2dCQUFFLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxDQUFDO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELGVBQWUsQ0FBQyxLQUFpQjtRQUM3QixPQUFPO1lBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPO1lBQ2hCLENBQUMsRUFBRSxLQUFLLENBQUMsT0FBTztTQUNuQixDQUFDO0lBQ04sQ0FBQztJQUVELFNBQVMsQ0FBQyxNQUFtQixFQUFFLFNBQWlCO1FBQzVDLElBQUksU0FBUyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUN4QyxZQUFZO1FBQ1osS0FBSyxHQUFXLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVyQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7SUFDbEMsQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFZLEVBQUUsSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZO1FBQ2xFLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDO2VBQzVDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hELENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRhYmxlRG5kUmVzdWx0RHRvIH0gZnJvbSBcIi4uL2R0by90YWJsZS5kbmQucmVzdWx0LmR0b1wiO1xuXG5leHBvcnQgY2xhc3MgRG5kU3VwcG9ydCB7XG5cbiAgICB0YWJsZTogSFRNTEVsZW1lbnQ7XG4gICAgdGJvZHk6IEhUTUxFbGVtZW50O1xuICBcbiAgICBjdXJyUm93OiBIVE1MRWxlbWVudCB8IG51bGwgPSBudWxsO1xuICAgIGRyYWdFbGVtOiBIVE1MRWxlbWVudCB8IG51bGwgPSBudWxsO1xuICAgIG1vdXNlRG93blg6IG51bWJlciA9IDA7XG4gICAgbW91c2VEb3duWTogbnVtYmVyID0gMDtcbiAgICBtb3VzZVg6IG51bWJlciA9IDA7XG4gICAgbW91c2VZOiBudW1iZXIgPSAwO1xuICAgIG1vdXNlRHJhZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIGRuZFJlc3VsdDogVGFibGVEbmRSZXN1bHREdG8gPSBuZXcgVGFibGVEbmRSZXN1bHREdG8oKTtcbiAgICBzd2FwOiBGdW5jdGlvbiA9IChfZHRvOiBUYWJsZURuZFJlc3VsdER0bykgPT4geyB9O1xuXG4gICAgY29uc3RydWN0b3IodGFibGU6IEhUTUxFbGVtZW50LCBzd2FwOiAoZHRvOiBUYWJsZURuZFJlc3VsdER0bykgPT4gdm9pZCkge1xuICAgICAgICB0aGlzLnN3YXAgPSBzd2FwO1xuICAgICAgICB0aGlzLnRhYmxlID0gdGFibGU7XG4gICAgICAgIHRoaXMudGJvZHkgPSB0aGlzLnRhYmxlLnF1ZXJ5U2VsZWN0b3IoJ3Rib2R5JykhO1xuICAgIH1cbiAgXG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5fYmluZE1vdXNlKCk7XG4gICAgfVxuICBcbiAgICBfYmluZE1vdXNlKCkge1xuICAgICAgICB0aGlzLnRhYmxlLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIChldmVudDogTW91c2VFdmVudCkgPT4ge1xuICAgICAgICAgICAgaWYoZXZlbnQuYnV0dG9uICE9IDApIHJldHVybjtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbGV0IHRhcmdldCA9IHRoaXMuX2dldFRhcmdldFJvdyhldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnQpO1xuICAgICAgICAgICAgaWYodGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyUm93ID0gdGFyZ2V0O1xuICAgICAgICAgICAgICAgIHRoaXMuZG5kUmVzdWx0LmZyb21JbmRleCA9IEFycmF5LmZyb20odGhpcy50Ym9keS5jaGlsZHJlbikuaW5kZXhPZih0aGlzLmN1cnJSb3chKTtcbiAgICAgICAgICAgICAgICB0aGlzLl9hZGREcmFnZ2FibGVSb3codGFyZ2V0KTtcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJSb3chLmNsYXNzTGlzdC5hZGQoJ2RyYWdnaW5nJyk7XG5cbiAgICAgICAgICAgICAgICBsZXQgY29vcmRzID0gdGhpcy5fZ2V0TW91c2VDb29yZHMoZXZlbnQpO1xuICAgICAgICAgICAgICAgIHRoaXMubW91c2VEb3duWCA9IGNvb3Jkcy54O1xuICAgICAgICAgICAgICAgIHRoaXMubW91c2VEb3duWSA9IGNvb3Jkcy55OyAgICAgIFxuXG4gICAgICAgICAgICAgICAgdGhpcy5tb3VzZURyYWcgPSB0cnVlOyAgIFxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMudGFibGUuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgKGV2ZW50OiBNb3VzZUV2ZW50KSA9PiB7XG4gICAgICAgICAgICBpZighdGhpcy5tb3VzZURyYWcpIHJldHVybjtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbGV0IGNvb3JkcyA9IHRoaXMuX2dldE1vdXNlQ29vcmRzKGV2ZW50KTtcbiAgICAgICAgICAgIHRoaXMubW91c2VYID0gY29vcmRzLnggLSB0aGlzLm1vdXNlRG93blg7XG4gICAgICAgICAgICB0aGlzLm1vdXNlWSA9IGNvb3Jkcy55IC0gdGhpcy5tb3VzZURvd25ZOyAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuX21vdmVSb3codGhpcy5tb3VzZVgsIHRoaXMubW91c2VZKTtcbiAgICAgICAgfSk7XG4gICAgICAgICAgICBcbiAgICAgICAgdGhpcy50YWJsZS5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgKF9ldmVudCkgPT4ge1xuICAgICAgICAgICAgaWYoIXRoaXMubW91c2VEcmFnKSByZXR1cm47XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuY3VyclJvdyEuY2xhc3NMaXN0LnJlbW92ZSgnZHJhZ2dpbmcnKTtcbiAgICAgICAgICAgIHRoaXMudGFibGUhLnJlbW92ZUNoaWxkKHRoaXMuZHJhZ0VsZW0hKTtcbiAgICAgICAgICAgIHRoaXMuZHJhZ0VsZW0gPSBudWxsO1xuICAgICAgICAgICAgdGhpcy5tb3VzZURyYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuZG5kUmVzdWx0LnRvSW5kZXggPSBBcnJheS5mcm9tKHRoaXMudGJvZHkuY2hpbGRyZW4pLmluZGV4T2YodGhpcy5jdXJyUm93ISk7XG4gICAgICAgICAgICBpZiAodGhpcy5kbmRSZXN1bHQuZnJvbUluZGV4ICE9PSB0aGlzLmRuZFJlc3VsdC50b0luZGV4KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zd2FwKHRoaXMuZG5kUmVzdWx0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuZG5kUmVzdWx0ID0gbmV3IFRhYmxlRG5kUmVzdWx0RHRvKCk7XG4gICAgICAgIH0pOyAgXG4gICAgfVxuICBcbiAgICBfc3dhcFJvdyhyb3c6IEhUTUxFbGVtZW50LCBpbmRleDogbnVtYmVyKSB7XG4gICAgICAgIGxldCBjdXJySW5kZXggPSBBcnJheS5mcm9tKHRoaXMudGJvZHkuY2hpbGRyZW4pLmluZGV4T2YodGhpcy5jdXJyUm93ISksXG4gICAgICAgICAgICByb3cxOiBIVE1MRWxlbWVudCA9IGN1cnJJbmRleCA+IGluZGV4ID8gdGhpcy5jdXJyUm93ISA6IHJvdyxcbiAgICAgICAgICAgIHJvdzI6IEhUTUxFbGVtZW50ID0gY3VyckluZGV4ID4gaW5kZXggPyByb3cgOiB0aGlzLmN1cnJSb3chO1xuICAgICAgICBpZiAoY3VyckluZGV4ICE9IC0xKSB7XG4gICAgICAgICAgICB0aGlzLnRib2R5Lmluc2VydEJlZm9yZShyb3cxLCByb3cyKTtcbiAgICAgICAgfSBlbHNlIHsgXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInJlbW92ZSBkcmFnIGVsZW1lbnRcIik7XG4gICAgICAgICAgICB0aGlzLmN1cnJSb3chLmNsYXNzTGlzdC5yZW1vdmUoJ2RyYWdnaW5nJyk7XG4gICAgICAgICAgICB0aGlzLnRhYmxlIS5yZW1vdmVDaGlsZCh0aGlzLmRyYWdFbGVtISk7XG4gICAgICAgICAgICB0aGlzLmRyYWdFbGVtID0gbnVsbDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBcbiAgICBfbW92ZVJvdyh4OiBudW1iZXIsIHk6IG51bWJlcikge1xuICAgICAgICB0aGlzLmRyYWdFbGVtIS5zdHlsZS50cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZTNkKFwiICsgeCArIFwicHgsIFwiICsgeSArIFwicHgsIDApXCI7XG4gICAgICAgIFxuICAgICAgICBsZXRcdGRQb3MgPSB0aGlzLmRyYWdFbGVtIS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxcbiAgICAgICAgICAgIGN1cnJTdGFydFkgPSBkUG9zLnksIGN1cnJFbmRZID0gY3VyclN0YXJ0WSArIGRQb3MuaGVpZ2h0LFxuICAgICAgICAgICAgcm93cyA9IHRoaXMuX2dldFJvd3MoKTtcblxuICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgcm93cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IHJvd0VsZW06IEVsZW1lbnQgPSByb3dzW2ldLFxuICAgICAgICAgICAgcm93U2l6ZSA9IHJvd0VsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICAgICAgICByb3dTdGFydFkgPSByb3dTaXplLnksIHJvd0VuZFkgPSByb3dTdGFydFkgKyByb3dTaXplLmhlaWdodDtcblxuICAgICAgICAgICAgaWYodGhpcy5jdXJyUm93ICE9PSByb3dFbGVtICYmIHRoaXMuX2lzSW50ZXJzZWN0aW5nKGN1cnJTdGFydFksIGN1cnJFbmRZLCByb3dTdGFydFksIHJvd0VuZFkpKSB7XG4gICAgICAgICAgICAgICAgaWYgKE1hdGguYWJzKGN1cnJTdGFydFkgLSByb3dTdGFydFkpIDwgcm93U2l6ZS5oZWlnaHQgLyAyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3N3YXBSb3cocm93RWxlbSBhcyBIVE1MRWxlbWVudCwgaSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9ICAgIFxuICAgIH1cbiAgXG4gICAgX2FkZERyYWdnYWJsZVJvdyh0YXJnZXQ6IEhUTUxFbGVtZW50KSB7ICAgIFxuICAgICAgICB0aGlzLmRyYWdFbGVtID0gdGFyZ2V0LmNsb25lTm9kZSh0cnVlKSBhcyBIVE1MRWxlbWVudDtcbiAgICAgICAgdGhpcy5kcmFnRWxlbS5jbGFzc0xpc3QuYWRkKCdkcmFnZ2FibGUtZHJhZycpO1xuICAgICAgICAvL0B0cy1pZ25vcmVcbiAgICAgICAgdGhpcy5kcmFnRWxlbS5zdHlsZS5oZWlnaHQgPSB0aGlzLl9nZXRTdHlsZSh0YXJnZXQsICdoZWlnaHQnKTtcbiAgICAgICAgdGhpcy5kcmFnRWxlbS5zdHlsZS53aWR0aCA9IHRoaXMuX2dldFN0eWxlKHRhcmdldCwgJ3dpZHRoJyk7XG4gICAgICAgIC8vQHRzLWlnbm9yZVxuICAgICAgICB0aGlzLmRyYWdFbGVtLnN0eWxlLmJhY2tncm91bmRDb2xvciA9IHRoaXMuX2dldFN0eWxlKHRhcmdldCwgJ2JhY2tncm91bmRDb2xvcicpOyAgICAgXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCB0YXJnZXQuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBvbGRURDogSFRNTEVsZW1lbnQgPSB0YXJnZXQuY2hpbGRyZW5baV0gYXMgSFRNTEVsZW1lbnQsXG4gICAgICAgICAgICAgICAgbmV3VEQ6IEhUTUxFbGVtZW50ID0gdGhpcy5kcmFnRWxlbS5jaGlsZHJlbltpXSBhcyBIVE1MRWxlbWVudDtcbiAgICAgICAgICAgIG5ld1RELnN0eWxlLndpZHRoID0gdGhpcy5fZ2V0U3R5bGUob2xkVEQsICd3aWR0aCcpO1xuICAgICAgICAgICAgbmV3VEQuc3R5bGUuaGVpZ2h0ID0gdGhpcy5fZ2V0U3R5bGUob2xkVEQsICdoZWlnaHQnKTtcbiAgICAgICAgICAgIG5ld1RELnN0eWxlLnBhZGRpbmcgPSB0aGlzLl9nZXRTdHlsZShvbGRURCwgJ3BhZGRpbmcnKTtcbiAgICAgICAgICAgIG5ld1RELnN0eWxlLm1hcmdpbiA9IHRoaXMuX2dldFN0eWxlKG9sZFRELCAnbWFyZ2luJyk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHRoaXMudGFibGUuYXBwZW5kQ2hpbGQodGhpcy5kcmFnRWxlbSk7XG5cbiAgICAgICAgbGV0IHRQb3MgPSB0YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICAgICAgICBkUG9zID0gdGhpcy5kcmFnRWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgdGhpcy5kcmFnRWxlbS5zdHlsZS5ib3R0b20gPSAoKGRQb3MueSAtIHRQb3MueSkgLSB0UG9zLmhlaWdodCAvIDMpICsgXCJweFwiO1xuICAgICAgICB0aGlzLmRyYWdFbGVtLnN0eWxlLmxlZnQgPSBcIi0xcHhcIjsgICAgXG4gICAgICAgIFxuICAgICAgICB0aGlzLnRhYmxlLmRpc3BhdGNoRXZlbnQobmV3IE1vdXNlRXZlbnQoJ21vdXNlbW92ZScsXG4gICAgICAgICAgICB7IHZpZXc6IHdpbmRvdywgY2FuY2VsYWJsZTogdHJ1ZSwgYnViYmxlczogdHJ1ZSB9XG4gICAgICAgICkpOyAgICBcbiAgICB9XG4gIFxuICAgIF9nZXRSb3dzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy50YWJsZS5xdWVyeVNlbGVjdG9yQWxsKCd0Ym9keSB0cicpO1xuICAgIH1cbiAgXG4gICAgX2dldFRhcmdldFJvdyh0YXJnZXQ6IEhUTUxFbGVtZW50IHwgbnVsbCkge1xuICAgICAgICBpZiAodGFyZ2V0KSB7XG4gICAgICAgICAgICBsZXQgZWxlbU5hbWUgPSB0YXJnZXQudGFnTmFtZS50b0xvd2VyQ2FzZSgpO1xuICAgIFxuICAgICAgICAgICAgaWYoZWxlbU5hbWUgPT0gJ3RyJykgcmV0dXJuIHRhcmdldDtcbiAgICAgICAgICAgIGlmIChlbGVtTmFtZSA9PSAndGQnKSByZXR1cm4gdGFyZ2V0LmNsb3Nlc3QoJ3RyJyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG4gIFxuICAgIF9nZXRNb3VzZUNvb3JkcyhldmVudDogTW91c2VFdmVudCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgeDogZXZlbnQuY2xpZW50WCxcbiAgICAgICAgICAgIHk6IGV2ZW50LmNsaWVudFlcbiAgICAgICAgfTsgICAgXG4gICAgfSAgXG4gIFxuICAgIF9nZXRTdHlsZSh0YXJnZXQ6IEhUTUxFbGVtZW50LCBzdHlsZU5hbWU6IHN0cmluZykge1xuICAgICAgICBsZXQgY29tcFN0eWxlID0gZ2V0Q29tcHV0ZWRTdHlsZSh0YXJnZXQpLFxuICAgICAgICAvL0B0cy1pZ25vcmVcbiAgICAgICAgc3R5bGU6IHN0cmluZyA9IGNvbXBTdHlsZVtzdHlsZU5hbWVdO1xuXG4gICAgICAgIHJldHVybiBzdHlsZSA/IHN0eWxlIDogXCJub25lXCI7XG4gICAgfSAgXG4gIFxuICAgIF9pc0ludGVyc2VjdGluZyhtaW4wOiBudW1iZXIsIG1heDA6IG51bWJlciwgbWluMTogbnVtYmVyLCBtYXgxOiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgubWF4KG1pbjAsIG1heDApID49IE1hdGgubWluKG1pbjEsIG1heDEpXG4gICAgICAgICAgICAmJiBNYXRoLm1pbihtaW4wLCBtYXgwKSA8PSBNYXRoLm1heChtaW4xLCBtYXgxKTtcbiAgICB9XG59Il19