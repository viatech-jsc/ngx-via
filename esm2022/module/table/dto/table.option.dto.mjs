import { TableSortDto } from "./table.sort.dto";
import { TableFilterDto } from "./table.filter.dto";
export class TableOptionDto {
    constructor(tableColumns = [], tableRows = []) {
        this._tableColumns = tableColumns;
        this._tableRows = tableRows;
        this._totalItems = 0;
        this._itemsPerPage = 10;
        this._currentPage = 0;
        this._tableFilter = new TableFilterDto();
        this._tableActions = new Array();
        this._defaultSort = new TableSortDto();
        this._loading = false;
    }
    get loading() {
        return this._loading;
    }
    set loading(value) {
        this._loading = value;
    }
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns() {
        return this._tableColumns;
    }
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows() {
        return this._tableRows;
    }
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems() {
        return this._totalItems;
    }
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage() {
        return this._itemsPerPage;
    }
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage() {
        return this._currentPage;
    }
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter() {
        return this._tableFilter;
    }
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions() {
        return this._tableActions;
    }
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort() {
        return this._defaultSort;
    }
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value) {
        this._tableColumns = value;
    }
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value) {
        this._tableRows = value;
    }
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value) {
        this._totalItems = value;
    }
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value) {
        this._itemsPerPage = value;
    }
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value) {
        this._currentPage = value;
    }
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value) {
        this._tableFilter = value;
    }
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value) {
        this._tableActions = value;
    }
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value) {
        this._defaultSort = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUub3B0aW9uLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kdG8vdGFibGUub3B0aW9uLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE1BQU0sT0FBTyxjQUFjO0lBV3ZCLFlBQ0ksZUFBc0MsRUFBRSxFQUN4QyxZQUFnQyxFQUFFO1FBRWxDLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxjQUFjLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksS0FBSyxFQUFrQixDQUFDO1FBQ2pELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDO0lBRUQsSUFBVyxPQUFPO1FBQ2QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxJQUFXLE9BQU8sQ0FBQyxLQUFjO1FBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7O09BR0c7SUFDTixJQUFXLFlBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVM7UUFDbkIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFVBQVU7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFdBQVc7UUFDckIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzFCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFdBQVc7UUFDckIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzFCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFdBQVc7UUFDckIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzFCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVksQ0FBQyxLQUF1QjtRQUM5QyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTLENBQUMsS0FBb0I7UUFDeEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsVUFBVSxDQUFDLEtBQWE7UUFDbEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWSxDQUFDLEtBQWE7UUFDcEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsV0FBVyxDQUFDLEtBQWE7UUFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsV0FBVyxDQUFDLEtBQXFCO1FBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVksQ0FBQyxLQUF1QjtRQUM5QyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxXQUFXLENBQUMsS0FBbUI7UUFDekMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztDQUVEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVGFibGVSb3dEdG8gfSBmcm9tIFwiLi90YWJsZS5yb3cuZHRvXCI7XG5pbXBvcnQgeyBUYWJsZUFjdGlvbkR0byB9IGZyb20gXCIuL3RhYmxlLmFjdGlvbi5kdG9cIjtcbmltcG9ydCB7IFRhYmxlU29ydER0byB9IGZyb20gXCIuL3RhYmxlLnNvcnQuZHRvXCI7XG5pbXBvcnQgeyBUYWJsZUNvbHVtbkR0byB9IGZyb20gXCIuL3RhYmxlLmNvbHVtbi5kdG9cIjtcbmltcG9ydCB7IFRhYmxlRmlsdGVyRHRvIH0gZnJvbSBcIi4vdGFibGUuZmlsdGVyLmR0b1wiO1xuXG5leHBvcnQgY2xhc3MgVGFibGVPcHRpb25EdG8ge1xuICAgIHByaXZhdGUgX3RhYmxlQ29sdW1uczogQXJyYXk8VGFibGVDb2x1bW5EdG8+O1xuICAgIHByaXZhdGUgX3RhYmxlUm93czogQXJyYXk8VGFibGVSb3dEdG8+O1xuICAgIHByaXZhdGUgX3RvdGFsSXRlbXM6IG51bWJlcjtcbiAgICBwcml2YXRlIF9pdGVtc1BlclBhZ2U6IG51bWJlcjtcbiAgICBwcml2YXRlIF9jdXJyZW50UGFnZTogbnVtYmVyO1xuICAgIHByaXZhdGUgX3RhYmxlRmlsdGVyOiBUYWJsZUZpbHRlckR0bztcbiAgICBwcml2YXRlIF90YWJsZUFjdGlvbnM6IFRhYmxlQWN0aW9uRHRvW107XG4gICAgcHJpdmF0ZSBfZGVmYXVsdFNvcnQ6IFRhYmxlU29ydER0bztcbiAgICBwcml2YXRlIF9sb2FkaW5nOiBib29sZWFuO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHRhYmxlQ29sdW1uczogQXJyYXk8VGFibGVDb2x1bW5EdG8+ID0gW10sXG4gICAgICAgIHRhYmxlUm93czogQXJyYXk8VGFibGVSb3dEdG8+ID0gW11cbiAgICApIHtcbiAgICAgICAgdGhpcy5fdGFibGVDb2x1bW5zID0gdGFibGVDb2x1bW5zO1xuICAgICAgICB0aGlzLl90YWJsZVJvd3MgPSB0YWJsZVJvd3M7XG4gICAgICAgIHRoaXMuX3RvdGFsSXRlbXMgPSAwO1xuICAgICAgICB0aGlzLl9pdGVtc1BlclBhZ2UgPSAxMDtcbiAgICAgICAgdGhpcy5fY3VycmVudFBhZ2UgPSAwO1xuICAgICAgICB0aGlzLl90YWJsZUZpbHRlciA9IG5ldyBUYWJsZUZpbHRlckR0bygpO1xuICAgICAgICB0aGlzLl90YWJsZUFjdGlvbnMgPSBuZXcgQXJyYXk8VGFibGVBY3Rpb25EdG8+KCk7XG4gICAgICAgIHRoaXMuX2RlZmF1bHRTb3J0ID0gbmV3IFRhYmxlU29ydER0bygpO1xuICAgICAgICB0aGlzLl9sb2FkaW5nID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBsb2FkaW5nKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fbG9hZGluZztcbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0IGxvYWRpbmcodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fbG9hZGluZyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZUNvbHVtbnNcbiAgICAgKiByZXR1cm4ge1RhYmxlQ29sdW1uW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdGFibGVDb2x1bW5zKCk6IFRhYmxlQ29sdW1uRHRvW10ge1xuXHRcdHJldHVybiB0aGlzLl90YWJsZUNvbHVtbnM7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZVJvd3NcbiAgICAgKiByZXR1cm4ge1RhYmxlUm93W119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdGFibGVSb3dzKCk6IFRhYmxlUm93RHRvW10ge1xuXHRcdHJldHVybiB0aGlzLl90YWJsZVJvd3M7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0b3RhbEl0ZW1zXG4gICAgICogcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdG90YWxJdGVtcygpOiBudW1iZXIge1xuXHRcdHJldHVybiB0aGlzLl90b3RhbEl0ZW1zO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaXRlbXNQZXJQYWdlXG4gICAgICogcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgaXRlbXNQZXJQYWdlKCk6IG51bWJlciB7XG5cdFx0cmV0dXJuIHRoaXMuX2l0ZW1zUGVyUGFnZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGN1cnJlbnRQYWdlXG4gICAgICogcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgY3VycmVudFBhZ2UoKTogbnVtYmVyIHtcblx0XHRyZXR1cm4gdGhpcy5fY3VycmVudFBhZ2U7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZUZpbHRlclxuICAgICAqIHJldHVybiB7VGFibGVGaWx0ZXJ9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdGFibGVGaWx0ZXIoKTogVGFibGVGaWx0ZXJEdG8ge1xuXHRcdHJldHVybiB0aGlzLl90YWJsZUZpbHRlcjtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHRhYmxlQWN0aW9uc1xuICAgICAqIHJldHVybiB7VGFibGVBY3Rpb25bXX1cbiAgICAgKi9cblx0cHVibGljIGdldCB0YWJsZUFjdGlvbnMoKTogVGFibGVBY3Rpb25EdG9bXSB7XG5cdFx0cmV0dXJuIHRoaXMuX3RhYmxlQWN0aW9ucztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRlZmF1bHRTb3J0XG4gICAgICogcmV0dXJuIHtUYWJsZURlZmF1bHRTb3J0fVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGRlZmF1bHRTb3J0KCk6IFRhYmxlU29ydER0byB7XG5cdFx0cmV0dXJuIHRoaXMuX2RlZmF1bHRTb3J0O1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdGFibGVDb2x1bW5zXG4gICAgICogcGFyYW0ge1RhYmxlQ29sdW1uW119IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdGFibGVDb2x1bW5zKHZhbHVlOiBUYWJsZUNvbHVtbkR0b1tdKSB7XG5cdFx0dGhpcy5fdGFibGVDb2x1bW5zID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0YWJsZVJvd3NcbiAgICAgKiBwYXJhbSB7VGFibGVSb3dbXX0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0YWJsZVJvd3ModmFsdWU6IFRhYmxlUm93RHRvW10pIHtcblx0XHR0aGlzLl90YWJsZVJvd3MgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHRvdGFsSXRlbXNcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHRvdGFsSXRlbXModmFsdWU6IG51bWJlcikge1xuXHRcdHRoaXMuX3RvdGFsSXRlbXMgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGl0ZW1zUGVyUGFnZVxuICAgICAqIHBhcmFtIHtudW1iZXJ9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgaXRlbXNQZXJQYWdlKHZhbHVlOiBudW1iZXIpIHtcblx0XHR0aGlzLl9pdGVtc1BlclBhZ2UgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGN1cnJlbnRQYWdlXG4gICAgICogcGFyYW0ge251bWJlcn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBjdXJyZW50UGFnZSh2YWx1ZTogbnVtYmVyKSB7XG5cdFx0dGhpcy5fY3VycmVudFBhZ2UgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHRhYmxlRmlsdGVyXG4gICAgICogcGFyYW0ge1RhYmxlRmlsdGVyfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHRhYmxlRmlsdGVyKHZhbHVlOiBUYWJsZUZpbHRlckR0bykge1xuXHRcdHRoaXMuX3RhYmxlRmlsdGVyID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0YWJsZUFjdGlvbnNcbiAgICAgKiBwYXJhbSB7VGFibGVBY3Rpb25bXX0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0YWJsZUFjdGlvbnModmFsdWU6IFRhYmxlQWN0aW9uRHRvW10pIHtcblx0XHR0aGlzLl90YWJsZUFjdGlvbnMgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGRlZmF1bHRTb3J0XG4gICAgICogcGFyYW0ge1RhYmxlRGVmYXVsdFNvcnR9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgZGVmYXVsdFNvcnQodmFsdWU6IFRhYmxlU29ydER0bykge1xuXHRcdHRoaXMuX2RlZmF1bHRTb3J0ID0gdmFsdWU7XG5cdH1cblxufVxuIl19