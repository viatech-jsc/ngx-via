export class TableRowDto {
    constructor(tableCells = [], css = "") {
        this._tableCells = tableCells;
        this._css = css;
        this._editAble = false;
        this._activeAble = false;
        this._deactiveAble = false;
        this._deleteAble = false;
    }
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells() {
        return this._tableCells;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble() {
        return this._editAble;
    }
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble() {
        return this._deleteAble;
    }
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble() {
        return this._deactiveAble;
    }
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble() {
        return this._activeAble;
    }
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value) {
        this._tableCells = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value) {
        this._editAble = value;
    }
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value) {
        this._deleteAble = value;
    }
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value) {
        this._deactiveAble = value;
    }
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value) {
        this._activeAble = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUucm93LmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kdG8vdGFibGUucm93LmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8sV0FBVztJQVFwQixZQUNJLGFBQWtDLEVBQUUsRUFDcEMsTUFBYyxFQUFFO1FBRWhCLElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFVBQVU7UUFDakIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLEdBQUc7UUFDVixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUTtRQUNmLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVLENBQUMsS0FBcUI7UUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRyxDQUFDLEtBQWE7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUSxDQUFDLEtBQWM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsVUFBVSxDQUFDLEtBQWM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsWUFBWSxDQUFDLEtBQWM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsVUFBVSxDQUFDLEtBQWM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztDQUVKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4vdGFibGUuY2VsbC5kdG9cIjtcblxuZXhwb3J0IGNsYXNzIFRhYmxlUm93RHRvIHtcbiAgICBwcml2YXRlIF90YWJsZUNlbGxzOiBBcnJheTxUYWJsZUNlbGxEdG8+O1xuICAgIHByaXZhdGUgX2Nzczogc3RyaW5nO1xuICAgIHByaXZhdGUgX2VkaXRBYmxlOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2RlbGV0ZUFibGU6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfZGVhY3RpdmVBYmxlOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2FjdGl2ZUFibGU6IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgdGFibGVDZWxsczogQXJyYXk8VGFibGVDZWxsRHRvPiA9IFtdLFxuICAgICAgICBjc3M6IHN0cmluZyA9IFwiXCJcbiAgICApIHtcbiAgICAgICAgdGhpcy5fdGFibGVDZWxscyA9IHRhYmxlQ2VsbHM7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5fZWRpdEFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fYWN0aXZlQWJsZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9kZWFjdGl2ZUFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fZGVsZXRlQWJsZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZUNlbGxzXG4gICAgICogcmV0dXJuIHtUYWJsZUNlbGxbXX1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IHRhYmxlQ2VsbHMoKTogVGFibGVDZWxsRHRvW10ge1xuICAgICAgICByZXR1cm4gdGhpcy5fdGFibGVDZWxscztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY3NzXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgcHVibGljIGdldCBjc3MoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NzcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZWRpdEFibGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBlZGl0QWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VkaXRBYmxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWxldGVBYmxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgZGVsZXRlQWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlbGV0ZUFibGU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRlYWN0aXZlQWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGRlYWN0aXZlQWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlYWN0aXZlQWJsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYWN0aXZlQWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGFjdGl2ZUFibGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9hY3RpdmVBYmxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0YWJsZUNlbGxzXG4gICAgICogcGFyYW0ge1RhYmxlQ2VsbFtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgdGFibGVDZWxscyh2YWx1ZTogVGFibGVDZWxsRHRvW10pIHtcbiAgICAgICAgdGhpcy5fdGFibGVDZWxscyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjc3NcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgY3NzKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fY3NzID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGVkaXRBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBlZGl0QWJsZSh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9lZGl0QWJsZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkZWxldGVBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBkZWxldGVBYmxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2RlbGV0ZUFibGUgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZGVhY3RpdmVBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBkZWFjdGl2ZUFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fZGVhY3RpdmVBYmxlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGFjdGl2ZUFibGVcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGFjdGl2ZUFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fYWN0aXZlQWJsZSA9IHZhbHVlO1xuICAgIH1cblxufSJdfQ==