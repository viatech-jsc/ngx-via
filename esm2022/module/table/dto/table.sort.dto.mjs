import { SORT } from "../constant";
export class TableSortDto {
    constructor(sortColumnName = "", sortType = SORT.ASC) {
        this._sortColumnName = sortColumnName;
        this._sortType = sortType;
    }
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName() {
        return this._sortColumnName;
    }
    /**
     * Getter sortType
     * return {string}
     */
    get sortType() {
        return this._sortType;
    }
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value) {
        this._sortColumnName = value;
    }
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value) {
        this._sortType = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuc29ydC5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZHRvL3RhYmxlLnNvcnQuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFbkMsTUFBTSxPQUFPLFlBQVk7SUFJdkIsWUFDRSxpQkFBeUIsRUFBRSxFQUMzQixXQUFtQixJQUFJLENBQUMsR0FBRztRQUV6QixJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztRQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxjQUFjO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM3QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN2QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxjQUFjLENBQUMsS0FBYTtRQUN0QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYTtRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4uL2NvbnN0YW50XCI7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZVNvcnREdG8ge1xuICAgIHByaXZhdGUgX3NvcnRDb2x1bW5OYW1lOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfc29ydFR5cGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBzb3J0Q29sdW1uTmFtZTogc3RyaW5nID0gXCJcIixcbiAgICBzb3J0VHlwZTogc3RyaW5nID0gU09SVC5BU0NcbiAgKSB7XG4gICAgICB0aGlzLl9zb3J0Q29sdW1uTmFtZSA9IHNvcnRDb2x1bW5OYW1lO1xuICAgICAgdGhpcy5fc29ydFR5cGUgPSBzb3J0VHlwZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgc29ydENvbHVtbk5hbWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBzb3J0Q29sdW1uTmFtZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9zb3J0Q29sdW1uTmFtZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHNvcnRUeXBlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgc29ydFR5cGUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fc29ydFR5cGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzb3J0Q29sdW1uTmFtZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgc29ydENvbHVtbk5hbWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3NvcnRDb2x1bW5OYW1lID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzb3J0VHlwZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgc29ydFR5cGUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3NvcnRUeXBlID0gdmFsdWU7XG5cdH1cblxufSJdfQ==