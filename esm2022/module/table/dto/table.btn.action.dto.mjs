export class TableBtnActionDto {
    constructor(name = "", callbackFunc, disabled = false, iconUrl = undefined, isDropdownItem = false) {
        this._isDropdownItem = false;
        this._name = name;
        this._buttonAction = callbackFunc;
        this._disabled = disabled;
        this._iconUrl = iconUrl;
        this._isDropdownItem = isDropdownItem;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction() {
        return this._buttonAction;
    }
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl() {
        return this._iconUrl;
    }
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem() {
        return this._isDropdownItem;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value) {
        this._buttonAction = value;
    }
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value) {
        this._disabled = value;
    }
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value) {
        this._iconUrl = value;
    }
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value) {
        this._isDropdownItem = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuYnRuLmFjdGlvbi5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZHRvL3RhYmxlLmJ0bi5hY3Rpb24uZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sT0FBTyxpQkFBaUI7SUFPMUIsWUFDSSxPQUFlLEVBQUUsRUFDakIsWUFBd0MsRUFDeEMsV0FBb0IsS0FBSyxFQUN6QixVQUE4QixTQUFTLEVBQ3ZDLGlCQUEwQixLQUFLO1FBUDNCLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBU3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDO0lBQzFDLENBQUM7SUFHRDs7O09BR0c7SUFDTixJQUFXLElBQUk7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTztRQUNqQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsY0FBYztRQUN4QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSSxDQUFDLEtBQWE7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDcEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWSxDQUFDLEtBQWU7UUFDdEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUSxDQUFDLEtBQWM7UUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTyxDQUFDLEtBQXlCO1FBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGNBQWMsQ0FBQyxLQUFjO1FBQ3ZDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7Q0FFRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRhYmxlUm93RHRvIH0gZnJvbSBcIi4vdGFibGUucm93LmR0b1wiO1xuXG5leHBvcnQgY2xhc3MgVGFibGVCdG5BY3Rpb25EdG8ge1xuICAgIHByaXZhdGUgX25hbWU6IHN0cmluZztcbiAgICBwcml2YXRlIF9idXR0b25BY3Rpb246IEZ1bmN0aW9uO1xuICAgIHByaXZhdGUgX2Rpc2FibGVkOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2ljb25Vcmw/OiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfaXNEcm9wZG93bkl0ZW06IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBuYW1lOiBzdHJpbmcgPSBcIlwiLFxuICAgICAgICBjYWxsYmFja0Z1bmM6IChyb3c6IFRhYmxlUm93RHRvKSA9PiB2b2lkLFxuICAgICAgICBkaXNhYmxlZDogYm9vbGVhbiA9IGZhbHNlLFxuICAgICAgICBpY29uVXJsOiBzdHJpbmcgfCB1bmRlZmluZWQgPSB1bmRlZmluZWQsXG4gICAgICAgIGlzRHJvcGRvd25JdGVtOiBib29sZWFuID0gZmFsc2VcbiAgICApIHtcbiAgICAgICAgdGhpcy5fbmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuX2J1dHRvbkFjdGlvbiA9IGNhbGxiYWNrRnVuYztcbiAgICAgICAgdGhpcy5fZGlzYWJsZWQgPSBkaXNhYmxlZDtcbiAgICAgICAgdGhpcy5faWNvblVybCA9IGljb25Vcmw7XG4gICAgICAgIHRoaXMuX2lzRHJvcGRvd25JdGVtID0gaXNEcm9wZG93bkl0ZW07XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbmFtZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IG5hbWUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fbmFtZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGJ1dHRvbkFjdGlvblxuICAgICAqIHJldHVybiB7RnVuY3Rpb259XG4gICAgICovXG5cdHB1YmxpYyBnZXQgYnV0dG9uQWN0aW9uKCk6IEZ1bmN0aW9uIHtcblx0XHRyZXR1cm4gdGhpcy5fYnV0dG9uQWN0aW9uO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZGlzYWJsZWRcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG5cdHB1YmxpYyBnZXQgZGlzYWJsZWQoKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMuX2Rpc2FibGVkO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaWNvblVybFxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGljb25VcmwoKTogc3RyaW5nIHwgdW5kZWZpbmVkIHtcblx0XHRyZXR1cm4gdGhpcy5faWNvblVybDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGlzRHJvcGRvd25JdGVtXG4gICAgICogcmV0dXJuIHtib29sZWFuIH1cbiAgICAgKi9cblx0cHVibGljIGdldCBpc0Ryb3Bkb3duSXRlbSgpOiBib29sZWFuICB7XG5cdFx0cmV0dXJuIHRoaXMuX2lzRHJvcGRvd25JdGVtO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbmFtZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgbmFtZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fbmFtZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYnV0dG9uQWN0aW9uXG4gICAgICogcGFyYW0ge0Z1bmN0aW9ufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGJ1dHRvbkFjdGlvbih2YWx1ZTogRnVuY3Rpb24pIHtcblx0XHR0aGlzLl9idXR0b25BY3Rpb24gPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGRpc2FibGVkXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcblx0XHR0aGlzLl9kaXNhYmxlZCA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgaWNvblVybFxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgaWNvblVybCh2YWx1ZTogc3RyaW5nIHwgdW5kZWZpbmVkKSB7XG5cdFx0dGhpcy5faWNvblVybCA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgaXNEcm9wZG93bkl0ZW1cbiAgICAgKiBwYXJhbSB7Ym9vbGVhbiB9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgaXNEcm9wZG93bkl0ZW0odmFsdWU6IGJvb2xlYW4gKSB7XG5cdFx0dGhpcy5faXNEcm9wZG93bkl0ZW0gPSB2YWx1ZTtcblx0fVxuXG59XG4iXX0=