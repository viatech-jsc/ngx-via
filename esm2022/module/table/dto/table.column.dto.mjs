import { COLUMN_TYPE, SORT } from '../constant';
import { ColumnFilterBaseDto } from './filter/column.filter.base.dto';
export class TableColumnDto {
    constructor(value = "", name = "", sortable = false, sort = SORT.NONE, css = "", hidden = false) {
        this._name = name;
        this._value = value;
        this._sort = sort;
        this._sortable = sortable;
        this._css = css;
        this._hidden = hidden;
        this.filter = new ColumnFilterBaseDto();
        this.type = COLUMN_TYPE.DEFAULT;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable() {
        return this._sortable;
    }
    /**
     * Getter sort
     * return {string}
     */
    get sort() {
        return this._sort;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value) {
        this._sortable = value;
    }
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value) {
        this._sort = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value) {
        this._hidden = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29sdW1uLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kdG8vdGFibGUuY29sdW1uLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUV0RSxNQUFNLE9BQU8sY0FBYztJQVV2QixZQUNJLFFBQWdCLEVBQUUsRUFDbEIsT0FBZSxFQUFFLEVBQ2pCLFdBQW9CLEtBQUssRUFDekIsT0FBZSxJQUFJLENBQUMsSUFBSSxFQUN4QixNQUFjLEVBQUUsRUFDaEIsU0FBa0IsS0FBSztRQUV2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksbUJBQW1CLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUk7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsR0FBRztRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNsQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxNQUFNO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYztRQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxNQUFNLENBQUMsS0FBYztRQUMvQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDT0xVTU5fVFlQRSwgU09SVCB9IGZyb20gJy4uL2NvbnN0YW50JztcbmltcG9ydCB7IENvbHVtbkZpbHRlckJhc2VEdG8gfSBmcm9tICcuL2ZpbHRlci9jb2x1bW4uZmlsdGVyLmJhc2UuZHRvJztcblxuZXhwb3J0IGNsYXNzIFRhYmxlQ29sdW1uRHRvIHtcbiAgICBwcml2YXRlIF9uYW1lOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfdmFsdWU6IHN0cmluZztcbiAgICBwcml2YXRlIF9zb3J0YWJsZTogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9zb3J0OiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfY3NzOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfaGlkZGVuOiBib29sZWFuO1xuICAgIGZpbHRlcjogQ29sdW1uRmlsdGVyQmFzZUR0bztcbiAgICB0eXBlOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgdmFsdWU6IHN0cmluZyA9IFwiXCIsXG4gICAgICAgIG5hbWU6IHN0cmluZyA9IFwiXCIsXG4gICAgICAgIHNvcnRhYmxlOiBib29sZWFuID0gZmFsc2UsXG4gICAgICAgIHNvcnQ6IHN0cmluZyA9IFNPUlQuTk9ORSxcbiAgICAgICAgY3NzOiBzdHJpbmcgPSBcIlwiLFxuICAgICAgICBoaWRkZW46IGJvb2xlYW4gPSBmYWxzZVxuICAgICkge1xuICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgdGhpcy5fc29ydCA9IHNvcnQ7XG4gICAgICAgIHRoaXMuX3NvcnRhYmxlID0gc29ydGFibGU7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5faGlkZGVuID0gaGlkZGVuO1xuICAgICAgICB0aGlzLmZpbHRlciA9IG5ldyBDb2x1bW5GaWx0ZXJCYXNlRHRvKCk7XG4gICAgICAgIHRoaXMudHlwZSA9IENPTFVNTl9UWVBFLkRFRkFVTFQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG5hbWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBuYW1lKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX25hbWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB2YWx1ZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHZhbHVlKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgc29ydGFibGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG5cdHB1YmxpYyBnZXQgc29ydGFibGUoKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMuX3NvcnRhYmxlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgc29ydFxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNvcnQoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fc29ydDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGNzc1xuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGNzcygpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9jc3M7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBoaWRkZW5cbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG5cdHB1YmxpYyBnZXQgaGlkZGVuKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLl9oaWRkZW47XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBuYW1lXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBuYW1lKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9uYW1lID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB2YWx1ZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdmFsdWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3ZhbHVlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzb3J0YWJsZVxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHNvcnRhYmxlKHZhbHVlOiBib29sZWFuKSB7XG5cdFx0dGhpcy5fc29ydGFibGUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHNvcnQodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3NvcnQgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGNzc1xuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgY3NzKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9jc3MgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGhpZGRlblxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGhpZGRlbih2YWx1ZTogYm9vbGVhbikge1xuXHRcdHRoaXMuX2hpZGRlbiA9IHZhbHVlO1xuXHR9XG5cbn0iXX0=