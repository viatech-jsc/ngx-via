import { CELL_TYPE } from "../constant";
export class TableCellDto {
    constructor(value = "", css = "", style = null, type = CELL_TYPE.STRING, routeLink = "", buttonActions = [], buttonDropdownActions = []) {
        this._value = value;
        this._css = css;
        this._type = type;
        this._routeLink = routeLink;
        this._buttonActions = buttonActions;
        this._buttonDropdownActions = buttonDropdownActions;
        this._style = style;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    get style() {
        return this._style;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter type
     * return {string}
     */
    get type() {
        return this._type;
    }
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink() {
        return this._routeLink;
    }
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions() {
        return this._buttonActions;
    }
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions() {
        return this._buttonDropdownActions;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    set style(value) {
        this._style = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter type
     * param {string} value
     */
    set type(value) {
        this._type = value;
    }
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value) {
        this._routeLink = value;
    }
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value) {
        this._buttonActions = value;
    }
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value) {
        this._buttonDropdownActions = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY2VsbC5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZHRvL3RhYmxlLmNlbGwuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFHeEMsTUFBTSxPQUFPLFlBQVk7SUFTckIsWUFDSSxRQUFnQixFQUFFLEVBQ2xCLE1BQWMsRUFBRSxFQUNoQixRQUEwQyxJQUFJLEVBQzlDLE9BQWUsU0FBUyxDQUFDLE1BQU0sRUFDL0IsWUFBb0IsRUFBRSxFQUN0QixnQkFBMEMsRUFBRSxFQUM1Qyx3QkFBa0QsRUFBRTtRQUVwRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztRQUNwQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcscUJBQXFCLENBQUM7UUFDcEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsS0FBSztRQUNmLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNqQixDQUFDO0lBRUQsSUFBVyxLQUFLO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2xCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUk7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsU0FBUztRQUNuQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDeEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsYUFBYTtRQUN2QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDNUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcscUJBQXFCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO0lBQ3BDLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEtBQUssQ0FBQyxLQUFhO1FBQzdCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFRCxJQUFXLEtBQUssQ0FBQyxLQUF1QztRQUN2RCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxhQUFhLENBQUMsS0FBMEI7UUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcscUJBQXFCLENBQUMsS0FBMEI7UUFDMUQsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztJQUNyQyxDQUFDO0NBR0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDRUxMX1RZUEUgfSBmcm9tIFwiLi4vY29uc3RhbnRcIjtcbmltcG9ydCB7IFRhYmxlQnRuQWN0aW9uRHRvIH0gZnJvbSBcIi4vdGFibGUuYnRuLmFjdGlvbi5kdG9cIjtcblxuZXhwb3J0IGNsYXNzIFRhYmxlQ2VsbER0b3tcbiAgICBwcml2YXRlIF92YWx1ZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX2Nzczogc3RyaW5nO1xuICAgIHByaXZhdGUgX3N0eWxlOiB7IFtrbGFzczogc3RyaW5nXTogYW55OyB9IHwgbnVsbDtcbiAgICBwcml2YXRlIF90eXBlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcm91dGVMaW5rOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfYnV0dG9uQWN0aW9uczogQXJyYXk8VGFibGVCdG5BY3Rpb25EdG8+O1xuICAgIHByaXZhdGUgX2J1dHRvbkRyb3Bkb3duQWN0aW9uczogQXJyYXk8VGFibGVCdG5BY3Rpb25EdG8+O1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHZhbHVlOiBzdHJpbmcgPSBcIlwiLFxuICAgICAgICBjc3M6IHN0cmluZyA9IFwiXCIsXG4gICAgICAgIHN0eWxlOiB7IFtrbGFzczogc3RyaW5nXTogYW55OyB9IHwgbnVsbCA9IG51bGwsXG4gICAgICAgIHR5cGU6IHN0cmluZyA9IENFTExfVFlQRS5TVFJJTkcsXG4gICAgICAgIHJvdXRlTGluazogc3RyaW5nID0gXCJcIixcbiAgICAgICAgYnV0dG9uQWN0aW9uczogQXJyYXk8VGFibGVCdG5BY3Rpb25EdG8+ID0gW10sXG4gICAgICAgIGJ1dHRvbkRyb3Bkb3duQWN0aW9uczogQXJyYXk8VGFibGVCdG5BY3Rpb25EdG8+ID0gW10sXG4gICAgKSB7XG4gICAgICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5fdHlwZSA9IHR5cGU7XG4gICAgICAgIHRoaXMuX3JvdXRlTGluayA9IHJvdXRlTGluaztcbiAgICAgICAgdGhpcy5fYnV0dG9uQWN0aW9ucyA9IGJ1dHRvbkFjdGlvbnM7XG4gICAgICAgIHRoaXMuX2J1dHRvbkRyb3Bkb3duQWN0aW9ucyA9IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucztcbiAgICAgICAgdGhpcy5fc3R5bGUgPSBzdHlsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdmFsdWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl92YWx1ZTtcbiAgICB9XG4gICAgXG4gICAgcHVibGljIGdldCBzdHlsZSgpOiB7IFtrbGFzczogc3RyaW5nXTogYW55OyB9IHwgbnVsbCB7XG5cdFx0cmV0dXJuIHRoaXMuX3N0eWxlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY3NzXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgY3NzKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2Nzcztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHR5cGVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCB0eXBlKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3R5cGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciByb3V0ZUxpbmtcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCByb3V0ZUxpbmsoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fcm91dGVMaW5rO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYnV0dG9uQWN0aW9uc1xuICAgICAqIHJldHVybiB7QnV0dG9uQWN0aW9uW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgYnV0dG9uQWN0aW9ucygpOiBUYWJsZUJ0bkFjdGlvbkR0b1tdIHtcblx0XHRyZXR1cm4gdGhpcy5fYnV0dG9uQWN0aW9ucztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGJ1dHRvbkRyb3Bkb3duQWN0aW9uc1xuICAgICAqIHJldHVybiB7QnV0dG9uQWN0aW9uW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgYnV0dG9uRHJvcGRvd25BY3Rpb25zKCk6IFRhYmxlQnRuQWN0aW9uRHRvW10ge1xuXHRcdHJldHVybiB0aGlzLl9idXR0b25Ecm9wZG93bkFjdGlvbnM7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB2YWx1ZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdmFsdWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3ZhbHVlID0gdmFsdWU7XG5cdH1cblxuXHRwdWJsaWMgc2V0IHN0eWxlKHZhbHVlOiB7IFtrbGFzczogc3RyaW5nXTogYW55OyB9IHwgbnVsbCkge1xuXHRcdHRoaXMuX3N0eWxlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjc3NcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNzcyh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fY3NzID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0eXBlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0eXBlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl90eXBlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciByb3V0ZUxpbmtcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHJvdXRlTGluayh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fcm91dGVMaW5rID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBidXR0b25BY3Rpb25zXG4gICAgICogcGFyYW0ge0J1dHRvbkFjdGlvbltdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGJ1dHRvbkFjdGlvbnModmFsdWU6IFRhYmxlQnRuQWN0aW9uRHRvW10pIHtcblx0XHR0aGlzLl9idXR0b25BY3Rpb25zID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBidXR0b25Ecm9wZG93bkFjdGlvbnNcbiAgICAgKiBwYXJhbSB7QnV0dG9uQWN0aW9uW119IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgYnV0dG9uRHJvcGRvd25BY3Rpb25zKHZhbHVlOiBUYWJsZUJ0bkFjdGlvbkR0b1tdKSB7XG5cdFx0dGhpcy5fYnV0dG9uRHJvcGRvd25BY3Rpb25zID0gdmFsdWU7XG5cdH1cblxuXG59XG4iXX0=