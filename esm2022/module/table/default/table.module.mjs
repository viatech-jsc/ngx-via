import { NgModule } from '@angular/core';
import { NgbPaginationModule, NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { TableComponent } from './table';
import { CommonModule } from '@angular/common';
import { FilterPipe } from './pipe/filter';
import { RouterModule } from '@angular/router';
import { ColumnFilterMultComp } from './column/filter/multiple/multiple';
import { ColumnFilterTextComp } from './column/filter/text/text';
import { CellComp } from './row/cell/cell';
import { CellImageComp } from './row/cell/image/image';
import { CellInputComp } from './row/cell/input/input';
import { CellDefaultComp } from './row/cell/default/default';
import { CellStatusComp } from './row/cell/status/status';
import { CellDateTimeComp } from './row/cell/datetime/datetime';
import { CellActionComp } from './row/cell/action/action';
import { CellCheckboxComp } from './row/cell/checkbox/component';
import { ColumnComp } from './column/column';
import { ColumnDefaultComp } from './column/default/column';
import { ColumnCheckboxComp } from './column/checkbox/column';
import * as i0 from "@angular/core";
export class TableModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: TableModule, declarations: [TableComponent,
            FilterPipe,
            ColumnComp,
            ColumnDefaultComp,
            ColumnCheckboxComp,
            ColumnFilterMultComp,
            ColumnFilterTextComp,
            CellComp,
            CellImageComp,
            CellInputComp,
            CellDefaultComp,
            CellStatusComp,
            CellDateTimeComp,
            CellActionComp,
            CellCheckboxComp], imports: [RouterModule,
            FormsModule,
            CommonModule,
            NgbPaginationModule,
            NgbDropdownModule], exports: [TableComponent,
            FilterPipe] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, imports: [RouterModule,
            FormsModule,
            CommonModule,
            NgbPaginationModule,
            NgbDropdownModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TableModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        RouterModule,
                        FormsModule,
                        CommonModule,
                        NgbPaginationModule,
                        NgbDropdownModule,
                    ],
                    declarations: [
                        TableComponent,
                        FilterPipe,
                        ColumnComp,
                        ColumnDefaultComp,
                        ColumnCheckboxComp,
                        ColumnFilterMultComp,
                        ColumnFilterTextComp,
                        CellComp,
                        CellImageComp,
                        CellInputComp,
                        CellDefaultComp,
                        CellStatusComp,
                        CellDateTimeComp,
                        CellActionComp,
                        CellCheckboxComp,
                    ],
                    exports: [
                        TableComponent,
                        FilterPipe
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvdGFibGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLGlCQUFpQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7QUFnQzlELE1BQU0sT0FBTyxXQUFXOzhHQUFYLFdBQVc7K0dBQVgsV0FBVyxpQkFyQnBCLGNBQWM7WUFDZCxVQUFVO1lBQ1YsVUFBVTtZQUNWLGlCQUFpQjtZQUNqQixrQkFBa0I7WUFDbEIsb0JBQW9CO1lBQ3BCLG9CQUFvQjtZQUNwQixRQUFRO1lBQ1IsYUFBYTtZQUNiLGFBQWE7WUFDYixlQUFlO1lBQ2YsY0FBYztZQUNkLGdCQUFnQjtZQUNoQixjQUFjO1lBQ2QsZ0JBQWdCLGFBckJoQixZQUFZO1lBQ1osV0FBVztZQUNYLFlBQVk7WUFDWixtQkFBbUI7WUFDbkIsaUJBQWlCLGFBb0JqQixjQUFjO1lBQ2QsVUFBVTsrR0FHRCxXQUFXLFlBNUJwQixZQUFZO1lBQ1osV0FBVztZQUNYLFlBQVk7WUFDWixtQkFBbUI7WUFDbkIsaUJBQWlCOzsyRkF3QlIsV0FBVztrQkE5QnZCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxZQUFZO3dCQUNaLG1CQUFtQjt3QkFDbkIsaUJBQWlCO3FCQUNsQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osY0FBYzt3QkFDZCxVQUFVO3dCQUNWLFVBQVU7d0JBQ1YsaUJBQWlCO3dCQUNqQixrQkFBa0I7d0JBQ2xCLG9CQUFvQjt3QkFDcEIsb0JBQW9CO3dCQUNwQixRQUFRO3dCQUNSLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsZ0JBQWdCO3dCQUNoQixjQUFjO3dCQUNkLGdCQUFnQjtxQkFDakI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGNBQWM7d0JBQ2QsVUFBVTtxQkFDWDtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ2JQYWdpbmF0aW9uTW9kdWxlLCBOZ2JEcm9wZG93bk1vZHVsZSB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7IFRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi90YWJsZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nOyAgXG5pbXBvcnQgeyBGaWx0ZXJQaXBlIH0gZnJvbSAnLi9waXBlL2ZpbHRlcic7XG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQ29sdW1uRmlsdGVyTXVsdENvbXAgfSBmcm9tICcuL2NvbHVtbi9maWx0ZXIvbXVsdGlwbGUvbXVsdGlwbGUnO1xuaW1wb3J0IHsgQ29sdW1uRmlsdGVyVGV4dENvbXAgfSBmcm9tICcuL2NvbHVtbi9maWx0ZXIvdGV4dC90ZXh0JztcbmltcG9ydCB7IENlbGxDb21wIH0gZnJvbSAnLi9yb3cvY2VsbC9jZWxsJztcbmltcG9ydCB7IENlbGxJbWFnZUNvbXAgfSBmcm9tICcuL3Jvdy9jZWxsL2ltYWdlL2ltYWdlJztcbmltcG9ydCB7IENlbGxJbnB1dENvbXAgfSBmcm9tICcuL3Jvdy9jZWxsL2lucHV0L2lucHV0JztcbmltcG9ydCB7IENlbGxEZWZhdWx0Q29tcCB9IGZyb20gJy4vcm93L2NlbGwvZGVmYXVsdC9kZWZhdWx0JztcbmltcG9ydCB7IENlbGxTdGF0dXNDb21wIH0gZnJvbSAnLi9yb3cvY2VsbC9zdGF0dXMvc3RhdHVzJztcbmltcG9ydCB7IENlbGxEYXRlVGltZUNvbXAgfSBmcm9tICcuL3Jvdy9jZWxsL2RhdGV0aW1lL2RhdGV0aW1lJztcbmltcG9ydCB7IENlbGxBY3Rpb25Db21wIH0gZnJvbSAnLi9yb3cvY2VsbC9hY3Rpb24vYWN0aW9uJztcbmltcG9ydCB7IENlbGxDaGVja2JveENvbXAgfSBmcm9tICcuL3Jvdy9jZWxsL2NoZWNrYm94L2NvbXBvbmVudCc7XG5pbXBvcnQgeyBDb2x1bW5Db21wIH0gZnJvbSAnLi9jb2x1bW4vY29sdW1uJztcbmltcG9ydCB7IENvbHVtbkRlZmF1bHRDb21wIH0gZnJvbSAnLi9jb2x1bW4vZGVmYXVsdC9jb2x1bW4nO1xuaW1wb3J0IHsgQ29sdW1uQ2hlY2tib3hDb21wIH0gZnJvbSAnLi9jb2x1bW4vY2hlY2tib3gvY29sdW1uJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIFJvdXRlck1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgTmdiUGFnaW5hdGlvbk1vZHVsZSxcbiAgICBOZ2JEcm9wZG93bk1vZHVsZSxcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgVGFibGVDb21wb25lbnQsXG4gICAgRmlsdGVyUGlwZSxcbiAgICBDb2x1bW5Db21wLFxuICAgIENvbHVtbkRlZmF1bHRDb21wLFxuICAgIENvbHVtbkNoZWNrYm94Q29tcCxcbiAgICBDb2x1bW5GaWx0ZXJNdWx0Q29tcCxcbiAgICBDb2x1bW5GaWx0ZXJUZXh0Q29tcCxcbiAgICBDZWxsQ29tcCxcbiAgICBDZWxsSW1hZ2VDb21wLFxuICAgIENlbGxJbnB1dENvbXAsXG4gICAgQ2VsbERlZmF1bHRDb21wLFxuICAgIENlbGxTdGF0dXNDb21wLFxuICAgIENlbGxEYXRlVGltZUNvbXAsXG4gICAgQ2VsbEFjdGlvbkNvbXAsXG4gICAgQ2VsbENoZWNrYm94Q29tcCxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIFRhYmxlQ29tcG9uZW50LFxuICAgIEZpbHRlclBpcGVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBUYWJsZU1vZHVsZSB7IH1cbiJdfQ==