import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
export class ColumnDefaultComp {
    constructor() {
        this.actionSort = new EventEmitter();
        this.column = new TableColumnDto();
    }
    ngOnInit() {
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.actionSort.emit(column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnDefaultComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnDefaultComp, selector: "via-table-column-default", inputs: { column: "column" }, outputs: { actionSort: "actionSort" }, ngImport: i0, template: "<span (click)=\"doSort(column)\">{{column.value}}</span>", styles: [""] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnDefaultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-default", template: "<span (click)=\"doSort(column)\">{{column.value}}</span>" }]
        }], ctorParameters: () => [], propDecorators: { actionSort: [{
                type: Output
            }], column: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvY29sdW1uL2RlZmF1bHQvY29sdW1uLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvY29sdW1uL2RlZmF1bHQvY29sdW1uLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0JBQStCLENBQUM7O0FBTy9ELE1BQU0sT0FBTyxpQkFBaUI7SUFNNUI7UUFKVSxlQUFVLEdBQWlDLElBQUksWUFBWSxFQUFFLENBQUM7UUFFL0QsV0FBTSxHQUFtQixJQUFJLGNBQWMsRUFBRSxDQUFDO0lBSXZELENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFzQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3JCLE9BQU87UUFDVCxDQUFDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0IsQ0FBQzs4R0FsQlUsaUJBQWlCO2tHQUFqQixpQkFBaUIscUlDUjlCLDBEQUFzRDs7MkZEUXpDLGlCQUFpQjtrQkFMN0IsU0FBUzsrQkFDRSwwQkFBMEI7d0RBTTFCLFVBQVU7c0JBQW5CLE1BQU07Z0JBRUUsTUFBTTtzQkFBZCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgVGFibGVDb2x1bW5EdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jb2x1bW4tZGVmYXVsdFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2NvbHVtbi5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9jb2x1bW4uc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDb2x1bW5EZWZhdWx0Q29tcHtcblxuICBAT3V0cHV0KCkgYWN0aW9uU29ydDogRXZlbnRFbWl0dGVyPFRhYmxlQ29sdW1uRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBASW5wdXQoKSBjb2x1bW46IFRhYmxlQ29sdW1uRHRvID0gbmV3IFRhYmxlQ29sdW1uRHRvKCk7XG4gIFxuICBjb25zdHJ1Y3RvcihcbiAgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIGRvU29ydChjb2x1bW46IFRhYmxlQ29sdW1uRHRvKTogdm9pZCB7XG4gICAgaWYgKCFjb2x1bW4uc29ydGFibGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5hY3Rpb25Tb3J0LmVtaXQoY29sdW1uKTtcbiAgfVxuXG59XG4iLCI8c3BhbiAoY2xpY2spPVwiZG9Tb3J0KGNvbHVtbilcIj57e2NvbHVtbi52YWx1ZX19PC9zcGFuPiJdfQ==