import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
export class ColumnCheckboxComp {
    constructor() {
        this.selectColumn = new EventEmitter();
        this.column = new TableColumnDto();
        this.checked = false;
    }
    ngOnInit() {
        this.buildUI();
    }
    buildUI() {
        this.checked = this.column.value.toLowerCase() === JSON.stringify(true);
    }
    change() {
        this.column.value = JSON.stringify(this.checked);
        this.selectColumn.emit(this.column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnCheckboxComp, selector: "via-table-column-checkbox", inputs: { column: "column" }, outputs: { selectColumn: "selectColumn" }, ngImport: i0, template: "<span class=\"column-checkbox-wrapper\">\n    <input \n        type=\"checkbox\"\n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".column-checkbox-wrapper{padding:3px}.column-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1.CheckboxControlValueAccessor, selector: "input[type=checkbox][formControlName],input[type=checkbox][formControl],input[type=checkbox][ngModel]" }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnCheckboxComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-column-checkbox", template: "<span class=\"column-checkbox-wrapper\">\n    <input \n        type=\"checkbox\"\n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".column-checkbox-wrapper{padding:3px}.column-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { selectColumn: [{
                type: Output
            }], column: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvY29sdW1uL2NoZWNrYm94L2NvbHVtbi50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L2NvbHVtbi9jaGVja2JveC9jb2x1bW4uaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7O0FBTy9ELE1BQU0sT0FBTyxrQkFBa0I7SUFRN0I7UUFOVSxpQkFBWSxHQUFpQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRWpFLFdBQU0sR0FBbUIsSUFBSSxjQUFjLEVBQUUsQ0FBQztRQUV2RCxZQUFPLEdBQVksS0FBSyxDQUFDO0lBSXpCLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCxPQUFPO1FBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7OEdBdkJVLGtCQUFrQjtrR0FBbEIsa0JBQWtCLDBJQ1IvQiw0SkFLTzs7MkZER00sa0JBQWtCO2tCQUw5QixTQUFTOytCQUNFLDJCQUEyQjt3REFNM0IsWUFBWTtzQkFBckIsTUFBTTtnQkFFRSxNQUFNO3NCQUFkLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBUYWJsZUNvbHVtbkR0byB9IGZyb20gXCIuLi8uLi8uLi9kdG8vdGFibGUuY29sdW1uLmR0b1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLXRhYmxlLWNvbHVtbi1jaGVja2JveFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2NvbHVtbi5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9jb2x1bW4uc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDb2x1bW5DaGVja2JveENvbXB7XG5cbiAgQE91dHB1dCgpIHNlbGVjdENvbHVtbjogRXZlbnRFbWl0dGVyPFRhYmxlQ29sdW1uRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBASW5wdXQoKSBjb2x1bW46IFRhYmxlQ29sdW1uRHRvID0gbmV3IFRhYmxlQ29sdW1uRHRvKCk7XG5cbiAgY2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5idWlsZFVJKCk7XG4gIH1cblxuICBidWlsZFVJKCkgeyBcbiAgICB0aGlzLmNoZWNrZWQgPSB0aGlzLmNvbHVtbi52YWx1ZS50b0xvd2VyQ2FzZSgpID09PSBKU09OLnN0cmluZ2lmeSh0cnVlKTtcbiAgfVxuXG4gIGNoYW5nZSgpIHtcbiAgICB0aGlzLmNvbHVtbi52YWx1ZSA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY2hlY2tlZCk7XG4gICAgdGhpcy5zZWxlY3RDb2x1bW4uZW1pdCh0aGlzLmNvbHVtbik7XG4gIH1cblxufVxuIiwiPHNwYW4gY2xhc3M9XCJjb2x1bW4tY2hlY2tib3gtd3JhcHBlclwiPlxuICAgIDxpbnB1dCBcbiAgICAgICAgdHlwZT1cImNoZWNrYm94XCJcbiAgICAgICAgWyhuZ01vZGVsKV09XCJjaGVja2VkXCJcbiAgICAgICAgKGNoYW5nZSk9XCJjaGFuZ2UoKVwiPlxuPC9zcGFuPiJdfQ==