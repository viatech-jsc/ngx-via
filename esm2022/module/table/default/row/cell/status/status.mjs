import { Component, Input } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellStatusComp {
    constructor() {
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellStatusComp, selector: "via-table-cell-status", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-status", template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdHVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvcm93L2NlbGwvc3RhdHVzL3N0YXR1cy50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L3Jvdy9jZWxsL3N0YXR1cy9zdGF0dXMuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7OztBQU85RCxNQUFNLE9BQU8sY0FBYztJQUl6QjtRQUZTLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUlqRCxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7OEdBVFUsY0FBYztrR0FBZCxjQUFjLHVGQ1IzQixpSUFNTzs7MkZERU0sY0FBYztrQkFMMUIsU0FBUzsrQkFDRSx1QkFBdUI7d0RBTXhCLElBQUk7c0JBQVosS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2R0by90YWJsZS5jZWxsLmR0b1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLXRhYmxlLWNlbGwtc3RhdHVzXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vc3RhdHVzLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3N0YXR1cy5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxTdGF0dXNDb21we1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuXG59XG4iLCI8c3BhbiBcbiAgICB0aXRsZT1cInt7Y2VsbC52YWx1ZSB9fVwiIFxuICAgIGNsYXNzPVwic3RhdHVzXCIgXG4gICAgW25nQ2xhc3NdPVwiY2VsbC52YWx1ZVwiXG4gICAgPlxuICAgIHt7Y2VsbC52YWx1ZX19XG48L3NwYW4+Il19