import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common";
export class CellDefaultComp {
    constructor(router) {
        this.router = router;
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, deps: [{ token: i1.Router }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDefaultComp, selector: "via-table-cell-default", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"], dependencies: [{ kind: "directive", type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-default", template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i1.Router }], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L3Jvdy9jZWxsL2RlZmF1bHQvZGVmYXVsdC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L3Jvdy9jZWxsL2RlZmF1bHQvZGVmYXVsdC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7Ozs7QUFPOUQsTUFBTSxPQUFPLGVBQWU7SUFJMUIsWUFDVSxNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUhmLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUtqRCxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxXQUFXLENBQUMsSUFBa0I7UUFDNUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3pDLENBQUM7SUFDSCxDQUFDOzhHQWhCVSxlQUFlO2tHQUFmLGVBQWUsd0ZDVDVCLGlMQU1POzsyRkRHTSxlQUFlO2tCQUwzQixTQUFTOytCQUNFLHdCQUF3QjsyRUFNekIsSUFBSTtzQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBUYWJsZUNlbGxEdG8gfSBmcm9tIFwiLi4vLi4vLi4vLi4vZHRvL3RhYmxlLmNlbGwuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbC1kZWZhdWx0XCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZGVmYXVsdC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kZWZhdWx0LnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ2VsbERlZmF1bHRDb21we1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgY2VsbENsaWNrZWQoY2VsbDogVGFibGVDZWxsRHRvKTogdm9pZCB7XG4gICAgaWYgKGNlbGwucm91dGVMaW5rLnRyaW0oKS5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbY2VsbC5yb3V0ZUxpbmtdKTtcbiAgICB9XG4gIH1cbn1cbiIsIjxzcGFuIFxuICAgIHRpdGxlPVwie3tjZWxsLnZhbHVlfX1cIiBcbiAgICAoY2xpY2spPVwiY2VsbENsaWNrZWQoY2VsbClcIiBcbiAgICBbbmdDbGFzc109XCJjZWxsLnJvdXRlTGluay50cmltKCkubGVuZ3RoID4gMCA/ICdoYW5kJyA6ICcnXCJcbiAgICA+XG4gICAge3tjZWxsLnZhbHVlfX1cbjwvc3Bhbj4iXX0=