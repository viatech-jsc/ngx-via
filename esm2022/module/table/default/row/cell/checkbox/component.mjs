import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import { TableRowDto } from "../../../../dto/table.row.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
export class CellCheckboxComp {
    constructor() {
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.rowUpdated = new EventEmitter();
        this.checked = false;
    }
    ngOnInit() {
        this.buildUI();
    }
    buildUI() {
        this.checked = this.cell.value.toLowerCase() === JSON.stringify(true);
    }
    change() {
        this.cell.value = JSON.stringify(this.checked);
        this.rowUpdated.emit(this.row);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellCheckboxComp, selector: "via-table-cell-checkbox", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, ngImport: i0, template: "<span class=\"cell-checkbox-wrapper\">\n    <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1.CheckboxControlValueAccessor, selector: "input[type=checkbox][formControlName],input[type=checkbox][formControl],input[type=checkbox][ngModel]" }, { kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-checkbox", template: "<span class=\"cell-checkbox-wrapper\">\n    <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { row: [{
                type: Input
            }], cell: [{
                type: Input
            }], rowUpdated: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvcm93L2NlbGwvY2hlY2tib3gvY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL2RlZmF1bHQvcm93L2NlbGwvY2hlY2tib3gvaW5kZXguaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sK0JBQStCLENBQUM7OztBQU81RCxNQUFNLE9BQU8sZ0JBQWdCO0lBUzNCO1FBUFMsUUFBRyxHQUFnQixJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV2QyxlQUFVLEdBQThCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckUsWUFBTyxHQUFZLEtBQUssQ0FBQztJQUl6QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNqQixDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDOzhHQXhCVSxnQkFBZ0I7a0dBQWhCLGdCQUFnQiw0SUNUN0IsaU1BTU87OzJGREdNLGdCQUFnQjtrQkFMNUIsU0FBUzsrQkFDRSx5QkFBeUI7d0RBTTFCLEdBQUc7c0JBQVgsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUs7Z0JBRUksVUFBVTtzQkFBbkIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcbmltcG9ydCB7IFRhYmxlUm93RHRvIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2R0by90YWJsZS5yb3cuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbC1jaGVja2JveFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2luZGV4Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3N0eWxlcy5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxDaGVja2JveENvbXB7XG4gIFxuICBASW5wdXQoKSByb3c6IFRhYmxlUm93RHRvID0gbmV3IFRhYmxlUm93RHRvKCk7XG4gIEBJbnB1dCgpIGNlbGw6IFRhYmxlQ2VsbER0byA9IG5ldyBUYWJsZUNlbGxEdG8oKTtcbiAgXG4gIEBPdXRwdXQoKSByb3dVcGRhdGVkOiBFdmVudEVtaXR0ZXI8VGFibGVSb3dEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBcbiAgY2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5idWlsZFVJKCk7XG4gIH1cblxuICBidWlsZFVJKCkgeyBcbiAgICB0aGlzLmNoZWNrZWQgPSB0aGlzLmNlbGwudmFsdWUudG9Mb3dlckNhc2UoKSA9PT0gSlNPTi5zdHJpbmdpZnkodHJ1ZSk7XG4gIH1cblxuICBjaGFuZ2UoKSB7XG4gICAgdGhpcy5jZWxsLnZhbHVlID0gSlNPTi5zdHJpbmdpZnkodGhpcy5jaGVja2VkKTtcbiAgICB0aGlzLnJvd1VwZGF0ZWQuZW1pdCh0aGlzLnJvdyk7XG4gIH1cblxufVxuIiwiPHNwYW4gY2xhc3M9XCJjZWxsLWNoZWNrYm94LXdyYXBwZXJcIj5cbiAgICA8aW5wdXQgXG4gICAgICAgIHR5cGU9XCJjaGVja2JveFwiIFxuICAgICAgICBbYXR0ci52YWx1ZV09XCJjZWxsLnZhbHVlXCIgXG4gICAgICAgIFsobmdNb2RlbCldPVwiY2hlY2tlZFwiXG4gICAgICAgIChjaGFuZ2UpPVwiY2hhbmdlKClcIj5cbjwvc3Bhbj4iXX0=