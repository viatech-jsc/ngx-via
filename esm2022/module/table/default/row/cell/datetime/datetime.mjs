import { Component, Input } from "@angular/core";
import { TABLE_DATE_FORMAT } from "../../../../constant";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellDateTimeComp {
    constructor() {
        this.cell = new TableCellDto();
        this.dateFormat = TABLE_DATE_FORMAT;
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDateTimeComp, selector: "via-table-cell-datetime", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>", styles: [""], dependencies: [{ kind: "pipe", type: i1.DatePipe, name: "date" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-datetime", template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>" }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZGVmYXVsdC9yb3cvY2VsbC9kYXRldGltZS9kYXRldGltZS50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L3Jvdy9jZWxsL2RhdGV0aW1lL2RhdGV0aW1lLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGdDQUFnQyxDQUFDOzs7QUFPOUQsTUFBTSxPQUFPLGdCQUFnQjtJQU0zQjtRQUpTLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVqRCxlQUFVLEdBQVcsaUJBQWlCLENBQUM7SUFJdkMsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDOzhHQVhVLGdCQUFnQjtrR0FBaEIsZ0JBQWdCLHlGQ1Q3QiwrR0FJTzs7MkZES00sZ0JBQWdCO2tCQUw1QixTQUFTOytCQUNFLHlCQUF5Qjt3REFNMUIsSUFBSTtzQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBUQUJMRV9EQVRFX0ZPUk1BVCB9IGZyb20gXCIuLi8uLi8uLi8uLi9jb25zdGFudFwiO1xuaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2R0by90YWJsZS5jZWxsLmR0b1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLXRhYmxlLWNlbGwtZGF0ZXRpbWVcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9kYXRldGltZS5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kYXRldGltZS5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxEYXRlVGltZUNvbXB7XG4gIFxuICBASW5wdXQoKSBjZWxsOiBUYWJsZUNlbGxEdG8gPSBuZXcgVGFibGVDZWxsRHRvKCk7XG5cbiAgZGF0ZUZvcm1hdDogc3RyaW5nID0gVEFCTEVfREFURV9GT1JNQVQ7XG4gIFxuICBjb25zdHJ1Y3RvcihcbiAgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG5cbn1cbiIsIjxzcGFuIFxuICAgIHRpdGxlPVwie3tjZWxsLnZhbHVlICB8IGRhdGU6ZGF0ZUZvcm1hdH19XCJcbiAgICA+XG4gICAge3tjZWxsLnZhbHVlIHwgZGF0ZTpkYXRlRm9ybWF0fX1cbjwvc3Bhbj4iXX0=