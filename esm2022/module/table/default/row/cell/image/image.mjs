import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export class CellImageComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellImageComp, selector: "via-table-cell-image", inputs: { cell: "cell" }, outputs: { openImagePreview: "openImagePreview" }, ngImport: i0, template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, decorators: [{
            type: Component,
            args: [{ selector: "via-table-cell-image", template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvZGVmYXVsdC9yb3cvY2VsbC9pbWFnZS9pbWFnZS50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9kZWZhdWx0L3Jvdy9jZWxsL2ltYWdlL2ltYWdlLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7O0FBTzlELE1BQU0sT0FBTyxhQUFhO0lBTXhCO1FBSlUscUJBQWdCLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFN0QsU0FBSSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBSWpELENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUFlO1FBQzNCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs4R0FmVSxhQUFhO2tHQUFiLGFBQWEseUlDUjFCLHdSQUtPOzsyRkRHTSxhQUFhO2tCQUx6QixTQUFTOytCQUNFLHNCQUFzQjt3REFNdEIsZ0JBQWdCO3NCQUF6QixNQUFNO2dCQUVFLElBQUk7c0JBQVosS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jZWxsLWltYWdlXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vaW1hZ2UuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vaW1hZ2Uuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDZWxsSW1hZ2VDb21we1xuICBcbiAgQE91dHB1dCgpIG9wZW5JbWFnZVByZXZpZXc6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBkb09wZW5QcmV2aWV3KGltZ0NvZGU6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMub3BlbkltYWdlUHJldmlldy5lbWl0KGltZ0NvZGUpO1xuICB9XG5cbn1cbiIsIjxzcGFuIGNsYXNzPVwidGh1bWJuYWlsLXdyYXBwZXJcIj5cbiAgICA8IS0tIDxpbWcgKGNsaWNrKT1cImRvT3BlblByZXZpZXcoY2VsbC52YWx1ZSlcIiB3aWR0aD1cIjYwcHhcIlxuICAgICAgICBbc3JjXT1cImRvd25sb2FkRmlsZVBhdGggKyBjZWxsLnZhbHVlICsgJ190aHVtYidcIiAvPiAtLT5cbiAgICAgICAgPGltZyAoY2xpY2spPVwiZG9PcGVuUHJldmlldyhjZWxsLnZhbHVlKVwiIHdpZHRoPVwiNjBweFwiXG4gICAgICAgIFtzcmNdPVwiY2VsbC52YWx1ZVwiIC8+XG48L3NwYW4+Il19