import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { MatTableComponent } from './table';
import { MatMenuModule } from '@angular/material/menu';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { ColumnFilterMultComp } from './column/filter/multiple/multiple';
import { ColumnFilterTextComp } from './column/filter/text/text';
import { ColumnComp } from './column/column';
import { CellComp } from './row/cell/cell';
import { CellImageComp } from './row/cell/image/image';
import { CellInputComp } from './row/cell/input/input';
import { CellDefaultComp } from './row/cell/default/default';
import { CellStatusComp } from './row/cell/status/status';
import { CellDateTimeComp } from './row/cell/datetime/datetime';
import { CellActionComp } from './row/cell/action/action';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { MatFilterPipe } from './pipe/filter';
import { CellCheckboxComp } from './row/cell/checkbox/component';
import * as i0 from "@angular/core";
// import { PaginatorI18n } from './i18n/paginator.i18n';
export class MatTableModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, declarations: [MatFilterPipe,
            MatTableComponent,
            ColumnComp,
            ColumnFilterMultComp,
            ColumnFilterTextComp,
            CellComp,
            CellImageComp,
            CellInputComp,
            CellDefaultComp,
            CellStatusComp,
            CellDateTimeComp,
            CellActionComp,
            CellCheckboxComp], imports: [FormsModule,
            CommonModule,
            RouterModule,
            MatPaginatorModule,
            MatMenuModule,
            MatIconModule,
            MatButtonModule,
            MatBadgeModule,
            MatCheckboxModule,
            MatListModule,
            MatFormFieldModule,
            MatInputModule,
            MatProgressSpinnerModule], exports: [MatTableComponent,
            MatFilterPipe] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, imports: [FormsModule,
            CommonModule,
            RouterModule,
            MatPaginatorModule,
            MatMenuModule,
            MatIconModule,
            MatButtonModule,
            MatBadgeModule,
            MatCheckboxModule,
            MatListModule,
            MatFormFieldModule,
            MatInputModule,
            MatProgressSpinnerModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatTableModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        FormsModule,
                        CommonModule,
                        RouterModule,
                        MatPaginatorModule,
                        MatMenuModule,
                        MatIconModule,
                        MatButtonModule,
                        MatBadgeModule,
                        MatCheckboxModule,
                        MatListModule,
                        MatFormFieldModule,
                        MatInputModule,
                        MatProgressSpinnerModule
                    ],
                    declarations: [
                        MatFilterPipe,
                        MatTableComponent,
                        ColumnComp,
                        ColumnFilterMultComp,
                        ColumnFilterTextComp,
                        CellComp,
                        CellImageComp,
                        CellInputComp,
                        CellDefaultComp,
                        CellStatusComp,
                        CellDateTimeComp,
                        CellActionComp,
                        CellCheckboxComp
                    ],
                    exports: [
                        MatTableComponent,
                        MatFilterPipe
                    ],
                    providers: [
                    // { provide: MatPaginatorIntl, useClass: PaginatorI18n }
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL21hdGVyaWFsL3RhYmxlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDNUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDekUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDakUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUM5RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7QUFDakUseURBQXlEO0FBeUN6RCxNQUFNLE9BQU8sY0FBYzs4R0FBZCxjQUFjOytHQUFkLGNBQWMsaUJBdEJ2QixhQUFhO1lBQ2IsaUJBQWlCO1lBQ2pCLFVBQVU7WUFDVixvQkFBb0I7WUFDcEIsb0JBQW9CO1lBQ3BCLFFBQVE7WUFDUixhQUFhO1lBQ2IsYUFBYTtZQUNiLGVBQWU7WUFDZixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLGNBQWM7WUFDZCxnQkFBZ0IsYUEzQmhCLFdBQVc7WUFDWCxZQUFZO1lBQ1osWUFBWTtZQUNaLGtCQUFrQjtZQUNsQixhQUFhO1lBQ2IsYUFBYTtZQUNiLGVBQWU7WUFDZixjQUFjO1lBQ2QsaUJBQWlCO1lBQ2pCLGFBQWE7WUFDYixrQkFBa0I7WUFDbEIsY0FBYztZQUNkLHdCQUF3QixhQWtCeEIsaUJBQWlCO1lBQ2pCLGFBQWE7K0dBTUosY0FBYyxZQXJDdkIsV0FBVztZQUNYLFlBQVk7WUFDWixZQUFZO1lBQ1osa0JBQWtCO1lBQ2xCLGFBQWE7WUFDYixhQUFhO1lBQ2IsZUFBZTtZQUNmLGNBQWM7WUFDZCxpQkFBaUI7WUFDakIsYUFBYTtZQUNiLGtCQUFrQjtZQUNsQixjQUFjO1lBQ2Qsd0JBQXdCOzsyRkF5QmYsY0FBYztrQkF2QzFCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFdBQVc7d0JBQ1gsWUFBWTt3QkFDWixZQUFZO3dCQUNaLGtCQUFrQjt3QkFDbEIsYUFBYTt3QkFDYixhQUFhO3dCQUNiLGVBQWU7d0JBQ2YsY0FBYzt3QkFDZCxpQkFBaUI7d0JBQ2pCLGFBQWE7d0JBQ2Isa0JBQWtCO3dCQUNsQixjQUFjO3dCQUNkLHdCQUF3QjtxQkFDekI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLGFBQWE7d0JBQ2IsaUJBQWlCO3dCQUNqQixVQUFVO3dCQUNWLG9CQUFvQjt3QkFDcEIsb0JBQW9CO3dCQUNwQixRQUFRO3dCQUNSLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsZ0JBQWdCO3dCQUNoQixjQUFjO3dCQUNkLGdCQUFnQjtxQkFDakI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGlCQUFpQjt3QkFDakIsYUFBYTtxQkFDZDtvQkFDRCxTQUFTLEVBQUU7b0JBQ1QseURBQXlEO3FCQUMxRDtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTWF0VGFibGVDb21wb25lbnQgfSBmcm9tICcuL3RhYmxlJztcbmltcG9ydCB7IE1hdE1lbnVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9tZW51JztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXRQYWdpbmF0b3JNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InO1xuaW1wb3J0IHsgTWF0SWNvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2ljb24nO1xuaW1wb3J0IHsgTWF0QnV0dG9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uJztcbmltcG9ydCB7IE1hdEJhZGdlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYmFkZ2UnO1xuaW1wb3J0IHsgTWF0Q2hlY2tib3hNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jaGVja2JveCc7XG5pbXBvcnQgeyBNYXRMaXN0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbGlzdCc7XG5pbXBvcnQgeyBDb2x1bW5GaWx0ZXJNdWx0Q29tcCB9IGZyb20gJy4vY29sdW1uL2ZpbHRlci9tdWx0aXBsZS9tdWx0aXBsZSc7XG5pbXBvcnQgeyBDb2x1bW5GaWx0ZXJUZXh0Q29tcCB9IGZyb20gJy4vY29sdW1uL2ZpbHRlci90ZXh0L3RleHQnO1xuaW1wb3J0IHsgQ29sdW1uQ29tcCB9IGZyb20gJy4vY29sdW1uL2NvbHVtbic7XG5pbXBvcnQgeyBDZWxsQ29tcCB9IGZyb20gJy4vcm93L2NlbGwvY2VsbCc7XG5pbXBvcnQgeyBDZWxsSW1hZ2VDb21wIH0gZnJvbSAnLi9yb3cvY2VsbC9pbWFnZS9pbWFnZSc7XG5pbXBvcnQgeyBDZWxsSW5wdXRDb21wIH0gZnJvbSAnLi9yb3cvY2VsbC9pbnB1dC9pbnB1dCc7XG5pbXBvcnQgeyBDZWxsRGVmYXVsdENvbXAgfSBmcm9tICcuL3Jvdy9jZWxsL2RlZmF1bHQvZGVmYXVsdCc7XG5pbXBvcnQgeyBDZWxsU3RhdHVzQ29tcCB9IGZyb20gJy4vcm93L2NlbGwvc3RhdHVzL3N0YXR1cyc7XG5pbXBvcnQgeyBDZWxsRGF0ZVRpbWVDb21wIH0gZnJvbSAnLi9yb3cvY2VsbC9kYXRldGltZS9kYXRldGltZSc7XG5pbXBvcnQgeyBDZWxsQWN0aW9uQ29tcCB9IGZyb20gJy4vcm93L2NlbGwvYWN0aW9uL2FjdGlvbic7XG5pbXBvcnQgeyBNYXRGb3JtRmllbGRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9mb3JtLWZpZWxkJztcbmltcG9ydCB7IE1hdElucHV0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaW5wdXQnO1xuaW1wb3J0IHsgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3Mtc3Bpbm5lcic7XG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTWF0RmlsdGVyUGlwZSB9IGZyb20gJy4vcGlwZS9maWx0ZXInO1xuaW1wb3J0IHsgQ2VsbENoZWNrYm94Q29tcCB9IGZyb20gJy4vcm93L2NlbGwvY2hlY2tib3gvY29tcG9uZW50Jztcbi8vIGltcG9ydCB7IFBhZ2luYXRvckkxOG4gfSBmcm9tICcuL2kxOG4vcGFnaW5hdG9yLmkxOG4nO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFJvdXRlck1vZHVsZSxcbiAgICBNYXRQYWdpbmF0b3JNb2R1bGUsXG4gICAgTWF0TWVudU1vZHVsZSxcbiAgICBNYXRJY29uTW9kdWxlLFxuICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICBNYXRCYWRnZU1vZHVsZSxcbiAgICBNYXRDaGVja2JveE1vZHVsZSxcbiAgICBNYXRMaXN0TW9kdWxlLFxuICAgIE1hdEZvcm1GaWVsZE1vZHVsZSxcbiAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgTWF0RmlsdGVyUGlwZSxcbiAgICBNYXRUYWJsZUNvbXBvbmVudCxcbiAgICBDb2x1bW5Db21wLFxuICAgIENvbHVtbkZpbHRlck11bHRDb21wLFxuICAgIENvbHVtbkZpbHRlclRleHRDb21wLFxuICAgIENlbGxDb21wLFxuICAgIENlbGxJbWFnZUNvbXAsXG4gICAgQ2VsbElucHV0Q29tcCxcbiAgICBDZWxsRGVmYXVsdENvbXAsXG4gICAgQ2VsbFN0YXR1c0NvbXAsXG4gICAgQ2VsbERhdGVUaW1lQ29tcCxcbiAgICBDZWxsQWN0aW9uQ29tcCxcbiAgICBDZWxsQ2hlY2tib3hDb21wXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBNYXRUYWJsZUNvbXBvbmVudCxcbiAgICBNYXRGaWx0ZXJQaXBlXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIC8vIHsgcHJvdmlkZTogTWF0UGFnaW5hdG9ySW50bCwgdXNlQ2xhc3M6IFBhZ2luYXRvckkxOG4gfVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE1hdFRhYmxlTW9kdWxlIHsgfVxuIl19