import { Component, EventEmitter, Input, Output } from "@angular/core";
import { SORT } from "../../constant";
import { TableColumnDto } from "../../dto/table.column.dto";
import { TableI18nDto } from "../../dto/table.i18n.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "./filter/multiple/multiple";
import * as i3 from "./filter/text/text";
export class ColumnComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.sortColumn = new EventEmitter();
        this.column = new TableColumnDto();
        this.i18n = new TableI18nDto();
    }
    ngOnInit() {
    }
    doFilterOnColumn(req) {
        this.dropdownFilter.emit(req);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC;
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC;
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.sortColumn.emit(column);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: ColumnComp, selector: "via-mat-table-column", inputs: { column: "column", i18n: "i18n" }, outputs: { dropdownFilter: "dropdownFilter", sortColumn: "sortColumn" }, ngImport: i0, template: "<div class=\"header-container\">\n    <span (click)=\"doSort(column)\">{{column.value}}</span>\n\n    <via-mat-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-mult>\n    \n    <via-mat-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"], dependencies: [{ kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i2.ColumnFilterMultComp, selector: "via-mat-table-column-filter-mult", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }, { kind: "component", type: i3.ColumnFilterTextComp, selector: "via-mat-table-column-filter-text", inputs: ["column", "i18n"], outputs: ["dropdownFilter"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: ColumnComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-column", template: "<div class=\"header-container\">\n    <span (click)=\"doSort(column)\">{{column.value}}</span>\n\n    <via-mat-table-column-filter-mult \n        *ngIf=\"column.filter.type==='MULTIPLE'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-mult>\n    \n    <via-mat-table-column-filter-text \n        *ngIf=\"column.filter.type==='TEXT'\"\n        [column]=\"column\"\n        [i18n]=\"i18n\"\n        (dropdownFilter)=\"doFilterOnColumn($event)\">\n    </via-mat-table-column-filter-text>\n\n    <i *ngIf=\"isSortAsc(column)\" class=\"fas fa-sort-amount-down\" aria-hidden=\"true\"></i>\n    <i *ngIf=\"isSortDesc(column)\" class=\"fas fa-sort-amount-up\" aria-hidden=\"true\"></i>\n</div>", styles: [".header-container{display:flex;flex-direction:row;width:100%;justify-content:flex-start;align-items:center}.header-container i{text-align:left;margin-left:10px;color:#000}\n"] }]
        }], ctorParameters: () => [], propDecorators: { dropdownFilter: [{
                type: Output
            }], sortColumn: [{
                type: Output
            }], column: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL21hdGVyaWFsL2NvbHVtbi9jb2x1bW4udHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvY29sdW1uL2NvbHVtbi5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXRDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7Ozs7O0FBT3hELE1BQU0sT0FBTyxVQUFVO0lBUXJCO1FBTlUsbUJBQWMsR0FBc0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RSxlQUFVLEdBQWlDLElBQUksWUFBWSxFQUFFLENBQUM7UUFFL0QsV0FBTSxHQUFtQixJQUFJLGNBQWMsRUFBRSxDQUFDO1FBQzlDLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUlqRCxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxHQUF3QjtRQUN2QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsU0FBUyxDQUFDLE1BQXNCO1FBQzlCLE9BQU8sTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxVQUFVLENBQUMsTUFBc0I7UUFDL0IsT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFzQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3JCLE9BQU87UUFDVCxDQUFDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0IsQ0FBQzs4R0FoQ1UsVUFBVTtrR0FBVixVQUFVLGlMQ1h2QixpeEJBbUJNOzsyRkRSTyxVQUFVO2tCQUx0QixTQUFTOytCQUNFLHNCQUFzQjt3REFNdEIsY0FBYztzQkFBdkIsTUFBTTtnQkFDRyxVQUFVO3NCQUFuQixNQUFNO2dCQUVFLE1BQU07c0JBQWQsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4uLy4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBDb2x1bW5GaWx0ZXJCYXNlRHRvIH0gZnJvbSBcIi4uLy4uL2R0by9maWx0ZXIvY29sdW1uLmZpbHRlci5iYXNlLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVDb2x1bW5EdG8gfSBmcm9tIFwiLi4vLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG9cIjtcbmltcG9ydCB7IFRhYmxlSTE4bkR0byB9IGZyb20gXCIuLi8uLi9kdG8vdGFibGUuaTE4bi5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS1tYXQtdGFibGUtY29sdW1uXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vY29sdW1uLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2NvbHVtbi5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENvbHVtbkNvbXB7XG5cbiAgQE91dHB1dCgpIGRyb3Bkb3duRmlsdGVyOiBFdmVudEVtaXR0ZXI8Q29sdW1uRmlsdGVyQmFzZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBzb3J0Q29sdW1uOiBFdmVudEVtaXR0ZXI8VGFibGVDb2x1bW5EdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIEBJbnB1dCgpIGNvbHVtbjogVGFibGVDb2x1bW5EdG8gPSBuZXcgVGFibGVDb2x1bW5EdG8oKTtcbiAgQElucHV0KCkgaTE4bjogVGFibGVJMThuRHRvID0gbmV3IFRhYmxlSTE4bkR0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBkb0ZpbHRlck9uQ29sdW1uKHJlcTogQ29sdW1uRmlsdGVyQmFzZUR0bykge1xuICAgIHRoaXMuZHJvcGRvd25GaWx0ZXIuZW1pdChyZXEpO1xuICB9XG5cbiAgaXNTb3J0QXNjKGNvbHVtbjogVGFibGVDb2x1bW5EdG8pOiBib29sZWFuIHtcbiAgICByZXR1cm4gY29sdW1uLnNvcnQgPT09IFNPUlQuQVNDO1xuICB9XG5cbiAgaXNTb3J0RGVzYyhjb2x1bW46IFRhYmxlQ29sdW1uRHRvKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGNvbHVtbi5zb3J0ID09PSBTT1JULkRFU0M7XG4gIH1cblxuICBkb1NvcnQoY29sdW1uOiBUYWJsZUNvbHVtbkR0byk6IHZvaWQge1xuICAgIGlmICghY29sdW1uLnNvcnRhYmxlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuc29ydENvbHVtbi5lbWl0KGNvbHVtbik7XG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cImhlYWRlci1jb250YWluZXJcIj5cbiAgICA8c3BhbiAoY2xpY2spPVwiZG9Tb3J0KGNvbHVtbilcIj57e2NvbHVtbi52YWx1ZX19PC9zcGFuPlxuXG4gICAgPHZpYS1tYXQtdGFibGUtY29sdW1uLWZpbHRlci1tdWx0IFxuICAgICAgICAqbmdJZj1cImNvbHVtbi5maWx0ZXIudHlwZT09PSdNVUxUSVBMRSdcIlxuICAgICAgICBbY29sdW1uXT1cImNvbHVtblwiXG4gICAgICAgIFtpMThuXT1cImkxOG5cIlxuICAgICAgICAoZHJvcGRvd25GaWx0ZXIpPVwiZG9GaWx0ZXJPbkNvbHVtbigkZXZlbnQpXCI+XG4gICAgPC92aWEtbWF0LXRhYmxlLWNvbHVtbi1maWx0ZXItbXVsdD5cbiAgICBcbiAgICA8dmlhLW1hdC10YWJsZS1jb2x1bW4tZmlsdGVyLXRleHQgXG4gICAgICAgICpuZ0lmPVwiY29sdW1uLmZpbHRlci50eXBlPT09J1RFWFQnXCJcbiAgICAgICAgW2NvbHVtbl09XCJjb2x1bW5cIlxuICAgICAgICBbaTE4bl09XCJpMThuXCJcbiAgICAgICAgKGRyb3Bkb3duRmlsdGVyKT1cImRvRmlsdGVyT25Db2x1bW4oJGV2ZW50KVwiPlxuICAgIDwvdmlhLW1hdC10YWJsZS1jb2x1bW4tZmlsdGVyLXRleHQ+XG5cbiAgICA8aSAqbmdJZj1cImlzU29ydEFzYyhjb2x1bW4pXCIgY2xhc3M9XCJmYXMgZmEtc29ydC1hbW91bnQtZG93blwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICA8aSAqbmdJZj1cImlzU29ydERlc2MoY29sdW1uKVwiIGNsYXNzPVwiZmFzIGZhLXNvcnQtYW1vdW50LXVwXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuPC9kaXY+Il19