import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class MatFilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe }); }
    static { this.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, name: "matFilterColumn" }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: MatFilterPipe, decorators: [{
            type: Pipe,
            args: [{ name: 'matFilterColumn' }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL21hdGVyaWFsL3BpcGUvZmlsdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUlwRCxNQUFNLE9BQU8sYUFBYTtJQUN0QixTQUFTLENBQUMsT0FBOEI7UUFDcEMsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFDLEVBQUU7WUFDNUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzhHQUxRLGFBQWE7NEdBQWIsYUFBYTs7MkZBQWIsYUFBYTtrQkFEekIsSUFBSTttQkFBQyxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFRhYmxlQ29sdW1uRHRvIH0gZnJvbSAnLi4vLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG8nO1xuXG5AUGlwZSh7IG5hbWU6ICdtYXRGaWx0ZXJDb2x1bW4nIH0pXG5leHBvcnQgY2xhc3MgTWF0RmlsdGVyUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm17XG4gICAgdHJhbnNmb3JtKGNvbHVtbnM6IEFycmF5PFRhYmxlQ29sdW1uRHRvPikge1xuICAgICAgICByZXR1cm4gY29sdW1ucy5maWx0ZXIoKGNvbHVtbik9PntcbiAgICAgICAgICAgIHJldHVybiAhY29sdW1uLmhpZGRlbjtcbiAgICAgICAgfSk7XG4gICAgfVxufSJdfQ==