import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export class CellImageComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview?.emit(imgCode);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellImageComp, selector: "via-mat-table-cell-image", inputs: { cell: "cell" }, outputs: { openImagePreview: "openImagePreview" }, ngImport: i0, template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellImageComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-image", template: "<span class=\"thumbnail-wrapper\">\n    <!-- <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"downloadFilePath + cell.value + '_thumb'\" /> -->\n        <img (click)=\"doOpenPreview(cell.value)\" width=\"60px\"\n        [src]=\"cell.value\" />\n</span>", styles: [".thumbnail-wrapper{width:60px;height:30px;overflow:hidden;cursor:pointer}\n"] }]
        }], ctorParameters: () => [], propDecorators: { openImagePreview: [{
                type: Output
            }], cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvaW1hZ2UvaW1hZ2UudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvaW1hZ2UvaW1hZ2UuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7QUFPOUQsTUFBTSxPQUFPLGFBQWE7SUFNeEI7UUFKVSxxQkFBZ0IsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUU3RCxTQUFJLEdBQWlCLElBQUksWUFBWSxFQUFFLENBQUM7SUFJakQsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBRUQsYUFBYSxDQUFDLE9BQWU7UUFDM0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QyxDQUFDOzhHQWZVLGFBQWE7a0dBQWIsYUFBYSw2SUNSMUIsd1JBS087OzJGREdNLGFBQWE7a0JBTHpCLFNBQVM7K0JBQ0UsMEJBQTBCO3dEQU0xQixnQkFBZ0I7c0JBQXpCLE1BQU07Z0JBRUUsSUFBSTtzQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2R0by90YWJsZS5jZWxsLmR0b1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLW1hdC10YWJsZS1jZWxsLWltYWdlXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vaW1hZ2UuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vaW1hZ2Uuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDZWxsSW1hZ2VDb21we1xuICBcbiAgQE91dHB1dCgpIG9wZW5JbWFnZVByZXZpZXc6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBkb09wZW5QcmV2aWV3KGltZ0NvZGU6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMub3BlbkltYWdlUHJldmlldz8uZW1pdChpbWdDb2RlKTtcbiAgfVxuXG59XG4iLCI8c3BhbiBjbGFzcz1cInRodW1ibmFpbC13cmFwcGVyXCI+XG4gICAgPCEtLSA8aW1nIChjbGljayk9XCJkb09wZW5QcmV2aWV3KGNlbGwudmFsdWUpXCIgd2lkdGg9XCI2MHB4XCJcbiAgICAgICAgW3NyY109XCJkb3dubG9hZEZpbGVQYXRoICsgY2VsbC52YWx1ZSArICdfdGh1bWInXCIgLz4gLS0+XG4gICAgICAgIDxpbWcgKGNsaWNrKT1cImRvT3BlblByZXZpZXcoY2VsbC52YWx1ZSlcIiB3aWR0aD1cIjYwcHhcIlxuICAgICAgICBbc3JjXT1cImNlbGwudmFsdWVcIiAvPlxuPC9zcGFuPiJdfQ==