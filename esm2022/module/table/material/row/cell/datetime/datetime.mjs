import { Component, Input } from "@angular/core";
import { TABLE_DATE_FORMAT } from "../../../../constant";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellDateTimeComp {
    constructor() {
        this.cell = new TableCellDto();
        this.dateFormat = TABLE_DATE_FORMAT;
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDateTimeComp, selector: "via-mat-table-cell-datetime", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>", styles: [""], dependencies: [{ kind: "pipe", type: i1.DatePipe, name: "date" }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDateTimeComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-datetime", template: "<span \n    title=\"{{cell.value  | date:dateFormat}}\"\n    >\n    {{cell.value | date:dateFormat}}\n</span>" }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvZGF0ZXRpbWUvZGF0ZXRpbWUudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvZGF0ZXRpbWUvZGF0ZXRpbWUuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7OztBQU85RCxNQUFNLE9BQU8sZ0JBQWdCO0lBTTNCO1FBSlMsU0FBSSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBRWpELGVBQVUsR0FBVyxpQkFBaUIsQ0FBQztJQUl2QyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7OEdBWFUsZ0JBQWdCO2tHQUFoQixnQkFBZ0IsNkZDVDdCLCtHQUlPOzsyRkRLTSxnQkFBZ0I7a0JBTDVCLFNBQVM7K0JBQ0UsNkJBQTZCO3dEQU05QixJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFRBQkxFX0RBVEVfRk9STUFUIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBUYWJsZUNlbGxEdG8gfSBmcm9tIFwiLi4vLi4vLi4vLi4vZHRvL3RhYmxlLmNlbGwuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtbWF0LXRhYmxlLWNlbGwtZGF0ZXRpbWVcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9kYXRldGltZS5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kYXRldGltZS5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxEYXRlVGltZUNvbXB7XG4gIFxuICBASW5wdXQoKSBjZWxsOiBUYWJsZUNlbGxEdG8gPSBuZXcgVGFibGVDZWxsRHRvKCk7XG5cbiAgZGF0ZUZvcm1hdDogc3RyaW5nID0gVEFCTEVfREFURV9GT1JNQVQ7XG4gIFxuICBjb25zdHJ1Y3RvcihcbiAgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG5cbn1cbiIsIjxzcGFuIFxuICAgIHRpdGxlPVwie3tjZWxsLnZhbHVlICB8IGRhdGU6ZGF0ZUZvcm1hdH19XCJcbiAgICA+XG4gICAge3tjZWxsLnZhbHVlIHwgZGF0ZTpkYXRlRm9ybWF0fX1cbjwvc3Bhbj4iXX0=