import { Component, Input } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellStatusComp {
    constructor() {
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellStatusComp, selector: "via-mat-table-cell-status", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellStatusComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-status", template: "<span \n    title=\"{{cell.value }}\" \n    class=\"status\" \n    [ngClass]=\"cell.value\"\n    >\n    {{cell.value}}\n</span>", styles: [".ACTION{text-align:center!important}\n"] }]
        }], ctorParameters: () => [], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdHVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL21hdGVyaWFsL3Jvdy9jZWxsL3N0YXR1cy9zdGF0dXMudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvc3RhdHVzL3N0YXR1cy5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7O0FBTzlELE1BQU0sT0FBTyxjQUFjO0lBSXpCO1FBRlMsU0FBSSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBSWpELENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs4R0FUVSxjQUFjO2tHQUFkLGNBQWMsMkZDUjNCLGlJQU1POzsyRkRFTSxjQUFjO2tCQUwxQixTQUFTOytCQUNFLDJCQUEyQjt3REFNNUIsSUFBSTtzQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBUYWJsZUNlbGxEdG8gfSBmcm9tIFwiLi4vLi4vLi4vLi4vZHRvL3RhYmxlLmNlbGwuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtbWF0LXRhYmxlLWNlbGwtc3RhdHVzXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vc3RhdHVzLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3N0YXR1cy5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxTdGF0dXNDb21we1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuXG59XG4iLCI8c3BhbiBcbiAgICB0aXRsZT1cInt7Y2VsbC52YWx1ZSB9fVwiIFxuICAgIGNsYXNzPVwic3RhdHVzXCIgXG4gICAgW25nQ2xhc3NdPVwiY2VsbC52YWx1ZVwiXG4gICAgPlxuICAgIHt7Y2VsbC52YWx1ZX19XG48L3NwYW4+Il19