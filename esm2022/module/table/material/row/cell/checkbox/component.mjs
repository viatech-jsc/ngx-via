import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import { TableRowDto } from "../../../../dto/table.row.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/material/checkbox";
export class CellCheckboxComp {
    constructor() {
        this.row = new TableRowDto();
        this.cell = new TableCellDto();
        this.rowUpdated = new EventEmitter();
        this.checked = false;
    }
    ngOnInit() {
        this.checked = this.cell.value.toLowerCase() === "true";
    }
    change() {
        this.cell.value = this.checked + "";
        this.rowUpdated.emit(this.row);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellCheckboxComp, selector: "via-mat-table-cell-checkbox", inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, ngImport: i0, template: "<span class=\"cell-checkbox-wrapper\">\n    <!-- <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\"> -->\n    <mat-checkbox \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n    </mat-checkbox>\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"], dependencies: [{ kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "component", type: i2.MatCheckbox, selector: "mat-checkbox", inputs: ["aria-label", "aria-labelledby", "aria-describedby", "id", "required", "labelPosition", "name", "value", "disableRipple", "tabIndex", "color", "checked", "disabled", "indeterminate"], outputs: ["change", "indeterminateChange"], exportAs: ["matCheckbox"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellCheckboxComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-checkbox", template: "<span class=\"cell-checkbox-wrapper\">\n    <!-- <input \n        type=\"checkbox\" \n        [attr.value]=\"cell.value\" \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\"> -->\n    <mat-checkbox \n        [(ngModel)]=\"checked\"\n        (change)=\"change()\">\n    </mat-checkbox>\n</span>", styles: [".cell-checkbox-wrapper{padding:3px}.cell-checkbox-wrapper input[type=checkbox]{width:18px;height:18px}\n"] }]
        }], ctorParameters: () => [], propDecorators: { row: [{
                type: Input
            }], cell: [{
                type: Input
            }], rowUpdated: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL3RhYmxlL21hdGVyaWFsL3Jvdy9jZWxsL2NoZWNrYm94L2NvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9tYXRlcmlhbC9yb3cvY2VsbC9jaGVja2JveC9pbmRleC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7OztBQU81RCxNQUFNLE9BQU8sZ0JBQWdCO0lBUzNCO1FBUFMsUUFBRyxHQUFnQixJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLFNBQUksR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV2QyxlQUFVLEdBQThCLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckUsWUFBTyxHQUFZLEtBQUssQ0FBQztJQUl6QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssTUFBTSxDQUFDO0lBQzFELENBQUM7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7OEdBcEJVLGdCQUFnQjtrR0FBaEIsZ0JBQWdCLGdKQ1Q3QixvVEFVTzs7MkZERE0sZ0JBQWdCO2tCQUw1QixTQUFTOytCQUNFLDZCQUE2Qjt3REFNOUIsR0FBRztzQkFBWCxLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFFSSxVQUFVO3NCQUFuQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2R0by90YWJsZS5jZWxsLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVSb3dEdG8gfSBmcm9tIFwiLi4vLi4vLi4vLi4vZHRvL3RhYmxlLnJvdy5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS1tYXQtdGFibGUtY2VsbC1jaGVja2JveFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2luZGV4Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3N0eWxlcy5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxDaGVja2JveENvbXB7XG4gIFxuICBASW5wdXQoKSByb3c6IFRhYmxlUm93RHRvID0gbmV3IFRhYmxlUm93RHRvKCk7XG4gIEBJbnB1dCgpIGNlbGw6IFRhYmxlQ2VsbER0byA9IG5ldyBUYWJsZUNlbGxEdG8oKTtcbiAgXG4gIEBPdXRwdXQoKSByb3dVcGRhdGVkOiBFdmVudEVtaXR0ZXI8VGFibGVSb3dEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBcbiAgY2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5jaGVja2VkID0gdGhpcy5jZWxsLnZhbHVlLnRvTG93ZXJDYXNlKCkgPT09IFwidHJ1ZVwiO1xuICB9XG5cbiAgY2hhbmdlKCkge1xuICAgIHRoaXMuY2VsbC52YWx1ZSA9IHRoaXMuY2hlY2tlZCArIFwiXCI7XG4gICAgdGhpcy5yb3dVcGRhdGVkLmVtaXQodGhpcy5yb3cpO1xuICB9XG5cbn1cbiIsIjxzcGFuIGNsYXNzPVwiY2VsbC1jaGVja2JveC13cmFwcGVyXCI+XG4gICAgPCEtLSA8aW5wdXQgXG4gICAgICAgIHR5cGU9XCJjaGVja2JveFwiIFxuICAgICAgICBbYXR0ci52YWx1ZV09XCJjZWxsLnZhbHVlXCIgXG4gICAgICAgIFsobmdNb2RlbCldPVwiY2hlY2tlZFwiXG4gICAgICAgIChjaGFuZ2UpPVwiY2hhbmdlKClcIj4gLS0+XG4gICAgPG1hdC1jaGVja2JveCBcbiAgICAgICAgWyhuZ01vZGVsKV09XCJjaGVja2VkXCJcbiAgICAgICAgKGNoYW5nZSk9XCJjaGFuZ2UoKVwiPlxuICAgIDwvbWF0LWNoZWNrYm94PlxuPC9zcGFuPiJdfQ==