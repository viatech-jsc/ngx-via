import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { TableCellDto } from "../../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common";
export class CellDefaultComp {
    constructor(router) {
        this.router = router;
        this.cell = new TableCellDto();
    }
    ngOnInit() {
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, deps: [{ token: i1.Router }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CellDefaultComp, selector: "via-mat-table-cell-default", inputs: { cell: "cell" }, ngImport: i0, template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"], dependencies: [{ kind: "directive", type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CellDefaultComp, decorators: [{
            type: Component,
            args: [{ selector: "via-mat-table-cell-default", template: "<span \n    title=\"{{cell.value}}\" \n    (click)=\"cellClicked(cell)\" \n    [ngClass]=\"cell.routeLink.trim().length > 0 ? 'hand' : ''\"\n    >\n    {{cell.value}}\n</span>", styles: [".hand{cursor:pointer}\n"] }]
        }], ctorParameters: () => [{ type: i1.Router }], propDecorators: { cell: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90YWJsZS9tYXRlcmlhbC9yb3cvY2VsbC9kZWZhdWx0L2RlZmF1bHQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGFibGUvbWF0ZXJpYWwvcm93L2NlbGwvZGVmYXVsdC9kZWZhdWx0Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7OztBQU85RCxNQUFNLE9BQU8sZUFBZTtJQUkxQixZQUNVLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBSGYsU0FBSSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBS2pELENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFrQjtRQUM1QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDekMsQ0FBQztJQUNILENBQUM7OEdBaEJVLGVBQWU7a0dBQWYsZUFBZSw0RkNUNUIsaUxBTU87OzJGREdNLGVBQWU7a0JBTDNCLFNBQVM7K0JBQ0UsNEJBQTRCOzJFQU03QixJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS1tYXQtdGFibGUtY2VsbC1kZWZhdWx0XCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZGVmYXVsdC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kZWZhdWx0LnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ2VsbERlZmF1bHRDb21we1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvID0gbmV3IFRhYmxlQ2VsbER0bygpO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgY2VsbENsaWNrZWQoY2VsbDogVGFibGVDZWxsRHRvKTogdm9pZCB7XG4gICAgaWYgKGNlbGwucm91dGVMaW5rLnRyaW0oKS5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbY2VsbC5yb3V0ZUxpbmtdKTtcbiAgICB9XG4gIH1cbn1cbiIsIjxzcGFuIFxuICAgIHRpdGxlPVwie3tjZWxsLnZhbHVlfX1cIiBcbiAgICAoY2xpY2spPVwiY2VsbENsaWNrZWQoY2VsbClcIiBcbiAgICBbbmdDbGFzc109XCJjZWxsLnJvdXRlTGluay50cmltKCkubGVuZ3RoID4gMCA/ICdoYW5kJyA6ICcnXCJcbiAgICA+XG4gICAge3tjZWxsLnZhbHVlfX1cbjwvc3Bhbj4iXX0=