import { Injectable } from '@angular/core';
import { NavigationEnd } from '@angular/router'; // import Router and NavigationEnd
import * as i0 from "@angular/core";
export class GoogleAnalyticsService {
    //************************
    // Import this to app.component.ts
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //     GoogleAnalyticsService.subscribeRouterEvents(event);
    //   });
    //************************
    constructor() { }
    // /**
    //   * Emit google analytics event
    //   * Fire event example:
    //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
    //   * param {string} eventCategory
    //   * param {string} eventAction
    //   * param {string} eventLabel
    //   * param {number} eventValue
    //   */
    eventEmitter(eventCategory, eventAction, eventLabel = undefined, eventValue = undefined) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventLabel: eventLabel,
            eventAction: eventAction,
            eventValue: eventValue
        });
    }
    subscribeRouterEvents(event) {
        if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
        }
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: GoogleAnalyticsService, decorators: [{
            type: Injectable
        }], ctorParameters: () => [] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLmFuYWx5dGljcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL2dvb2dsZV9hbmFseXRpY3MvZ29vZ2xlLmFuYWx5dGljcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGlCQUFpQixDQUFDLENBQUMsa0NBQWtDOztBQUluRixNQUFNLE9BQU8sc0JBQXNCO0lBRy9CLDBCQUEwQjtJQUMxQixrQ0FBa0M7SUFDbEMscUVBQXFFO0lBQ3JFLDBDQUEwQztJQUMxQywyREFBMkQ7SUFDM0QsUUFBUTtJQUNSLDBCQUEwQjtJQUMxQixnQkFBZ0IsQ0FBQztJQUVqQixNQUFNO0lBQ04sa0NBQWtDO0lBQ2xDLDBCQUEwQjtJQUMxQixxRUFBcUU7SUFDckUsbUNBQW1DO0lBQ25DLGlDQUFpQztJQUNqQyxnQ0FBZ0M7SUFDaEMsZ0NBQWdDO0lBQ2hDLE9BQU87SUFDQSxZQUFZLENBQUMsYUFBcUIsRUFBRSxXQUFtQixFQUFFLGFBQWlDLFNBQVMsRUFBRSxhQUFpQyxTQUFTO1FBQ2xKLEVBQUUsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFO1lBQ2hCLGFBQWEsRUFBRSxhQUFhO1lBQzVCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLFdBQVcsRUFBRSxXQUFXO1lBQ3hCLFVBQVUsRUFBRSxVQUFVO1NBQ3pCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxLQUFVO1FBQzVCLElBQUksS0FBSyxZQUFZLGFBQWEsRUFBRSxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzNDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDM0IsQ0FBQztJQUNMLENBQUM7OEdBbkNRLHNCQUFzQjtrSEFBdEIsc0JBQXNCOzsyRkFBdEIsc0JBQXNCO2tCQURsQyxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2aWdhdGlvbkVuZCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7IC8vIGltcG9ydCBSb3V0ZXIgYW5kIE5hdmlnYXRpb25FbmRcbmRlY2xhcmUgbGV0IGdhOiBGdW5jdGlvbjsgLy8gRGVjbGFyZSBnYSBhcyBhIGZ1bmN0aW9uXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBHb29nbGVBbmFseXRpY3NTZXJ2aWNlIHtcblxuXG4gICAgLy8qKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAvLyBJbXBvcnQgdGhpcyB0byBhcHAuY29tcG9uZW50LnRzXG4gICAgLy8gc3Vic2NyaWJlIHRvIHJvdXRlciBldmVudHMgYW5kIHNlbmQgcGFnZSB2aWV3cyB0byBHb29nbGUgQW5hbHl0aWNzXG4gICAgLy8gdGhpcy5yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgLy8gICAgIEdvb2dsZUFuYWx5dGljc1NlcnZpY2Uuc3Vic2NyaWJlUm91dGVyRXZlbnRzKGV2ZW50KTtcbiAgICAvLyAgIH0pO1xuICAgIC8vKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIC8vIC8qKlxuICAgIC8vICAgKiBFbWl0IGdvb2dsZSBhbmFseXRpY3MgZXZlbnRcbiAgICAvLyAgICogRmlyZSBldmVudCBleGFtcGxlOlxuICAgIC8vICAgKiB0aGlzLmVtaXRFdmVudChcInRlc3RDYXRlZ29yeVwiLCBcInRlc3RBY3Rpb25cIiwgXCJ0ZXN0TGFiZWxcIiwgMTApO1xuICAgIC8vICAgKiBwYXJhbSB7c3RyaW5nfSBldmVudENhdGVnb3J5XG4gICAgLy8gICAqIHBhcmFtIHtzdHJpbmd9IGV2ZW50QWN0aW9uXG4gICAgLy8gICAqIHBhcmFtIHtzdHJpbmd9IGV2ZW50TGFiZWxcbiAgICAvLyAgICogcGFyYW0ge251bWJlcn0gZXZlbnRWYWx1ZVxuICAgIC8vICAgKi9cbiAgICBwdWJsaWMgZXZlbnRFbWl0dGVyKGV2ZW50Q2F0ZWdvcnk6IHN0cmluZywgZXZlbnRBY3Rpb246IHN0cmluZywgZXZlbnRMYWJlbDogc3RyaW5nIHwgdW5kZWZpbmVkID0gdW5kZWZpbmVkLCBldmVudFZhbHVlOiBudW1iZXIgfCB1bmRlZmluZWQgPSB1bmRlZmluZWQpIHtcbiAgICAgICAgZ2EoJ3NlbmQnLCAnZXZlbnQnLCB7XG4gICAgICAgICAgICBldmVudENhdGVnb3J5OiBldmVudENhdGVnb3J5LFxuICAgICAgICAgICAgZXZlbnRMYWJlbDogZXZlbnRMYWJlbCxcbiAgICAgICAgICAgIGV2ZW50QWN0aW9uOiBldmVudEFjdGlvbixcbiAgICAgICAgICAgIGV2ZW50VmFsdWU6IGV2ZW50VmFsdWVcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3Vic2NyaWJlUm91dGVyRXZlbnRzKGV2ZW50OiBhbnkpIHtcbiAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkge1xuICAgICAgICAgICAgZ2EoJ3NldCcsICdwYWdlJywgZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHMpO1xuICAgICAgICAgICAgZ2EoJ3NlbmQnLCAncGFnZXZpZXcnKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==