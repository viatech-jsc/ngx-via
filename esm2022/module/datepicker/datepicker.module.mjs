import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbDatepickerI18n, NgbDateAdapter, NgbDateParserFormatter } from "@ng-bootstrap/ng-bootstrap";
import { DateAdapter } from './date.adapter';
import { DateParserFormatter } from './date.parser.formatter';
import { DatepickerComponent } from './datepicker';
import { DatepickerI18n } from './datepicker.i18n';
import { I18nService } from './i18n.service';
import * as i0 from "@angular/core";
export class DatepickerModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, declarations: [DatepickerComponent], imports: [CommonModule,
            ReactiveFormsModule,
            NgbDatepickerModule], exports: [DatepickerComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, providers: [
            I18nService,
            { provide: NgbDatepickerI18n, useClass: DatepickerI18n },
            { provide: NgbDateAdapter, useClass: DateAdapter },
            { provide: NgbDateParserFormatter, useClass: DateParserFormatter }
        ], imports: [CommonModule,
            ReactiveFormsModule,
            NgbDatepickerModule] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [DatepickerComponent],
                    imports: [
                        CommonModule,
                        ReactiveFormsModule,
                        NgbDatepickerModule,
                    ],
                    exports: [
                        DatepickerComponent
                    ],
                    providers: [
                        I18nService,
                        { provide: NgbDatepickerI18n, useClass: DatepickerI18n },
                        { provide: NgbDateAdapter, useClass: DateAdapter },
                        { provide: NgbDateParserFormatter, useClass: DateParserFormatter }
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvZGF0ZXBpY2tlci9kYXRlcGlja2VyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUgsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUNuRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOztBQW1CN0MsTUFBTSxPQUFPLGdCQUFnQjs4R0FBaEIsZ0JBQWdCOytHQUFoQixnQkFBZ0IsaUJBaEJaLG1CQUFtQixhQUVoQyxZQUFZO1lBQ1osbUJBQW1CO1lBQ25CLG1CQUFtQixhQUduQixtQkFBbUI7K0dBU1YsZ0JBQWdCLGFBUGhCO1lBQ1QsV0FBVztZQUNYLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxjQUFjLEVBQUU7WUFDeEQsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUU7WUFDbEQsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFO1NBQ25FLFlBWkMsWUFBWTtZQUNaLG1CQUFtQjtZQUNuQixtQkFBbUI7OzJGQVlWLGdCQUFnQjtrQkFqQjVCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsbUJBQW1CLENBQUM7b0JBQ25DLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLG1CQUFtQjt3QkFDbkIsbUJBQW1CO3FCQUNwQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsbUJBQW1CO3FCQUNwQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1QsV0FBVzt3QkFDWCxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFO3dCQUN4RCxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRTt3QkFDbEQsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFO3FCQUNuRTtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE5nYkRhdGVwaWNrZXJNb2R1bGUsIE5nYkRhdGVwaWNrZXJJMThuLCBOZ2JEYXRlQWRhcHRlciwgTmdiRGF0ZVBhcnNlckZvcm1hdHRlciB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIgfSBmcm9tICcuL2RhdGUuYWRhcHRlcic7XG5pbXBvcnQgeyBEYXRlUGFyc2VyRm9ybWF0dGVyIH0gZnJvbSAnLi9kYXRlLnBhcnNlci5mb3JtYXR0ZXInO1xuaW1wb3J0IHsgRGF0ZXBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4vZGF0ZXBpY2tlcic7XG5pbXBvcnQgeyBEYXRlcGlja2VySTE4biB9IGZyb20gJy4vZGF0ZXBpY2tlci5pMThuJztcbmltcG9ydCB7IEkxOG5TZXJ2aWNlIH0gZnJvbSAnLi9pMThuLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtEYXRlcGlja2VyQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIE5nYkRhdGVwaWNrZXJNb2R1bGUsXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBEYXRlcGlja2VyQ29tcG9uZW50XG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIEkxOG5TZXJ2aWNlLFxuICAgIHsgcHJvdmlkZTogTmdiRGF0ZXBpY2tlckkxOG4sIHVzZUNsYXNzOiBEYXRlcGlja2VySTE4biB9LFxuICAgIHsgcHJvdmlkZTogTmdiRGF0ZUFkYXB0ZXIsIHVzZUNsYXNzOiBEYXRlQWRhcHRlciB9LFxuICAgIHsgcHJvdmlkZTogTmdiRGF0ZVBhcnNlckZvcm1hdHRlciwgdXNlQ2xhc3M6IERhdGVQYXJzZXJGb3JtYXR0ZXIgfVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERhdGVwaWNrZXJNb2R1bGUgeyB9XG4iXX0=