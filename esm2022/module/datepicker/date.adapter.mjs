import { Injectable } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import * as i0 from "@angular/core";
export class DateAdapter extends NgbDateAdapter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '-';
    }
    fromModel(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    toModel(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter, deps: null, target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DateAdapter, decorators: [{
            type: Injectable
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS5hZGFwdGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL2RhdGVwaWNrZXIvZGF0ZS5hZGFwdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGNBQWMsRUFBaUIsTUFBTSw0QkFBNEIsQ0FBQzs7QUFHM0UsTUFBTSxPQUFPLFdBQVksU0FBUSxjQUFzQjtJQUR2RDs7UUFHVyxjQUFTLEdBQVcsR0FBRyxDQUFDO0tBaUJsQztJQWZDLFNBQVMsQ0FBQyxLQUFvQjtRQUM1QixJQUFJLEtBQUssRUFBRSxDQUFDO1lBQ1YsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkMsT0FBTztnQkFDTCxHQUFHLEVBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzNCLEtBQUssRUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDN0IsSUFBSSxFQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDO2FBQzdCLENBQUM7UUFDSixDQUFDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsT0FBTyxDQUFDLElBQTBCO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMzRixDQUFDOzhHQWxCVSxXQUFXO2tIQUFYLFdBQVc7OzJGQUFYLFdBQVc7a0JBRHZCLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdiRGF0ZUFkYXB0ZXIsIE5nYkRhdGVTdHJ1Y3QgfSBmcm9tICdAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEYXRlQWRhcHRlciBleHRlbmRzIE5nYkRhdGVBZGFwdGVyPHN0cmluZz4ge1xuXG4gIHJlYWRvbmx5IERFTElNSVRFUjogc3RyaW5nID0gJy0nO1xuXG4gIGZyb21Nb2RlbCh2YWx1ZTogc3RyaW5nIHwgbnVsbCk6IE5nYkRhdGVTdHJ1Y3QgfCBudWxsIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIGxldCBkYXRlID0gdmFsdWUuc3BsaXQodGhpcy5ERUxJTUlURVIpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZGF5IDogcGFyc2VJbnQoZGF0ZVswXSwgMTApLFxuICAgICAgICBtb250aCA6IHBhcnNlSW50KGRhdGVbMV0sIDEwKSxcbiAgICAgICAgeWVhciA6IHBhcnNlSW50KGRhdGVbMl0sIDEwKVxuICAgICAgfTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB0b01vZGVsKGRhdGU6IE5nYkRhdGVTdHJ1Y3QgfCBudWxsKTogc3RyaW5nIHwgbnVsbCB7XG4gICAgcmV0dXJuIGRhdGUgPyBkYXRlLmRheSArIHRoaXMuREVMSU1JVEVSICsgZGF0ZS5tb250aCArIHRoaXMuREVMSU1JVEVSICsgZGF0ZS55ZWFyIDogbnVsbDtcbiAgfVxufSJdfQ==