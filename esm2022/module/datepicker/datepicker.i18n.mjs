import { Injectable } from "@angular/core";
import { I18N_VALUES } from "./constant";
import { NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { I18nService } from "./i18n.service";
import * as i0 from "@angular/core";
import * as i1 from "./i18n.service";
export class DatepickerI18n extends NgbDatepickerI18n {
    getWeekdayLabel(weekday, width) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    constructor(_i18n) {
        super();
        this._i18n = _i18n;
    }
    getWeekdayShortName(weekday) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month) {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month) {
        return this.getMonthShortName(month);
    }
    getDayAriaLabel(date) {
        return `${date.day}-${date.month}-${date.year}`;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n, deps: [{ token: i1.I18nService }], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerI18n, decorators: [{
            type: Injectable
        }], ctorParameters: () => [{ type: i1.I18nService }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5pMThuLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmd4LXZpYS9zcmMvbW9kdWxlL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci5pMThuLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN6QyxPQUFPLEVBQWlCLGlCQUFpQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDOUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7QUFJN0MsTUFBTSxPQUFPLGNBQWUsU0FBUSxpQkFBaUI7SUFDeEMsZUFBZSxDQUFDLE9BQWUsRUFBRSxLQUFvQztRQUMxRSxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVELFlBQW9CLEtBQWtCO1FBQ2xDLEtBQUssRUFBRSxDQUFDO1FBRFEsVUFBSyxHQUFMLEtBQUssQ0FBYTtJQUV0QyxDQUFDO0lBRUQsbUJBQW1CLENBQUMsT0FBZTtRQUMvQixPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUNELGlCQUFpQixDQUFDLEtBQWE7UUFDM0IsT0FBTyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxLQUFhO1FBQzFCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxlQUFlLENBQUMsSUFBbUI7UUFDL0IsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDcEQsQ0FBQzs4R0F0QlEsY0FBYztrSEFBZCxjQUFjOzsyRkFBZCxjQUFjO2tCQUQxQixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBJMThOX1ZBTFVFUyB9IGZyb20gXCIuL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBOZ2JEYXRlU3RydWN0LCBOZ2JEYXRlcGlja2VySTE4biB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHsgSTE4blNlcnZpY2UgfSBmcm9tIFwiLi9pMThuLnNlcnZpY2VcIjtcbmltcG9ydCB7IFRyYW5zbGF0aW9uV2lkdGggfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEYXRlcGlja2VySTE4biBleHRlbmRzIE5nYkRhdGVwaWNrZXJJMThuIHtcbiAgICBvdmVycmlkZSBnZXRXZWVrZGF5TGFiZWwod2Vla2RheTogbnVtYmVyLCB3aWR0aD86IFRyYW5zbGF0aW9uV2lkdGggfCB1bmRlZmluZWQpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gSTE4Tl9WQUxVRVNbdGhpcy5faTE4bi5sYW5ndWFnZV0ud2Vla2RheXNbd2Vla2RheSAtIDFdO1xuICAgIH1cblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2kxOG46IEkxOG5TZXJ2aWNlKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgZ2V0V2Vla2RheVNob3J0TmFtZSh3ZWVrZGF5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gSTE4Tl9WQUxVRVNbdGhpcy5faTE4bi5sYW5ndWFnZV0ud2Vla2RheXNbd2Vla2RheSAtIDFdO1xuICAgIH1cbiAgICBnZXRNb250aFNob3J0TmFtZShtb250aDogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIEkxOE5fVkFMVUVTW3RoaXMuX2kxOG4ubGFuZ3VhZ2VdLm1vbnRoc1ttb250aCAtIDFdO1xuICAgIH1cblxuICAgIGdldE1vbnRoRnVsbE5hbWUobW9udGg6IG51bWJlcik6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldE1vbnRoU2hvcnROYW1lKG1vbnRoKTtcbiAgICB9XG5cbiAgICBnZXREYXlBcmlhTGFiZWwoZGF0ZTogTmdiRGF0ZVN0cnVjdCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiBgJHtkYXRlLmRheX0tJHtkYXRlLm1vbnRofS0ke2RhdGUueWVhcn1gO1xuICAgIH1cbn0iXX0=