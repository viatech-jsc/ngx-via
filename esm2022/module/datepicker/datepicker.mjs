import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import { NgbDateParserFormatter, NgbDate, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import random from "lodash/random";
import { FormUtils } from "../../utils/form.utils";
import { DATE_FORMAT_WITHOUT_TIME } from "./constant";
import { DatepickerI18nDto } from "./dto/datepicker.i18n.dto";
import { I18nService } from "./i18n.service";
import * as i0 from "@angular/core";
import * as i1 from "./i18n.service";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
import * as i3 from "@angular/common";
export class DatepickerComponent {
    constructor(_i18n, formatter, calendar) {
        this._i18n = _i18n;
        this.formatter = formatter;
        this.calendar = calendar;
        this.today = new Date();
        this.datepickerItemChange = new EventEmitter();
        this.dateFormat = undefined;
        this.datepickerItem = undefined;
        this.maxDate = undefined;
        this.minDate = undefined;
        this.autoClose = true;
        this.disabled = false;
        this.readOnly = false;
        this.placeholder = "dd/MM/yyy";
        this.pickerImg = undefined;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.datepickerId = "";
        this.datepickerId = "datepicker" + random();
        if (this.dateFormat) {
            this.dateFormat = DATE_FORMAT_WITHOUT_TIME;
        }
        if (this.minDate) {
            this.minDate = {
                day: 1,
                month: 1,
                year: this.today.getFullYear() - 100
            };
        }
        this._i18n.language = "en";
        this.setLanguage("en");
    }
    ngOnInit() {
    }
    formControlInstance() {
        return this.viaControl;
    }
    selected(date) {
        this.datepickerItem = date;
        this.viaControl.setValue(date);
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    setLanguage(language) {
        this._i18n.language = language;
        // this.placeholder = this._i18n.language == "en"? "dd/mm/yyyy": "nn/tt/nnnn";
    }
    getDateSelected() {
        if (this.datepickerItem) {
            return new Date(this.datepickerItem.year, this.datepickerItem.month - 1, this.datepickerItem.day);
        }
        return undefined;
    }
    dateSelect(date) {
        this.viaControl.setValue(date);
        this.datepickerItemChange?.emit(date);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    validateInput(input) {
        const parsed = this.formatter.parse(input);
        let value = undefined;
        if (parsed == undefined || parsed == null || (parsed && this.calendar.isValid(NgbDate.from(parsed)))) {
            let v = NgbDate.from(parsed);
            if (v) {
                value = v;
            }
        }
        this.dateSelect(value);
        return value;
    }
    format(datepickerItem) {
        return this.formatter.format(datepickerItem ? datepickerItem : null);
    }
    checkError(date) {
        const parsed = this.formatter.parse(date);
        let isValid = false;
        if (parsed && this.calendar.isValid(NgbDate.from(parsed))) {
            isValid = true;
        }
        return isValid;
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerComponent, deps: [{ token: i1.I18nService }, { token: i2.NgbDateParserFormatter }, { token: i2.NgbCalendar }], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: DatepickerComponent, selector: "via-datepicker", inputs: { dateFormat: "dateFormat", datepickerItem: "datepickerItem", maxDate: "maxDate", minDate: "minDate", autoClose: "autoClose", disabled: "disabled", readOnly: "readOnly", placeholder: "placeholder", pickerImg: "pickerImg", viaControl: "viaControl", i18n: "i18n" }, outputs: { datepickerItemChange: "datepickerItemChange" }, ngImport: i0, template: "<div class=\"via-datepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"datepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"input-wrapper\">\n        <input \n            [attr.id]=\"datepickerId\"\n            class=\"form-control\" \n            [placeholder]=\"placeholder\" \n            [startDate]=\"$any(datepickerItem)\" \n            #datePickerIpt \n            name=\"datePickerIpt\" \n            [value]=\"format(datepickerItem)\" \n            (input)=\"datepickerItem = validateInput(datePickerIpt.value)\" \n            ngbDatepicker \n            #d=\"ngbDatepicker\" \n            [minDate]=\"$any(minDate)\" \n            [maxDate]=\"$any(maxDate)\"\n            (dateSelect)=\"dateSelect($event)\"\n            [autoClose]=\"autoClose\"\n            [attr.disabled]=\"disabled\"\n            [disabled]=\"disabled\"\n            [readOnly]=\"readOnly\" />\n\n        <img *ngIf=\"pickerImg\" (click)=\"d.toggle()\" class=\"date-icon\" [src]=\"pickerImg\" />\n        <i *ngIf=\"!pickerImg\" (click)=\"d.toggle()\" class=\"far fa-calendar date-icon\" aria-hidden=\"true\"></i>\n    </div>\n</div>", styles: [".via-datepicker{width:100%;border-radius:3px;display:flex;flex-direction:column;position:relative;cursor:pointer}.via-datepicker .danger-text{color:brown}.via-datepicker .input-wrapper{position:relative}.via-datepicker .input-wrapper .form-control{min-height:46px}.via-datepicker .input-wrapper .date-icon{position:absolute;right:10px;top:13px;width:20px}.via-datepicker .input-wrapper .disabled-background{background:#f7f8f9}.via-datepicker .input-wrapper .datepicker-container{display:flex;list-style:none;margin:0;flex:1;color:#999;padding:5px 10px}.via-datepicker .input-wrapper .datepicker-container li{margin:0}.via-datepicker .input-wrapper .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}.via-datepicker .input-wrapper .datepicker-container .datepicker img{width:20px;margin-right:5px}.via-datepicker .input-wrapper .datepicker-container .datepicker span{flex:8}.via-datepicker .input-wrapper .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}.via-datepicker .input-wrapper .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0;border:0}\n"], dependencies: [{ kind: "directive", type: i3.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i2.NgbInputDatepicker, selector: "input[ngbDatepicker]", inputs: ["autoClose", "contentTemplate", "datepickerClass", "dayTemplate", "dayTemplateData", "displayMonths", "firstDayOfWeek", "footerTemplate", "markDisabled", "minDate", "maxDate", "navigation", "outsideDays", "placement", "popperOptions", "restoreFocus", "showWeekNumbers", "startDate", "container", "positionTarget", "weekdays", "disabled"], outputs: ["dateSelect", "navigate", "closed"], exportAs: ["ngbDatepicker"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: DatepickerComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-datepicker", template: "<div class=\"via-datepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"datepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <div class=\"input-wrapper\">\n        <input \n            [attr.id]=\"datepickerId\"\n            class=\"form-control\" \n            [placeholder]=\"placeholder\" \n            [startDate]=\"$any(datepickerItem)\" \n            #datePickerIpt \n            name=\"datePickerIpt\" \n            [value]=\"format(datepickerItem)\" \n            (input)=\"datepickerItem = validateInput(datePickerIpt.value)\" \n            ngbDatepicker \n            #d=\"ngbDatepicker\" \n            [minDate]=\"$any(minDate)\" \n            [maxDate]=\"$any(maxDate)\"\n            (dateSelect)=\"dateSelect($event)\"\n            [autoClose]=\"autoClose\"\n            [attr.disabled]=\"disabled\"\n            [disabled]=\"disabled\"\n            [readOnly]=\"readOnly\" />\n\n        <img *ngIf=\"pickerImg\" (click)=\"d.toggle()\" class=\"date-icon\" [src]=\"pickerImg\" />\n        <i *ngIf=\"!pickerImg\" (click)=\"d.toggle()\" class=\"far fa-calendar date-icon\" aria-hidden=\"true\"></i>\n    </div>\n</div>", styles: [".via-datepicker{width:100%;border-radius:3px;display:flex;flex-direction:column;position:relative;cursor:pointer}.via-datepicker .danger-text{color:brown}.via-datepicker .input-wrapper{position:relative}.via-datepicker .input-wrapper .form-control{min-height:46px}.via-datepicker .input-wrapper .date-icon{position:absolute;right:10px;top:13px;width:20px}.via-datepicker .input-wrapper .disabled-background{background:#f7f8f9}.via-datepicker .input-wrapper .datepicker-container{display:flex;list-style:none;margin:0;flex:1;color:#999;padding:5px 10px}.via-datepicker .input-wrapper .datepicker-container li{margin:0}.via-datepicker .input-wrapper .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}.via-datepicker .input-wrapper .datepicker-container .datepicker img{width:20px;margin-right:5px}.via-datepicker .input-wrapper .datepicker-container .datepicker span{flex:8}.via-datepicker .input-wrapper .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}.via-datepicker .input-wrapper .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0;border:0}\n"] }]
        }], ctorParameters: () => [{ type: i1.I18nService }, { type: i2.NgbDateParserFormatter }, { type: i2.NgbCalendar }], propDecorators: { datepickerItemChange: [{
                type: Output
            }], dateFormat: [{
                type: Input
            }], datepickerItem: [{
                type: Input
            }], maxDate: [{
                type: Input
            }], minDate: [{
                type: Input
            }], autoClose: [{
                type: Input
            }], disabled: [{
                type: Input
            }], readOnly: [{
                type: Input
            }], placeholder: [{
                type: Input
            }], pickerImg: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS9kYXRlcGlja2VyL2RhdGVwaWNrZXIudHMiLCIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvZGF0ZXBpY2tlci9kYXRlcGlja2VyLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFpQixzQkFBc0IsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDekcsT0FBTyxNQUFNLE1BQU0sZUFBZSxDQUFDO0FBQ25DLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDdEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7OztBQU83QyxNQUFNLE9BQU8sbUJBQW1CO0lBbUI5QixZQUNTLEtBQWtCLEVBQ2xCLFNBQWlDLEVBQ2hDLFFBQXFCO1FBRnRCLFVBQUssR0FBTCxLQUFLLENBQWE7UUFDbEIsY0FBUyxHQUFULFNBQVMsQ0FBd0I7UUFDaEMsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQXJCL0IsVUFBSyxHQUFTLElBQUksSUFBSSxFQUFFLENBQUM7UUFFZix5QkFBb0IsR0FBZ0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV4RSxlQUFVLEdBQVksU0FBUyxDQUFDO1FBQ2hDLG1CQUFjLEdBQW1CLFNBQVMsQ0FBQztRQUMzQyxZQUFPLEdBQW1CLFNBQVMsQ0FBQztRQUNwQyxZQUFPLEdBQW1CLFNBQVMsQ0FBQztRQUNwQyxjQUFTLEdBQW1DLElBQUksQ0FBQztRQUNqRCxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFDMUIsZ0JBQVcsR0FBVyxXQUFXLENBQUM7UUFDbEMsY0FBUyxHQUFZLFNBQVMsQ0FBQztRQUMvQixlQUFVLEdBQWdCLElBQUksV0FBVyxFQUFFLENBQUM7UUFDNUMsU0FBSSxHQUF1QixTQUFTLENBQUM7UUFFOUMsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFPeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFDNUMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyx3QkFBd0IsQ0FBQztRQUM3QyxDQUFDO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRztnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixLQUFLLEVBQUUsQ0FBQztnQkFDUixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxHQUFHO2FBQ3JDLENBQUM7UUFDSixDQUFDO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN6QixDQUFDO0lBRUQsUUFBUSxDQUFDLElBQW1CO1FBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxRQUFRLENBQUMsSUFBa0I7UUFDekIsT0FBTyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxXQUFXLENBQUMsUUFBZ0I7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQy9CLDhFQUE4RTtJQUNoRixDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3hCLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEcsQ0FBQztRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ25CLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBK0I7UUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsT0FBTyxDQUFDLEdBQVE7UUFDZCxPQUFPLEdBQUcsS0FBSyxJQUFJLElBQUksR0FBRyxLQUFLLFNBQVMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsYUFBYSxDQUFDLEtBQWE7UUFDekIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0MsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLElBQUksTUFBTSxJQUFJLFNBQVMsSUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDckcsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNOLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDWixDQUFDO1FBQ0gsQ0FBQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQXlDO1FBQzlDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBWTtRQUNyQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQyxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUM7WUFDekQsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNqQixDQUFDO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQzs4R0FwR1UsbUJBQW1CO2tHQUFuQixtQkFBbUIsaVlDZGhDLGlzQ0E0Qk07OzJGRGRPLG1CQUFtQjtrQkFML0IsU0FBUzsrQkFDRSxnQkFBZ0I7K0lBT2hCLG9CQUFvQjtzQkFBN0IsTUFBTTtnQkFFRSxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLGNBQWM7c0JBQXRCLEtBQUs7Z0JBQ0csT0FBTztzQkFBZixLQUFLO2dCQUNHLE9BQU87c0JBQWYsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxXQUFXO3NCQUFuQixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csVUFBVTtzQkFBbEIsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTmdiRGF0ZVN0cnVjdCwgTmdiRGF0ZVBhcnNlckZvcm1hdHRlciwgTmdiRGF0ZSwgTmdiQ2FsZW5kYXIgfSBmcm9tIFwiQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXBcIjtcbmltcG9ydCByYW5kb20gZnJvbSBcImxvZGFzaC9yYW5kb21cIjtcbmltcG9ydCB7IEZvcm1VdGlscyB9IGZyb20gXCIuLi8uLi91dGlscy9mb3JtLnV0aWxzXCI7XG5pbXBvcnQgeyBEQVRFX0ZPUk1BVF9XSVRIT1VUX1RJTUUgfSBmcm9tIFwiLi9jb25zdGFudFwiO1xuaW1wb3J0IHsgRGF0ZXBpY2tlckkxOG5EdG8gfSBmcm9tIFwiLi9kdG8vZGF0ZXBpY2tlci5pMThuLmR0b1wiO1xuaW1wb3J0IHsgSTE4blNlcnZpY2UgfSBmcm9tIFwiLi9pMThuLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS1kYXRlcGlja2VyXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZGF0ZXBpY2tlci5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kYXRlcGlja2VyLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgRGF0ZXBpY2tlckNvbXBvbmVudCB7XG4gIHRvZGF5OiBEYXRlID0gbmV3IERhdGUoKTtcblxuICBAT3V0cHV0KCkgZGF0ZXBpY2tlckl0ZW1DaGFuZ2U6IEV2ZW50RW1pdHRlcjxOZ2JEYXRlU3RydWN0PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBASW5wdXQoKSBkYXRlRm9ybWF0Pzogc3RyaW5nID0gdW5kZWZpbmVkO1xuICBASW5wdXQoKSBkYXRlcGlja2VySXRlbT86IE5nYkRhdGVTdHJ1Y3QgPSB1bmRlZmluZWQ7XG4gIEBJbnB1dCgpIG1heERhdGU/OiBOZ2JEYXRlU3RydWN0ID0gdW5kZWZpbmVkO1xuICBASW5wdXQoKSBtaW5EYXRlPzogTmdiRGF0ZVN0cnVjdCA9IHVuZGVmaW5lZDtcbiAgQElucHV0KCkgYXV0b0Nsb3NlOiBib29sZWFuIHwgXCJpbnNpZGVcIiB8IFwib3V0c2lkZVwiID0gdHJ1ZTtcbiAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgcmVhZE9ubHk6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI6IHN0cmluZyA9IFwiZGQvTU0veXl5XCI7XG4gIEBJbnB1dCgpIHBpY2tlckltZz86IHN0cmluZyA9IHVuZGVmaW5lZDtcbiAgQElucHV0KCkgdmlhQ29udHJvbDogRm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcbiAgQElucHV0KCkgaTE4bj86IERhdGVwaWNrZXJJMThuRHRvID0gdW5kZWZpbmVkO1xuXG4gIGRhdGVwaWNrZXJJZDogc3RyaW5nID0gXCJcIjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgX2kxOG46IEkxOG5TZXJ2aWNlLFxuICAgIHB1YmxpYyBmb3JtYXR0ZXI6IE5nYkRhdGVQYXJzZXJGb3JtYXR0ZXIsXG4gICAgcHJpdmF0ZSBjYWxlbmRhcjogTmdiQ2FsZW5kYXJcbiAgKSB7XG4gICAgdGhpcy5kYXRlcGlja2VySWQgPSBcImRhdGVwaWNrZXJcIiArIHJhbmRvbSgpO1xuICAgIGlmICh0aGlzLmRhdGVGb3JtYXQpIHtcbiAgICAgIHRoaXMuZGF0ZUZvcm1hdCA9IERBVEVfRk9STUFUX1dJVEhPVVRfVElNRTtcbiAgICB9XG4gICAgaWYgKHRoaXMubWluRGF0ZSkge1xuICAgICAgdGhpcy5taW5EYXRlID0ge1xuICAgICAgICBkYXk6IDEsXG4gICAgICAgIG1vbnRoOiAxLFxuICAgICAgICB5ZWFyOiB0aGlzLnRvZGF5LmdldEZ1bGxZZWFyKCkgLSAxMDBcbiAgICAgIH07XG4gICAgfVxuICAgIHRoaXMuX2kxOG4ubGFuZ3VhZ2UgPSBcImVuXCI7XG4gICAgdGhpcy5zZXRMYW5ndWFnZShcImVuXCIpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxuICBmb3JtQ29udHJvbEluc3RhbmNlKCkge1xuICAgIHJldHVybiB0aGlzLnZpYUNvbnRyb2w7XG4gIH1cblxuICBzZWxlY3RlZChkYXRlOiBOZ2JEYXRlU3RydWN0KSB7IFxuICAgIHRoaXMuZGF0ZXBpY2tlckl0ZW0gPSBkYXRlO1xuICAgIHRoaXMudmlhQ29udHJvbC5zZXRWYWx1ZShkYXRlKTtcbiAgfVxuICBcbiAgcmVxdWlyZWQoZm9ybT86IEZvcm1Db250cm9sKSB7XG4gICAgcmV0dXJuIEZvcm1VdGlscy5nZXRJbnN0YW5jZSgpLnJlcXVpcmVkKGZvcm0pO1xuICB9XG5cbiAgc2V0TGFuZ3VhZ2UobGFuZ3VhZ2U6IHN0cmluZykge1xuICAgIHRoaXMuX2kxOG4ubGFuZ3VhZ2UgPSBsYW5ndWFnZTtcbiAgICAvLyB0aGlzLnBsYWNlaG9sZGVyID0gdGhpcy5faTE4bi5sYW5ndWFnZSA9PSBcImVuXCI/IFwiZGQvbW0veXl5eVwiOiBcIm5uL3R0L25ubm5cIjtcbiAgfVxuXG4gIGdldERhdGVTZWxlY3RlZCgpOiBEYXRlIHwgdW5kZWZpbmVkIHtcbiAgICBpZiAodGhpcy5kYXRlcGlja2VySXRlbSkge1xuICAgICAgcmV0dXJuIG5ldyBEYXRlKHRoaXMuZGF0ZXBpY2tlckl0ZW0ueWVhciwgdGhpcy5kYXRlcGlja2VySXRlbS5tb250aCAtIDEsIHRoaXMuZGF0ZXBpY2tlckl0ZW0uZGF5KTtcbiAgICB9XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIGRhdGVTZWxlY3QoZGF0ZTogTmdiRGF0ZVN0cnVjdCB8IHVuZGVmaW5lZCk6IHZvaWQge1xuICAgIHRoaXMudmlhQ29udHJvbC5zZXRWYWx1ZShkYXRlKTtcbiAgICB0aGlzLmRhdGVwaWNrZXJJdGVtQ2hhbmdlPy5lbWl0KGRhdGUpO1xuICB9XG5cbiAgaXNWYWxpZChvYmo6IGFueSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBvYmogIT09IG51bGwgJiYgb2JqICE9PSB1bmRlZmluZWQ7XG4gIH1cblxuICB2YWxpZGF0ZUlucHV0KGlucHV0OiBzdHJpbmcpOiBOZ2JEYXRlIHwgdW5kZWZpbmVkIHtcbiAgICBjb25zdCBwYXJzZWQgPSB0aGlzLmZvcm1hdHRlci5wYXJzZShpbnB1dCk7XG4gICAgbGV0IHZhbHVlID0gdW5kZWZpbmVkO1xuICAgIGlmIChwYXJzZWQgPT0gdW5kZWZpbmVkIHx8IHBhcnNlZCA9PSBudWxsIHx8IChwYXJzZWQgJiYgdGhpcy5jYWxlbmRhci5pc1ZhbGlkKE5nYkRhdGUuZnJvbShwYXJzZWQpKSkpIHtcbiAgICAgIGxldCB2ID0gTmdiRGF0ZS5mcm9tKHBhcnNlZCk7XG4gICAgICBpZiAodikge1xuICAgICAgICB2YWx1ZSA9IHY7XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMuZGF0ZVNlbGVjdCh2YWx1ZSk7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG5cbiAgZm9ybWF0KGRhdGVwaWNrZXJJdGVtOiBOZ2JEYXRlU3RydWN0IHwgdW5kZWZpbmVkKXtcbiAgICByZXR1cm4gdGhpcy5mb3JtYXR0ZXIuZm9ybWF0KGRhdGVwaWNrZXJJdGVtID8gZGF0ZXBpY2tlckl0ZW0gOiBudWxsKTtcbiAgfVxuXG4gIGNoZWNrRXJyb3IoZGF0ZTogc3RyaW5nKXtcbiAgICBjb25zdCBwYXJzZWQgPSB0aGlzLmZvcm1hdHRlci5wYXJzZShkYXRlKTtcbiAgICBsZXQgaXNWYWxpZCA9IGZhbHNlO1xuICAgIGlmIChwYXJzZWQgJiYgdGhpcy5jYWxlbmRhci5pc1ZhbGlkKE5nYkRhdGUuZnJvbShwYXJzZWQpKSl7XG4gICAgICBpc1ZhbGlkID0gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGlzVmFsaWQ7XG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJ2aWEtZGF0ZXBpY2tlciBmb3JtLWdyb3VwXCI+XG4gICAgPGxhYmVsICpuZ0lmPVwiaTE4blwiIFthdHRyLmZvcl09XCJkYXRlcGlja2VySWRcIj5cbiAgICAgICAge3tpMThuLmxhYmVsfX1cbiAgICAgICAgPHNwYW4gY2xhc3M9XCJkYW5nZXItdGV4dFwiPiB7e3JlcXVpcmVkKHZpYUNvbnRyb2wpfX08L3NwYW4+XG4gICAgPC9sYWJlbD5cbiAgICA8ZGl2IGNsYXNzPVwiaW5wdXQtd3JhcHBlclwiPlxuICAgICAgICA8aW5wdXQgXG4gICAgICAgICAgICBbYXR0ci5pZF09XCJkYXRlcGlja2VySWRcIlxuICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBcbiAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJwbGFjZWhvbGRlclwiIFxuICAgICAgICAgICAgW3N0YXJ0RGF0ZV09XCIkYW55KGRhdGVwaWNrZXJJdGVtKVwiIFxuICAgICAgICAgICAgI2RhdGVQaWNrZXJJcHQgXG4gICAgICAgICAgICBuYW1lPVwiZGF0ZVBpY2tlcklwdFwiIFxuICAgICAgICAgICAgW3ZhbHVlXT1cImZvcm1hdChkYXRlcGlja2VySXRlbSlcIiBcbiAgICAgICAgICAgIChpbnB1dCk9XCJkYXRlcGlja2VySXRlbSA9IHZhbGlkYXRlSW5wdXQoZGF0ZVBpY2tlcklwdC52YWx1ZSlcIiBcbiAgICAgICAgICAgIG5nYkRhdGVwaWNrZXIgXG4gICAgICAgICAgICAjZD1cIm5nYkRhdGVwaWNrZXJcIiBcbiAgICAgICAgICAgIFttaW5EYXRlXT1cIiRhbnkobWluRGF0ZSlcIiBcbiAgICAgICAgICAgIFttYXhEYXRlXT1cIiRhbnkobWF4RGF0ZSlcIlxuICAgICAgICAgICAgKGRhdGVTZWxlY3QpPVwiZGF0ZVNlbGVjdCgkZXZlbnQpXCJcbiAgICAgICAgICAgIFthdXRvQ2xvc2VdPVwiYXV0b0Nsb3NlXCJcbiAgICAgICAgICAgIFthdHRyLmRpc2FibGVkXT1cImRpc2FibGVkXCJcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJkaXNhYmxlZFwiXG4gICAgICAgICAgICBbcmVhZE9ubHldPVwicmVhZE9ubHlcIiAvPlxuXG4gICAgICAgIDxpbWcgKm5nSWY9XCJwaWNrZXJJbWdcIiAoY2xpY2spPVwiZC50b2dnbGUoKVwiIGNsYXNzPVwiZGF0ZS1pY29uXCIgW3NyY109XCJwaWNrZXJJbWdcIiAvPlxuICAgICAgICA8aSAqbmdJZj1cIiFwaWNrZXJJbWdcIiAoY2xpY2spPVwiZC50b2dnbGUoKVwiIGNsYXNzPVwiZmFyIGZhLWNhbGVuZGFyIGRhdGUtaWNvblwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==