import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { CarouselOptionDto } from "./dto/carousel.option.dto";
import ViaCarousel from "./lib/carousel";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CarouselComponent {
    constructor() {
        this.carouselElement = undefined;
        this.option = new CarouselOptionDto();
        this.afterChange = new EventEmitter();
        this.beforeChange = new EventEmitter();
        this.mounted = new EventEmitter();
        this.created = new EventEmitter();
        this.slideChanged = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.move = new EventEmitter();
        this.destroyed = new EventEmitter();
        this.carousel = undefined;
        this.activeIndex = 1;
        this.dotHelper = [];
    }
    ngOnInit() {
        this.activeIndex = this.option.initial;
    }
    ngAfterViewInit() {
        setTimeout(() => {
            if (this.option.autoInit) {
                this.init();
            }
        }, 0);
    }
    instance() {
        return this.carousel;
    }
    init() {
        this.carousel = ViaCarousel(this.carouselElement.nativeElement, {
            autoAdjustSlidesPerView: this.option.autoAdjustSlidesPerView,
            centered: this.option.centered,
            scaleCenter: this.option.scaleCenter,
            breakpoints: this.option.breakpoints,
            controls: this.option.controls,
            dragSpeed: this.option.dragSpeed,
            friction: this.option.friction,
            loop: this.option.loop,
            initial: this.option.initial,
            duration: this.option.duration,
            preventEvent: this.option.preventEvent,
            slides: this.option.slides,
            vertical: this.option.vertical,
            resetSlide: this.option.resetSlide,
            slidesPerView: this.option.slidesPerView,
            spacing: this.option.spacing,
            mode: this.option.mode,
            rtl: this.option.rtl,
            rubberband: this.option.rubberband,
            cancelOnLeave: this.option.cancelOnLeave,
            afterChange: (s) => {
                this.afterChange?.emit(s);
            },
            beforeChange: (s) => {
                this.beforeChange?.emit(s);
            },
            mounted: (s) => {
                this.mounted?.emit(s);
            },
            created: (s) => {
                this.created?.emit(s);
            },
            slideChanged: (s) => {
                this.dotHelper = [...Array(s.details().size).keys()];
                this.activeIndex = s.details().relativeSlide;
                this.slideChanged?.emit(s);
            },
            dragEnd: (s) => {
                this.dragEnd?.emit(s);
            },
            dragStart: (s) => {
                this.dragStart?.emit(s);
            },
            move: (s) => {
                this.move?.emit(s);
            },
            destroyed: (s) => {
                this.destroyed?.emit(s);
            }
        });
    }
    shouldShowDots() {
        return this.carousel && this.option.dots;
    }
    shouldShowNav() {
        return this.carousel && this.option.nav;
    }
    ngOnDestroy() {
        this.carousel?.destroy();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: CarouselComponent, selector: "via-carousel", inputs: { option: "option" }, outputs: { afterChange: "afterChange", beforeChange: "beforeChange", mounted: "mounted", created: "created", slideChanged: "slideChanged", dragEnd: "dragEnd", dragStart: "dragStart", move: "move", destroyed: "destroyed" }, viewQueries: [{ propertyName: "carouselElement", first: true, predicate: ["carouselElement"], descendants: true }], ngImport: i0, template: "<div class=\"via-carousel-wrapper\">\n    <div class=\"via-carousel\" #carouselElement>\n        <ng-content></ng-content>\n    </div>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--left ' + (activeIndex === 0 ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.prev()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path\n        d=\"M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z\"/>\n    </svg>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--right ' +\n        (carousel!.details().size - 1 === activeIndex ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.next()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path d=\"M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z\" />\n    </svg>\n    <div class=\"dots\" *ngIf=\"shouldShowDots()\">\n        <button\n            *ngFor=\"let slide of dotHelper; let i = index\"\n            (click)=\"carousel!.moveToSlideRelative(i)\"\n            [class]=\"'dot ' + (i === activeIndex ? 'active' : '')\"\n        ></button>\n    </div>\n</div>", styles: [".via-carousel-wrapper{width:100%;height:100%;position:relative}.via-carousel-wrapper .via-carousel{display:flex;overflow:hidden;position:relative;-webkit-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}.via-carousel-wrapper .via-carousel-item{position:relative;overflow:hidden;width:100%;min-height:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-v]{flex-wrap:wrap}.via-carousel-wrapper .via-carousel[data-via-carousel-v] .via-carousel-wrapper .via-carousel-item{width:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-moves] *{pointer-events:none}.via-carousel-wrapper .dots{display:flex;padding:10px 0;justify-content:center}.via-carousel-wrapper .dot{border:none;width:10px;height:10px;background:#c5c5c5;border-radius:50%;margin:0 5px;padding:5px;cursor:pointer}.via-carousel-wrapper .dot:focus{outline:none}.via-carousel-wrapper .dot.active{background:var(--primary-color, #9a0000)}.via-carousel-wrapper .arrow{width:15px;height:15px;position:absolute;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);fill:gray;cursor:pointer}.via-carousel-wrapper .arrow--left{left:5px}.via-carousel-wrapper .arrow--right{left:auto;right:5px}.via-carousel-wrapper .arrow--disabled{fill:#8080804d}\n"], dependencies: [{ kind: "directive", type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: CarouselComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-carousel", template: "<div class=\"via-carousel-wrapper\">\n    <div class=\"via-carousel\" #carouselElement>\n        <ng-content></ng-content>\n    </div>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--left ' + (activeIndex === 0 ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.prev()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path\n        d=\"M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z\"/>\n    </svg>\n    <svg\n        *ngIf=\"shouldShowNav()\"\n        [ngClass]=\"\n        'arrow arrow--right ' +\n        (carousel!.details().size - 1 === activeIndex ? 'arrow--disabled' : '')\n        \"\n        (click)=\"carousel!.next()\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        viewBox=\"0 0 24 24\">\n        <path d=\"M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z\" />\n    </svg>\n    <div class=\"dots\" *ngIf=\"shouldShowDots()\">\n        <button\n            *ngFor=\"let slide of dotHelper; let i = index\"\n            (click)=\"carousel!.moveToSlideRelative(i)\"\n            [class]=\"'dot ' + (i === activeIndex ? 'active' : '')\"\n        ></button>\n    </div>\n</div>", styles: [".via-carousel-wrapper{width:100%;height:100%;position:relative}.via-carousel-wrapper .via-carousel{display:flex;overflow:hidden;position:relative;-webkit-user-select:none;user-select:none;-webkit-touch-callout:none;-khtml-user-select:none;-ms-touch-action:pan-y;touch-action:pan-y;-webkit-tap-highlight-color:transparent}.via-carousel-wrapper .via-carousel-item{position:relative;overflow:hidden;width:100%;min-height:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-v]{flex-wrap:wrap}.via-carousel-wrapper .via-carousel[data-via-carousel-v] .via-carousel-wrapper .via-carousel-item{width:100%}.via-carousel-wrapper .via-carousel[data-via-carousel-moves] *{pointer-events:none}.via-carousel-wrapper .dots{display:flex;padding:10px 0;justify-content:center}.via-carousel-wrapper .dot{border:none;width:10px;height:10px;background:#c5c5c5;border-radius:50%;margin:0 5px;padding:5px;cursor:pointer}.via-carousel-wrapper .dot:focus{outline:none}.via-carousel-wrapper .dot.active{background:var(--primary-color, #9a0000)}.via-carousel-wrapper .arrow{width:15px;height:15px;position:absolute;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);fill:gray;cursor:pointer}.via-carousel-wrapper .arrow--left{left:5px}.via-carousel-wrapper .arrow--right{left:auto;right:5px}.via-carousel-wrapper .arrow--disabled{fill:#8080804d}\n"] }]
        }], ctorParameters: () => [], propDecorators: { carouselElement: [{
                type: ViewChild,
                args: ["carouselElement"]
            }], option: [{
                type: Input
            }], afterChange: [{
                type: Output
            }], beforeChange: [{
                type: Output
            }], mounted: [{
                type: Output
            }], created: [{
                type: Output
            }], slideChanged: [{
                type: Output
            }], dragEnd: [{
                type: Output
            }], dragStart: [{
                type: Output
            }], move: [{
                type: Output
            }], destroyed: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvY2Fyb3VzZWwvY2Fyb3VzZWwudHMiLCIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvY2Fyb3VzZWwvY2Fyb3VzZWwuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFOUYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDOUQsT0FBTyxXQUFXLE1BQU0sZ0JBQWdCLENBQUM7OztBQU96QyxNQUFNLE9BQU8saUJBQWlCO0lBb0I1QjtRQWxCOEIsb0JBQWUsR0FBa0MsU0FBUyxDQUFDO1FBRWhGLFdBQU0sR0FBc0IsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1FBRW5ELGdCQUFXLEdBQXNDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDcEUsaUJBQVksR0FBc0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyRSxZQUFPLEdBQXNDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDaEUsWUFBTyxHQUFzQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hFLGlCQUFZLEdBQXNDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckUsWUFBTyxHQUFzQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hFLGNBQVMsR0FBc0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNsRSxTQUFJLEdBQXNDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0QsY0FBUyxHQUFzQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRTVFLGFBQVEsR0FBeUIsU0FBUyxDQUFDO1FBQzNDLGdCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBQ3hCLGNBQVMsR0FBa0IsRUFBRSxDQUFDO0lBRzlCLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUN6QyxDQUFDO0lBRUQsZUFBZTtRQUNiLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNkLENBQUM7UUFDSCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRUQsUUFBUTtRQUNOLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFnQixDQUFDLGFBQWEsRUFBRTtZQUMvRCx1QkFBdUIsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLHVCQUF1QjtZQUM1RCxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRO1lBQzlCLFdBQVcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVc7WUFDcEMsV0FBVyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVztZQUNwQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRO1lBQzlCLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDaEMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUTtZQUM5QixJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87WUFDNUIsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUTtZQUM5QixZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZO1lBQ3RDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07WUFDMUIsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUTtZQUM5QixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVO1lBQ2xDLGFBQWEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWE7WUFDeEMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTztZQUM1QixJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJO1lBQ3RCLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7WUFDcEIsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVTtZQUNsQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhO1lBQ3hDLFdBQVcsRUFBRSxDQUFDLENBQXNCLEVBQUUsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsQ0FBQztZQUNELFlBQVksRUFBRSxDQUFDLENBQXNCLEVBQUUsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsQ0FBQztZQUNELE9BQU8sRUFBRSxDQUFDLENBQXNCLEVBQUUsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQztZQUNELE9BQU8sRUFBRSxDQUFDLENBQXNCLEVBQUUsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQztZQUNELFlBQVksRUFBRSxDQUFDLENBQXNCLEVBQUUsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxhQUFhLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLENBQUM7WUFDRCxPQUFPLEVBQUUsQ0FBQyxDQUFzQixFQUFFLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLENBQUM7WUFDRCxTQUFTLEVBQUUsQ0FBQyxDQUFzQixFQUFFLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLENBQUM7WUFDRCxJQUFJLEVBQUUsQ0FBQyxDQUFzQixFQUFFLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLENBQUM7WUFDRCxTQUFTLEVBQUUsQ0FBQyxDQUFzQixFQUFFLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFCLENBQUM7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsY0FBYztRQUNaLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUMzQyxDQUFDO0lBRUQsYUFBYTtRQUNYLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUMxQyxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLENBQUM7SUFDM0IsQ0FBQzs4R0F2R1UsaUJBQWlCO2tHQUFqQixpQkFBaUIscWFDVjlCLDRyQ0FpQ007OzJGRHZCTyxpQkFBaUI7a0JBTDdCLFNBQVM7K0JBQ0UsY0FBYzt3REFNTSxlQUFlO3NCQUE1QyxTQUFTO3VCQUFDLGlCQUFpQjtnQkFFbkIsTUFBTTtzQkFBZCxLQUFLO2dCQUVJLFdBQVc7c0JBQXBCLE1BQU07Z0JBQ0csWUFBWTtzQkFBckIsTUFBTTtnQkFDRyxPQUFPO3NCQUFoQixNQUFNO2dCQUNHLE9BQU87c0JBQWhCLE1BQU07Z0JBQ0csWUFBWTtzQkFBckIsTUFBTTtnQkFDRyxPQUFPO3NCQUFoQixNQUFNO2dCQUNHLFNBQVM7c0JBQWxCLE1BQU07Z0JBQ0csSUFBSTtzQkFBYixNQUFNO2dCQUNHLFNBQVM7c0JBQWxCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENhcm91c2VsSW5zdGFuY2VEdG8gfSBmcm9tIFwiLi9kdG8vY2Fyb3VzZWwuaW5zdGFuY2UuZHRvXCI7XG5pbXBvcnQgeyBDYXJvdXNlbE9wdGlvbkR0byB9IGZyb20gXCIuL2R0by9jYXJvdXNlbC5vcHRpb24uZHRvXCI7XG5pbXBvcnQgVmlhQ2Fyb3VzZWwgZnJvbSBcIi4vbGliL2Nhcm91c2VsXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtY2Fyb3VzZWxcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9jYXJvdXNlbC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9jYXJvdXNlbC5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENhcm91c2VsQ29tcG9uZW50IHtcbiAgXG4gIEBWaWV3Q2hpbGQoXCJjYXJvdXNlbEVsZW1lbnRcIikgY2Fyb3VzZWxFbGVtZW50PzogRWxlbWVudFJlZjxIVE1MSW5wdXRFbGVtZW50PiA9IHVuZGVmaW5lZDtcblxuICBASW5wdXQoKSBvcHRpb246IENhcm91c2VsT3B0aW9uRHRvID0gbmV3IENhcm91c2VsT3B0aW9uRHRvKCk7XG5cbiAgQE91dHB1dCgpIGFmdGVyQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q2Fyb3VzZWxJbnN0YW5jZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBiZWZvcmVDaGFuZ2U6IEV2ZW50RW1pdHRlcjxDYXJvdXNlbEluc3RhbmNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1vdW50ZWQ6IEV2ZW50RW1pdHRlcjxDYXJvdXNlbEluc3RhbmNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGNyZWF0ZWQ6IEV2ZW50RW1pdHRlcjxDYXJvdXNlbEluc3RhbmNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIHNsaWRlQ2hhbmdlZDogRXZlbnRFbWl0dGVyPENhcm91c2VsSW5zdGFuY2VEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZHJhZ0VuZDogRXZlbnRFbWl0dGVyPENhcm91c2VsSW5zdGFuY2VEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZHJhZ1N0YXJ0OiBFdmVudEVtaXR0ZXI8Q2Fyb3VzZWxJbnN0YW5jZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtb3ZlOiBFdmVudEVtaXR0ZXI8Q2Fyb3VzZWxJbnN0YW5jZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBkZXN0cm95ZWQ6IEV2ZW50RW1pdHRlcjxDYXJvdXNlbEluc3RhbmNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBjYXJvdXNlbD86IENhcm91c2VsSW5zdGFuY2VEdG8gPSB1bmRlZmluZWQ7XG4gIGFjdGl2ZUluZGV4OiBudW1iZXIgPSAxO1xuICBkb3RIZWxwZXI6IEFycmF5PE51bWJlcj4gPSBbXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkgeyBcbiAgICB0aGlzLmFjdGl2ZUluZGV4ID0gdGhpcy5vcHRpb24uaW5pdGlhbDtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHsgXG4gICAgICBpZiAodGhpcy5vcHRpb24uYXV0b0luaXQpIHsgXG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgICAgfVxuICAgIH0sIDApO1xuICB9XG5cbiAgaW5zdGFuY2UoKSB7IFxuICAgIHJldHVybiB0aGlzLmNhcm91c2VsO1xuICB9XG5cbiAgaW5pdCgpIHsgXG4gICAgdGhpcy5jYXJvdXNlbCA9IFZpYUNhcm91c2VsKHRoaXMuY2Fyb3VzZWxFbGVtZW50IS5uYXRpdmVFbGVtZW50LCB7XG4gICAgICBhdXRvQWRqdXN0U2xpZGVzUGVyVmlldzogdGhpcy5vcHRpb24uYXV0b0FkanVzdFNsaWRlc1BlclZpZXcsXG4gICAgICBjZW50ZXJlZDogdGhpcy5vcHRpb24uY2VudGVyZWQsXG4gICAgICBzY2FsZUNlbnRlcjogdGhpcy5vcHRpb24uc2NhbGVDZW50ZXIsXG4gICAgICBicmVha3BvaW50czogdGhpcy5vcHRpb24uYnJlYWtwb2ludHMsXG4gICAgICBjb250cm9sczogdGhpcy5vcHRpb24uY29udHJvbHMsXG4gICAgICBkcmFnU3BlZWQ6IHRoaXMub3B0aW9uLmRyYWdTcGVlZCxcbiAgICAgIGZyaWN0aW9uOiB0aGlzLm9wdGlvbi5mcmljdGlvbixcbiAgICAgIGxvb3A6IHRoaXMub3B0aW9uLmxvb3AsXG4gICAgICBpbml0aWFsOiB0aGlzLm9wdGlvbi5pbml0aWFsLFxuICAgICAgZHVyYXRpb246IHRoaXMub3B0aW9uLmR1cmF0aW9uLFxuICAgICAgcHJldmVudEV2ZW50OiB0aGlzLm9wdGlvbi5wcmV2ZW50RXZlbnQsXG4gICAgICBzbGlkZXM6IHRoaXMub3B0aW9uLnNsaWRlcyxcbiAgICAgIHZlcnRpY2FsOiB0aGlzLm9wdGlvbi52ZXJ0aWNhbCxcbiAgICAgIHJlc2V0U2xpZGU6IHRoaXMub3B0aW9uLnJlc2V0U2xpZGUsXG4gICAgICBzbGlkZXNQZXJWaWV3OiB0aGlzLm9wdGlvbi5zbGlkZXNQZXJWaWV3LFxuICAgICAgc3BhY2luZzogdGhpcy5vcHRpb24uc3BhY2luZyxcbiAgICAgIG1vZGU6IHRoaXMub3B0aW9uLm1vZGUsXG4gICAgICBydGw6IHRoaXMub3B0aW9uLnJ0bCxcbiAgICAgIHJ1YmJlcmJhbmQ6IHRoaXMub3B0aW9uLnJ1YmJlcmJhbmQsXG4gICAgICBjYW5jZWxPbkxlYXZlOiB0aGlzLm9wdGlvbi5jYW5jZWxPbkxlYXZlLFxuICAgICAgYWZ0ZXJDaGFuZ2U6IChzOiBDYXJvdXNlbEluc3RhbmNlRHRvKSA9PiB7XG4gICAgICAgIHRoaXMuYWZ0ZXJDaGFuZ2U/LmVtaXQocyk7XG4gICAgICB9LFxuICAgICAgYmVmb3JlQ2hhbmdlOiAoczogQ2Fyb3VzZWxJbnN0YW5jZUR0bykgPT4ge1xuICAgICAgICB0aGlzLmJlZm9yZUNoYW5nZT8uZW1pdChzKTtcbiAgICAgIH0sXG4gICAgICBtb3VudGVkOiAoczogQ2Fyb3VzZWxJbnN0YW5jZUR0bykgPT4ge1xuICAgICAgICB0aGlzLm1vdW50ZWQ/LmVtaXQocyk7XG4gICAgICB9LFxuICAgICAgY3JlYXRlZDogKHM6IENhcm91c2VsSW5zdGFuY2VEdG8pID0+IHtcbiAgICAgICAgdGhpcy5jcmVhdGVkPy5lbWl0KHMpO1xuICAgICAgfSxcbiAgICAgIHNsaWRlQ2hhbmdlZDogKHM6IENhcm91c2VsSW5zdGFuY2VEdG8pID0+IHtcbiAgICAgICAgdGhpcy5kb3RIZWxwZXIgPSBbLi4uQXJyYXkocy5kZXRhaWxzKCkuc2l6ZSkua2V5cygpXTtcbiAgICAgICAgdGhpcy5hY3RpdmVJbmRleCA9IHMuZGV0YWlscygpLnJlbGF0aXZlU2xpZGU7XG4gICAgICAgIHRoaXMuc2xpZGVDaGFuZ2VkPy5lbWl0KHMpO1xuICAgICAgfSxcbiAgICAgIGRyYWdFbmQ6IChzOiBDYXJvdXNlbEluc3RhbmNlRHRvKSA9PiB7XG4gICAgICAgIHRoaXMuZHJhZ0VuZD8uZW1pdChzKTtcbiAgICAgIH0sXG4gICAgICBkcmFnU3RhcnQ6IChzOiBDYXJvdXNlbEluc3RhbmNlRHRvKSA9PiB7XG4gICAgICAgIHRoaXMuZHJhZ1N0YXJ0Py5lbWl0KHMpO1xuICAgICAgfSxcbiAgICAgIG1vdmU6IChzOiBDYXJvdXNlbEluc3RhbmNlRHRvKSA9PiB7XG4gICAgICAgIHRoaXMubW92ZT8uZW1pdChzKTtcbiAgICAgIH0sXG4gICAgICBkZXN0cm95ZWQ6IChzOiBDYXJvdXNlbEluc3RhbmNlRHRvKSA9PiB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkPy5lbWl0KHMpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgc2hvdWxkU2hvd0RvdHMoKSB7IFxuICAgIHJldHVybiB0aGlzLmNhcm91c2VsICYmIHRoaXMub3B0aW9uLmRvdHM7XG4gIH1cblxuICBzaG91bGRTaG93TmF2KCkgeyBcbiAgICByZXR1cm4gdGhpcy5jYXJvdXNlbCAmJiB0aGlzLm9wdGlvbi5uYXY7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLmNhcm91c2VsPy5kZXN0cm95KCk7XG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cInZpYS1jYXJvdXNlbC13cmFwcGVyXCI+XG4gICAgPGRpdiBjbGFzcz1cInZpYS1jYXJvdXNlbFwiICNjYXJvdXNlbEVsZW1lbnQ+XG4gICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cbiAgICA8L2Rpdj5cbiAgICA8c3ZnXG4gICAgICAgICpuZ0lmPVwic2hvdWxkU2hvd05hdigpXCJcbiAgICAgICAgW25nQ2xhc3NdPVwiXG4gICAgICAgICdhcnJvdyBhcnJvdy0tbGVmdCAnICsgKGFjdGl2ZUluZGV4ID09PSAwID8gJ2Fycm93LS1kaXNhYmxlZCcgOiAnJylcbiAgICAgICAgXCJcbiAgICAgICAgKGNsaWNrKT1cImNhcm91c2VsIS5wcmV2KClcIlxuICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgdmlld0JveD1cIjAgMCAyNCAyNFwiPlxuICAgICAgICA8cGF0aFxuICAgICAgICBkPVwiTTE2LjY3IDBsMi44MyAyLjgyOS05LjMzOSA5LjE3NSA5LjMzOSA5LjE2Ny0yLjgzIDIuODI5LTEyLjE3LTExLjk5NnpcIi8+XG4gICAgPC9zdmc+XG4gICAgPHN2Z1xuICAgICAgICAqbmdJZj1cInNob3VsZFNob3dOYXYoKVwiXG4gICAgICAgIFtuZ0NsYXNzXT1cIlxuICAgICAgICAnYXJyb3cgYXJyb3ctLXJpZ2h0ICcgK1xuICAgICAgICAoY2Fyb3VzZWwhLmRldGFpbHMoKS5zaXplIC0gMSA9PT0gYWN0aXZlSW5kZXggPyAnYXJyb3ctLWRpc2FibGVkJyA6ICcnKVxuICAgICAgICBcIlxuICAgICAgICAoY2xpY2spPVwiY2Fyb3VzZWwhLm5leHQoKVwiXG4gICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICB2aWV3Qm94PVwiMCAwIDI0IDI0XCI+XG4gICAgICAgIDxwYXRoIGQ9XCJNNSAzbDMuMDU3LTMgMTEuOTQzIDEyLTExLjk0MyAxMi0zLjA1Ny0zIDktOXpcIiAvPlxuICAgIDwvc3ZnPlxuICAgIDxkaXYgY2xhc3M9XCJkb3RzXCIgKm5nSWY9XCJzaG91bGRTaG93RG90cygpXCI+XG4gICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICpuZ0Zvcj1cImxldCBzbGlkZSBvZiBkb3RIZWxwZXI7IGxldCBpID0gaW5kZXhcIlxuICAgICAgICAgICAgKGNsaWNrKT1cImNhcm91c2VsIS5tb3ZlVG9TbGlkZVJlbGF0aXZlKGkpXCJcbiAgICAgICAgICAgIFtjbGFzc109XCInZG90ICcgKyAoaSA9PT0gYWN0aXZlSW5kZXggPyAnYWN0aXZlJyA6ICcnKVwiXG4gICAgICAgID48L2J1dHRvbj5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==