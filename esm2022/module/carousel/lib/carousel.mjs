import './polyfills';
function ViaCarousel(initialContainer, initialOptions = {}) {
    const attributeMoving = 'data-via-carousel-moves';
    const attributeVertical = 'data-via-carousel-v';
    let container;
    let events = [];
    let touchControls;
    let length;
    let origin;
    let slides;
    let width;
    let slidesPerView;
    let spacing;
    let resizeLastWidth;
    let breakpointCurrent = null;
    let optionsChanged = false;
    let sliderCreated = false;
    let trackCurrentIdx;
    let trackPosition = 0;
    let trackMeasurePoints = [];
    let trackDirection;
    let trackMeasureTimeout;
    let trackSpeed;
    let trackSlidePositions;
    let trackProgress;
    let options;
    // touch/swipe helper
    let touchIndexStart;
    let touchActive;
    let touchIdentifier;
    let touchLastX;
    let touchLastClientX;
    let touchLastClientY;
    let touchMultiplicator;
    let touchJustStarted;
    let touchSumDistance;
    let touchChecked;
    // animation
    let reqId;
    let startTime;
    let moveDistance;
    let moveDuration;
    let moveEasing;
    let moved;
    let moveForceFinish;
    let moveCallBack;
    function eventAdd(element, event, handler, options = {}) {
        element.addEventListener(event, handler, options);
        events.push([element, event, handler, options]);
    }
    function eventDrag(e) {
        if (!touchActive ||
            touchIdentifier !== eventGetIdentifier(e) ||
            !isTouchable())
            return;
        const x = eventGetX(e).x;
        if (!eventIsSlide(e) && touchJustStarted) {
            return eventDragStop(e);
        }
        if (touchJustStarted) {
            trackMeasureReset();
            touchLastX = x;
            touchJustStarted = false;
        }
        if (e.cancelable)
            e.preventDefault();
        const touchDistance = touchLastX - x;
        touchSumDistance += Math.abs(touchDistance);
        if (!touchChecked && touchSumDistance > 5) {
            touchChecked = true;
            container.setAttribute(attributeMoving, true);
        }
        trackAdd(touchMultiplicator(touchDistance, pubfuncs) * (!isRtl() ? 1 : -1), e.timeStamp);
        touchLastX = x;
    }
    function eventDragStart(e) {
        if (touchActive || !isTouchable() || eventIsIgnoreTarget(e.target))
            return;
        touchActive = true;
        touchJustStarted = true;
        touchIdentifier = eventGetIdentifier(e);
        touchChecked = false;
        touchSumDistance = 0;
        eventIsSlide(e);
        moveAnimateAbort();
        touchIndexStart = trackCurrentIdx;
        touchLastX = eventGetX(e).x;
        trackAdd(0, e.timeStamp);
        hook('dragStart');
    }
    function eventDragStop(e) {
        if (!touchActive ||
            touchIdentifier !== eventGetIdentifier(e, true) ||
            !isTouchable())
            return;
        container.removeAttribute(attributeMoving);
        touchActive = false;
        moveWithSpeed();
        hook('dragEnd');
    }
    function eventGetChangedTouches(e) {
        return e.changedTouches;
    }
    function eventGetIdentifier(e, changedTouches = false) {
        const touches = changedTouches
            ? eventGetChangedTouches(e)
            : eventGetTargetTouches(e);
        return !touches ? 'default' : touches[0] ? touches[0].identifier : 'error';
    }
    function eventGetTargetTouches(e) {
        return e.targetTouches;
    }
    function eventGetX(e) {
        const touches = eventGetTargetTouches(e);
        return {
            x: isVerticalSlider()
                ? !touches
                    ? e.pageY
                    : touches[0].screenY
                : !touches
                    ? e.pageX
                    : touches[0].screenX,
            timestamp: e.timeStamp,
        };
    }
    function eventIsIgnoreTarget(target) {
        return target.hasAttribute(options.preventEvent);
    }
    function eventIsSlide(e) {
        const touches = eventGetTargetTouches(e);
        if (!touches)
            return true;
        const touch = touches[0];
        const x = isVerticalSlider() ? touch.clientY : touch.clientX;
        const y = isVerticalSlider() ? touch.clientX : touch.clientY;
        const isSlide = touchLastClientX !== undefined &&
            touchLastClientY !== undefined &&
            Math.abs(touchLastClientY - y) <= Math.abs(touchLastClientX - x);
        touchLastClientX = x;
        touchLastClientY = y;
        return isSlide;
    }
    function eventWheel(e) {
        if (!isTouchable())
            return;
        if (touchActive)
            e.preventDefault();
    }
    function eventsAdd() {
        eventAdd(window, 'orientationchange', sliderResizeFix);
        eventAdd(window, 'resize', () => sliderResize());
        eventAdd(container, 'dragstart', function (e) {
            if (!isTouchable())
                return;
            e.preventDefault();
        });
        eventAdd(container, 'mousedown', eventDragStart);
        eventAdd(options.cancelOnLeave ? container : window, 'mousemove', eventDrag);
        if (options.cancelOnLeave)
            eventAdd(container, 'mouseleave', eventDragStop);
        eventAdd(window, 'mouseup', eventDragStop);
        eventAdd(container, 'touchstart', eventDragStart, {
            passive: true,
        });
        eventAdd(container, 'touchmove', eventDrag, {
            passive: false,
        });
        eventAdd(container, 'touchend', eventDragStop, {
            passive: true,
        });
        eventAdd(container, 'touchcancel', eventDragStop, {
            passive: true,
        });
        eventAdd(window, 'wheel', eventWheel, {
            passive: false,
        });
    }
    function eventsRemove() {
        events.forEach(event => {
            event[0].removeEventListener(event[1], event[2], event[3]);
        });
        events = [];
    }
    function hook(hook) {
        if (options[hook])
            options[hook](pubfuncs);
    }
    function isCenterMode() {
        return options.centered;
    }
    function isTouchable() {
        return touchControls !== undefined ? touchControls : options.controls;
    }
    function isLoop() {
        return options.loop && length > 1;
    }
    function isRtl() {
        return options.rtl;
    }
    function isRubberband() {
        return !options.loop && options.rubberband;
    }
    function isVerticalSlider() {
        return !!options.vertical;
    }
    function moveAnimate() {
        reqId = window.requestAnimationFrame(moveAnimateUpdate);
    }
    function moveAnimateAbort() {
        if (reqId) {
            window.cancelAnimationFrame(reqId);
            reqId = null;
        }
        startTime = null;
    }
    function moveAnimateUpdate(timestamp) {
        if (!startTime)
            startTime = timestamp;
        const duration = timestamp - startTime;
        let add = moveCalcValue(duration);
        if (duration >= moveDuration) {
            trackAdd(moveDistance - moved, false);
            if (moveCallBack)
                return moveCallBack();
            hook('afterChange');
            return;
        }
        const offset = trackCalculateOffset(add);
        if (offset !== 0 && !isLoop() && !isRubberband() && !moveForceFinish) {
            trackAdd(add - offset, false);
            return;
        }
        if (offset !== 0 && isRubberband() && !moveForceFinish) {
            return moveRubberband(Math.sign(offset));
        }
        moved += add;
        trackAdd(add, false);
        moveAnimate();
    }
    function moveCalcValue(progress) {
        const value = moveDistance * moveEasing(progress / moveDuration) - moved;
        return value;
    }
    function moveWithSpeed() {
        hook('beforeChange');
        switch (options.mode) {
            case 'free':
                moveFree();
                break;
            case 'free-snap':
                moveSnapFree();
                break;
            case 'snap':
            default:
                moveSnapOne();
                break;
        }
    }
    function moveSnapOne() {
        const startIndex = slidesPerView === 1 && trackDirection !== 0
            ? touchIndexStart
            : trackCurrentIdx;
        moveToIdx(startIndex + Math.sign(trackDirection));
    }
    function moveToIdx(idx, forceFinish, duration = options.duration, relative = false, nearest = false) {
        // forceFinish is used to ignore boundaries when rubberband movement is active
        idx = trackGetIdx(idx, relative, nearest);
        const easing = t => 1 + --t * t * t * t * t;
        moveTo(trackGetIdxDistance(idx), duration, easing, forceFinish);
    }
    function moveFree() {
        // todo: refactor!
        if (trackSpeed === 0)
            return trackCalculateOffset(0) && !isLoop()
                ? moveToIdx(trackCurrentIdx)
                : false;
        const friction = options.friction / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const duration = Math.abs(trackSpeed / friction) * 6;
        const easing = function (t) {
            return 1 - Math.pow(1 - t, 5);
        };
        moveTo(distance, duration, easing);
    }
    function moveSnapFree() {
        // todo: refactor!
        if (trackSpeed === 0)
            return moveToIdx(trackCurrentIdx);
        const friction = options.friction / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const duration = Math.abs(trackSpeed / friction) * 6;
        const easing = function (t) {
            return 1 - Math.pow(1 - t, 5);
        };
        const idx_trend = (trackPosition + distance) / (width / slidesPerView);
        const idx = trackDirection === -1 ? Math.floor(idx_trend) : Math.ceil(idx_trend);
        moveTo(idx * (width / slidesPerView) - trackPosition, duration, easing);
    }
    function moveRubberband() {
        moveAnimateAbort();
        // todo: refactor!
        if (trackSpeed === 0)
            return moveToIdx(trackCurrentIdx, true);
        const friction = 0.04 / Math.pow(Math.abs(trackSpeed), -0.5);
        const distance = (Math.pow(trackSpeed, 2) / friction) * Math.sign(trackSpeed);
        const easing = function (t) {
            return --t * t * t + 1;
        };
        const speed = trackSpeed;
        const cb = () => {
            moveTo(trackGetIdxDistance(trackGetIdx(trackCurrentIdx)), 500, easing, true);
        };
        moveTo(distance, Math.abs(speed / friction) * 3, easing, true, cb);
    }
    function moveTo(distance, duration, easing, forceFinish, cb) {
        moveAnimateAbort();
        moveDistance = distance;
        moved = 0;
        moveDuration = duration;
        moveEasing = easing;
        moveForceFinish = forceFinish;
        moveCallBack = cb;
        startTime = null;
        moveAnimate();
    }
    function sliderBind(force_resize) {
        let _container = getElements(initialContainer);
        if (!_container.length)
            return;
        container = _container[0];
        sliderResize(force_resize);
        eventsAdd();
        hook('mounted');
    }
    function sliderCheckBreakpoint() {
        const breakpoints = initialOptions.breakpoints || [];
        let lastValid;
        for (let value in breakpoints) {
            if (window.matchMedia(value).matches)
                lastValid = value;
        }
        if (lastValid === breakpointCurrent)
            return true;
        breakpointCurrent = lastValid;
        const _options = breakpointCurrent
            ? breakpoints[breakpointCurrent]
            : initialOptions;
        if (_options.breakpoints && breakpointCurrent)
            delete _options.breakpoints;
        options = { ...defaultOptions, ...initialOptions, ..._options };
        optionsChanged = true;
        resizeLastWidth = null;
        hook('optionsChanged');
        sliderRebind();
    }
    function sliderGetSlidesPerView(option) {
        if (typeof option === 'function')
            return option();
        const adjust = options.autoAdjustSlidesPerView;
        if (!adjust)
            length = Math.max(option, length);
        const max = isLoop() && adjust ? length - 1 : length;
        return clampValue(option, 1, Math.max(max, 1));
    }
    function sliderInit() {
        sliderCheckBreakpoint();
        sliderCreated = true;
        hook('created');
    }
    function sliderRebind(new_options, force_resize) {
        if (new_options)
            initialOptions = new_options;
        if (force_resize)
            breakpointCurrent = null;
        sliderUnbind();
        sliderBind(force_resize);
    }
    function sliderResize(force) {
        const windowWidth = window.innerWidth;
        if (!sliderCheckBreakpoint() || (windowWidth === resizeLastWidth && !force))
            return;
        resizeLastWidth = windowWidth;
        const optionSlides = options.slides;
        if (typeof optionSlides === 'number') {
            slides = null;
            length = optionSlides;
        }
        else {
            slides = getElements(optionSlides, container);
            length = slides ? slides.length : 0;
        }
        const dragSpeed = options.dragSpeed;
        touchMultiplicator =
            typeof dragSpeed === 'function' ? dragSpeed : val => val * dragSpeed;
        width = isVerticalSlider() ? container.offsetHeight : container.offsetWidth;
        slidesPerView = sliderGetSlidesPerView(options.slidesPerView);
        spacing = clampValue(options.spacing, 0, width / (slidesPerView - 1) - 1);
        width += spacing;
        origin = isCenterMode()
            ? (width / 2 - width / slidesPerView / 2) / width
            : 0;
        slidesSetWidths();
        const currentIdx = !sliderCreated || (optionsChanged && options.resetSlide)
            ? options.initial
            : trackCurrentIdx;
        trackSetPositionByIdx(isLoop() ? currentIdx : trackClampIndex(currentIdx));
        if (isVerticalSlider()) {
            container.setAttribute(attributeVertical, true);
        }
        optionsChanged = false;
    }
    function sliderResizeFix(force) {
        sliderResize();
        setTimeout(sliderResize, 500);
        setTimeout(sliderResize, 2000);
    }
    function sliderUnbind() {
        eventsRemove();
        slidesRemoveStyles();
        if (container && container.hasAttribute(attributeVertical))
            container.removeAttribute(attributeVertical);
        hook('destroyed');
    }
    function slidesSetPositions() {
        if (!slides)
            return;
        slides.forEach((slide, idx) => {
            const absoluteDistance = trackSlidePositions[idx].distance * width;
            const pos = absoluteDistance -
                idx *
                    (width / slidesPerView -
                        spacing / slidesPerView -
                        (spacing / slidesPerView) * (slidesPerView - 1));
            const x = isVerticalSlider() ? 0 : pos;
            const y = isVerticalSlider() ? pos : 0;
            const transformString = (options.scaleCenter && idx !== ((trackCurrentIdx % length) + length) % length) ? `translate3d(${x}px, ${y}px, 0) scale(0.8)` : `translate3d(${x}px, ${y}px, 0)`;
            slide.style.transform = transformString;
            slide.style.transition = 'all 0.4s ease';
            slide.style['-webkit-transform'] = transformString;
        });
    }
    function slidesSetWidths() {
        if (!slides)
            return;
        slides.forEach(slide => {
            const style = `calc(${100 / slidesPerView}% - ${(spacing / slidesPerView) * (slidesPerView - 1)}px)`;
            if (isVerticalSlider()) {
                slide.style['min-height'] = style;
                slide.style['max-height'] = style;
            }
            else {
                slide.style['min-width'] = style;
                slide.style['max-width'] = style;
            }
        });
    }
    function slidesRemoveStyles() {
        if (!slides)
            return;
        let styles = ['transform', '-webkit-transform'];
        styles = isVerticalSlider
            ? [...styles, 'min-height', 'max-height']
            : [...styles, 'min-width', 'max-width'];
        slides.forEach(slide => {
            styles.forEach(style => {
                slide.style.removeProperty(style);
            });
        });
    }
    function trackAdd(val, drag = true, timestamp = Date.now()) {
        trackMeasure(val, timestamp);
        if (drag)
            val = trackrubberband(val);
        trackPosition += val;
        trackMove();
    }
    function trackCalculateOffset(add) {
        const trackLength = (width * (length - 1 * (isCenterMode() ? 1 : slidesPerView))) /
            slidesPerView;
        const position = trackPosition + add;
        return position > trackLength
            ? position - trackLength
            : position < 0
                ? position
                : 0;
    }
    function trackClampIndex(idx) {
        return clampValue(idx, 0, length - 1 - (isCenterMode() ? 0 : slidesPerView - 1));
    }
    function trackGetDetails() {
        const trackProgressAbs = Math.abs(trackProgress);
        const progress = trackPosition < 0 ? 1 - trackProgressAbs : trackProgressAbs;
        return {
            direction: trackDirection,
            progressTrack: progress,
            progressSlides: (progress * length) / (length - 1),
            positions: trackSlidePositions,
            position: trackPosition,
            speed: trackSpeed,
            relativeSlide: ((trackCurrentIdx % length) + length) % length,
            absoluteSlide: trackCurrentIdx,
            size: length,
            slidesPerView,
            widthOrHeight: width,
        };
    }
    function trackGetIdx(idx, relative = false, nearest = false) {
        return !isLoop()
            ? trackClampIndex(idx)
            : !relative
                ? idx
                : trackGetRelativeIdx(idx, nearest);
    }
    function trackGetIdxDistance(idx) {
        return -(-((width / slidesPerView) * idx) + trackPosition);
    }
    function trackGetRelativeIdx(idx, nearest) {
        idx = ((idx % length) + length) % length;
        const current = ((trackCurrentIdx % length) + length) % length;
        const left = current < idx ? -current - length + idx : -(current - idx);
        const right = current > idx ? length - current + idx : idx - current;
        const add = nearest
            ? Math.abs(left) <= right
                ? left
                : right
            : idx < current
                ? left
                : right;
        return trackCurrentIdx + add;
    }
    function trackMeasure(val, timestamp) {
        // todo - improve measurement - it could be better for ios
        clearTimeout(trackMeasureTimeout);
        const direction = Math.sign(val);
        if (direction !== trackDirection)
            trackMeasureReset();
        trackDirection = direction;
        trackMeasurePoints.push({
            distance: val,
            time: timestamp,
        });
        trackMeasureTimeout = setTimeout(() => {
            trackMeasurePoints = [];
            trackSpeed = 0;
        }, 50);
        trackMeasurePoints = trackMeasurePoints.slice(-6);
        if (trackMeasurePoints.length <= 1 || trackDirection === 0)
            return (trackSpeed = 0);
        const distance = trackMeasurePoints
            .slice(0, -1)
            .reduce((acc, next) => acc + next.distance, 0);
        const end = trackMeasurePoints[trackMeasurePoints.length - 1].time;
        const start = trackMeasurePoints[0].time;
        trackSpeed = clampValue(distance / (end - start), -10, 10);
    }
    function trackMeasureReset() {
        trackMeasurePoints = [];
    }
    // todo - option for not calculating slides that are not in sight
    function trackMove() {
        trackProgress = isLoop()
            ? (trackPosition % ((width * length) / slidesPerView)) /
                ((width * length) / slidesPerView)
            : trackPosition / ((width * length) / slidesPerView);
        trackSetCurrentIdx();
        const slidePositions = [];
        for (let idx = 0; idx < length; idx++) {
            let distance = (((1 / length) * idx -
                (trackProgress < 0 && isLoop() ? trackProgress + 1 : trackProgress)) *
                length) /
                slidesPerView +
                origin;
            if (isLoop())
                distance +=
                    distance > (length - 1) / slidesPerView
                        ? -(length / slidesPerView)
                        : distance < -(length / slidesPerView) + 1
                            ? length / slidesPerView
                            : 0;
            const slideWidth = 1 / slidesPerView;
            const left = distance + slideWidth;
            const portion = left < slideWidth
                ? left / slideWidth
                : left > 1
                    ? 1 - ((left - 1) * slidesPerView) / 1
                    : 1;
            slidePositions.push({
                portion: portion < 0 || portion > 1 ? 0 : portion,
                distance: !isRtl() ? distance : distance * -1 + 1 - slideWidth,
            });
        }
        trackSlidePositions = slidePositions;
        slidesSetPositions();
        hook('move');
    }
    function trackrubberband(add) {
        if (isLoop())
            return add;
        const offset = trackCalculateOffset(add);
        if (!isRubberband())
            return add - offset;
        if (offset === 0)
            return add;
        const easing = t => (1 - Math.abs(t)) * (1 - Math.abs(t));
        return add * easing(offset / width);
    }
    function trackSetCurrentIdx() {
        const new_idx = Math.round(trackPosition / (width / slidesPerView));
        if (new_idx === trackCurrentIdx)
            return;
        if (!isLoop() && (new_idx < 0 || new_idx > length - 1))
            return;
        trackCurrentIdx = new_idx;
        hook('slideChanged');
    }
    function trackSetPositionByIdx(idx) {
        hook('beforeChange');
        trackAdd(trackGetIdxDistance(idx), false);
        hook('afterChange');
    }
    const defaultOptions = {
        autoAdjustSlidesPerView: true,
        centered: false,
        scaleCenter: false,
        breakpoints: null,
        controls: true,
        dragSpeed: 1,
        friction: 0.0025,
        loop: false,
        initial: 0,
        duration: 500,
        preventEvent: 'data-via-carousel-pe',
        slides: '.via-carousel__slide',
        vertical: false,
        resetSlide: false,
        slidesPerView: 1,
        spacing: 0,
        mode: 'snap',
        rtl: false,
        rubberband: true,
        cancelOnLeave: true,
    };
    const pubfuncs = {
        controls: active => {
            touchControls = active;
        },
        destroy: sliderUnbind,
        refresh(options) {
            sliderRebind(options, true);
        },
        next() {
            moveToIdx(trackCurrentIdx + 1, true);
        },
        prev() {
            moveToIdx(trackCurrentIdx - 1, true);
        },
        moveToSlide(idx, duration) {
            moveToIdx(idx, true, duration);
        },
        moveToSlideRelative(idx, nearest = false, duration) {
            moveToIdx(idx, true, duration, true, nearest);
        },
        resize() {
            sliderResize(true);
        },
        details() {
            return trackGetDetails();
        },
        options() {
            const opt = { ...options };
            delete opt.breakpoints;
            return opt;
        },
    };
    sliderInit();
    return pubfuncs;
}
export default ViaCarousel;
// helper functions
function convertToArray(nodeList) {
    return Array.prototype.slice.call(nodeList);
}
function getElements(element, wrapper = document) {
    return typeof element === 'function'
        ? convertToArray(element())
        : typeof element === 'string'
            ? convertToArray(wrapper.querySelectorAll(element))
            : element instanceof HTMLElement !== false
                ? [element]
                : element instanceof NodeList !== false
                    ? element
                    : [];
}
function clampValue(value, min, max) {
    return Math.min(Math.max(value, min), max);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvY2Fyb3VzZWwvbGliL2Nhcm91c2VsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sYUFBYSxDQUFBO0FBRXBCLFNBQVMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsR0FBRyxFQUFFO0lBQ3hELE1BQU0sZUFBZSxHQUFHLHlCQUF5QixDQUFBO0lBQ2pELE1BQU0saUJBQWlCLEdBQUcscUJBQXFCLENBQUE7SUFFL0MsSUFBSSxTQUFTLENBQUE7SUFDYixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUE7SUFDZixJQUFJLGFBQWEsQ0FBQTtJQUNqQixJQUFJLE1BQU0sQ0FBQTtJQUNWLElBQUksTUFBTSxDQUFBO0lBQ1YsSUFBSSxNQUFNLENBQUE7SUFDVixJQUFJLEtBQUssQ0FBQTtJQUNULElBQUksYUFBYSxDQUFBO0lBQ2pCLElBQUksT0FBTyxDQUFBO0lBQ1gsSUFBSSxlQUFlLENBQUE7SUFDbkIsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLENBQUE7SUFDNUIsSUFBSSxjQUFjLEdBQUcsS0FBSyxDQUFBO0lBQzFCLElBQUksYUFBYSxHQUFHLEtBQUssQ0FBQTtJQUV6QixJQUFJLGVBQWUsQ0FBQTtJQUNuQixJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUE7SUFDckIsSUFBSSxrQkFBa0IsR0FBRyxFQUFFLENBQUE7SUFDM0IsSUFBSSxjQUFjLENBQUE7SUFDbEIsSUFBSSxtQkFBbUIsQ0FBQTtJQUN2QixJQUFJLFVBQVUsQ0FBQTtJQUNkLElBQUksbUJBQW1CLENBQUE7SUFDdkIsSUFBSSxhQUFhLENBQUE7SUFFakIsSUFBSSxPQUFPLENBQUE7SUFFWCxxQkFBcUI7SUFDckIsSUFBSSxlQUFlLENBQUE7SUFDbkIsSUFBSSxXQUFXLENBQUE7SUFDZixJQUFJLGVBQWUsQ0FBQTtJQUNuQixJQUFJLFVBQVUsQ0FBQTtJQUNkLElBQUksZ0JBQWdCLENBQUE7SUFDcEIsSUFBSSxnQkFBZ0IsQ0FBQTtJQUNwQixJQUFJLGtCQUFrQixDQUFBO0lBQ3RCLElBQUksZ0JBQWdCLENBQUE7SUFDcEIsSUFBSSxnQkFBZ0IsQ0FBQTtJQUNwQixJQUFJLFlBQVksQ0FBQTtJQUVoQixZQUFZO0lBQ1osSUFBSSxLQUFLLENBQUE7SUFDVCxJQUFJLFNBQVMsQ0FBQTtJQUNiLElBQUksWUFBWSxDQUFBO0lBQ2hCLElBQUksWUFBWSxDQUFBO0lBQ2hCLElBQUksVUFBVSxDQUFBO0lBQ2QsSUFBSSxLQUFLLENBQUE7SUFDVCxJQUFJLGVBQWUsQ0FBQTtJQUNuQixJQUFJLFlBQVksQ0FBQTtJQUVoQixTQUFTLFFBQVEsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxPQUFPLEdBQUcsRUFBRTtRQUNyRCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUNqRCxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQTtJQUNqRCxDQUFDO0lBRUQsU0FBUyxTQUFTLENBQUMsQ0FBQztRQUNsQixJQUNFLENBQUMsV0FBVztZQUNaLGVBQWUsS0FBSyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDekMsQ0FBQyxXQUFXLEVBQUU7WUFFZCxPQUFNO1FBQ1IsTUFBTSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLGdCQUFnQixFQUFFLENBQUM7WUFDekMsT0FBTyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDekIsQ0FBQztRQUNELElBQUksZ0JBQWdCLEVBQUUsQ0FBQztZQUNyQixpQkFBaUIsRUFBRSxDQUFBO1lBQ25CLFVBQVUsR0FBRyxDQUFDLENBQUE7WUFDZCxnQkFBZ0IsR0FBRyxLQUFLLENBQUE7UUFDMUIsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDLFVBQVU7WUFBRSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7UUFDcEMsTUFBTSxhQUFhLEdBQUcsVUFBVSxHQUFHLENBQUMsQ0FBQTtRQUNwQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzNDLElBQUksQ0FBQyxZQUFZLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxFQUFFLENBQUM7WUFDMUMsWUFBWSxHQUFHLElBQUksQ0FBQTtZQUNuQixTQUFTLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUMvQyxDQUFDO1FBQ0QsUUFBUSxDQUNOLGtCQUFrQixDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFDakUsQ0FBQyxDQUFDLFNBQVMsQ0FDWixDQUFBO1FBRUQsVUFBVSxHQUFHLENBQUMsQ0FBQTtJQUNoQixDQUFDO0lBRUQsU0FBUyxjQUFjLENBQUMsQ0FBQztRQUN2QixJQUFJLFdBQVcsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFBRSxPQUFNO1FBQzFFLFdBQVcsR0FBRyxJQUFJLENBQUE7UUFDbEIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO1FBQ3ZCLGVBQWUsR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUN2QyxZQUFZLEdBQUcsS0FBSyxDQUFBO1FBQ3BCLGdCQUFnQixHQUFHLENBQUMsQ0FBQTtRQUNwQixZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDZixnQkFBZ0IsRUFBRSxDQUFBO1FBQ2xCLGVBQWUsR0FBRyxlQUFlLENBQUE7UUFDakMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDM0IsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ25CLENBQUM7SUFFRCxTQUFTLGFBQWEsQ0FBQyxDQUFDO1FBQ3RCLElBQ0UsQ0FBQyxXQUFXO1lBQ1osZUFBZSxLQUFLLGtCQUFrQixDQUFDLENBQUMsRUFBRSxJQUFJLENBQUM7WUFDL0MsQ0FBQyxXQUFXLEVBQUU7WUFFZCxPQUFNO1FBQ1IsU0FBUyxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUMxQyxXQUFXLEdBQUcsS0FBSyxDQUFBO1FBQ25CLGFBQWEsRUFBRSxDQUFBO1FBRWYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0lBQ2pCLENBQUM7SUFFRCxTQUFTLHNCQUFzQixDQUFDLENBQUM7UUFDL0IsT0FBTyxDQUFDLENBQUMsY0FBYyxDQUFBO0lBQ3pCLENBQUM7SUFFRCxTQUFTLGtCQUFrQixDQUFDLENBQUMsRUFBRSxjQUFjLEdBQUcsS0FBSztRQUNuRCxNQUFNLE9BQU8sR0FBRyxjQUFjO1lBQzVCLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7WUFDM0IsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQzVCLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUE7SUFDNUUsQ0FBQztJQUVELFNBQVMscUJBQXFCLENBQUMsQ0FBQztRQUM5QixPQUFPLENBQUMsQ0FBQyxhQUFhLENBQUE7SUFDeEIsQ0FBQztJQUVELFNBQVMsU0FBUyxDQUFDLENBQUM7UUFDbEIsTUFBTSxPQUFPLEdBQUcscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDeEMsT0FBTztZQUNMLENBQUMsRUFBRSxnQkFBZ0IsRUFBRTtnQkFDbkIsQ0FBQyxDQUFDLENBQUMsT0FBTztvQkFDUixDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7b0JBQ1QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO2dCQUN0QixDQUFDLENBQUMsQ0FBQyxPQUFPO29CQUNWLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDVCxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87WUFDdEIsU0FBUyxFQUFFLENBQUMsQ0FBQyxTQUFTO1NBQ3ZCLENBQUE7SUFDSCxDQUFDO0lBRUQsU0FBUyxtQkFBbUIsQ0FBQyxNQUFNO1FBQ2pDLE9BQU8sTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVELFNBQVMsWUFBWSxDQUFDLENBQUM7UUFDckIsTUFBTSxPQUFPLEdBQUcscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDeEMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLElBQUksQ0FBQTtRQUN6QixNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDeEIsTUFBTSxDQUFDLEdBQUcsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQTtRQUM1RCxNQUFNLENBQUMsR0FBRyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFBO1FBQzVELE1BQU0sT0FBTyxHQUNYLGdCQUFnQixLQUFLLFNBQVM7WUFDOUIsZ0JBQWdCLEtBQUssU0FBUztZQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFbEUsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFBO1FBQ3BCLGdCQUFnQixHQUFHLENBQUMsQ0FBQTtRQUNwQixPQUFPLE9BQU8sQ0FBQTtJQUNoQixDQUFDO0lBRUQsU0FBUyxVQUFVLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsV0FBVyxFQUFFO1lBQUUsT0FBTTtRQUMxQixJQUFJLFdBQVc7WUFBRSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7SUFDckMsQ0FBQztJQUVELFNBQVMsU0FBUztRQUNoQixRQUFRLENBQUMsTUFBTSxFQUFFLG1CQUFtQixFQUFFLGVBQWUsQ0FBQyxDQUFBO1FBQ3RELFFBQVEsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUE7UUFDaEQsUUFBUSxDQUFDLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDO1lBQzFDLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQUUsT0FBTTtZQUMxQixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7UUFDcEIsQ0FBQyxDQUFDLENBQUE7UUFDRixRQUFRLENBQUMsU0FBUyxFQUFFLFdBQVcsRUFBRSxjQUFjLENBQUMsQ0FBQTtRQUNoRCxRQUFRLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFBO1FBQzVFLElBQUksT0FBTyxDQUFDLGFBQWE7WUFBRSxRQUFRLENBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxhQUFhLENBQUMsQ0FBQTtRQUMzRSxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQTtRQUMxQyxRQUFRLENBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUU7WUFDaEQsT0FBTyxFQUFFLElBQUk7U0FDZCxDQUFDLENBQUE7UUFDRixRQUFRLENBQUMsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUU7WUFDMUMsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDLENBQUE7UUFDRixRQUFRLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUU7WUFDN0MsT0FBTyxFQUFFLElBQUk7U0FDZCxDQUFDLENBQUE7UUFDRixRQUFRLENBQUMsU0FBUyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUU7WUFDaEQsT0FBTyxFQUFFLElBQUk7U0FDZCxDQUFDLENBQUE7UUFDRixRQUFRLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUU7WUFDcEMsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsU0FBUyxZQUFZO1FBQ25CLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDckIsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDNUQsQ0FBQyxDQUFDLENBQUE7UUFDRixNQUFNLEdBQUcsRUFBRSxDQUFBO0lBQ2IsQ0FBQztJQUVELFNBQVMsSUFBSSxDQUFDLElBQUk7UUFDaEIsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQzVDLENBQUM7SUFFRCxTQUFTLFlBQVk7UUFDbkIsT0FBTyxPQUFPLENBQUMsUUFBUSxDQUFBO0lBQ3pCLENBQUM7SUFFRCxTQUFTLFdBQVc7UUFDbEIsT0FBTyxhQUFhLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUE7SUFDdkUsQ0FBQztJQUVELFNBQVMsTUFBTTtRQUNiLE9BQU8sT0FBTyxDQUFDLElBQUksSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFBO0lBQ25DLENBQUM7SUFFRCxTQUFTLEtBQUs7UUFDWixPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUE7SUFDcEIsQ0FBQztJQUVELFNBQVMsWUFBWTtRQUNuQixPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFBO0lBQzVDLENBQUM7SUFFRCxTQUFTLGdCQUFnQjtRQUN2QixPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFBO0lBQzNCLENBQUM7SUFFRCxTQUFTLFdBQVc7UUFDbEIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO0lBQ3pELENBQUM7SUFFRCxTQUFTLGdCQUFnQjtRQUN2QixJQUFJLEtBQUssRUFBRSxDQUFDO1lBQ1YsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUE7UUFDZCxDQUFDO1FBQ0QsU0FBUyxHQUFHLElBQUksQ0FBQTtJQUNsQixDQUFDO0lBRUQsU0FBUyxpQkFBaUIsQ0FBQyxTQUFTO1FBQ2xDLElBQUksQ0FBQyxTQUFTO1lBQUUsU0FBUyxHQUFHLFNBQVMsQ0FBQTtRQUNyQyxNQUFNLFFBQVEsR0FBRyxTQUFTLEdBQUcsU0FBUyxDQUFBO1FBQ3RDLElBQUksR0FBRyxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUNqQyxJQUFJLFFBQVEsSUFBSSxZQUFZLEVBQUUsQ0FBQztZQUM3QixRQUFRLENBQUMsWUFBWSxHQUFHLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQTtZQUNyQyxJQUFJLFlBQVk7Z0JBQUUsT0FBTyxZQUFZLEVBQUUsQ0FBQTtZQUN2QyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDbkIsT0FBTTtRQUNSLENBQUM7UUFFRCxNQUFNLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUN4QyxJQUFJLE1BQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDckUsUUFBUSxDQUFDLEdBQUcsR0FBRyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7WUFDN0IsT0FBTTtRQUNSLENBQUM7UUFDRCxJQUFJLE1BQU0sS0FBSyxDQUFDLElBQUksWUFBWSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN2RCxPQUFPLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUE7UUFDMUMsQ0FBQztRQUNELEtBQUssSUFBSSxHQUFHLENBQUE7UUFDWixRQUFRLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ3BCLFdBQVcsRUFBRSxDQUFBO0lBQ2YsQ0FBQztJQUVELFNBQVMsYUFBYSxDQUFDLFFBQVE7UUFDN0IsTUFBTSxLQUFLLEdBQUcsWUFBWSxHQUFHLFVBQVUsQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDLEdBQUcsS0FBSyxDQUFBO1FBQ3hFLE9BQU8sS0FBSyxDQUFBO0lBQ2QsQ0FBQztJQUVELFNBQVMsYUFBYTtRQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7UUFDcEIsUUFBUSxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDckIsS0FBSyxNQUFNO2dCQUNULFFBQVEsRUFBRSxDQUFBO2dCQUNWLE1BQUs7WUFDUCxLQUFLLFdBQVc7Z0JBQ2QsWUFBWSxFQUFFLENBQUE7Z0JBQ2QsTUFBSztZQUNQLEtBQUssTUFBTSxDQUFDO1lBQ1o7Z0JBQ0UsV0FBVyxFQUFFLENBQUE7Z0JBQ2IsTUFBSztRQUNULENBQUM7SUFDSCxDQUFDO0lBRUQsU0FBUyxXQUFXO1FBQ2xCLE1BQU0sVUFBVSxHQUNkLGFBQWEsS0FBSyxDQUFDLElBQUksY0FBYyxLQUFLLENBQUM7WUFDekMsQ0FBQyxDQUFDLGVBQWU7WUFDakIsQ0FBQyxDQUFDLGVBQWUsQ0FBQTtRQUNyQixTQUFTLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQTtJQUNuRCxDQUFDO0lBRUQsU0FBUyxTQUFTLENBQ2hCLEdBQUcsRUFDSCxXQUFXLEVBQ1gsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQzNCLFFBQVEsR0FBRyxLQUFLLEVBQ2hCLE9BQU8sR0FBRyxLQUFLO1FBRWYsOEVBQThFO1FBRTlFLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUN6QyxNQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDM0MsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUE7SUFDakUsQ0FBQztJQUVELFNBQVMsUUFBUTtRQUNmLGtCQUFrQjtRQUNsQixJQUFJLFVBQVUsS0FBSyxDQUFDO1lBQ2xCLE9BQU8sb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3pDLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDO2dCQUM1QixDQUFDLENBQUMsS0FBSyxDQUFBO1FBQ1gsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUN4RSxNQUFNLFFBQVEsR0FDWixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDOUQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3BELE1BQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQztZQUN4QixPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7UUFDL0IsQ0FBQyxDQUFBO1FBQ0QsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDcEMsQ0FBQztJQUVELFNBQVMsWUFBWTtRQUNuQixrQkFBa0I7UUFDbEIsSUFBSSxVQUFVLEtBQUssQ0FBQztZQUFFLE9BQU8sU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ3ZELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDeEUsTUFBTSxRQUFRLEdBQ1osQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQzlELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNwRCxNQUFNLE1BQU0sR0FBRyxVQUFVLENBQUM7WUFDeEIsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQy9CLENBQUMsQ0FBQTtRQUNELE1BQU0sU0FBUyxHQUFHLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxDQUFBO1FBQ3RFLE1BQU0sR0FBRyxHQUNQLGNBQWMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUN0RSxNQUFNLENBQUMsR0FBRyxHQUFHLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQyxHQUFHLGFBQWEsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDekUsQ0FBQztJQUVELFNBQVMsY0FBYztRQUNyQixnQkFBZ0IsRUFBRSxDQUFBO1FBQ2xCLGtCQUFrQjtRQUNsQixJQUFJLFVBQVUsS0FBSyxDQUFDO1lBQUUsT0FBTyxTQUFTLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQzdELE1BQU0sUUFBUSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUM1RCxNQUFNLFFBQVEsR0FDWixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7UUFFOUQsTUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDO1lBQ3hCLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDeEIsQ0FBQyxDQUFBO1FBRUQsTUFBTSxLQUFLLEdBQUcsVUFBVSxDQUFBO1FBQ3hCLE1BQU0sRUFBRSxHQUFHLEdBQUcsRUFBRTtZQUNkLE1BQU0sQ0FDSixtQkFBbUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFDakQsR0FBRyxFQUNILE1BQU0sRUFDTixJQUFJLENBQ0wsQ0FBQTtRQUNILENBQUMsQ0FBQTtRQUNELE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDcEUsQ0FBQztJQUVELFNBQVMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxFQUFFO1FBQ3pELGdCQUFnQixFQUFFLENBQUE7UUFDbEIsWUFBWSxHQUFHLFFBQVEsQ0FBQTtRQUN2QixLQUFLLEdBQUcsQ0FBQyxDQUFBO1FBQ1QsWUFBWSxHQUFHLFFBQVEsQ0FBQTtRQUN2QixVQUFVLEdBQUcsTUFBTSxDQUFBO1FBQ25CLGVBQWUsR0FBRyxXQUFXLENBQUE7UUFDN0IsWUFBWSxHQUFHLEVBQUUsQ0FBQTtRQUNqQixTQUFTLEdBQUcsSUFBSSxDQUFBO1FBQ2hCLFdBQVcsRUFBRSxDQUFBO0lBQ2YsQ0FBQztJQUVELFNBQVMsVUFBVSxDQUFDLFlBQVk7UUFDOUIsSUFBSSxVQUFVLEdBQUcsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDOUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNO1lBQUUsT0FBTTtRQUM5QixTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3pCLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUMxQixTQUFTLEVBQUUsQ0FBQTtRQUNYLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtJQUNqQixDQUFDO0lBRUQsU0FBUyxxQkFBcUI7UUFDNUIsTUFBTSxXQUFXLEdBQUcsY0FBYyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUE7UUFDcEQsSUFBSSxTQUFTLENBQUE7UUFDYixLQUFLLElBQUksS0FBSyxJQUFJLFdBQVcsRUFBRSxDQUFDO1lBQzlCLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPO2dCQUFFLFNBQVMsR0FBRyxLQUFLLENBQUE7UUFDekQsQ0FBQztRQUNELElBQUksU0FBUyxLQUFLLGlCQUFpQjtZQUFFLE9BQU8sSUFBSSxDQUFBO1FBQ2hELGlCQUFpQixHQUFHLFNBQVMsQ0FBQTtRQUM3QixNQUFNLFFBQVEsR0FBRyxpQkFBaUI7WUFDaEMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQztZQUNoQyxDQUFDLENBQUMsY0FBYyxDQUFBO1FBQ2xCLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxpQkFBaUI7WUFBRSxPQUFPLFFBQVEsQ0FBQyxXQUFXLENBQUE7UUFDMUUsT0FBTyxHQUFHLEVBQUUsR0FBRyxjQUFjLEVBQUUsR0FBRyxjQUFjLEVBQUUsR0FBRyxRQUFRLEVBQUUsQ0FBQTtRQUMvRCxjQUFjLEdBQUcsSUFBSSxDQUFBO1FBQ3JCLGVBQWUsR0FBRyxJQUFJLENBQUE7UUFFdEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDdEIsWUFBWSxFQUFFLENBQUE7SUFDaEIsQ0FBQztJQUVELFNBQVMsc0JBQXNCLENBQUMsTUFBTTtRQUNwQyxJQUFJLE9BQU8sTUFBTSxLQUFLLFVBQVU7WUFBRSxPQUFPLE1BQU0sRUFBRSxDQUFBO1FBQ2pELE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQTtRQUM5QyxJQUFJLENBQUMsTUFBTTtZQUFFLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUM5QyxNQUFNLEdBQUcsR0FBRyxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtRQUNwRCxPQUFPLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDaEQsQ0FBQztJQUVELFNBQVMsVUFBVTtRQUNqQixxQkFBcUIsRUFBRSxDQUFBO1FBQ3ZCLGFBQWEsR0FBRyxJQUFJLENBQUE7UUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0lBQ2pCLENBQUM7SUFFRCxTQUFTLFlBQVksQ0FBQyxXQUFXLEVBQUUsWUFBWTtRQUM3QyxJQUFJLFdBQVc7WUFBRSxjQUFjLEdBQUcsV0FBVyxDQUFBO1FBQzdDLElBQUksWUFBWTtZQUFFLGlCQUFpQixHQUFHLElBQUksQ0FBQTtRQUMxQyxZQUFZLEVBQUUsQ0FBQTtRQUNkLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUMxQixDQUFDO0lBRUQsU0FBUyxZQUFZLENBQUMsS0FBSztRQUN6QixNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFBO1FBQ3JDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsV0FBVyxLQUFLLGVBQWUsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUN6RSxPQUFNO1FBQ1IsZUFBZSxHQUFHLFdBQVcsQ0FBQTtRQUM3QixNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFBO1FBQ25DLElBQUksT0FBTyxZQUFZLEtBQUssUUFBUSxFQUFFLENBQUM7WUFDckMsTUFBTSxHQUFHLElBQUksQ0FBQTtZQUNiLE1BQU0sR0FBRyxZQUFZLENBQUE7UUFDdkIsQ0FBQzthQUFNLENBQUM7WUFDTixNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQTtZQUM3QyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckMsQ0FBQztRQUNELE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUE7UUFDbkMsa0JBQWtCO1lBQ2hCLE9BQU8sU0FBUyxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUE7UUFDdEUsS0FBSyxHQUFHLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUE7UUFDM0UsYUFBYSxHQUFHLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUM3RCxPQUFPLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUN6RSxLQUFLLElBQUksT0FBTyxDQUFBO1FBQ2hCLE1BQU0sR0FBRyxZQUFZLEVBQUU7WUFDckIsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxLQUFLLEdBQUcsYUFBYSxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUs7WUFDakQsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNMLGVBQWUsRUFBRSxDQUFBO1FBRWpCLE1BQU0sVUFBVSxHQUNkLENBQUMsYUFBYSxJQUFJLENBQUMsY0FBYyxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUM7WUFDdEQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPO1lBQ2pCLENBQUMsQ0FBQyxlQUFlLENBQUE7UUFDckIscUJBQXFCLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUE7UUFFMUUsSUFBSSxnQkFBZ0IsRUFBRSxFQUFFLENBQUM7WUFDdkIsU0FBUyxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUNqRCxDQUFDO1FBQ0QsY0FBYyxHQUFHLEtBQUssQ0FBQTtJQUN4QixDQUFDO0lBRUQsU0FBUyxlQUFlLENBQUMsS0FBSztRQUM1QixZQUFZLEVBQUUsQ0FBQTtRQUNkLFVBQVUsQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDN0IsVUFBVSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUNoQyxDQUFDO0lBRUQsU0FBUyxZQUFZO1FBQ25CLFlBQVksRUFBRSxDQUFBO1FBQ2Qsa0JBQWtCLEVBQUUsQ0FBQTtRQUNwQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDO1lBQ3hELFNBQVMsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7SUFDbkIsQ0FBQztJQUVELFNBQVMsa0JBQWtCO1FBQ3pCLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTTtRQUNuQixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQzVCLE1BQU0sZ0JBQWdCLEdBQUcsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtZQUNsRSxNQUFNLEdBQUcsR0FDUCxnQkFBZ0I7Z0JBQ2hCLEdBQUc7b0JBQ0QsQ0FBQyxLQUFLLEdBQUcsYUFBYTt3QkFDcEIsT0FBTyxHQUFHLGFBQWE7d0JBQ3ZCLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFdEQsTUFBTSxDQUFDLEdBQUcsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUE7WUFDdEMsTUFBTSxDQUFDLEdBQUcsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDdEMsTUFBTSxlQUFlLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN6TCxLQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUE7WUFDdkMsS0FBSyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDO1lBQ3pDLEtBQUssQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsR0FBRyxlQUFlLENBQUE7UUFDcEQsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsU0FBUyxlQUFlO1FBQ3RCLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTTtRQUNuQixNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLE1BQU0sS0FBSyxHQUFHLFFBQVEsR0FBRyxHQUFHLGFBQWEsT0FDdkMsQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUNoRCxLQUFLLENBQUE7WUFDTCxJQUFJLGdCQUFnQixFQUFFLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsR0FBRyxLQUFLLENBQUE7Z0JBQ2pDLEtBQUssQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsS0FBSyxDQUFBO1lBQ25DLENBQUM7aUJBQU0sQ0FBQztnQkFDTixLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQTtnQkFDaEMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRyxLQUFLLENBQUE7WUFDbEMsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELFNBQVMsa0JBQWtCO1FBQ3pCLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTTtRQUNuQixJQUFJLE1BQU0sR0FBRyxDQUFDLFdBQVcsRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO1FBQy9DLE1BQU0sR0FBRyxnQkFBZ0I7WUFDdkIsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLEVBQUUsWUFBWSxFQUFFLFlBQVksQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDekMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNyQixNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNyQixLQUFLLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNuQyxDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELFNBQVMsUUFBUSxDQUFDLEdBQUcsRUFBRSxJQUFJLEdBQUcsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFO1FBQ3hELFlBQVksQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUE7UUFDNUIsSUFBSSxJQUFJO1lBQUUsR0FBRyxHQUFHLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNwQyxhQUFhLElBQUksR0FBRyxDQUFBO1FBQ3BCLFNBQVMsRUFBRSxDQUFBO0lBQ2IsQ0FBQztJQUVELFNBQVMsb0JBQW9CLENBQUMsR0FBRztRQUMvQixNQUFNLFdBQVcsR0FDZixDQUFDLEtBQUssR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzdELGFBQWEsQ0FBQTtRQUNmLE1BQU0sUUFBUSxHQUFHLGFBQWEsR0FBRyxHQUFHLENBQUE7UUFDcEMsT0FBTyxRQUFRLEdBQUcsV0FBVztZQUMzQixDQUFDLENBQUMsUUFBUSxHQUFHLFdBQVc7WUFDeEIsQ0FBQyxDQUFDLFFBQVEsR0FBRyxDQUFDO2dCQUNkLENBQUMsQ0FBQyxRQUFRO2dCQUNWLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDUCxDQUFDO0lBRUQsU0FBUyxlQUFlLENBQUMsR0FBRztRQUMxQixPQUFPLFVBQVUsQ0FDZixHQUFHLEVBQ0gsQ0FBQyxFQUNELE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQ3RELENBQUE7SUFDSCxDQUFDO0lBRUQsU0FBUyxlQUFlO1FBQ3RCLE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUNoRCxNQUFNLFFBQVEsR0FBRyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFBO1FBQzVFLE9BQU87WUFDTCxTQUFTLEVBQUUsY0FBYztZQUN6QixhQUFhLEVBQUUsUUFBUTtZQUN2QixjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2xELFNBQVMsRUFBRSxtQkFBbUI7WUFDOUIsUUFBUSxFQUFFLGFBQWE7WUFDdkIsS0FBSyxFQUFFLFVBQVU7WUFDakIsYUFBYSxFQUFFLENBQUMsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTTtZQUM3RCxhQUFhLEVBQUUsZUFBZTtZQUM5QixJQUFJLEVBQUUsTUFBTTtZQUNaLGFBQWE7WUFDYixhQUFhLEVBQUUsS0FBSztTQUNyQixDQUFBO0lBQ0gsQ0FBQztJQUVELFNBQVMsV0FBVyxDQUFDLEdBQUcsRUFBRSxRQUFRLEdBQUcsS0FBSyxFQUFFLE9BQU8sR0FBRyxLQUFLO1FBQ3pELE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDZCxDQUFDLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUNYLENBQUMsQ0FBQyxHQUFHO2dCQUNMLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUE7SUFDdkMsQ0FBQztJQUVELFNBQVMsbUJBQW1CLENBQUMsR0FBRztRQUM5QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsYUFBYSxDQUFDLENBQUE7SUFDNUQsQ0FBQztJQUVELFNBQVMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLE9BQU87UUFDdkMsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFBO1FBQ3hDLE1BQU0sT0FBTyxHQUFHLENBQUMsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFBO1FBQzlELE1BQU0sSUFBSSxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUE7UUFDdkUsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUE7UUFDcEUsTUFBTSxHQUFHLEdBQUcsT0FBTztZQUNqQixDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLO2dCQUN2QixDQUFDLENBQUMsSUFBSTtnQkFDTixDQUFDLENBQUMsS0FBSztZQUNULENBQUMsQ0FBQyxHQUFHLEdBQUcsT0FBTztnQkFDZixDQUFDLENBQUMsSUFBSTtnQkFDTixDQUFDLENBQUMsS0FBSyxDQUFBO1FBQ1QsT0FBTyxlQUFlLEdBQUcsR0FBRyxDQUFBO0lBQzlCLENBQUM7SUFFRCxTQUFTLFlBQVksQ0FBQyxHQUFHLEVBQUUsU0FBUztRQUNsQywwREFBMEQ7UUFDMUQsWUFBWSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDakMsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNoQyxJQUFJLFNBQVMsS0FBSyxjQUFjO1lBQUUsaUJBQWlCLEVBQUUsQ0FBQTtRQUNyRCxjQUFjLEdBQUcsU0FBUyxDQUFBO1FBQzFCLGtCQUFrQixDQUFDLElBQUksQ0FBQztZQUN0QixRQUFRLEVBQUUsR0FBRztZQUNiLElBQUksRUFBRSxTQUFTO1NBQ2hCLENBQUMsQ0FBQTtRQUNGLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDcEMsa0JBQWtCLEdBQUcsRUFBRSxDQUFBO1lBQ3ZCLFVBQVUsR0FBRyxDQUFDLENBQUE7UUFDaEIsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ04sa0JBQWtCLEdBQUcsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDakQsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLGNBQWMsS0FBSyxDQUFDO1lBQ3hELE9BQU8sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFekIsTUFBTSxRQUFRLEdBQUcsa0JBQWtCO2FBQ2hDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDWixNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUNoRCxNQUFNLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO1FBQ2xFLE1BQU0sS0FBSyxHQUFHLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtRQUN4QyxVQUFVLEdBQUcsVUFBVSxDQUFDLFFBQVEsR0FBRyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQTtJQUM1RCxDQUFDO0lBRUQsU0FBUyxpQkFBaUI7UUFDeEIsa0JBQWtCLEdBQUcsRUFBRSxDQUFBO0lBQ3pCLENBQUM7SUFFRCxpRUFBaUU7SUFDakUsU0FBUyxTQUFTO1FBQ2hCLGFBQWEsR0FBRyxNQUFNLEVBQUU7WUFDdEIsQ0FBQyxDQUFDLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsYUFBYSxDQUFDLENBQUM7Z0JBQ3BELENBQUMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsYUFBYSxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQTtRQUV0RCxrQkFBa0IsRUFBRSxDQUFBO1FBQ3BCLE1BQU0sY0FBYyxHQUFHLEVBQUUsQ0FBQTtRQUN6QixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUM7WUFDdEMsSUFBSSxRQUFRLEdBQ1YsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLEdBQUc7Z0JBQ2xCLENBQUMsYUFBYSxHQUFHLENBQUMsSUFBSSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BFLE1BQU0sQ0FBQztnQkFDUCxhQUFhO2dCQUNmLE1BQU0sQ0FBQTtZQUNSLElBQUksTUFBTSxFQUFFO2dCQUNWLFFBQVE7b0JBQ04sUUFBUSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHLGFBQWE7d0JBQ3JDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQzt3QkFDM0IsQ0FBQyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7NEJBQzFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsYUFBYTs0QkFDeEIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUVULE1BQU0sVUFBVSxHQUFHLENBQUMsR0FBRyxhQUFhLENBQUE7WUFDcEMsTUFBTSxJQUFJLEdBQUcsUUFBUSxHQUFHLFVBQVUsQ0FBQTtZQUNsQyxNQUFNLE9BQU8sR0FDWCxJQUFJLEdBQUcsVUFBVTtnQkFDZixDQUFDLENBQUMsSUFBSSxHQUFHLFVBQVU7Z0JBQ25CLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQztvQkFDVixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQztvQkFDdEMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNQLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2xCLE9BQU8sRUFBRSxPQUFPLEdBQUcsQ0FBQyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztnQkFDakQsUUFBUSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxVQUFVO2FBQy9ELENBQUMsQ0FBQTtRQUNKLENBQUM7UUFDRCxtQkFBbUIsR0FBRyxjQUFjLENBQUE7UUFDcEMsa0JBQWtCLEVBQUUsQ0FBQTtRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDZCxDQUFDO0lBRUQsU0FBUyxlQUFlLENBQUMsR0FBRztRQUMxQixJQUFJLE1BQU0sRUFBRTtZQUFFLE9BQU8sR0FBRyxDQUFBO1FBQ3hCLE1BQU0sTUFBTSxHQUFHLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3hDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFBRSxPQUFPLEdBQUcsR0FBRyxNQUFNLENBQUE7UUFDeEMsSUFBSSxNQUFNLEtBQUssQ0FBQztZQUFFLE9BQU8sR0FBRyxDQUFBO1FBQzVCLE1BQU0sTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUN6RCxPQUFPLEdBQUcsR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFBO0lBQ3JDLENBQUM7SUFFRCxTQUFTLGtCQUFrQjtRQUN6QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFBO1FBQ25FLElBQUksT0FBTyxLQUFLLGVBQWU7WUFBRSxPQUFNO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksT0FBTyxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFBRSxPQUFNO1FBQzlELGVBQWUsR0FBRyxPQUFPLENBQUE7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO0lBQ3RCLENBQUM7SUFFRCxTQUFTLHFCQUFxQixDQUFDLEdBQUc7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3BCLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7SUFDckIsQ0FBQztJQUVELE1BQU0sY0FBYyxHQUFHO1FBQ3JCLHVCQUF1QixFQUFFLElBQUk7UUFDN0IsUUFBUSxFQUFFLEtBQUs7UUFDZixXQUFXLEVBQUUsS0FBSztRQUNsQixXQUFXLEVBQUUsSUFBSTtRQUNqQixRQUFRLEVBQUUsSUFBSTtRQUNkLFNBQVMsRUFBRSxDQUFDO1FBQ1osUUFBUSxFQUFFLE1BQU07UUFDaEIsSUFBSSxFQUFFLEtBQUs7UUFDWCxPQUFPLEVBQUUsQ0FBQztRQUNWLFFBQVEsRUFBRSxHQUFHO1FBQ2IsWUFBWSxFQUFFLHNCQUFzQjtRQUNwQyxNQUFNLEVBQUUsc0JBQXNCO1FBQzlCLFFBQVEsRUFBRSxLQUFLO1FBQ2YsVUFBVSxFQUFFLEtBQUs7UUFDakIsYUFBYSxFQUFFLENBQUM7UUFDaEIsT0FBTyxFQUFFLENBQUM7UUFDVixJQUFJLEVBQUUsTUFBTTtRQUNaLEdBQUcsRUFBRSxLQUFLO1FBQ1YsVUFBVSxFQUFFLElBQUk7UUFDaEIsYUFBYSxFQUFFLElBQUk7S0FDcEIsQ0FBQTtJQUVELE1BQU0sUUFBUSxHQUFHO1FBQ2YsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ2pCLGFBQWEsR0FBRyxNQUFNLENBQUE7UUFDeEIsQ0FBQztRQUNELE9BQU8sRUFBRSxZQUFZO1FBQ3JCLE9BQU8sQ0FBQyxPQUFPO1lBQ2IsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUM3QixDQUFDO1FBQ0QsSUFBSTtZQUNGLFNBQVMsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ3RDLENBQUM7UUFDRCxJQUFJO1lBQ0YsU0FBUyxDQUFDLGVBQWUsR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDdEMsQ0FBQztRQUNELFdBQVcsQ0FBQyxHQUFHLEVBQUUsUUFBUTtZQUN2QixTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUNoQyxDQUFDO1FBQ0QsbUJBQW1CLENBQUMsR0FBRyxFQUFFLE9BQU8sR0FBRyxLQUFLLEVBQUUsUUFBUTtZQUNoRCxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBQy9DLENBQUM7UUFDRCxNQUFNO1lBQ0osWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ3BCLENBQUM7UUFDRCxPQUFPO1lBQ0wsT0FBTyxlQUFlLEVBQUUsQ0FBQTtRQUMxQixDQUFDO1FBQ0QsT0FBTztZQUNMLE1BQU0sR0FBRyxHQUFHLEVBQUUsR0FBRyxPQUFPLEVBQUUsQ0FBQTtZQUMxQixPQUFPLEdBQUcsQ0FBQyxXQUFXLENBQUE7WUFDdEIsT0FBTyxHQUFHLENBQUE7UUFDWixDQUFDO0tBQ0YsQ0FBQTtJQUVELFVBQVUsRUFBRSxDQUFBO0lBRVosT0FBTyxRQUFRLENBQUE7QUFDakIsQ0FBQztBQUVELGVBQWUsV0FBVyxDQUFBO0FBRTFCLG1CQUFtQjtBQUVuQixTQUFTLGNBQWMsQ0FBQyxRQUFRO0lBQzlCLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQzdDLENBQUM7QUFFRCxTQUFTLFdBQVcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxHQUFHLFFBQVE7SUFDOUMsT0FBTyxPQUFPLE9BQU8sS0FBSyxVQUFVO1FBQ2xDLENBQUMsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDLE9BQU8sT0FBTyxLQUFLLFFBQVE7WUFDN0IsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxDQUFDLE9BQU8sWUFBWSxXQUFXLEtBQUssS0FBSztnQkFDMUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUNYLENBQUMsQ0FBQyxPQUFPLFlBQVksUUFBUSxLQUFLLEtBQUs7b0JBQ3ZDLENBQUMsQ0FBQyxPQUFPO29CQUNULENBQUMsQ0FBQyxFQUFFLENBQUE7QUFDUixDQUFDO0FBRUQsU0FBUyxVQUFVLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxHQUFHO0lBQ2pDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtBQUM1QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICcuL3BvbHlmaWxscydcblxuZnVuY3Rpb24gVmlhQ2Fyb3VzZWwoaW5pdGlhbENvbnRhaW5lciwgaW5pdGlhbE9wdGlvbnMgPSB7fSkge1xuICBjb25zdCBhdHRyaWJ1dGVNb3ZpbmcgPSAnZGF0YS12aWEtY2Fyb3VzZWwtbW92ZXMnXG4gIGNvbnN0IGF0dHJpYnV0ZVZlcnRpY2FsID0gJ2RhdGEtdmlhLWNhcm91c2VsLXYnXG5cbiAgbGV0IGNvbnRhaW5lclxuICBsZXQgZXZlbnRzID0gW11cbiAgbGV0IHRvdWNoQ29udHJvbHNcbiAgbGV0IGxlbmd0aFxuICBsZXQgb3JpZ2luXG4gIGxldCBzbGlkZXNcbiAgbGV0IHdpZHRoXG4gIGxldCBzbGlkZXNQZXJWaWV3XG4gIGxldCBzcGFjaW5nXG4gIGxldCByZXNpemVMYXN0V2lkdGhcbiAgbGV0IGJyZWFrcG9pbnRDdXJyZW50ID0gbnVsbFxuICBsZXQgb3B0aW9uc0NoYW5nZWQgPSBmYWxzZVxuICBsZXQgc2xpZGVyQ3JlYXRlZCA9IGZhbHNlXG5cbiAgbGV0IHRyYWNrQ3VycmVudElkeFxuICBsZXQgdHJhY2tQb3NpdGlvbiA9IDBcbiAgbGV0IHRyYWNrTWVhc3VyZVBvaW50cyA9IFtdXG4gIGxldCB0cmFja0RpcmVjdGlvblxuICBsZXQgdHJhY2tNZWFzdXJlVGltZW91dFxuICBsZXQgdHJhY2tTcGVlZFxuICBsZXQgdHJhY2tTbGlkZVBvc2l0aW9uc1xuICBsZXQgdHJhY2tQcm9ncmVzc1xuXG4gIGxldCBvcHRpb25zXG5cbiAgLy8gdG91Y2gvc3dpcGUgaGVscGVyXG4gIGxldCB0b3VjaEluZGV4U3RhcnRcbiAgbGV0IHRvdWNoQWN0aXZlXG4gIGxldCB0b3VjaElkZW50aWZpZXJcbiAgbGV0IHRvdWNoTGFzdFhcbiAgbGV0IHRvdWNoTGFzdENsaWVudFhcbiAgbGV0IHRvdWNoTGFzdENsaWVudFlcbiAgbGV0IHRvdWNoTXVsdGlwbGljYXRvclxuICBsZXQgdG91Y2hKdXN0U3RhcnRlZFxuICBsZXQgdG91Y2hTdW1EaXN0YW5jZVxuICBsZXQgdG91Y2hDaGVja2VkXG5cbiAgLy8gYW5pbWF0aW9uXG4gIGxldCByZXFJZFxuICBsZXQgc3RhcnRUaW1lXG4gIGxldCBtb3ZlRGlzdGFuY2VcbiAgbGV0IG1vdmVEdXJhdGlvblxuICBsZXQgbW92ZUVhc2luZ1xuICBsZXQgbW92ZWRcbiAgbGV0IG1vdmVGb3JjZUZpbmlzaFxuICBsZXQgbW92ZUNhbGxCYWNrXG5cbiAgZnVuY3Rpb24gZXZlbnRBZGQoZWxlbWVudCwgZXZlbnQsIGhhbmRsZXIsIG9wdGlvbnMgPSB7fSkge1xuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlciwgb3B0aW9ucylcbiAgICBldmVudHMucHVzaChbZWxlbWVudCwgZXZlbnQsIGhhbmRsZXIsIG9wdGlvbnNdKVxuICB9XG5cbiAgZnVuY3Rpb24gZXZlbnREcmFnKGUpIHtcbiAgICBpZiAoXG4gICAgICAhdG91Y2hBY3RpdmUgfHxcbiAgICAgIHRvdWNoSWRlbnRpZmllciAhPT0gZXZlbnRHZXRJZGVudGlmaWVyKGUpIHx8XG4gICAgICAhaXNUb3VjaGFibGUoKVxuICAgIClcbiAgICAgIHJldHVyblxuICAgIGNvbnN0IHggPSBldmVudEdldFgoZSkueFxuICAgIGlmICghZXZlbnRJc1NsaWRlKGUpICYmIHRvdWNoSnVzdFN0YXJ0ZWQpIHtcbiAgICAgIHJldHVybiBldmVudERyYWdTdG9wKGUpXG4gICAgfVxuICAgIGlmICh0b3VjaEp1c3RTdGFydGVkKSB7XG4gICAgICB0cmFja01lYXN1cmVSZXNldCgpXG4gICAgICB0b3VjaExhc3RYID0geFxuICAgICAgdG91Y2hKdXN0U3RhcnRlZCA9IGZhbHNlXG4gICAgfVxuICAgIGlmIChlLmNhbmNlbGFibGUpIGUucHJldmVudERlZmF1bHQoKVxuICAgIGNvbnN0IHRvdWNoRGlzdGFuY2UgPSB0b3VjaExhc3RYIC0geFxuICAgIHRvdWNoU3VtRGlzdGFuY2UgKz0gTWF0aC5hYnModG91Y2hEaXN0YW5jZSlcbiAgICBpZiAoIXRvdWNoQ2hlY2tlZCAmJiB0b3VjaFN1bURpc3RhbmNlID4gNSkge1xuICAgICAgdG91Y2hDaGVja2VkID0gdHJ1ZVxuICAgICAgY29udGFpbmVyLnNldEF0dHJpYnV0ZShhdHRyaWJ1dGVNb3ZpbmcsIHRydWUpXG4gICAgfVxuICAgIHRyYWNrQWRkKFxuICAgICAgdG91Y2hNdWx0aXBsaWNhdG9yKHRvdWNoRGlzdGFuY2UsIHB1YmZ1bmNzKSAqICghaXNSdGwoKSA/IDEgOiAtMSksXG4gICAgICBlLnRpbWVTdGFtcFxuICAgIClcblxuICAgIHRvdWNoTGFzdFggPSB4XG4gIH1cblxuICBmdW5jdGlvbiBldmVudERyYWdTdGFydChlKSB7XG4gICAgaWYgKHRvdWNoQWN0aXZlIHx8ICFpc1RvdWNoYWJsZSgpIHx8IGV2ZW50SXNJZ25vcmVUYXJnZXQoZS50YXJnZXQpKSByZXR1cm5cbiAgICB0b3VjaEFjdGl2ZSA9IHRydWVcbiAgICB0b3VjaEp1c3RTdGFydGVkID0gdHJ1ZVxuICAgIHRvdWNoSWRlbnRpZmllciA9IGV2ZW50R2V0SWRlbnRpZmllcihlKVxuICAgIHRvdWNoQ2hlY2tlZCA9IGZhbHNlXG4gICAgdG91Y2hTdW1EaXN0YW5jZSA9IDBcbiAgICBldmVudElzU2xpZGUoZSlcbiAgICBtb3ZlQW5pbWF0ZUFib3J0KClcbiAgICB0b3VjaEluZGV4U3RhcnQgPSB0cmFja0N1cnJlbnRJZHhcbiAgICB0b3VjaExhc3RYID0gZXZlbnRHZXRYKGUpLnhcbiAgICB0cmFja0FkZCgwLCBlLnRpbWVTdGFtcClcbiAgICBob29rKCdkcmFnU3RhcnQnKVxuICB9XG5cbiAgZnVuY3Rpb24gZXZlbnREcmFnU3RvcChlKSB7XG4gICAgaWYgKFxuICAgICAgIXRvdWNoQWN0aXZlIHx8XG4gICAgICB0b3VjaElkZW50aWZpZXIgIT09IGV2ZW50R2V0SWRlbnRpZmllcihlLCB0cnVlKSB8fFxuICAgICAgIWlzVG91Y2hhYmxlKClcbiAgICApXG4gICAgICByZXR1cm5cbiAgICBjb250YWluZXIucmVtb3ZlQXR0cmlidXRlKGF0dHJpYnV0ZU1vdmluZylcbiAgICB0b3VjaEFjdGl2ZSA9IGZhbHNlXG4gICAgbW92ZVdpdGhTcGVlZCgpXG5cbiAgICBob29rKCdkcmFnRW5kJylcbiAgfVxuXG4gIGZ1bmN0aW9uIGV2ZW50R2V0Q2hhbmdlZFRvdWNoZXMoZSkge1xuICAgIHJldHVybiBlLmNoYW5nZWRUb3VjaGVzXG4gIH1cblxuICBmdW5jdGlvbiBldmVudEdldElkZW50aWZpZXIoZSwgY2hhbmdlZFRvdWNoZXMgPSBmYWxzZSkge1xuICAgIGNvbnN0IHRvdWNoZXMgPSBjaGFuZ2VkVG91Y2hlc1xuICAgICAgPyBldmVudEdldENoYW5nZWRUb3VjaGVzKGUpXG4gICAgICA6IGV2ZW50R2V0VGFyZ2V0VG91Y2hlcyhlKVxuICAgIHJldHVybiAhdG91Y2hlcyA/ICdkZWZhdWx0JyA6IHRvdWNoZXNbMF0gPyB0b3VjaGVzWzBdLmlkZW50aWZpZXIgOiAnZXJyb3InXG4gIH1cblxuICBmdW5jdGlvbiBldmVudEdldFRhcmdldFRvdWNoZXMoZSkge1xuICAgIHJldHVybiBlLnRhcmdldFRvdWNoZXNcbiAgfVxuXG4gIGZ1bmN0aW9uIGV2ZW50R2V0WChlKSB7XG4gICAgY29uc3QgdG91Y2hlcyA9IGV2ZW50R2V0VGFyZ2V0VG91Y2hlcyhlKVxuICAgIHJldHVybiB7XG4gICAgICB4OiBpc1ZlcnRpY2FsU2xpZGVyKClcbiAgICAgICAgPyAhdG91Y2hlc1xuICAgICAgICAgID8gZS5wYWdlWVxuICAgICAgICAgIDogdG91Y2hlc1swXS5zY3JlZW5ZXG4gICAgICAgIDogIXRvdWNoZXNcbiAgICAgICAgPyBlLnBhZ2VYXG4gICAgICAgIDogdG91Y2hlc1swXS5zY3JlZW5YLFxuICAgICAgdGltZXN0YW1wOiBlLnRpbWVTdGFtcCxcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBldmVudElzSWdub3JlVGFyZ2V0KHRhcmdldCkge1xuICAgIHJldHVybiB0YXJnZXQuaGFzQXR0cmlidXRlKG9wdGlvbnMucHJldmVudEV2ZW50KVxuICB9XG5cbiAgZnVuY3Rpb24gZXZlbnRJc1NsaWRlKGUpIHtcbiAgICBjb25zdCB0b3VjaGVzID0gZXZlbnRHZXRUYXJnZXRUb3VjaGVzKGUpXG4gICAgaWYgKCF0b3VjaGVzKSByZXR1cm4gdHJ1ZVxuICAgIGNvbnN0IHRvdWNoID0gdG91Y2hlc1swXVxuICAgIGNvbnN0IHggPSBpc1ZlcnRpY2FsU2xpZGVyKCkgPyB0b3VjaC5jbGllbnRZIDogdG91Y2guY2xpZW50WFxuICAgIGNvbnN0IHkgPSBpc1ZlcnRpY2FsU2xpZGVyKCkgPyB0b3VjaC5jbGllbnRYIDogdG91Y2guY2xpZW50WVxuICAgIGNvbnN0IGlzU2xpZGUgPVxuICAgICAgdG91Y2hMYXN0Q2xpZW50WCAhPT0gdW5kZWZpbmVkICYmXG4gICAgICB0b3VjaExhc3RDbGllbnRZICE9PSB1bmRlZmluZWQgJiZcbiAgICAgIE1hdGguYWJzKHRvdWNoTGFzdENsaWVudFkgLSB5KSA8PSBNYXRoLmFicyh0b3VjaExhc3RDbGllbnRYIC0geClcblxuICAgIHRvdWNoTGFzdENsaWVudFggPSB4XG4gICAgdG91Y2hMYXN0Q2xpZW50WSA9IHlcbiAgICByZXR1cm4gaXNTbGlkZVxuICB9XG5cbiAgZnVuY3Rpb24gZXZlbnRXaGVlbChlKSB7XG4gICAgaWYgKCFpc1RvdWNoYWJsZSgpKSByZXR1cm5cbiAgICBpZiAodG91Y2hBY3RpdmUpIGUucHJldmVudERlZmF1bHQoKVxuICB9XG5cbiAgZnVuY3Rpb24gZXZlbnRzQWRkKCkge1xuICAgIGV2ZW50QWRkKHdpbmRvdywgJ29yaWVudGF0aW9uY2hhbmdlJywgc2xpZGVyUmVzaXplRml4KVxuICAgIGV2ZW50QWRkKHdpbmRvdywgJ3Jlc2l6ZScsICgpID0+IHNsaWRlclJlc2l6ZSgpKVxuICAgIGV2ZW50QWRkKGNvbnRhaW5lciwgJ2RyYWdzdGFydCcsIGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoIWlzVG91Y2hhYmxlKCkpIHJldHVyblxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgfSlcbiAgICBldmVudEFkZChjb250YWluZXIsICdtb3VzZWRvd24nLCBldmVudERyYWdTdGFydClcbiAgICBldmVudEFkZChvcHRpb25zLmNhbmNlbE9uTGVhdmUgPyBjb250YWluZXIgOiB3aW5kb3csICdtb3VzZW1vdmUnLCBldmVudERyYWcpXG4gICAgaWYgKG9wdGlvbnMuY2FuY2VsT25MZWF2ZSkgZXZlbnRBZGQoY29udGFpbmVyLCAnbW91c2VsZWF2ZScsIGV2ZW50RHJhZ1N0b3ApXG4gICAgZXZlbnRBZGQod2luZG93LCAnbW91c2V1cCcsIGV2ZW50RHJhZ1N0b3ApXG4gICAgZXZlbnRBZGQoY29udGFpbmVyLCAndG91Y2hzdGFydCcsIGV2ZW50RHJhZ1N0YXJ0LCB7XG4gICAgICBwYXNzaXZlOiB0cnVlLFxuICAgIH0pXG4gICAgZXZlbnRBZGQoY29udGFpbmVyLCAndG91Y2htb3ZlJywgZXZlbnREcmFnLCB7XG4gICAgICBwYXNzaXZlOiBmYWxzZSxcbiAgICB9KVxuICAgIGV2ZW50QWRkKGNvbnRhaW5lciwgJ3RvdWNoZW5kJywgZXZlbnREcmFnU3RvcCwge1xuICAgICAgcGFzc2l2ZTogdHJ1ZSxcbiAgICB9KVxuICAgIGV2ZW50QWRkKGNvbnRhaW5lciwgJ3RvdWNoY2FuY2VsJywgZXZlbnREcmFnU3RvcCwge1xuICAgICAgcGFzc2l2ZTogdHJ1ZSxcbiAgICB9KVxuICAgIGV2ZW50QWRkKHdpbmRvdywgJ3doZWVsJywgZXZlbnRXaGVlbCwge1xuICAgICAgcGFzc2l2ZTogZmFsc2UsXG4gICAgfSlcbiAgfVxuXG4gIGZ1bmN0aW9uIGV2ZW50c1JlbW92ZSgpIHtcbiAgICBldmVudHMuZm9yRWFjaChldmVudCA9PiB7XG4gICAgICBldmVudFswXS5yZW1vdmVFdmVudExpc3RlbmVyKGV2ZW50WzFdLCBldmVudFsyXSwgZXZlbnRbM10pXG4gICAgfSlcbiAgICBldmVudHMgPSBbXVxuICB9XG5cbiAgZnVuY3Rpb24gaG9vayhob29rKSB7XG4gICAgaWYgKG9wdGlvbnNbaG9va10pIG9wdGlvbnNbaG9va10ocHViZnVuY3MpXG4gIH1cblxuICBmdW5jdGlvbiBpc0NlbnRlck1vZGUoKSB7XG4gICAgcmV0dXJuIG9wdGlvbnMuY2VudGVyZWRcbiAgfVxuXG4gIGZ1bmN0aW9uIGlzVG91Y2hhYmxlKCkge1xuICAgIHJldHVybiB0b3VjaENvbnRyb2xzICE9PSB1bmRlZmluZWQgPyB0b3VjaENvbnRyb2xzIDogb3B0aW9ucy5jb250cm9sc1xuICB9XG5cbiAgZnVuY3Rpb24gaXNMb29wKCkge1xuICAgIHJldHVybiBvcHRpb25zLmxvb3AgJiYgbGVuZ3RoID4gMVxuICB9XG5cbiAgZnVuY3Rpb24gaXNSdGwoKSB7XG4gICAgcmV0dXJuIG9wdGlvbnMucnRsXG4gIH1cblxuICBmdW5jdGlvbiBpc1J1YmJlcmJhbmQoKSB7XG4gICAgcmV0dXJuICFvcHRpb25zLmxvb3AgJiYgb3B0aW9ucy5ydWJiZXJiYW5kXG4gIH1cblxuICBmdW5jdGlvbiBpc1ZlcnRpY2FsU2xpZGVyKCkge1xuICAgIHJldHVybiAhIW9wdGlvbnMudmVydGljYWxcbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVBbmltYXRlKCkge1xuICAgIHJlcUlkID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShtb3ZlQW5pbWF0ZVVwZGF0ZSlcbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVBbmltYXRlQWJvcnQoKSB7XG4gICAgaWYgKHJlcUlkKSB7XG4gICAgICB3aW5kb3cuY2FuY2VsQW5pbWF0aW9uRnJhbWUocmVxSWQpXG4gICAgICByZXFJZCA9IG51bGxcbiAgICB9XG4gICAgc3RhcnRUaW1lID0gbnVsbFxuICB9XG5cbiAgZnVuY3Rpb24gbW92ZUFuaW1hdGVVcGRhdGUodGltZXN0YW1wKSB7XG4gICAgaWYgKCFzdGFydFRpbWUpIHN0YXJ0VGltZSA9IHRpbWVzdGFtcFxuICAgIGNvbnN0IGR1cmF0aW9uID0gdGltZXN0YW1wIC0gc3RhcnRUaW1lXG4gICAgbGV0IGFkZCA9IG1vdmVDYWxjVmFsdWUoZHVyYXRpb24pXG4gICAgaWYgKGR1cmF0aW9uID49IG1vdmVEdXJhdGlvbikge1xuICAgICAgdHJhY2tBZGQobW92ZURpc3RhbmNlIC0gbW92ZWQsIGZhbHNlKVxuICAgICAgaWYgKG1vdmVDYWxsQmFjaykgcmV0dXJuIG1vdmVDYWxsQmFjaygpXG4gICAgICBob29rKCdhZnRlckNoYW5nZScpXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBjb25zdCBvZmZzZXQgPSB0cmFja0NhbGN1bGF0ZU9mZnNldChhZGQpXG4gICAgaWYgKG9mZnNldCAhPT0gMCAmJiAhaXNMb29wKCkgJiYgIWlzUnViYmVyYmFuZCgpICYmICFtb3ZlRm9yY2VGaW5pc2gpIHtcbiAgICAgIHRyYWNrQWRkKGFkZCAtIG9mZnNldCwgZmFsc2UpXG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgaWYgKG9mZnNldCAhPT0gMCAmJiBpc1J1YmJlcmJhbmQoKSAmJiAhbW92ZUZvcmNlRmluaXNoKSB7XG4gICAgICByZXR1cm4gbW92ZVJ1YmJlcmJhbmQoTWF0aC5zaWduKG9mZnNldCkpXG4gICAgfVxuICAgIG1vdmVkICs9IGFkZFxuICAgIHRyYWNrQWRkKGFkZCwgZmFsc2UpXG4gICAgbW92ZUFuaW1hdGUoKVxuICB9XG5cbiAgZnVuY3Rpb24gbW92ZUNhbGNWYWx1ZShwcm9ncmVzcykge1xuICAgIGNvbnN0IHZhbHVlID0gbW92ZURpc3RhbmNlICogbW92ZUVhc2luZyhwcm9ncmVzcyAvIG1vdmVEdXJhdGlvbikgLSBtb3ZlZFxuICAgIHJldHVybiB2YWx1ZVxuICB9XG5cbiAgZnVuY3Rpb24gbW92ZVdpdGhTcGVlZCgpIHtcbiAgICBob29rKCdiZWZvcmVDaGFuZ2UnKVxuICAgIHN3aXRjaCAob3B0aW9ucy5tb2RlKSB7XG4gICAgICBjYXNlICdmcmVlJzpcbiAgICAgICAgbW92ZUZyZWUoKVxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnZnJlZS1zbmFwJzpcbiAgICAgICAgbW92ZVNuYXBGcmVlKClcbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ3NuYXAnOlxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgbW92ZVNuYXBPbmUoKVxuICAgICAgICBicmVha1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVTbmFwT25lKCkge1xuICAgIGNvbnN0IHN0YXJ0SW5kZXggPVxuICAgICAgc2xpZGVzUGVyVmlldyA9PT0gMSAmJiB0cmFja0RpcmVjdGlvbiAhPT0gMFxuICAgICAgICA/IHRvdWNoSW5kZXhTdGFydFxuICAgICAgICA6IHRyYWNrQ3VycmVudElkeFxuICAgIG1vdmVUb0lkeChzdGFydEluZGV4ICsgTWF0aC5zaWduKHRyYWNrRGlyZWN0aW9uKSlcbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVUb0lkeChcbiAgICBpZHgsXG4gICAgZm9yY2VGaW5pc2gsXG4gICAgZHVyYXRpb24gPSBvcHRpb25zLmR1cmF0aW9uLFxuICAgIHJlbGF0aXZlID0gZmFsc2UsXG4gICAgbmVhcmVzdCA9IGZhbHNlXG4gICkge1xuICAgIC8vIGZvcmNlRmluaXNoIGlzIHVzZWQgdG8gaWdub3JlIGJvdW5kYXJpZXMgd2hlbiBydWJiZXJiYW5kIG1vdmVtZW50IGlzIGFjdGl2ZVxuXG4gICAgaWR4ID0gdHJhY2tHZXRJZHgoaWR4LCByZWxhdGl2ZSwgbmVhcmVzdClcbiAgICBjb25zdCBlYXNpbmcgPSB0ID0+IDEgKyAtLXQgKiB0ICogdCAqIHQgKiB0XG4gICAgbW92ZVRvKHRyYWNrR2V0SWR4RGlzdGFuY2UoaWR4KSwgZHVyYXRpb24sIGVhc2luZywgZm9yY2VGaW5pc2gpXG4gIH1cblxuICBmdW5jdGlvbiBtb3ZlRnJlZSgpIHtcbiAgICAvLyB0b2RvOiByZWZhY3RvciFcbiAgICBpZiAodHJhY2tTcGVlZCA9PT0gMClcbiAgICAgIHJldHVybiB0cmFja0NhbGN1bGF0ZU9mZnNldCgwKSAmJiAhaXNMb29wKClcbiAgICAgICAgPyBtb3ZlVG9JZHgodHJhY2tDdXJyZW50SWR4KVxuICAgICAgICA6IGZhbHNlXG4gICAgY29uc3QgZnJpY3Rpb24gPSBvcHRpb25zLmZyaWN0aW9uIC8gTWF0aC5wb3coTWF0aC5hYnModHJhY2tTcGVlZCksIC0wLjUpXG4gICAgY29uc3QgZGlzdGFuY2UgPVxuICAgICAgKE1hdGgucG93KHRyYWNrU3BlZWQsIDIpIC8gZnJpY3Rpb24pICogTWF0aC5zaWduKHRyYWNrU3BlZWQpXG4gICAgY29uc3QgZHVyYXRpb24gPSBNYXRoLmFicyh0cmFja1NwZWVkIC8gZnJpY3Rpb24pICogNlxuICAgIGNvbnN0IGVhc2luZyA9IGZ1bmN0aW9uICh0KSB7XG4gICAgICByZXR1cm4gMSAtIE1hdGgucG93KDEgLSB0LCA1KVxuICAgIH1cbiAgICBtb3ZlVG8oZGlzdGFuY2UsIGR1cmF0aW9uLCBlYXNpbmcpXG4gIH1cblxuICBmdW5jdGlvbiBtb3ZlU25hcEZyZWUoKSB7XG4gICAgLy8gdG9kbzogcmVmYWN0b3IhXG4gICAgaWYgKHRyYWNrU3BlZWQgPT09IDApIHJldHVybiBtb3ZlVG9JZHgodHJhY2tDdXJyZW50SWR4KVxuICAgIGNvbnN0IGZyaWN0aW9uID0gb3B0aW9ucy5mcmljdGlvbiAvIE1hdGgucG93KE1hdGguYWJzKHRyYWNrU3BlZWQpLCAtMC41KVxuICAgIGNvbnN0IGRpc3RhbmNlID1cbiAgICAgIChNYXRoLnBvdyh0cmFja1NwZWVkLCAyKSAvIGZyaWN0aW9uKSAqIE1hdGguc2lnbih0cmFja1NwZWVkKVxuICAgIGNvbnN0IGR1cmF0aW9uID0gTWF0aC5hYnModHJhY2tTcGVlZCAvIGZyaWN0aW9uKSAqIDZcbiAgICBjb25zdCBlYXNpbmcgPSBmdW5jdGlvbiAodCkge1xuICAgICAgcmV0dXJuIDEgLSBNYXRoLnBvdygxIC0gdCwgNSlcbiAgICB9XG4gICAgY29uc3QgaWR4X3RyZW5kID0gKHRyYWNrUG9zaXRpb24gKyBkaXN0YW5jZSkgLyAod2lkdGggLyBzbGlkZXNQZXJWaWV3KVxuICAgIGNvbnN0IGlkeCA9XG4gICAgICB0cmFja0RpcmVjdGlvbiA9PT0gLTEgPyBNYXRoLmZsb29yKGlkeF90cmVuZCkgOiBNYXRoLmNlaWwoaWR4X3RyZW5kKVxuICAgIG1vdmVUbyhpZHggKiAod2lkdGggLyBzbGlkZXNQZXJWaWV3KSAtIHRyYWNrUG9zaXRpb24sIGR1cmF0aW9uLCBlYXNpbmcpXG4gIH1cblxuICBmdW5jdGlvbiBtb3ZlUnViYmVyYmFuZCgpIHtcbiAgICBtb3ZlQW5pbWF0ZUFib3J0KClcbiAgICAvLyB0b2RvOiByZWZhY3RvciFcbiAgICBpZiAodHJhY2tTcGVlZCA9PT0gMCkgcmV0dXJuIG1vdmVUb0lkeCh0cmFja0N1cnJlbnRJZHgsIHRydWUpXG4gICAgY29uc3QgZnJpY3Rpb24gPSAwLjA0IC8gTWF0aC5wb3coTWF0aC5hYnModHJhY2tTcGVlZCksIC0wLjUpXG4gICAgY29uc3QgZGlzdGFuY2UgPVxuICAgICAgKE1hdGgucG93KHRyYWNrU3BlZWQsIDIpIC8gZnJpY3Rpb24pICogTWF0aC5zaWduKHRyYWNrU3BlZWQpXG5cbiAgICBjb25zdCBlYXNpbmcgPSBmdW5jdGlvbiAodCkge1xuICAgICAgcmV0dXJuIC0tdCAqIHQgKiB0ICsgMVxuICAgIH1cblxuICAgIGNvbnN0IHNwZWVkID0gdHJhY2tTcGVlZFxuICAgIGNvbnN0IGNiID0gKCkgPT4ge1xuICAgICAgbW92ZVRvKFxuICAgICAgICB0cmFja0dldElkeERpc3RhbmNlKHRyYWNrR2V0SWR4KHRyYWNrQ3VycmVudElkeCkpLFxuICAgICAgICA1MDAsXG4gICAgICAgIGVhc2luZyxcbiAgICAgICAgdHJ1ZVxuICAgICAgKVxuICAgIH1cbiAgICBtb3ZlVG8oZGlzdGFuY2UsIE1hdGguYWJzKHNwZWVkIC8gZnJpY3Rpb24pICogMywgZWFzaW5nLCB0cnVlLCBjYilcbiAgfVxuXG4gIGZ1bmN0aW9uIG1vdmVUbyhkaXN0YW5jZSwgZHVyYXRpb24sIGVhc2luZywgZm9yY2VGaW5pc2gsIGNiKSB7XG4gICAgbW92ZUFuaW1hdGVBYm9ydCgpXG4gICAgbW92ZURpc3RhbmNlID0gZGlzdGFuY2VcbiAgICBtb3ZlZCA9IDBcbiAgICBtb3ZlRHVyYXRpb24gPSBkdXJhdGlvblxuICAgIG1vdmVFYXNpbmcgPSBlYXNpbmdcbiAgICBtb3ZlRm9yY2VGaW5pc2ggPSBmb3JjZUZpbmlzaFxuICAgIG1vdmVDYWxsQmFjayA9IGNiXG4gICAgc3RhcnRUaW1lID0gbnVsbFxuICAgIG1vdmVBbmltYXRlKClcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlckJpbmQoZm9yY2VfcmVzaXplKSB7XG4gICAgbGV0IF9jb250YWluZXIgPSBnZXRFbGVtZW50cyhpbml0aWFsQ29udGFpbmVyKVxuICAgIGlmICghX2NvbnRhaW5lci5sZW5ndGgpIHJldHVyblxuICAgIGNvbnRhaW5lciA9IF9jb250YWluZXJbMF1cbiAgICBzbGlkZXJSZXNpemUoZm9yY2VfcmVzaXplKVxuICAgIGV2ZW50c0FkZCgpXG4gICAgaG9vaygnbW91bnRlZCcpXG4gIH1cblxuICBmdW5jdGlvbiBzbGlkZXJDaGVja0JyZWFrcG9pbnQoKSB7XG4gICAgY29uc3QgYnJlYWtwb2ludHMgPSBpbml0aWFsT3B0aW9ucy5icmVha3BvaW50cyB8fCBbXVxuICAgIGxldCBsYXN0VmFsaWRcbiAgICBmb3IgKGxldCB2YWx1ZSBpbiBicmVha3BvaW50cykge1xuICAgICAgaWYgKHdpbmRvdy5tYXRjaE1lZGlhKHZhbHVlKS5tYXRjaGVzKSBsYXN0VmFsaWQgPSB2YWx1ZVxuICAgIH1cbiAgICBpZiAobGFzdFZhbGlkID09PSBicmVha3BvaW50Q3VycmVudCkgcmV0dXJuIHRydWVcbiAgICBicmVha3BvaW50Q3VycmVudCA9IGxhc3RWYWxpZFxuICAgIGNvbnN0IF9vcHRpb25zID0gYnJlYWtwb2ludEN1cnJlbnRcbiAgICAgID8gYnJlYWtwb2ludHNbYnJlYWtwb2ludEN1cnJlbnRdXG4gICAgICA6IGluaXRpYWxPcHRpb25zXG4gICAgaWYgKF9vcHRpb25zLmJyZWFrcG9pbnRzICYmIGJyZWFrcG9pbnRDdXJyZW50KSBkZWxldGUgX29wdGlvbnMuYnJlYWtwb2ludHNcbiAgICBvcHRpb25zID0geyAuLi5kZWZhdWx0T3B0aW9ucywgLi4uaW5pdGlhbE9wdGlvbnMsIC4uLl9vcHRpb25zIH1cbiAgICBvcHRpb25zQ2hhbmdlZCA9IHRydWVcbiAgICByZXNpemVMYXN0V2lkdGggPSBudWxsXG5cbiAgICBob29rKCdvcHRpb25zQ2hhbmdlZCcpXG4gICAgc2xpZGVyUmViaW5kKClcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlckdldFNsaWRlc1BlclZpZXcob3B0aW9uKSB7XG4gICAgaWYgKHR5cGVvZiBvcHRpb24gPT09ICdmdW5jdGlvbicpIHJldHVybiBvcHRpb24oKVxuICAgIGNvbnN0IGFkanVzdCA9IG9wdGlvbnMuYXV0b0FkanVzdFNsaWRlc1BlclZpZXdcbiAgICBpZiAoIWFkanVzdCkgbGVuZ3RoID0gTWF0aC5tYXgob3B0aW9uLCBsZW5ndGgpXG4gICAgY29uc3QgbWF4ID0gaXNMb29wKCkgJiYgYWRqdXN0ID8gbGVuZ3RoIC0gMSA6IGxlbmd0aFxuICAgIHJldHVybiBjbGFtcFZhbHVlKG9wdGlvbiwgMSwgTWF0aC5tYXgobWF4LCAxKSlcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlckluaXQoKSB7XG4gICAgc2xpZGVyQ2hlY2tCcmVha3BvaW50KClcbiAgICBzbGlkZXJDcmVhdGVkID0gdHJ1ZVxuICAgIGhvb2soJ2NyZWF0ZWQnKVxuICB9XG5cbiAgZnVuY3Rpb24gc2xpZGVyUmViaW5kKG5ld19vcHRpb25zLCBmb3JjZV9yZXNpemUpIHtcbiAgICBpZiAobmV3X29wdGlvbnMpIGluaXRpYWxPcHRpb25zID0gbmV3X29wdGlvbnNcbiAgICBpZiAoZm9yY2VfcmVzaXplKSBicmVha3BvaW50Q3VycmVudCA9IG51bGxcbiAgICBzbGlkZXJVbmJpbmQoKVxuICAgIHNsaWRlckJpbmQoZm9yY2VfcmVzaXplKVxuICB9XG5cbiAgZnVuY3Rpb24gc2xpZGVyUmVzaXplKGZvcmNlKSB7XG4gICAgY29uc3Qgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aFxuICAgIGlmICghc2xpZGVyQ2hlY2tCcmVha3BvaW50KCkgfHwgKHdpbmRvd1dpZHRoID09PSByZXNpemVMYXN0V2lkdGggJiYgIWZvcmNlKSlcbiAgICAgIHJldHVyblxuICAgIHJlc2l6ZUxhc3RXaWR0aCA9IHdpbmRvd1dpZHRoXG4gICAgY29uc3Qgb3B0aW9uU2xpZGVzID0gb3B0aW9ucy5zbGlkZXNcbiAgICBpZiAodHlwZW9mIG9wdGlvblNsaWRlcyA9PT0gJ251bWJlcicpIHtcbiAgICAgIHNsaWRlcyA9IG51bGxcbiAgICAgIGxlbmd0aCA9IG9wdGlvblNsaWRlc1xuICAgIH0gZWxzZSB7XG4gICAgICBzbGlkZXMgPSBnZXRFbGVtZW50cyhvcHRpb25TbGlkZXMsIGNvbnRhaW5lcilcbiAgICAgIGxlbmd0aCA9IHNsaWRlcyA/IHNsaWRlcy5sZW5ndGggOiAwXG4gICAgfVxuICAgIGNvbnN0IGRyYWdTcGVlZCA9IG9wdGlvbnMuZHJhZ1NwZWVkXG4gICAgdG91Y2hNdWx0aXBsaWNhdG9yID1cbiAgICAgIHR5cGVvZiBkcmFnU3BlZWQgPT09ICdmdW5jdGlvbicgPyBkcmFnU3BlZWQgOiB2YWwgPT4gdmFsICogZHJhZ1NwZWVkXG4gICAgd2lkdGggPSBpc1ZlcnRpY2FsU2xpZGVyKCkgPyBjb250YWluZXIub2Zmc2V0SGVpZ2h0IDogY29udGFpbmVyLm9mZnNldFdpZHRoXG4gICAgc2xpZGVzUGVyVmlldyA9IHNsaWRlckdldFNsaWRlc1BlclZpZXcob3B0aW9ucy5zbGlkZXNQZXJWaWV3KVxuICAgIHNwYWNpbmcgPSBjbGFtcFZhbHVlKG9wdGlvbnMuc3BhY2luZywgMCwgd2lkdGggLyAoc2xpZGVzUGVyVmlldyAtIDEpIC0gMSlcbiAgICB3aWR0aCArPSBzcGFjaW5nXG4gICAgb3JpZ2luID0gaXNDZW50ZXJNb2RlKClcbiAgICAgID8gKHdpZHRoIC8gMiAtIHdpZHRoIC8gc2xpZGVzUGVyVmlldyAvIDIpIC8gd2lkdGhcbiAgICAgIDogMFxuICAgIHNsaWRlc1NldFdpZHRocygpXG5cbiAgICBjb25zdCBjdXJyZW50SWR4ID1cbiAgICAgICFzbGlkZXJDcmVhdGVkIHx8IChvcHRpb25zQ2hhbmdlZCAmJiBvcHRpb25zLnJlc2V0U2xpZGUpXG4gICAgICAgID8gb3B0aW9ucy5pbml0aWFsXG4gICAgICAgIDogdHJhY2tDdXJyZW50SWR4XG4gICAgdHJhY2tTZXRQb3NpdGlvbkJ5SWR4KGlzTG9vcCgpID8gY3VycmVudElkeCA6IHRyYWNrQ2xhbXBJbmRleChjdXJyZW50SWR4KSlcblxuICAgIGlmIChpc1ZlcnRpY2FsU2xpZGVyKCkpIHtcbiAgICAgIGNvbnRhaW5lci5zZXRBdHRyaWJ1dGUoYXR0cmlidXRlVmVydGljYWwsIHRydWUpXG4gICAgfVxuICAgIG9wdGlvbnNDaGFuZ2VkID0gZmFsc2VcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlclJlc2l6ZUZpeChmb3JjZSkge1xuICAgIHNsaWRlclJlc2l6ZSgpXG4gICAgc2V0VGltZW91dChzbGlkZXJSZXNpemUsIDUwMClcbiAgICBzZXRUaW1lb3V0KHNsaWRlclJlc2l6ZSwgMjAwMClcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlclVuYmluZCgpIHtcbiAgICBldmVudHNSZW1vdmUoKVxuICAgIHNsaWRlc1JlbW92ZVN0eWxlcygpXG4gICAgaWYgKGNvbnRhaW5lciAmJiBjb250YWluZXIuaGFzQXR0cmlidXRlKGF0dHJpYnV0ZVZlcnRpY2FsKSlcbiAgICAgIGNvbnRhaW5lci5yZW1vdmVBdHRyaWJ1dGUoYXR0cmlidXRlVmVydGljYWwpXG4gICAgaG9vaygnZGVzdHJveWVkJylcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlc1NldFBvc2l0aW9ucygpIHtcbiAgICBpZiAoIXNsaWRlcykgcmV0dXJuXG4gICAgc2xpZGVzLmZvckVhY2goKHNsaWRlLCBpZHgpID0+IHtcbiAgICAgIGNvbnN0IGFic29sdXRlRGlzdGFuY2UgPSB0cmFja1NsaWRlUG9zaXRpb25zW2lkeF0uZGlzdGFuY2UgKiB3aWR0aFxuICAgICAgY29uc3QgcG9zID1cbiAgICAgICAgYWJzb2x1dGVEaXN0YW5jZSAtXG4gICAgICAgIGlkeCAqXG4gICAgICAgICAgKHdpZHRoIC8gc2xpZGVzUGVyVmlldyAtXG4gICAgICAgICAgICBzcGFjaW5nIC8gc2xpZGVzUGVyVmlldyAtXG4gICAgICAgICAgICAoc3BhY2luZyAvIHNsaWRlc1BlclZpZXcpICogKHNsaWRlc1BlclZpZXcgLSAxKSlcblxuICAgICAgY29uc3QgeCA9IGlzVmVydGljYWxTbGlkZXIoKSA/IDAgOiBwb3NcbiAgICAgIGNvbnN0IHkgPSBpc1ZlcnRpY2FsU2xpZGVyKCkgPyBwb3MgOiAwXG4gICAgICBjb25zdCB0cmFuc2Zvcm1TdHJpbmcgPSAob3B0aW9ucy5zY2FsZUNlbnRlciAmJiBpZHggIT09ICgodHJhY2tDdXJyZW50SWR4ICUgbGVuZ3RoKSArIGxlbmd0aCkgJSBsZW5ndGgpID8gYHRyYW5zbGF0ZTNkKCR7eH1weCwgJHt5fXB4LCAwKSBzY2FsZSgwLjgpYCA6IGB0cmFuc2xhdGUzZCgke3h9cHgsICR7eX1weCwgMClgO1xuICAgICAgc2xpZGUuc3R5bGUudHJhbnNmb3JtID0gdHJhbnNmb3JtU3RyaW5nXG4gICAgICBzbGlkZS5zdHlsZS50cmFuc2l0aW9uID0gJ2FsbCAwLjRzIGVhc2UnO1xuICAgICAgc2xpZGUuc3R5bGVbJy13ZWJraXQtdHJhbnNmb3JtJ10gPSB0cmFuc2Zvcm1TdHJpbmdcbiAgICB9KVxuICB9XG5cbiAgZnVuY3Rpb24gc2xpZGVzU2V0V2lkdGhzKCkge1xuICAgIGlmICghc2xpZGVzKSByZXR1cm5cbiAgICBzbGlkZXMuZm9yRWFjaChzbGlkZSA9PiB7XG4gICAgICBjb25zdCBzdHlsZSA9IGBjYWxjKCR7MTAwIC8gc2xpZGVzUGVyVmlld30lIC0gJHtcbiAgICAgICAgKHNwYWNpbmcgLyBzbGlkZXNQZXJWaWV3KSAqIChzbGlkZXNQZXJWaWV3IC0gMSlcbiAgICAgIH1weClgXG4gICAgICBpZiAoaXNWZXJ0aWNhbFNsaWRlcigpKSB7XG4gICAgICAgIHNsaWRlLnN0eWxlWydtaW4taGVpZ2h0J10gPSBzdHlsZVxuICAgICAgICBzbGlkZS5zdHlsZVsnbWF4LWhlaWdodCddID0gc3R5bGVcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNsaWRlLnN0eWxlWydtaW4td2lkdGgnXSA9IHN0eWxlXG4gICAgICAgIHNsaWRlLnN0eWxlWydtYXgtd2lkdGgnXSA9IHN0eWxlXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIGZ1bmN0aW9uIHNsaWRlc1JlbW92ZVN0eWxlcygpIHtcbiAgICBpZiAoIXNsaWRlcykgcmV0dXJuXG4gICAgbGV0IHN0eWxlcyA9IFsndHJhbnNmb3JtJywgJy13ZWJraXQtdHJhbnNmb3JtJ11cbiAgICBzdHlsZXMgPSBpc1ZlcnRpY2FsU2xpZGVyXG4gICAgICA/IFsuLi5zdHlsZXMsICdtaW4taGVpZ2h0JywgJ21heC1oZWlnaHQnXVxuICAgICAgOiBbLi4uc3R5bGVzLCAnbWluLXdpZHRoJywgJ21heC13aWR0aCddXG4gICAgc2xpZGVzLmZvckVhY2goc2xpZGUgPT4ge1xuICAgICAgc3R5bGVzLmZvckVhY2goc3R5bGUgPT4ge1xuICAgICAgICBzbGlkZS5zdHlsZS5yZW1vdmVQcm9wZXJ0eShzdHlsZSlcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYWNrQWRkKHZhbCwgZHJhZyA9IHRydWUsIHRpbWVzdGFtcCA9IERhdGUubm93KCkpIHtcbiAgICB0cmFja01lYXN1cmUodmFsLCB0aW1lc3RhbXApXG4gICAgaWYgKGRyYWcpIHZhbCA9IHRyYWNrcnViYmVyYmFuZCh2YWwpXG4gICAgdHJhY2tQb3NpdGlvbiArPSB2YWxcbiAgICB0cmFja01vdmUoKVxuICB9XG5cbiAgZnVuY3Rpb24gdHJhY2tDYWxjdWxhdGVPZmZzZXQoYWRkKSB7XG4gICAgY29uc3QgdHJhY2tMZW5ndGggPVxuICAgICAgKHdpZHRoICogKGxlbmd0aCAtIDEgKiAoaXNDZW50ZXJNb2RlKCkgPyAxIDogc2xpZGVzUGVyVmlldykpKSAvXG4gICAgICBzbGlkZXNQZXJWaWV3XG4gICAgY29uc3QgcG9zaXRpb24gPSB0cmFja1Bvc2l0aW9uICsgYWRkXG4gICAgcmV0dXJuIHBvc2l0aW9uID4gdHJhY2tMZW5ndGhcbiAgICAgID8gcG9zaXRpb24gLSB0cmFja0xlbmd0aFxuICAgICAgOiBwb3NpdGlvbiA8IDBcbiAgICAgID8gcG9zaXRpb25cbiAgICAgIDogMFxuICB9XG5cbiAgZnVuY3Rpb24gdHJhY2tDbGFtcEluZGV4KGlkeCkge1xuICAgIHJldHVybiBjbGFtcFZhbHVlKFxuICAgICAgaWR4LFxuICAgICAgMCxcbiAgICAgIGxlbmd0aCAtIDEgLSAoaXNDZW50ZXJNb2RlKCkgPyAwIDogc2xpZGVzUGVyVmlldyAtIDEpXG4gICAgKVxuICB9XG5cbiAgZnVuY3Rpb24gdHJhY2tHZXREZXRhaWxzKCkge1xuICAgIGNvbnN0IHRyYWNrUHJvZ3Jlc3NBYnMgPSBNYXRoLmFicyh0cmFja1Byb2dyZXNzKVxuICAgIGNvbnN0IHByb2dyZXNzID0gdHJhY2tQb3NpdGlvbiA8IDAgPyAxIC0gdHJhY2tQcm9ncmVzc0FicyA6IHRyYWNrUHJvZ3Jlc3NBYnNcbiAgICByZXR1cm4ge1xuICAgICAgZGlyZWN0aW9uOiB0cmFja0RpcmVjdGlvbixcbiAgICAgIHByb2dyZXNzVHJhY2s6IHByb2dyZXNzLFxuICAgICAgcHJvZ3Jlc3NTbGlkZXM6IChwcm9ncmVzcyAqIGxlbmd0aCkgLyAobGVuZ3RoIC0gMSksXG4gICAgICBwb3NpdGlvbnM6IHRyYWNrU2xpZGVQb3NpdGlvbnMsXG4gICAgICBwb3NpdGlvbjogdHJhY2tQb3NpdGlvbixcbiAgICAgIHNwZWVkOiB0cmFja1NwZWVkLFxuICAgICAgcmVsYXRpdmVTbGlkZTogKCh0cmFja0N1cnJlbnRJZHggJSBsZW5ndGgpICsgbGVuZ3RoKSAlIGxlbmd0aCxcbiAgICAgIGFic29sdXRlU2xpZGU6IHRyYWNrQ3VycmVudElkeCxcbiAgICAgIHNpemU6IGxlbmd0aCxcbiAgICAgIHNsaWRlc1BlclZpZXcsXG4gICAgICB3aWR0aE9ySGVpZ2h0OiB3aWR0aCxcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiB0cmFja0dldElkeChpZHgsIHJlbGF0aXZlID0gZmFsc2UsIG5lYXJlc3QgPSBmYWxzZSkge1xuICAgIHJldHVybiAhaXNMb29wKClcbiAgICAgID8gdHJhY2tDbGFtcEluZGV4KGlkeClcbiAgICAgIDogIXJlbGF0aXZlXG4gICAgICA/IGlkeFxuICAgICAgOiB0cmFja0dldFJlbGF0aXZlSWR4KGlkeCwgbmVhcmVzdClcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYWNrR2V0SWR4RGlzdGFuY2UoaWR4KSB7XG4gICAgcmV0dXJuIC0oLSgod2lkdGggLyBzbGlkZXNQZXJWaWV3KSAqIGlkeCkgKyB0cmFja1Bvc2l0aW9uKVxuICB9XG5cbiAgZnVuY3Rpb24gdHJhY2tHZXRSZWxhdGl2ZUlkeChpZHgsIG5lYXJlc3QpIHtcbiAgICBpZHggPSAoKGlkeCAlIGxlbmd0aCkgKyBsZW5ndGgpICUgbGVuZ3RoXG4gICAgY29uc3QgY3VycmVudCA9ICgodHJhY2tDdXJyZW50SWR4ICUgbGVuZ3RoKSArIGxlbmd0aCkgJSBsZW5ndGhcbiAgICBjb25zdCBsZWZ0ID0gY3VycmVudCA8IGlkeCA/IC1jdXJyZW50IC0gbGVuZ3RoICsgaWR4IDogLShjdXJyZW50IC0gaWR4KVxuICAgIGNvbnN0IHJpZ2h0ID0gY3VycmVudCA+IGlkeCA/IGxlbmd0aCAtIGN1cnJlbnQgKyBpZHggOiBpZHggLSBjdXJyZW50XG4gICAgY29uc3QgYWRkID0gbmVhcmVzdFxuICAgICAgPyBNYXRoLmFicyhsZWZ0KSA8PSByaWdodFxuICAgICAgICA/IGxlZnRcbiAgICAgICAgOiByaWdodFxuICAgICAgOiBpZHggPCBjdXJyZW50XG4gICAgICA/IGxlZnRcbiAgICAgIDogcmlnaHRcbiAgICByZXR1cm4gdHJhY2tDdXJyZW50SWR4ICsgYWRkXG4gIH1cblxuICBmdW5jdGlvbiB0cmFja01lYXN1cmUodmFsLCB0aW1lc3RhbXApIHtcbiAgICAvLyB0b2RvIC0gaW1wcm92ZSBtZWFzdXJlbWVudCAtIGl0IGNvdWxkIGJlIGJldHRlciBmb3IgaW9zXG4gICAgY2xlYXJUaW1lb3V0KHRyYWNrTWVhc3VyZVRpbWVvdXQpXG4gICAgY29uc3QgZGlyZWN0aW9uID0gTWF0aC5zaWduKHZhbClcbiAgICBpZiAoZGlyZWN0aW9uICE9PSB0cmFja0RpcmVjdGlvbikgdHJhY2tNZWFzdXJlUmVzZXQoKVxuICAgIHRyYWNrRGlyZWN0aW9uID0gZGlyZWN0aW9uXG4gICAgdHJhY2tNZWFzdXJlUG9pbnRzLnB1c2goe1xuICAgICAgZGlzdGFuY2U6IHZhbCxcbiAgICAgIHRpbWU6IHRpbWVzdGFtcCxcbiAgICB9KVxuICAgIHRyYWNrTWVhc3VyZVRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRyYWNrTWVhc3VyZVBvaW50cyA9IFtdXG4gICAgICB0cmFja1NwZWVkID0gMFxuICAgIH0sIDUwKVxuICAgIHRyYWNrTWVhc3VyZVBvaW50cyA9IHRyYWNrTWVhc3VyZVBvaW50cy5zbGljZSgtNilcbiAgICBpZiAodHJhY2tNZWFzdXJlUG9pbnRzLmxlbmd0aCA8PSAxIHx8IHRyYWNrRGlyZWN0aW9uID09PSAwKVxuICAgICAgcmV0dXJuICh0cmFja1NwZWVkID0gMClcblxuICAgIGNvbnN0IGRpc3RhbmNlID0gdHJhY2tNZWFzdXJlUG9pbnRzXG4gICAgICAuc2xpY2UoMCwgLTEpXG4gICAgICAucmVkdWNlKChhY2MsIG5leHQpID0+IGFjYyArIG5leHQuZGlzdGFuY2UsIDApXG4gICAgY29uc3QgZW5kID0gdHJhY2tNZWFzdXJlUG9pbnRzW3RyYWNrTWVhc3VyZVBvaW50cy5sZW5ndGggLSAxXS50aW1lXG4gICAgY29uc3Qgc3RhcnQgPSB0cmFja01lYXN1cmVQb2ludHNbMF0udGltZVxuICAgIHRyYWNrU3BlZWQgPSBjbGFtcFZhbHVlKGRpc3RhbmNlIC8gKGVuZCAtIHN0YXJ0KSwgLTEwLCAxMClcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYWNrTWVhc3VyZVJlc2V0KCkge1xuICAgIHRyYWNrTWVhc3VyZVBvaW50cyA9IFtdXG4gIH1cblxuICAvLyB0b2RvIC0gb3B0aW9uIGZvciBub3QgY2FsY3VsYXRpbmcgc2xpZGVzIHRoYXQgYXJlIG5vdCBpbiBzaWdodFxuICBmdW5jdGlvbiB0cmFja01vdmUoKSB7XG4gICAgdHJhY2tQcm9ncmVzcyA9IGlzTG9vcCgpXG4gICAgICA/ICh0cmFja1Bvc2l0aW9uICUgKCh3aWR0aCAqIGxlbmd0aCkgLyBzbGlkZXNQZXJWaWV3KSkgL1xuICAgICAgICAoKHdpZHRoICogbGVuZ3RoKSAvIHNsaWRlc1BlclZpZXcpXG4gICAgICA6IHRyYWNrUG9zaXRpb24gLyAoKHdpZHRoICogbGVuZ3RoKSAvIHNsaWRlc1BlclZpZXcpXG5cbiAgICB0cmFja1NldEN1cnJlbnRJZHgoKVxuICAgIGNvbnN0IHNsaWRlUG9zaXRpb25zID0gW11cbiAgICBmb3IgKGxldCBpZHggPSAwOyBpZHggPCBsZW5ndGg7IGlkeCsrKSB7XG4gICAgICBsZXQgZGlzdGFuY2UgPVxuICAgICAgICAoKCgxIC8gbGVuZ3RoKSAqIGlkeCAtXG4gICAgICAgICAgKHRyYWNrUHJvZ3Jlc3MgPCAwICYmIGlzTG9vcCgpID8gdHJhY2tQcm9ncmVzcyArIDEgOiB0cmFja1Byb2dyZXNzKSkgKlxuICAgICAgICAgIGxlbmd0aCkgL1xuICAgICAgICAgIHNsaWRlc1BlclZpZXcgK1xuICAgICAgICBvcmlnaW5cbiAgICAgIGlmIChpc0xvb3AoKSlcbiAgICAgICAgZGlzdGFuY2UgKz1cbiAgICAgICAgICBkaXN0YW5jZSA+IChsZW5ndGggLSAxKSAvIHNsaWRlc1BlclZpZXdcbiAgICAgICAgICAgID8gLShsZW5ndGggLyBzbGlkZXNQZXJWaWV3KVxuICAgICAgICAgICAgOiBkaXN0YW5jZSA8IC0obGVuZ3RoIC8gc2xpZGVzUGVyVmlldykgKyAxXG4gICAgICAgICAgICA/IGxlbmd0aCAvIHNsaWRlc1BlclZpZXdcbiAgICAgICAgICAgIDogMFxuXG4gICAgICBjb25zdCBzbGlkZVdpZHRoID0gMSAvIHNsaWRlc1BlclZpZXdcbiAgICAgIGNvbnN0IGxlZnQgPSBkaXN0YW5jZSArIHNsaWRlV2lkdGhcbiAgICAgIGNvbnN0IHBvcnRpb24gPVxuICAgICAgICBsZWZ0IDwgc2xpZGVXaWR0aFxuICAgICAgICAgID8gbGVmdCAvIHNsaWRlV2lkdGhcbiAgICAgICAgICA6IGxlZnQgPiAxXG4gICAgICAgICAgPyAxIC0gKChsZWZ0IC0gMSkgKiBzbGlkZXNQZXJWaWV3KSAvIDFcbiAgICAgICAgICA6IDFcbiAgICAgIHNsaWRlUG9zaXRpb25zLnB1c2goe1xuICAgICAgICBwb3J0aW9uOiBwb3J0aW9uIDwgMCB8fCBwb3J0aW9uID4gMSA/IDAgOiBwb3J0aW9uLFxuICAgICAgICBkaXN0YW5jZTogIWlzUnRsKCkgPyBkaXN0YW5jZSA6IGRpc3RhbmNlICogLTEgKyAxIC0gc2xpZGVXaWR0aCxcbiAgICAgIH0pXG4gICAgfVxuICAgIHRyYWNrU2xpZGVQb3NpdGlvbnMgPSBzbGlkZVBvc2l0aW9uc1xuICAgIHNsaWRlc1NldFBvc2l0aW9ucygpXG4gICAgaG9vaygnbW92ZScpXG4gIH1cblxuICBmdW5jdGlvbiB0cmFja3J1YmJlcmJhbmQoYWRkKSB7XG4gICAgaWYgKGlzTG9vcCgpKSByZXR1cm4gYWRkXG4gICAgY29uc3Qgb2Zmc2V0ID0gdHJhY2tDYWxjdWxhdGVPZmZzZXQoYWRkKVxuICAgIGlmICghaXNSdWJiZXJiYW5kKCkpIHJldHVybiBhZGQgLSBvZmZzZXRcbiAgICBpZiAob2Zmc2V0ID09PSAwKSByZXR1cm4gYWRkXG4gICAgY29uc3QgZWFzaW5nID0gdCA9PiAoMSAtIE1hdGguYWJzKHQpKSAqICgxIC0gTWF0aC5hYnModCkpXG4gICAgcmV0dXJuIGFkZCAqIGVhc2luZyhvZmZzZXQgLyB3aWR0aClcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYWNrU2V0Q3VycmVudElkeCgpIHtcbiAgICBjb25zdCBuZXdfaWR4ID0gTWF0aC5yb3VuZCh0cmFja1Bvc2l0aW9uIC8gKHdpZHRoIC8gc2xpZGVzUGVyVmlldykpXG4gICAgaWYgKG5ld19pZHggPT09IHRyYWNrQ3VycmVudElkeCkgcmV0dXJuXG4gICAgaWYgKCFpc0xvb3AoKSAmJiAobmV3X2lkeCA8IDAgfHwgbmV3X2lkeCA+IGxlbmd0aCAtIDEpKSByZXR1cm5cbiAgICB0cmFja0N1cnJlbnRJZHggPSBuZXdfaWR4XG4gICAgaG9vaygnc2xpZGVDaGFuZ2VkJylcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYWNrU2V0UG9zaXRpb25CeUlkeChpZHgpIHtcbiAgICBob29rKCdiZWZvcmVDaGFuZ2UnKVxuICAgIHRyYWNrQWRkKHRyYWNrR2V0SWR4RGlzdGFuY2UoaWR4KSwgZmFsc2UpXG4gICAgaG9vaygnYWZ0ZXJDaGFuZ2UnKVxuICB9XG5cbiAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XG4gICAgYXV0b0FkanVzdFNsaWRlc1BlclZpZXc6IHRydWUsXG4gICAgY2VudGVyZWQ6IGZhbHNlLFxuICAgIHNjYWxlQ2VudGVyOiBmYWxzZSxcbiAgICBicmVha3BvaW50czogbnVsbCxcbiAgICBjb250cm9sczogdHJ1ZSxcbiAgICBkcmFnU3BlZWQ6IDEsXG4gICAgZnJpY3Rpb246IDAuMDAyNSxcbiAgICBsb29wOiBmYWxzZSxcbiAgICBpbml0aWFsOiAwLFxuICAgIGR1cmF0aW9uOiA1MDAsXG4gICAgcHJldmVudEV2ZW50OiAnZGF0YS12aWEtY2Fyb3VzZWwtcGUnLFxuICAgIHNsaWRlczogJy52aWEtY2Fyb3VzZWxfX3NsaWRlJyxcbiAgICB2ZXJ0aWNhbDogZmFsc2UsXG4gICAgcmVzZXRTbGlkZTogZmFsc2UsXG4gICAgc2xpZGVzUGVyVmlldzogMSxcbiAgICBzcGFjaW5nOiAwLFxuICAgIG1vZGU6ICdzbmFwJyxcbiAgICBydGw6IGZhbHNlLFxuICAgIHJ1YmJlcmJhbmQ6IHRydWUsXG4gICAgY2FuY2VsT25MZWF2ZTogdHJ1ZSxcbiAgfVxuXG4gIGNvbnN0IHB1YmZ1bmNzID0ge1xuICAgIGNvbnRyb2xzOiBhY3RpdmUgPT4ge1xuICAgICAgdG91Y2hDb250cm9scyA9IGFjdGl2ZVxuICAgIH0sXG4gICAgZGVzdHJveTogc2xpZGVyVW5iaW5kLFxuICAgIHJlZnJlc2gob3B0aW9ucykge1xuICAgICAgc2xpZGVyUmViaW5kKG9wdGlvbnMsIHRydWUpXG4gICAgfSxcbiAgICBuZXh0KCkge1xuICAgICAgbW92ZVRvSWR4KHRyYWNrQ3VycmVudElkeCArIDEsIHRydWUpXG4gICAgfSxcbiAgICBwcmV2KCkge1xuICAgICAgbW92ZVRvSWR4KHRyYWNrQ3VycmVudElkeCAtIDEsIHRydWUpXG4gICAgfSxcbiAgICBtb3ZlVG9TbGlkZShpZHgsIGR1cmF0aW9uKSB7XG4gICAgICBtb3ZlVG9JZHgoaWR4LCB0cnVlLCBkdXJhdGlvbilcbiAgICB9LFxuICAgIG1vdmVUb1NsaWRlUmVsYXRpdmUoaWR4LCBuZWFyZXN0ID0gZmFsc2UsIGR1cmF0aW9uKSB7XG4gICAgICBtb3ZlVG9JZHgoaWR4LCB0cnVlLCBkdXJhdGlvbiwgdHJ1ZSwgbmVhcmVzdClcbiAgICB9LFxuICAgIHJlc2l6ZSgpIHtcbiAgICAgIHNsaWRlclJlc2l6ZSh0cnVlKVxuICAgIH0sXG4gICAgZGV0YWlscygpIHtcbiAgICAgIHJldHVybiB0cmFja0dldERldGFpbHMoKVxuICAgIH0sXG4gICAgb3B0aW9ucygpIHtcbiAgICAgIGNvbnN0IG9wdCA9IHsgLi4ub3B0aW9ucyB9XG4gICAgICBkZWxldGUgb3B0LmJyZWFrcG9pbnRzXG4gICAgICByZXR1cm4gb3B0XG4gICAgfSxcbiAgfVxuXG4gIHNsaWRlckluaXQoKVxuXG4gIHJldHVybiBwdWJmdW5jc1xufVxuXG5leHBvcnQgZGVmYXVsdCBWaWFDYXJvdXNlbFxuXG4vLyBoZWxwZXIgZnVuY3Rpb25zXG5cbmZ1bmN0aW9uIGNvbnZlcnRUb0FycmF5KG5vZGVMaXN0KSB7XG4gIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlTGlzdClcbn1cblxuZnVuY3Rpb24gZ2V0RWxlbWVudHMoZWxlbWVudCwgd3JhcHBlciA9IGRvY3VtZW50KSB7XG4gIHJldHVybiB0eXBlb2YgZWxlbWVudCA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gY29udmVydFRvQXJyYXkoZWxlbWVudCgpKVxuICAgIDogdHlwZW9mIGVsZW1lbnQgPT09ICdzdHJpbmcnXG4gICAgPyBjb252ZXJ0VG9BcnJheSh3cmFwcGVyLnF1ZXJ5U2VsZWN0b3JBbGwoZWxlbWVudCkpXG4gICAgOiBlbGVtZW50IGluc3RhbmNlb2YgSFRNTEVsZW1lbnQgIT09IGZhbHNlXG4gICAgPyBbZWxlbWVudF1cbiAgICA6IGVsZW1lbnQgaW5zdGFuY2VvZiBOb2RlTGlzdCAhPT0gZmFsc2VcbiAgICA/IGVsZW1lbnRcbiAgICA6IFtdXG59XG5cbmZ1bmN0aW9uIGNsYW1wVmFsdWUodmFsdWUsIG1pbiwgbWF4KSB7XG4gIHJldHVybiBNYXRoLm1pbihNYXRoLm1heCh2YWx1ZSwgbWluKSwgbWF4KVxufVxuIl19