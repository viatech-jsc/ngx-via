export class UploadErrorDto {
    constructor(field = "", message = "") {
        this.field = field;
        this.message = message;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmVycm9yLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS91cGxvYWRfZmlsZS9kdG8vdXBsb2FkLmVycm9yLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLE9BQU8sY0FBYztJQUl2QixZQUNJLFFBQWdCLEVBQUUsRUFDbEIsVUFBa0IsRUFBRTtRQUVwQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztJQUMzQixDQUFDO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBVcGxvYWRFcnJvckR0byB7XG4gICAgcHVibGljIGZpZWxkOiBzdHJpbmc7XG4gICAgcHVibGljIG1lc3NhZ2U6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBmaWVsZDogc3RyaW5nID0gXCJcIixcbiAgICAgICAgbWVzc2FnZTogc3RyaW5nID0gXCJcIlxuICAgICkge1xuICAgICAgICB0aGlzLmZpZWxkID0gZmllbGQ7XG4gICAgICAgIHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgfVxufSJdfQ==