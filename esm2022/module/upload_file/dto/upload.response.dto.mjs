export class UploadResponseDto {
    constructor(responseType = "", message = "", code = "") {
        this._message = message;
        this._responseType = responseType;
        this._code = code;
    }
    /**
     * Getter responseType
     * return {string}
     */
    get responseType() {
        return this._responseType;
    }
    /**
     * Getter message
     * return {string}
     */
    get message() {
        return this._message;
    }
    /**
     * Getter code
     * return {string}
     */
    get code() {
        return this._code;
    }
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value) {
        this._responseType = value;
    }
    /**
     * Setter message
     * param {string} value
     */
    set message(value) {
        this._message = value;
    }
    /**
     * Setter code
     * param {string} value
     */
    set code(value) {
        this._code = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnJlc3BvbnNlLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS91cGxvYWRfZmlsZS9kdG8vdXBsb2FkLnJlc3BvbnNlLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLE9BQU8saUJBQWlCO0lBSzFCLFlBQ0ksZUFBdUIsRUFBRSxFQUN6QixVQUFrQixFQUFFLEVBQ3BCLE9BQWUsRUFBRTtRQUVqQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxZQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUMzQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxPQUFPO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN0QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ25CLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVksQ0FBQyxLQUFhO1FBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE9BQU8sQ0FBQyxLQUFhO1FBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUksQ0FBQyxLQUFhO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3BCLENBQUM7Q0FHRCIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBVcGxvYWRSZXNwb25zZUR0byB7XG4gICAgcHJpdmF0ZSBfcmVzcG9uc2VUeXBlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfbWVzc2FnZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX2NvZGU6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZXNwb25zZVR5cGU6IHN0cmluZyA9IFwiXCIsXG4gICAgICAgIG1lc3NhZ2U6IHN0cmluZyA9IFwiXCIsXG4gICAgICAgIGNvZGU6IHN0cmluZyA9IFwiXCJcbiAgICApIHtcbiAgICAgICAgdGhpcy5fbWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgICAgIHRoaXMuX3Jlc3BvbnNlVHlwZSA9IHJlc3BvbnNlVHlwZTtcbiAgICAgICAgdGhpcy5fY29kZSA9IGNvZGU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHJlc3BvbnNlVHlwZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHJlc3BvbnNlVHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9yZXNwb25zZVR5cGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBtZXNzYWdlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgbWVzc2FnZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9tZXNzYWdlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY29kZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGNvZGUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fY29kZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHJlc3BvbnNlVHlwZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgcmVzcG9uc2VUeXBlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9yZXNwb25zZVR5cGUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG1lc3NhZ2VcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IG1lc3NhZ2UodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX21lc3NhZ2UgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGNvZGVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNvZGUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2NvZGUgPSB2YWx1ZTtcblx0fVxuXG5cbn0iXX0=