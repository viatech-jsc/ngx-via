import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import random from "lodash/random";
import { FormUtils } from "../../utils/form.utils";
import { TimepickerI18nDto } from "./dto/timepicker.i18n.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/common";
import * as i3 from "@ng-bootstrap/ng-bootstrap";
export class TimepickerComponent {
    constructor() {
        this.timepickerModelChange = new EventEmitter();
        this.timepickerModel = {
            hour: 0,
            minute: 0,
            second: 0,
        };
        this.showSeccond = false;
        this.showMeridian = false;
        this.disabled = false;
        this.spinners = false;
        this.viaControl = new FormControl();
        this.i18n = undefined;
        this.timepickerId = "";
        this.timepickerId = "timepicker" + random();
    }
    ngOnInit() {
    }
    formControlInstance() {
        return this.viaControl;
    }
    timeSelect(time) {
        this.viaControl.setValue(time);
        this.timepickerModelChange?.emit(time);
    }
    required(form) {
        return FormUtils.getInstance().required(form);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "17.3.5", type: TimepickerComponent, selector: "via-timepicker", inputs: { timepickerModel: "timepickerModel", showSeccond: "showSeccond", showMeridian: "showMeridian", disabled: "disabled", spinners: "spinners", viaControl: "viaControl", i18n: "i18n" }, outputs: { timepickerModelChange: "timepickerModelChange" }, ngImport: i0, template: "<div class=\"win-timepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"timepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <ngb-timepicker \n        [readonlyInputs]=\"disabled\" \n        [(ngModel)]=\"timepickerModel\" \n        [meridian]=\"showMeridian\" \n        (ngModelChange)=\"timeSelect($event)\" \n        [seconds]=\"showSeccond\"\n        [spinners]=\"spinners\">\n    </ngb-timepicker>\n</div>", styles: [".win-timepicker .danger-text{color:brown}\n"], dependencies: [{ kind: "directive", type: i1.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "component", type: i3.NgbTimepicker, selector: "ngb-timepicker", inputs: ["meridian", "spinners", "seconds", "hourStep", "minuteStep", "secondStep", "readonlyInputs", "size"], exportAs: ["ngbTimepicker"] }, { kind: "directive", type: i1.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "17.3.5", ngImport: i0, type: TimepickerComponent, decorators: [{
            type: Component,
            args: [{ selector: "via-timepicker", template: "<div class=\"win-timepicker form-group\">\n    <label *ngIf=\"i18n\" [attr.for]=\"timepickerId\">\n        {{i18n.label}}\n        <span class=\"danger-text\"> {{required(viaControl)}}</span>\n    </label>\n    <ngb-timepicker \n        [readonlyInputs]=\"disabled\" \n        [(ngModel)]=\"timepickerModel\" \n        [meridian]=\"showMeridian\" \n        (ngModelChange)=\"timeSelect($event)\" \n        [seconds]=\"showSeccond\"\n        [spinners]=\"spinners\">\n    </ngb-timepicker>\n</div>", styles: [".win-timepicker .danger-text{color:brown}\n"] }]
        }], ctorParameters: () => [], propDecorators: { timepickerModelChange: [{
                type: Output
            }], timepickerModel: [{
                type: Input
            }], showSeccond: [{
                type: Input
            }], showMeridian: [{
                type: Input
            }], disabled: [{
                type: Input
            }], spinners: [{
                type: Input
            }], viaControl: [{
                type: Input
            }], i18n: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25neC12aWEvc3JjL21vZHVsZS90aW1lcGlja2VyL3RpbWVwaWNrZXIudHMiLCIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9uZ3gtdmlhL3NyYy9tb2R1bGUvdGltZXBpY2tlci90aW1lcGlja2VyLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0MsT0FBTyxNQUFNLE1BQU0sZUFBZSxDQUFDO0FBQ25DLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQzs7Ozs7QUFPOUQsTUFBTSxPQUFPLG1CQUFtQjtJQWtCOUI7UUFoQlUsMEJBQXFCLEdBQWdDLElBQUksWUFBWSxFQUFFLENBQUM7UUFFekUsb0JBQWUsR0FBbUI7WUFDekMsSUFBSSxFQUFFLENBQUM7WUFDUCxNQUFNLEVBQUUsQ0FBQztZQUNULE1BQU0sRUFBRSxDQUFDO1NBQ1YsQ0FBQztRQUNPLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFDMUIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixlQUFVLEdBQWdCLElBQUksV0FBVyxFQUFFLENBQUM7UUFDNUMsU0FBSSxHQUF1QixTQUFTLENBQUM7UUFFOUMsaUJBQVksR0FBVyxFQUFFLENBQUM7UUFHeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLEdBQUcsTUFBTSxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN6QixDQUFDO0lBRUQsVUFBVSxDQUFDLElBQW1CO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFrQjtRQUN6QixPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDaEQsQ0FBQzs4R0FwQ1UsbUJBQW1CO2tHQUFuQixtQkFBbUIsaVRDWmhDLGtmQWFNOzsyRkRETyxtQkFBbUI7a0JBTC9CLFNBQVM7K0JBQ0UsZ0JBQWdCO3dEQU1oQixxQkFBcUI7c0JBQTlCLE1BQU07Z0JBRUUsZUFBZTtzQkFBdkIsS0FBSztnQkFLRyxXQUFXO3NCQUFuQixLQUFLO2dCQUNHLFlBQVk7c0JBQXBCLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNHLFVBQVU7c0JBQWxCLEtBQUs7Z0JBQ0csSUFBSTtzQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTmdiVGltZVN0cnVjdCB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHJhbmRvbSBmcm9tIFwibG9kYXNoL3JhbmRvbVwiO1xuaW1wb3J0IHsgRm9ybVV0aWxzIH0gZnJvbSBcIi4uLy4uL3V0aWxzL2Zvcm0udXRpbHNcIjtcbmltcG9ydCB7IFRpbWVwaWNrZXJJMThuRHRvIH0gZnJvbSBcIi4vZHRvL3RpbWVwaWNrZXIuaTE4bi5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10aW1lcGlja2VyXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vdGltZXBpY2tlci5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi90aW1lcGlja2VyLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVGltZXBpY2tlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQE91dHB1dCgpIHRpbWVwaWNrZXJNb2RlbENoYW5nZTogRXZlbnRFbWl0dGVyPE5nYlRpbWVTdHJ1Y3Q+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBcbiAgQElucHV0KCkgdGltZXBpY2tlck1vZGVsPzogTmdiVGltZVN0cnVjdCA9IHtcbiAgICBob3VyOiAwLFxuICAgIG1pbnV0ZTogMCxcbiAgICBzZWNvbmQ6IDAsXG4gIH07XG4gIEBJbnB1dCgpIHNob3dTZWNjb25kOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIHNob3dNZXJpZGlhbjogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBkaXNhYmxlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBzcGlubmVycyA9IGZhbHNlO1xuICBASW5wdXQoKSB2aWFDb250cm9sOiBGb3JtQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xuICBASW5wdXQoKSBpMThuPzogVGltZXBpY2tlckkxOG5EdG8gPSB1bmRlZmluZWQ7XG5cbiAgdGltZXBpY2tlcklkOiBzdHJpbmcgPSBcIlwiO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudGltZXBpY2tlcklkID0gXCJ0aW1lcGlja2VyXCIgKyByYW5kb20oKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQgeyBcbiAgfVxuXG4gIGZvcm1Db250cm9sSW5zdGFuY2UoKSB7XG4gICAgcmV0dXJuIHRoaXMudmlhQ29udHJvbDtcbiAgfVxuXG4gIHRpbWVTZWxlY3QodGltZTogTmdiVGltZVN0cnVjdCk6IHZvaWR7XG4gICAgdGhpcy52aWFDb250cm9sLnNldFZhbHVlKHRpbWUpO1xuICAgIHRoaXMudGltZXBpY2tlck1vZGVsQ2hhbmdlPy5lbWl0KHRpbWUpO1xuICB9XG4gIFxuICByZXF1aXJlZChmb3JtPzogRm9ybUNvbnRyb2wpIHtcbiAgICByZXR1cm4gRm9ybVV0aWxzLmdldEluc3RhbmNlKCkucmVxdWlyZWQoZm9ybSk7XG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJ3aW4tdGltZXBpY2tlciBmb3JtLWdyb3VwXCI+XG4gICAgPGxhYmVsICpuZ0lmPVwiaTE4blwiIFthdHRyLmZvcl09XCJ0aW1lcGlja2VySWRcIj5cbiAgICAgICAge3tpMThuLmxhYmVsfX1cbiAgICAgICAgPHNwYW4gY2xhc3M9XCJkYW5nZXItdGV4dFwiPiB7e3JlcXVpcmVkKHZpYUNvbnRyb2wpfX08L3NwYW4+XG4gICAgPC9sYWJlbD5cbiAgICA8bmdiLXRpbWVwaWNrZXIgXG4gICAgICAgIFtyZWFkb25seUlucHV0c109XCJkaXNhYmxlZFwiIFxuICAgICAgICBbKG5nTW9kZWwpXT1cInRpbWVwaWNrZXJNb2RlbFwiIFxuICAgICAgICBbbWVyaWRpYW5dPVwic2hvd01lcmlkaWFuXCIgXG4gICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInRpbWVTZWxlY3QoJGV2ZW50KVwiIFxuICAgICAgICBbc2Vjb25kc109XCJzaG93U2VjY29uZFwiXG4gICAgICAgIFtzcGlubmVyc109XCJzcGlubmVyc1wiPlxuICAgIDwvbmdiLXRpbWVwaWNrZXI+XG48L2Rpdj4iXX0=